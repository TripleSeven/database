﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.ProjectLPlayerContext
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.PlayerContext;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using BlackJack.ProjectL.UI;
using BlackJack.ServerFramework.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class ProjectLPlayerContext : PlayerContextBase, IComponentOwner
  {
    private float m_testMarqueeTime;
    private int m_testMarqueeCount;
    private float m_testChatTime;
    private int m_testChatCount;
    protected long m_serverTime;
    private IConfigDataLoader m_configDataLoader;
    private ComponenetManager<IComponentBase> m_components;
    public PlayerBasicInfoComponent m_playerBasicInfoComponent;
    private HeroComponent m_heroComponent;
    private BagComponent m_bagComponent;
    private BattleComponent m_battleComponent;
    private LevelComponent m_levelComponent;
    private RiftComponent m_riftComponent;
    private HeroDungeonComponent m_heroDungeonComponent;
    private ThearchyTrialComponent m_thearchyTrialComponent;
    private AnikiGymComponent m_anikiGymComponent;
    private TreasureMapComponent m_treasureMapComponent;
    private MemoryCorridorCompoment m_memoryCorridorComponent;
    private MailComponent m_mailComponent;
    private FixedStoreComponent m_fixedStoreComponent;
    private RandomStoreComponent m_randomStoreComponent;
    private SelectCardComponent m_selectCardComponent;
    private RaffleComponent m_raffleComponent;
    private ChatComponent m_chatComponent;
    private ChatGroupComponent m_chatGroupComponent;
    private MissionComponent m_missionComponent;
    private CommentComponent m_commentComponent;
    private TrainingGroundCompoment m_trainingGroundComponent;
    private ArenaComponent m_arenaComponent;
    private OperationalActivityCompoment m_operationalActivityComponent;
    private SurveyComponent m_surveyComponent;
    private DanmakuComponent m_danmakuComponent;
    private HeroTrainningComponent m_heroTrainningComponent;
    private FriendComponent m_friendComponent;
    private GlobalRankingListComponent m_globalRankingListComponent;
    private TeamComponent m_teamComponent;
    private HeroAssistantsCompoment m_heroAssistantsComponent;
    private HeroPhantomCompoment m_heroPhantomComponent;
    private CooperateBattleCompoment m_cooperateBattleComponent;
    private NoviceComponent m_noviceComponent;
    private RefluxComponent m_refluxComponent;
    private RechargeStoreComponent m_rechargeStoreComponent;
    private ResourceComponent m_resourceComponent;
    private RealTimePVPComponent m_realTimePVPComponent;
    private PeakArenaComponent m_peakArenaComponent;
    private GiftStoreComponent m_giftStoreComponent;
    private GuildComponent m_guildComponent;
    private UnchartedScoreComponent m_unchartedScoreComponent;
    private ClimbTowerComponent m_climbTowerComponent;
    private EternalShrineComponent m_eternalShrineComponent;
    private HeroAnthemComponent m_heroAnthemComponent;
    private CollectionActivityComponent m_collectionActivityComponent;
    private AncientCallComponent m_ancientCallComponent;
    private UnchartedLevelRaidComponent m_UnchartedLevelRaidComponent;
    private TimeSpan m_diffToServerTime;
    private TimeSpan m_diffToAssignTime;
    private DateTime m_lastReceiveMessageTime;
    private bool m_isPlayerInfoInitEnd;
    private bool m_isDataError;
    private bool m_initOpenStates;
    private Happening m_happening;
    private CurrentBattle m_currentBattle;
    private int m_playerArenaRank;
    private bool m_isNeedGetArenaPlayerInfo;
    private List<ProArenaTopRankPlayer> m_arenaTopRankPlayers;
    private bool ShowEnchantCancelConfirmPanel;
    private bool ShowEnchantSaveConfirmPanel;
    private RewardPlayerStatus m_beforeRewardPlayerStatus;
    private List<RewardHeroStatus> m_beforeRewardHeroStatus;
    private List<int> m_battleHeroIDs;
    private bool m_isNeedRebuildBattle;
    private int m_battleRegretCount;
    private ConfigDataBattleInfo m_firstBattleInfo;
    private List<NoticeText> m_waitingNoticeStringList;
    private bool m_isShowingNotice;
    private DateTime m_lastSendPeakArenaPlayOffChatTime;
    public Action<int, List<UserSummary>>[] EventsOnGetUserSummaryAck;
    private int summaryToken;
    public Dictionary<int, ProjectLPlayerContext.CollectionActivityState> m_collectionActivityStates;
    private bool m_needGetFriendSocialRelation;
    private List<UserSummary> m_friendList;
    private List<UserSummary> m_acrossServerFriendList;
    private List<UserSummary> m_findFriendList;
    private List<UserSummary> m_recommendFriendList;
    private Dictionary<int, string> m_monthCardStatePrefNameMap;
    private bool isHideEquipMaster;
    private Dictionary<int, Hero> m_allUseableDefaultHeros;
    private ConfigDataScenarioInfo m_activeNextScenarioInfo;
    private ConfigDataScenarioInfo m_activeLastFinishedScenarioInfo;
    private Dictionary<int, ProjectLPlayerContext.CurrentWaypointEvent> m_activeWaypointEvents;
    private int m_enterWorldUITaskCount;
    private List<ulong> ReadAnnounceActivityList;
    private bool m_isBuyGuideActivityViewed;
    private const string OpenActivityNoticeUIFlag = "OpenActivityNoticeUIFlag";
    private RedeemInfoAck m_RedeemInfo;
    private const int RedeemActivityConfigId = 17;
    private const int FansRewardsFromPBTCBTActivityId = 30;
    private List<ProUserSummary> m_peakArenaTopRankPlayerSummaries;
    private List<ProPeakArenaLeaderboardPlayerInfo> m_peakArenaTopRankPlayerInfos;
    private ProPeakArenaLeaderboardPlayerInfo m_peakArenaPlayerInfo;
    private int m_peakArenaSeasonId;
    private PeakArenaSeasonInfo m_peakAreanaSeasonInfo;
    public bool AlreadyGotPeakArenaPlayoffMatchupInfo;
    private string m_heroErrMsg;
    private ProRealTimePVPLeaderboardPlayerInfo m_realtimePVPPlayerInfo;
    private ProRealTimePVPUserInfo m_realtimePVPOpponentPlayerInfo;
    private List<ProRealTimePVPLeaderboardPlayerInfo> m_realtimePVPLocalLeaderboardPlayerInfos;
    private List<ProUserSummary> m_realtimePVPLocalLeaderboardUserSummarys;
    private List<ProRealTimePVPLeaderboardPlayerInfo> m_realtimePVPGlobalLeaderboardPlayerInfos;
    private List<ProUserSummary> m_realtimePVPGlobalLeaderboardUserSummarys;
    private bool m_isRealTimePVPGetTopPlayersReqGlobal;
    private List<int> m_opendRiftChapterIds;
    private ConfigDataRiftChapterInfo m_lastOpenedRiftChapterInfo;
    private bool[] m_rewardFlags;
    private Dictionary<int, bool> m_isSelectHeroRewardTransferToFragmentDict;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_IsFullMessageLog;
    private static DelegateBridge __Hotfix_SendMessage;
    private static DelegateBridge __Hotfix_OnGameServerMessage;
    private static DelegateBridge __Hotfix_OnMessage_420;
    private static DelegateBridge __Hotfix_OnMessage_419;
    private static DelegateBridge __Hotfix_OnMessage_101;
    private static DelegateBridge __Hotfix_AddOwnerComponent;
    private static DelegateBridge __Hotfix_RemoveOwnerComponent;
    private static DelegateBridge __Hotfix_GetOwnerComponent_0;
    private static DelegateBridge __Hotfix_GetOwnerComponent_1;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_TestMarquee;
    private static DelegateBridge __Hotfix_TestChat;
    private static DelegateBridge __Hotfix_GetSessionToken;
    private static DelegateBridge __Hotfix_GetDeviceId;
    private static DelegateBridge __Hotfix_GetClientVersion;
    private static DelegateBridge __Hotfix_IsDataCacheDirtyByPlayerInfoInitAck;
    private static DelegateBridge __Hotfix_IsPlayerInfoInitAck4CheckOnly;
    private static DelegateBridge __Hotfix_IsLocalGame;
    private static DelegateBridge __Hotfix_GetLocalTime;
    private static DelegateBridge __Hotfix_GetServerTime;
    private static DelegateBridge __Hotfix_LocalTimeToServerTime;
    private static DelegateBridge __Hotfix_SetAssignTime;
    private static DelegateBridge __Hotfix_ServerTimeToLocalTime;
    private static DelegateBridge __Hotfix_GetLastReceiveMessageTime;
    private static DelegateBridge __Hotfix_DataError;
    private static DelegateBridge __Hotfix_IsDataError;
    private static DelegateBridge __Hotfix_TestWriteMessageToFile;
    private static DelegateBridge __Hotfix_TestReadMessageFromFile;
    private static DelegateBridge __Hotfix_get_Happening;
    private static DelegateBridge __Hotfix_get_CurrentBattle;
    private static DelegateBridge __Hotfix_add_EventOnClientCheatNtf;
    private static DelegateBridge __Hotfix_remove_EventOnClientCheatNtf;
    private static DelegateBridge __Hotfix_add_EventOnServerDisconnectNtf;
    private static DelegateBridge __Hotfix_remove_EventOnServerDisconnectNtf;
    private static DelegateBridge __Hotfix_SendAncientCallBossAttackReq;
    private static DelegateBridge __Hotfix_SendAncientCallBossBattleFinishedReq;
    private static DelegateBridge __Hotfix_OnMessage_120;
    private static DelegateBridge __Hotfix_OnMessage_4;
    private static DelegateBridge __Hotfix_OnMessage_5;
    private static DelegateBridge __Hotfix_OnMessage_6;
    private static DelegateBridge __Hotfix_OnMessage_7;
    private static DelegateBridge __Hotfix_GetAncientCallInfo;
    private static DelegateBridge __Hotfix_GetCurrentAncientCallPeriod;
    private static DelegateBridge __Hotfix_GetRecentlyAncientCallPeriodInfo;
    private static DelegateBridge __Hotfix_GetAncientCallBossByBossId;
    private static DelegateBridge __Hotfix_GetAncientCallBossHistoryByBossId;
    private static DelegateBridge __Hotfix_GetAncientCallBossAllMissionList;
    private static DelegateBridge __Hotfix_GetAncientCallBossCompletedMissionList;
    private static DelegateBridge __Hotfix_IsShowAncientCallBossMissionRedMark;
    private static DelegateBridge __Hotfix_GetAncientCallBossProcessingMissionList;
    private static DelegateBridge __Hotfix_GetAncientCallBossCompleteMissionList;
    private static DelegateBridge __Hotfix_IsAncientCallCurrentPeriodExist;
    private static DelegateBridge __Hotfix_GetAncientCallBossIdList;
    private static DelegateBridge __Hotfix_IsAncientCallBossOpen;
    private static DelegateBridge __Hotfix_GetCurrentAncientCallBossId;
    private static DelegateBridge __Hotfix_GetAncientCallBossOpenTime;
    private static DelegateBridge __Hotfix_CanAttackAncientCallBoss;
    private static DelegateBridge __Hotfix_GetAncientCallCurrentBossStartTime;
    private static DelegateBridge __Hotfix_GetAncientCallCurrentBossEndTime;
    private static DelegateBridge __Hotfix_GetAncientCallCurrentPeriodStartTime;
    private static DelegateBridge __Hotfix_GetAncientCallCurrentPeriodEndTime;
    private static DelegateBridge __Hotfix_add_EventOnAncientCallBossAttackAck;
    private static DelegateBridge __Hotfix_remove_EventOnAncientCallBossAttackAck;
    private static DelegateBridge __Hotfix_add_EventOnAncientCallBossBattleFinishedAck;
    private static DelegateBridge __Hotfix_remove_EventOnAncientCallBossBattleFinishedAck;
    private static DelegateBridge __Hotfix_SendAnikiGymLevelAttackReq;
    private static DelegateBridge __Hotfix_SendAnikiGymLevelBattleFinishedReq;
    private static DelegateBridge __Hotfix_OnMessage_121;
    private static DelegateBridge __Hotfix_OnMessage_8;
    private static DelegateBridge __Hotfix_OnMessage_9;
    private static DelegateBridge __Hotfix_IsAnikiGymOpened;
    private static DelegateBridge __Hotfix_IsAnikiLevelOpened;
    private static DelegateBridge __Hotfix_CanAttackAnikiLevel;
    private static DelegateBridge __Hotfix_GetAnikiTicketCount;
    private static DelegateBridge __Hotfix_IsAnikiLevelFinished;
    private static DelegateBridge __Hotfix_GetAnikiGymMaxFinishedLevelId;
    private static DelegateBridge __Hotfix_GetAnikiDailyRewardRestCount;
    private static DelegateBridge __Hotfix_GetAnikiDailyRewardCountMax;
    private static DelegateBridge __Hotfix_IsAnikiLevelBlessing;
    private static DelegateBridge __Hotfix_add_EventOnAnikiGymLevelAttackAck;
    private static DelegateBridge __Hotfix_remove_EventOnAnikiGymLevelAttackAck;
    private static DelegateBridge __Hotfix_add_EventOnAnikiGymLevelBattleFinishedAck;
    private static DelegateBridge __Hotfix_remove_EventOnAnikiGymLevelBattleFinishedAck;
    private static DelegateBridge __Hotfix_SendArenaPlayerInfoGetReq;
    private static DelegateBridge __Hotfix_SendArenaFlushOpponentsReq;
    private static DelegateBridge __Hotfix_SendArenaOpponentViewReq;
    private static DelegateBridge __Hotfix_SendArenaOpponentAttackReq;
    private static DelegateBridge __Hotfix_SendArenaRevengeOpponentGetReq;
    private static DelegateBridge __Hotfix_SendArenaOpponentRevengeReq;
    private static DelegateBridge __Hotfix_SendArenaOpponentAttackFightingReq;
    private static DelegateBridge __Hotfix_SendArenaBattleFinishedReq;
    private static DelegateBridge __Hotfix_SendArenaPlayerSetDefensiveTeamReq;
    private static DelegateBridge __Hotfix_SendArenaVictoryPointsRewardGainReq;
    private static DelegateBridge __Hotfix_SendArenaBattleReportBaiscDataGetReq;
    private static DelegateBridge __Hotfix_SendArenaBattleReportPlayBackDataGetReq;
    private static DelegateBridge __Hotfix_SendArenaTopRankPlayersGetReq;
    private static DelegateBridge __Hotfix_SendArenaBattleReconnectReq;
    private static DelegateBridge __Hotfix_OnMessage_123;
    private static DelegateBridge __Hotfix_OnMessage_21;
    private static DelegateBridge __Hotfix_OnMessage_15;
    private static DelegateBridge __Hotfix_OnMessage_20;
    private static DelegateBridge __Hotfix_OnMessage_17;
    private static DelegateBridge __Hotfix_OnMessage_23;
    private static DelegateBridge __Hotfix_OnMessage_19;
    private static DelegateBridge __Hotfix_OnMessage_18;
    private static DelegateBridge __Hotfix_OnMessage_10;
    private static DelegateBridge __Hotfix_OnMessage_22;
    private static DelegateBridge __Hotfix_OnMessage_26;
    private static DelegateBridge __Hotfix_OnMessage_12;
    private static DelegateBridge __Hotfix_OnMessage_14;
    private static DelegateBridge __Hotfix_OnMessage_25;
    private static DelegateBridge __Hotfix_OnMessage_11;
    private static DelegateBridge __Hotfix_OnMessage_13;
    private static DelegateBridge __Hotfix_OnMessage_16;
    private static DelegateBridge __Hotfix_IsEmptyArenaPlayerInfo;
    private static DelegateBridge __Hotfix_IsNeedGetArenaPlayerInfo;
    private static DelegateBridge __Hotfix_IsNeedFlushArenaOpponents;
    private static DelegateBridge __Hotfix_GetPlayerArenaLevelId;
    private static DelegateBridge __Hotfix_GetPlayerArenaPoints;
    private static DelegateBridge __Hotfix_GetPlayerArenaVictoryPoints;
    private static DelegateBridge __Hotfix_GetArenaThisWeekVictoryNums;
    private static DelegateBridge __Hotfix_GetArenaThisWeekTotalBattleNums;
    private static DelegateBridge __Hotfix_IsArenaAutoBattle;
    private static DelegateBridge __Hotfix_GetArenaTicketCount;
    private static DelegateBridge __Hotfix_GetArenaHonour;
    private static DelegateBridge __Hotfix_CanGetArenaVictoryPointsReward;
    private static DelegateBridge __Hotfix_GetArenaVictoryPointsRewardStatus;
    private static DelegateBridge __Hotfix_GetArenaThisWeekDefendBattleIds;
    private static DelegateBridge __Hotfix_CanAttackArenaOpponent;
    private static DelegateBridge __Hotfix_CanRevengeArenaOpponent;
    private static DelegateBridge __Hotfix_GetArenaOpponents;
    private static DelegateBridge __Hotfix_GetArenaOpponentNextFlushTime;
    private static DelegateBridge __Hotfix_GetArenaOpponentDefensiveBattleInfo;
    private static DelegateBridge __Hotfix_GetArenaPlayerDefensiveTeam;
    private static DelegateBridge __Hotfix_IsArenaInSettleTime;
    private static DelegateBridge __Hotfix_GetAllArenaBattleReports;
    private static DelegateBridge __Hotfix_GetArenaPointChangeValue;
    private static DelegateBridge __Hotfix_GetPlayerArenaRank;
    private static DelegateBridge __Hotfix_GetArenaTopRankPlayers;
    private static DelegateBridge __Hotfix_CanReconnectArenaBattle;
    private static DelegateBridge __Hotfix_add_EventOnArenaPlayerInfoGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnArenaPlayerInfoGetAck;
    private static DelegateBridge __Hotfix_add_EventOnArenaFlushOpponentsAck;
    private static DelegateBridge __Hotfix_remove_EventOnArenaFlushOpponentsAck;
    private static DelegateBridge __Hotfix_add_EventOnArenaOpponentViewAck;
    private static DelegateBridge __Hotfix_remove_EventOnArenaOpponentViewAck;
    private static DelegateBridge __Hotfix_add_EventOnArenaOpponentAttackAck;
    private static DelegateBridge __Hotfix_remove_EventOnArenaOpponentAttackAck;
    private static DelegateBridge __Hotfix_add_EventOnArenaRevengeOpponentGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnArenaRevengeOpponentGetAck;
    private static DelegateBridge __Hotfix_add_EventOnArenaOpponentRevengeAck;
    private static DelegateBridge __Hotfix_remove_EventOnArenaOpponentRevengeAck;
    private static DelegateBridge __Hotfix_add_EventOnArenaOpponentAttackFightingAck;
    private static DelegateBridge __Hotfix_remove_EventOnArenaOpponentAttackFightingAck;
    private static DelegateBridge __Hotfix_add_EventOnArenaBattleFinishedAck;
    private static DelegateBridge __Hotfix_remove_EventOnArenaBattleFinishedAck;
    private static DelegateBridge __Hotfix_add_EventOnArenaPlayerSetDefensiveTeamAck;
    private static DelegateBridge __Hotfix_remove_EventOnArenaPlayerSetDefensiveTeamAck;
    private static DelegateBridge __Hotfix_add_EventOnArenaVictoryPointsRewardGainAck;
    private static DelegateBridge __Hotfix_remove_EventOnArenaVictoryPointsRewardGainAck;
    private static DelegateBridge __Hotfix_add_EventOnArenaBattleReportBasicDataGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnArenaBattleReportBasicDataGetAck;
    private static DelegateBridge __Hotfix_add_EventOnArenaBattleReportPlayBackDataGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnArenaBattleReportPlayBackDataGetAck;
    private static DelegateBridge __Hotfix_add_EventOnArenaTopRankPlayersGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnArenaTopRankPlayersGetAck;
    private static DelegateBridge __Hotfix_add_EventOnArenaBattleReconnectAck;
    private static DelegateBridge __Hotfix_remove_EventOnArenaBattleReconnectAck;
    private static DelegateBridge __Hotfix_add_EventOnArenaBattleReportNtf;
    private static DelegateBridge __Hotfix_remove_EventOnArenaBattleReportNtf;
    private static DelegateBridge __Hotfix_add_EventOnArenaFlushOpponentsNtf;
    private static DelegateBridge __Hotfix_remove_EventOnArenaFlushOpponentsNtf;
    private static DelegateBridge __Hotfix_SendHeroMaxLevelUseExpItemReq;
    private static DelegateBridge __Hotfix_SendOpenHeroSkinSelfSelectedBoxReq;
    private static DelegateBridge __Hotfix_SendOpenSelfSelectedBoxReq;
    private static DelegateBridge __Hotfix_SendUseBagItemReq;
    private static DelegateBridge __Hotfix_SendSellBagItemReq;
    private static DelegateBridge __Hotfix_SendLockAndUnLockEquipmentReq;
    private static DelegateBridge __Hotfix_SendEquipmentEnhanceReq;
    private static DelegateBridge __Hotfix_SendEquipmentStarLevelUpReq;
    private static DelegateBridge __Hotfix_SendBagItemDecomposeReq;
    private static DelegateBridge __Hotfix_SendBagItemComposeReq;
    private static DelegateBridge __Hotfix_SendEquipmentEnchantReq;
    private static DelegateBridge __Hotfix_SendEquipmentEnchantSaveReq;
    private static DelegateBridge __Hotfix_OnMessage_273;
    private static DelegateBridge __Hotfix_OnMessage_125;
    private static DelegateBridge __Hotfix_OnMessage_124;
    private static DelegateBridge __Hotfix_OnMessage_416;
    private static DelegateBridge __Hotfix_OnMessage_309;
    private static DelegateBridge __Hotfix_OnMessage_311;
    private static DelegateBridge __Hotfix_OnMessage_373;
    private static DelegateBridge __Hotfix_OnMessage_171;
    private static DelegateBridge __Hotfix_OnMessage_170;
    private static DelegateBridge __Hotfix_OnMessage_172;
    private static DelegateBridge __Hotfix_OnMessage_29;
    private static DelegateBridge __Hotfix_OnMessage_28;
    private static DelegateBridge __Hotfix_OnMessage_168;
    private static DelegateBridge __Hotfix_OnMessage_169;
    private static DelegateBridge __Hotfix_CanDecomposeBagItems;
    private static DelegateBridge __Hotfix_CanEnchantEquipment;
    private static DelegateBridge __Hotfix_GetBagItems;
    private static DelegateBridge __Hotfix_GetBagSize;
    private static DelegateBridge __Hotfix_GetBagItem;
    private static DelegateBridge __Hotfix_GetBagItemByType;
    private static DelegateBridge __Hotfix_IsExistBagItemIgnoreInstanceDifference;
    private static DelegateBridge __Hotfix_GetBagItemByInstanceId;
    private static DelegateBridge __Hotfix_GetBagItemCount;
    private static DelegateBridge __Hotfix_GetBagItemCountByType;
    private static DelegateBridge __Hotfix_GetBagItemCountByTypeAndID;
    private static DelegateBridge __Hotfix_GetInstanceBagItemInstanceIdsByContentId;
    private static DelegateBridge __Hotfix_GetBagItemCountByInstanceId;
    private static DelegateBridge __Hotfix_CanUseEnergyMedicine;
    private static DelegateBridge __Hotfix_CalculateEnhanceEquipmentExp;
    private static DelegateBridge __Hotfix_CalculateEnhanceEquipmentGold;
    private static DelegateBridge __Hotfix_CaculateEquipmentNextLevelExp_0;
    private static DelegateBridge __Hotfix_CaculateEquipmentNextLevelExp_1;
    private static DelegateBridge __Hotfix_CanEnhanceEquipment;
    private static DelegateBridge __Hotfix_CanLevelUpEquipmentStar;
    private static DelegateBridge __Hotfix_IsLevelTicketsMaxInMission;
    private static DelegateBridge __Hotfix_CalcEquipmentDecomposeReturnGold;
    private static DelegateBridge __Hotfix_GetEquipmentResonanceNums;
    private static DelegateBridge __Hotfix_GetEquipmentResonanceNumsByHeroAndEquipment;
    private static DelegateBridge __Hotfix_GetEquipmentResonanceNumsByHeroId;
    private static DelegateBridge __Hotfix_RemoveBagItemByType;
    private static DelegateBridge __Hotfix_IsShowEnchantCancelConfirmPanel;
    private static DelegateBridge __Hotfix_SetEnchantCancelConfirmFlag;
    private static DelegateBridge __Hotfix_IsShowEnchantSaveConfirmPanel;
    private static DelegateBridge __Hotfix_SetEnchantSaveConfirmFlag;
    private static DelegateBridge __Hotfix_HasOwn;
    private static DelegateBridge __Hotfix_GetLevelUpEquipmentStarItems;
    private static DelegateBridge __Hotfix_NeedUpdatePlayerResource;
    private static DelegateBridge __Hotfix_add_EventOnBagItemChangeNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBagItemChangeNtf;
    private static DelegateBridge __Hotfix_add_EventOnUseBagItemAck;
    private static DelegateBridge __Hotfix_remove_EventOnUseBagItemAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroMaxLevelUseExpItemAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroMaxLevelUseExpItemAck;
    private static DelegateBridge __Hotfix_add_EventOnSellBagItemAck;
    private static DelegateBridge __Hotfix_remove_EventOnSellBagItemAck;
    private static DelegateBridge __Hotfix_add_EventOnEquipmentLockAndUnLockAck;
    private static DelegateBridge __Hotfix_remove_EventOnEquipmentLockAndUnLockAck;
    private static DelegateBridge __Hotfix_add_EventOnEquipmentEnhanceAck;
    private static DelegateBridge __Hotfix_remove_EventOnEquipmentEnhanceAck;
    private static DelegateBridge __Hotfix_add_EventOnEquipmentStarLevelUpAck;
    private static DelegateBridge __Hotfix_remove_EventOnEquipmentStarLevelUpAck;
    private static DelegateBridge __Hotfix_add_EventOnBagItemDecomposeAck;
    private static DelegateBridge __Hotfix_remove_EventOnBagItemDecomposeAck;
    private static DelegateBridge __Hotfix_add_EventOnEquipmentEnchantAck;
    private static DelegateBridge __Hotfix_remove_EventOnEquipmentEnchantAck;
    private static DelegateBridge __Hotfix_add_EventOnEquipmentEnchantSaveAck;
    private static DelegateBridge __Hotfix_remove_EventOnEquipmentEnchantSaveAck;
    private static DelegateBridge __Hotfix_add_EventOnOpenHeroSkinSelfSelectedBoxAck;
    private static DelegateBridge __Hotfix_remove_EventOnOpenHeroSkinSelfSelectedBoxAck;
    private static DelegateBridge __Hotfix_add_EventOnOpenSelfSelectedBoxAck;
    private static DelegateBridge __Hotfix_remove_EventOnOpenSelfSelectedBoxAck;
    private static DelegateBridge __Hotfix_add_EventOnBagItemComposeAck;
    private static DelegateBridge __Hotfix_remove_EventOnBagItemComposeAck;
    private static DelegateBridge __Hotfix_SendBattleCancelReq;
    private static DelegateBridge __Hotfix_SendBattleTeamSetReq;
    private static DelegateBridge __Hotfix_SendBattleArmyRandomSeedGetReq;
    private static DelegateBridge __Hotfix_OnMessage_126;
    private static DelegateBridge __Hotfix_OnMessage_31;
    private static DelegateBridge __Hotfix_OnMessage_76;
    private static DelegateBridge __Hotfix_OnMessage_30;
    private static DelegateBridge __Hotfix_IsGameFunctionOpenByMonthCard;
    private static DelegateBridge __Hotfix_SetBattleBase;
    private static DelegateBridge __Hotfix_SetCurrentBattle;
    private static DelegateBridge __Hotfix_SetCurrentArenaBattle;
    private static DelegateBridge __Hotfix_SetCurrentArenaBattleReport;
    private static DelegateBridge __Hotfix_SetCurrentPVPBattle;
    private static DelegateBridge __Hotfix_SetCurrentRealTimePVPBattle;
    private static DelegateBridge __Hotfix_SetCurrentRealTimePVPBattleReport;
    private static DelegateBridge __Hotfix_SetCurrentPeakArenaBattle;
    private static DelegateBridge __Hotfix_SetCurrentPeakArenaBattleReport;
    private static DelegateBridge __Hotfix_SetCurrentPeakArenaMatchGroupId;
    private static DelegateBridge __Hotfix_IsCurrentBattleFirstBattle;
    private static DelegateBridge __Hotfix_GetBattleTeam;
    private static DelegateBridge __Hotfix_FindGamePlayTeamTypeId;
    private static DelegateBridge __Hotfix_CreateBattleHeroFromMyHero;
    private static DelegateBridge __Hotfix_CreateDefaultBattleHero;
    private static DelegateBridge __Hotfix_SaveBeforeRewardStatus;
    private static DelegateBridge __Hotfix_SaveBattleHeroIds;
    private static DelegateBridge __Hotfix_GetBeforeRewardPlayerStatus;
    private static DelegateBridge __Hotfix_GetBeforeRewardHeroStatus;
    private static DelegateBridge __Hotfix_GetProcessingBattle;
    private static DelegateBridge __Hotfix_GetArenaBattleStatus;
    private static DelegateBridge __Hotfix_GetArenaBattleId;
    private static DelegateBridge __Hotfix_SetNeedRebuildBattle;
    private static DelegateBridge __Hotfix_IsNeedRebuildBattle;
    private static DelegateBridge __Hotfix_GetGainBattleTreasrueIds;
    private static DelegateBridge __Hotfix_GetGainBattleTreasureCount;
    private static DelegateBridge __Hotfix_ComputeBattlePower_0;
    private static DelegateBridge __Hotfix_ComputeBattlePower_1;
    private static DelegateBridge __Hotfix_SetBattleRandomSeed;
    private static DelegateBridge __Hotfix_GetBattleRandomSeed;
    private static DelegateBridge __Hotfix_SetArenaBattleRandomSeed;
    private static DelegateBridge __Hotfix_GetArenaBattleRandomSeed;
    private static DelegateBridge __Hotfix_SetBattleArmyRandomSeed;
    private static DelegateBridge __Hotfix_GetBattleArmyRandomSeed;
    private static DelegateBridge __Hotfix_BattleFinishedError;
    private static DelegateBridge __Hotfix_GetDeadEnemyBattleActors;
    private static DelegateBridge __Hotfix_UpdateGeneralInviteInfos;
    private static DelegateBridge __Hotfix_SetRemainBattleRegretCount;
    private static DelegateBridge __Hotfix_GetRemainBattleRegretCount;
    private static DelegateBridge __Hotfix_UseBattleRegret;
    private static DelegateBridge __Hotfix_GetCurrentBattleFailEnergyCost;
    private static DelegateBridge __Hotfix_GetChapterIdFromLevelId;
    private static DelegateBridge __Hotfix_CompareInviteTimeout;
    private static DelegateBridge __Hotfix_add_EventOnBattleCancelAck;
    private static DelegateBridge __Hotfix_remove_EventOnBattleCancelAck;
    private static DelegateBridge __Hotfix_add_EventOnBattleTeamSetAck;
    private static DelegateBridge __Hotfix_remove_EventOnBattleTeamSetAck;
    private static DelegateBridge __Hotfix_add_EventOnBattleArmyRandomSeedGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnBattleArmyRandomSeedGetAck;
    private static DelegateBridge __Hotfix_SendTeamBattleRoomCreateReq;
    private static DelegateBridge __Hotfix_SendBattleRoomPlayerStatusChangeReq;
    private static DelegateBridge __Hotfix_OnMessage_69;
    private static DelegateBridge __Hotfix_SendBattleRoomHeroSetupReq;
    private static DelegateBridge __Hotfix_SendBattleRoomHeroSwapReq;
    private static DelegateBridge __Hotfix_SendBattleRoomHeroPutdownReq;
    private static DelegateBridge __Hotfix_SendBattleRoomHeroProtectReq;
    private static DelegateBridge __Hotfix_SendBattleRoomHeroBanReq;
    private static DelegateBridge __Hotfix_SendBattleRoomEndCurrentBPTurnReq;
    private static DelegateBridge __Hotfix_SendBattleRoomBattleCommandExecuteReq;
    private static DelegateBridge __Hotfix_SendBattleRoomQuitReq;
    private static DelegateBridge __Hotfix_SendBattleRoomPlayerActionBeginReq;
    private static DelegateBridge __Hotfix_SendBattleRoomGetReq;
    private static DelegateBridge __Hotfix_SendBattleRoomBattleLogReq;
    private static DelegateBridge __Hotfix_SendBattleRoomBPStageCommandExecuteReq;
    private static DelegateBridge __Hotfix_OnMessage_46;
    private static DelegateBridge __Hotfix_OnMessage_63;
    private static DelegateBridge __Hotfix_OnMessage_54;
    private static DelegateBridge __Hotfix_OnMessage_56;
    private static DelegateBridge __Hotfix_OnMessage_53;
    private static DelegateBridge __Hotfix_OnMessage_52;
    private static DelegateBridge __Hotfix_OnMessage_51;
    private static DelegateBridge __Hotfix_OnMessage_48;
    private static DelegateBridge __Hotfix_OnMessage_42;
    private static DelegateBridge __Hotfix_OnMessage_68;
    private static DelegateBridge __Hotfix_OnMessage_60;
    private static DelegateBridge __Hotfix_OnMessage_49;
    private static DelegateBridge __Hotfix_OnMessage_44;
    private static DelegateBridge __Hotfix_OnMessage_74;
    private static DelegateBridge __Hotfix_OnMessage_50;
    private static DelegateBridge __Hotfix_OnMessage_66;
    private static DelegateBridge __Hotfix_OnMessage_71;
    private static DelegateBridge __Hotfix_OnMessage_58;
    private static DelegateBridge __Hotfix_OnMessage_64;
    private static DelegateBridge __Hotfix_OnMessage_55;
    private static DelegateBridge __Hotfix_OnMessage_75;
    private static DelegateBridge __Hotfix_OnMessage_67;
    private static DelegateBridge __Hotfix_OnMessage_72;
    private static DelegateBridge __Hotfix_OnMessage_59;
    private static DelegateBridge __Hotfix_OnMessage_43;
    private static DelegateBridge __Hotfix_OnMessage_45;
    private static DelegateBridge __Hotfix_OnMessage_73;
    private static DelegateBridge __Hotfix_OnMessage_32;
    private static DelegateBridge __Hotfix_OnMessage_65;
    private static DelegateBridge __Hotfix_OnMessage_70;
    private static DelegateBridge __Hotfix_OnMessage_57;
    private static DelegateBridge __Hotfix_OnMessage_61;
    private static DelegateBridge __Hotfix_OnMessage_62;
    private static DelegateBridge __Hotfix_OnMessage_47;
    private static DelegateBridge __Hotfix_IsInBattleRoom;
    private static DelegateBridge __Hotfix_IsInBattleLiveRoom;
    private static DelegateBridge __Hotfix_GetBattleLiveRoomId;
    private static DelegateBridge __Hotfix_CheckCanJoinBattleRoom;
    private static DelegateBridge __Hotfix_GetBattleRoom;
    private static DelegateBridge __Hotfix_SetQuitBattleRoom;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomCreateAck;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomCreateAck;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomPlayerStatusChangeAck;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomPlayerStatusChangeAck;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomHeroSetupAck;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomHeroSetupAck;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomHeroSwapAck;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomHeroSwapAck;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomHeroPutdownAck;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomHeroPutdownAck;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomHeroProtectAck;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomHeroProtectAck;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomHeroBanAck;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomHeroBanAck;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomEndCurrentBPTurnAck;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomEndCurrentBPTurnAck;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomBattleCommandExecuteAck;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomBattleCommandExecuteAck;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomQuitAck;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomQuitAck;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomPlayerActionBeginAck;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomPlayerActionBeginAck;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomGetAck;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomBPStageCommandExecuteAck;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomBPStageCommandExecuteAck;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomTeamBattleJoinNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomTeamBattleJoinNtf;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomGuildMassiveCombatBattleJoinNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomGuildMassiveCombatBattleJoinNtf;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomPVPBattleJoinNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomPVPBattleJoinNtf;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomRealTimePVPBattleJoinNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomRealTimePVPBattleJoinNtf;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomPeakArenaBattleJoinNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomPeakArenaBattleJoinNtf;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomPlayerStatusChangedNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomPlayerStatusChangedNtf;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomQuitNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomQuitNtf;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomHeroSetupNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomHeroSetupNtf;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomTeamBattleStartNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomTeamBattleStartNtf;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomPVPBattleStartNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomPVPBattleStartNtf;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomRealTimePVPBattleStartNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomRealTimePVPBattleStartNtf;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomPeakArenaBattleStartNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomPeakArenaBattleStartNtf;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomBattleCommandExecuteNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomBattleCommandExecuteNtf;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomBPStageCommandExecuteNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomBPStageCommandExecuteNtf;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomTeamBattleFinishNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomTeamBattleFinishNtf;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomGuildMassiveCombatBattleFinishNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomGuildMassiveCombatBattleFinishNtf;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomPVPBattleFinishNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomPVPBattleFinishNtf;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomRealTimePVPBattleFinishNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomRealTimePVPBattleFinishNtf;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomPeakArenaBattleFinishNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomPeakArenaBattleFinishNtf;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomPlayerBattleInfoInitNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomPlayerBattleInfoInitNtf;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomPlayerBPStageInfoInitNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomPlayerBPStageInfoInitNtf;
    private static DelegateBridge __Hotfix_add_EventOnBattleRoomDataChangeNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBattleRoomDataChangeNtf;
    private static DelegateBridge __Hotfix_SendChatTextMessage;
    private static DelegateBridge __Hotfix_SendChatExpressionMessage;
    private static DelegateBridge __Hotfix_SendChatVoiceMessage;
    private static DelegateBridge __Hotfix_SendGetVoiceContentMessage;
    private static DelegateBridge __Hotfix_SendWorldEnterNewRoomMessage;
    private static DelegateBridge __Hotfix_SendGetChatContactUserSummaryInfoMessage;
    private static DelegateBridge __Hotfix_SendChatHeroEquipmentMessage;
    private static DelegateBridge __Hotfix_SendChatBattleReportMessage;
    private static DelegateBridge __Hotfix_OnMessage_85;
    private static DelegateBridge __Hotfix_OnMessage_127;
    private static DelegateBridge __Hotfix_OnMessage_96;
    private static DelegateBridge __Hotfix_OnMessage_97;
    private static DelegateBridge __Hotfix_OnMessage_84;
    private static DelegateBridge __Hotfix_OnMessage_98;
    private static DelegateBridge __Hotfix_OnMessage_99;
    private static DelegateBridge __Hotfix_OnMessage_95;
    private static DelegateBridge __Hotfix_GetChatGroupName;
    private static DelegateBridge __Hotfix_GetPrivateChatPlayeName;
    private static DelegateBridge __Hotfix_GetChatComponent;
    private static DelegateBridge __Hotfix_CanSendChatMessage;
    private static DelegateBridge __Hotfix_CanSendDanmaku;
    private static DelegateBridge __Hotfix_GetChatMessageList;
    private static DelegateBridge __Hotfix_GetUnReadChatMsgCount4PointPlayerOrGroup;
    private static DelegateBridge __Hotfix_GetGroupChatId;
    private static DelegateBridge __Hotfix_SetGroupChatTarget;
    private static DelegateBridge __Hotfix_ClearGroupChatTarget;
    private static DelegateBridge __Hotfix_SetPrivateChatTarget;
    private static DelegateBridge __Hotfix_CleanPrivateChatTarget;
    private static DelegateBridge __Hotfix_GetPrivateChatPlayerId;
    private static DelegateBridge __Hotfix_GetRecentPrivateChatIdList;
    private static DelegateBridge __Hotfix_GetNewChatMsgCount;
    private static DelegateBridge __Hotfix_GetGroupUnreadChatMsgCount;
    private static DelegateBridge __Hotfix_GetAssignGroupUnreadChatMsgCount;
    private static DelegateBridge __Hotfix_GetAssignPrivateUnreadChatMsgCount;
    private static DelegateBridge __Hotfix_GetPrivateUnreadChatMsgCount;
    private static DelegateBridge __Hotfix_GetGuildUnreadChatMsgCount;
    private static DelegateBridge __Hotfix_GetRoomIndex;
    private static DelegateBridge __Hotfix_ReadChat;
    private static DelegateBridge __Hotfix_ReadChatBeforeDate;
    private static DelegateBridge __Hotfix_RefreshGuildChatLastReadTime;
    private static DelegateBridge __Hotfix_GetGuildChatLastReadTime;
    private static DelegateBridge __Hotfix_GetRecentChatTargetList;
    private static DelegateBridge __Hotfix_ClearGuildChatContent;
    private static DelegateBridge __Hotfix_GetWaitingNoticeStringListFirstNotice;
    private static DelegateBridge __Hotfix_GetWaitingNoticeStringListCount;
    private static DelegateBridge __Hotfix_AddNoticeToWaitingNoticeStringList;
    private static DelegateBridge __Hotfix_SetIsShowingNoticeFlag;
    private static DelegateBridge __Hotfix_GetIsShowingNoticeFlag;
    private static DelegateBridge __Hotfix_add_EventOnChatMessageNtf;
    private static DelegateBridge __Hotfix_remove_EventOnChatMessageNtf;
    private static DelegateBridge __Hotfix_add_EventOnChatContactUserSummaryInfoAck;
    private static DelegateBridge __Hotfix_remove_EventOnChatContactUserSummaryInfoAck;
    private static DelegateBridge __Hotfix_add_EventOnChatMessageAck;
    private static DelegateBridge __Hotfix_remove_EventOnChatMessageAck;
    private static DelegateBridge __Hotfix_add_EventOnChatGetVoiceContentAck;
    private static DelegateBridge __Hotfix_remove_EventOnChatGetVoiceContentAck;
    private static DelegateBridge __Hotfix_add_EventOnChatEnterRoomAck;
    private static DelegateBridge __Hotfix_remove_EventOnChatEnterRoomAck;
    private static DelegateBridge __Hotfix_SendChangeOwnerReq;
    private static DelegateBridge __Hotfix_SendChangeNameReq;
    private static DelegateBridge __Hotfix_SendCreateChatGroupReq;
    private static DelegateBridge __Hotfix_SendGetChatGroupMemberReq;
    private static DelegateBridge __Hotfix_SendGetAllChatGroupReq;
    private static DelegateBridge __Hotfix_SendInviteToChatGroupReq;
    private static DelegateBridge __Hotfix_SendLeaveChatGroupReq;
    private static DelegateBridge __Hotfix_SendKickUserReq;
    private static DelegateBridge __Hotfix_SendGetUserSummaryReq;
    private static DelegateBridge __Hotfix_OnMessage_87;
    private static DelegateBridge __Hotfix_OnMessage_86;
    private static DelegateBridge __Hotfix_OnMessage_88;
    private static DelegateBridge __Hotfix_OnMessage_89;
    private static DelegateBridge __Hotfix_OnMessage_90;
    private static DelegateBridge __Hotfix_OnMessage_91;
    private static DelegateBridge __Hotfix_OnMessage_93;
    private static DelegateBridge __Hotfix_OnMessage_92;
    private static DelegateBridge __Hotfix_OnMessage_187;
    private static DelegateBridge __Hotfix_OnMessage_94;
    private static DelegateBridge __Hotfix_GetGuildMemberSummary;
    private static DelegateBridge __Hotfix_add_EventOnCreateChatGroupAck;
    private static DelegateBridge __Hotfix_remove_EventOnCreateChatGroupAck;
    private static DelegateBridge __Hotfix_add_EventOnGetAllChatGroupAck;
    private static DelegateBridge __Hotfix_remove_EventOnGetAllChatGroupAck;
    private static DelegateBridge __Hotfix_add_EventOnGetChatGroupMemberAck;
    private static DelegateBridge __Hotfix_remove_EventOnGetChatGroupMemberAck;
    private static DelegateBridge __Hotfix_add_EventOnChatGroupChangeOwnerAck;
    private static DelegateBridge __Hotfix_remove_EventOnChatGroupChangeOwnerAck;
    private static DelegateBridge __Hotfix_add_EventOnChatGroupChangeNameAck;
    private static DelegateBridge __Hotfix_remove_EventOnChatGroupChangeNameAck;
    private static DelegateBridge __Hotfix_add_EventOnInviteToChatGroupAck;
    private static DelegateBridge __Hotfix_remove_EventOnInviteToChatGroupAck;
    private static DelegateBridge __Hotfix_add_EventOnLeaveChatGroupAck;
    private static DelegateBridge __Hotfix_remove_EventOnLeaveChatGroupAck;
    private static DelegateBridge __Hotfix_add_EventOnKickUserFromGroupAck;
    private static DelegateBridge __Hotfix_remove_EventOnKickUserFromGroupAck;
    private static DelegateBridge __Hotfix_add_EventOnChatGroupUpdateNtf;
    private static DelegateBridge __Hotfix_remove_EventOnChatGroupUpdateNtf;
    private static DelegateBridge __Hotfix_SendClimbTowerGetReq;
    private static DelegateBridge __Hotfix_SendClimbTowerLevelAttackReq;
    private static DelegateBridge __Hotfix_SendClimbTowerLevelBattleFinishedReq;
    private static DelegateBridge __Hotfix_SendClimbTowerRaidReq;
    private static DelegateBridge __Hotfix_OnMessage_128;
    private static DelegateBridge __Hotfix_OnMessage_103;
    private static DelegateBridge __Hotfix_OnMessage_104;
    private static DelegateBridge __Hotfix_OnMessage_105;
    private static DelegateBridge __Hotfix_OnMessage_106;
    private static DelegateBridge __Hotfix_CanAttackClimbTowerFloor;
    private static DelegateBridge __Hotfix_GetClimbTowerNextFlushTime;
    private static DelegateBridge __Hotfix_GetClimbTowerMaxFloorId;
    private static DelegateBridge __Hotfix_GetClimbTowerFinishedFloorId;
    private static DelegateBridge __Hotfix_GetClimbTowerRaidMaxFloorId;
    private static DelegateBridge __Hotfix_GetClimbTowerHistoryMaxFloorId;
    private static DelegateBridge __Hotfix_ClimbTowerTryRaid;
    private static DelegateBridge __Hotfix_GetClimbTowerFloor;
    private static DelegateBridge __Hotfix_IsNeedFlush;
    private static DelegateBridge __Hotfix_add_EventOnClimbTowerGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnClimbTowerGetAck;
    private static DelegateBridge __Hotfix_add_EventOnClimbTowerLevelAttackAck;
    private static DelegateBridge __Hotfix_remove_EventOnClimbTowerLevelAttackAck;
    private static DelegateBridge __Hotfix_add_EventOnClimbTowerLevelBattleFinishedAck;
    private static DelegateBridge __Hotfix_remove_EventOnClimbTowerLevelBattleFinishedAck;
    private static DelegateBridge __Hotfix_add_EventOnClimbTowerRaidAck;
    private static DelegateBridge __Hotfix_remove_EventOnClimbTowerRaidAck;
    private static DelegateBridge __Hotfix_SendCollectionWayPointMoveReq;
    private static DelegateBridge __Hotfix_SendCollectionScenarioHandleReq;
    private static DelegateBridge __Hotfix_SendCollectionLevelAttackReq;
    private static DelegateBridge __Hotfix_SendCollectionLevelFinishReq;
    private static DelegateBridge __Hotfix_SendCollectionExchangeReq;
    private static DelegateBridge __Hotfix_SendCollectionEventHandleReq;
    private static DelegateBridge __Hotfix_SendCollectionEventAttackReq;
    private static DelegateBridge __Hotfix_SendCollectionEventBattleFinishedReq;
    private static DelegateBridge __Hotfix_OnMessage_129;
    private static DelegateBridge __Hotfix_OnMessage_114;
    private static DelegateBridge __Hotfix_OnMessage_113;
    private static DelegateBridge __Hotfix_OnMessage_111;
    private static DelegateBridge __Hotfix_OnMessage_112;
    private static DelegateBridge __Hotfix_OnMessage_110;
    private static DelegateBridge __Hotfix_OnMessage_0;
    private static DelegateBridge __Hotfix_OnCollectionEventAdd;
    private static DelegateBridge __Hotfix_OnMessage_118;
    private static DelegateBridge __Hotfix_OnCollectionEventDelete;
    private static DelegateBridge __Hotfix_OnMessage_109;
    private static DelegateBridge __Hotfix_OnMessage_107;
    private static DelegateBridge __Hotfix_OnMessage_108;
    private static DelegateBridge __Hotfix_IsCollectionActivityOpened;
    private static DelegateBridge __Hotfix_IsCollectionActivityDisplay;
    private static DelegateBridge __Hotfix_IsCollectionActivityExchangeTime;
    private static DelegateBridge __Hotfix_GetCollectionActivityOpenTime;
    private static DelegateBridge __Hotfix_GetCollectionActivityExchangeEndTime;
    private static DelegateBridge __Hotfix_CanMoveToCollectionActivityWaypoint;
    private static DelegateBridge __Hotfix_GetCollectionActivityWaypointState;
    private static DelegateBridge __Hotfix_GetCollectionActivityWaypointRedPointCount;
    private static DelegateBridge __Hotfix_GetCollectionActivityPlayerGraphicResource;
    private static DelegateBridge __Hotfix_CanHandleCollectionActivityScenario;
    private static DelegateBridge __Hotfix_CanAttackCollectionActivityLevel;
    private static DelegateBridge __Hotfix_CanAttackCollectionActivityEvent;
    private static DelegateBridge __Hotfix_CanHandleCollectionActivityEvent;
    private static DelegateBridge __Hotfix_GetNextCollectionActivityScenarioLevelInfo;
    private static DelegateBridge __Hotfix_GetCollectionActivityCurrentWaypointInfo;
    private static DelegateBridge __Hotfix_IsCollectionActivityScenarioLevelFinished;
    private static DelegateBridge __Hotfix_IsCollectionActivityChallengeLevelFinished;
    private static DelegateBridge __Hotfix_IsCollectionActivityLootLevelFinished;
    private static DelegateBridge __Hotfix_GetCollectionActivityMaxFinishedLootLevelId;
    private static DelegateBridge __Hotfix_IsCollectionActivityChallengePreLevelFinished;
    private static DelegateBridge __Hotfix_IsCollectionActivityLootPreLevelFinished;
    private static DelegateBridge __Hotfix_GetCollectionActivityScenarioLevelUnlockDay;
    private static DelegateBridge __Hotfix_GetCollectionActivityChallengeLevelUnlockDay;
    private static DelegateBridge __Hotfix_IsCollectionActivityChallengeLevelPlayerLevelVaild;
    private static DelegateBridge __Hotfix_GetCollectionActivityLootLevelUnlockDay;
    private static DelegateBridge __Hotfix_IsCollectionActivityLootLevelPlayerLevelVaild;
    private static DelegateBridge __Hotfix_IsCollectionActivityLootLevelUnlock;
    private static DelegateBridge __Hotfix_CanCollectionActivityExchangeItem;
    private static DelegateBridge __Hotfix_FindActivityInstanceIdByCollectionActivityId;
    private static DelegateBridge __Hotfix_IsCollectionActivityScoreRewardGot;
    private static DelegateBridge __Hotfix_GetCollectionActivityScore;
    private static DelegateBridge __Hotfix_CheckChangeActiveNextCollectionActivityScenarioLevelInfo;
    private static DelegateBridge __Hotfix_GetActiveNextCollectionActivityScenarioLevelInfo;
    private static DelegateBridge __Hotfix_UpdateActiveCollectionActivityWaypointEvents;
    private static DelegateBridge __Hotfix_GetActiveCollectionActivityWaypointEvents;
    private static DelegateBridge __Hotfix_GetActiveCollectionActivityEventInfo;
    private static DelegateBridge __Hotfix_add_EventOnCollectionWayPointMoveAck;
    private static DelegateBridge __Hotfix_remove_EventOnCollectionWayPointMoveAck;
    private static DelegateBridge __Hotfix_add_EventOnCollectionScenarioHandleAck;
    private static DelegateBridge __Hotfix_remove_EventOnCollectionScenarioHandleAck;
    private static DelegateBridge __Hotfix_add_EventOnCollectionLevelAttackAck;
    private static DelegateBridge __Hotfix_remove_EventOnCollectionLevelAttackAck;
    private static DelegateBridge __Hotfix_add_EventOnCollectionLevelFinishAck;
    private static DelegateBridge __Hotfix_remove_EventOnCollectionLevelFinishAck;
    private static DelegateBridge __Hotfix_add_EventOnCollectionExchangeAck;
    private static DelegateBridge __Hotfix_remove_EventOnCollectionExchangeAck;
    private static DelegateBridge __Hotfix_add_EventOnCollectionEventAdd;
    private static DelegateBridge __Hotfix_remove_EventOnCollectionEventAdd;
    private static DelegateBridge __Hotfix_add_EventOnCollectionEventDelete;
    private static DelegateBridge __Hotfix_remove_EventOnCollectionEventDelete;
    private static DelegateBridge __Hotfix_add_EventOnCollectionEventHandleAck;
    private static DelegateBridge __Hotfix_remove_EventOnCollectionEventHandleAck;
    private static DelegateBridge __Hotfix_add_EventOnCollectionEventAttackAck;
    private static DelegateBridge __Hotfix_remove_EventOnCollectionEventAttackAck;
    private static DelegateBridge __Hotfix_add_EventOnCollectionEventBattleFinishedAck;
    private static DelegateBridge __Hotfix_remove_EventOnCollectionEventBattleFinishedAck;
    private static DelegateBridge __Hotfix_SendGetHeroCommentReq;
    private static DelegateBridge __Hotfix_SendCommentHeroReq;
    private static DelegateBridge __Hotfix_SendPraiseHeroCommentEntryReq;
    private static DelegateBridge __Hotfix_SendViewCommenterHeroReq;
    private static DelegateBridge __Hotfix_OnMessage_253;
    private static DelegateBridge __Hotfix_OnMessage_251;
    private static DelegateBridge __Hotfix_OnMessage_252;
    private static DelegateBridge __Hotfix_OnMessage_115;
    private static DelegateBridge __Hotfix_add_EventOnHeroCommentGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroCommentGetAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroCommentSendAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroCommentSendAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroCommentPraisedAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroCommentPraisedAck;
    private static DelegateBridge __Hotfix_add_EventOnCommenterHeroViewAck;
    private static DelegateBridge __Hotfix_remove_EventOnCommenterHeroViewAck;
    private static DelegateBridge __Hotfix_GetHeroComments;
    private static DelegateBridge __Hotfix_CanPraiseHeroCommentEntry;
    private static DelegateBridge __Hotfix_OnMessage_130;
    private static DelegateBridge __Hotfix_IsCooperateBattleOpened;
    private static DelegateBridge __Hotfix_IsCooperateBattleDisplayable;
    private static DelegateBridge __Hotfix_GetCooperateBattleDailyRewardRestCount;
    private static DelegateBridge __Hotfix_GetCooperateBattleDailyRewardCountMax;
    private static DelegateBridge __Hotfix_GetFirstOpenedCooperateBattle;
    private static DelegateBridge __Hotfix_IsCooperateBattleLevelOpened;
    private static DelegateBridge __Hotfix_CanAttackCooperateBattleLevel;
    private static DelegateBridge __Hotfix_IsCooperateBattleLevelFinished;
    private static DelegateBridge __Hotfix_GetCooperateBattleMaxFinishedLevelId;
    private static DelegateBridge __Hotfix_GetCooperateBattleMaxOpenedLevelId;
    private static DelegateBridge __Hotfix_SendLevelDanmakuGetReq;
    private static DelegateBridge __Hotfix_SendLevelDanmakuPostReq;
    private static DelegateBridge __Hotfix_OnMessage_283;
    private static DelegateBridge __Hotfix_OnMessage_284;
    private static DelegateBridge __Hotfix_GetLevelDanmaku;
    private static DelegateBridge __Hotfix_GetPostedLevelDanmaku;
    private static DelegateBridge __Hotfix_PostLevelDanmaku;
    private static DelegateBridge __Hotfix_CanGetLevelDanmaku;
    private static DelegateBridge __Hotfix_CanPostLevelDanmaku;
    private static DelegateBridge __Hotfix_ClearLevelDanmaku;
    private static DelegateBridge __Hotfix_CanUseDanmaku;
    private static DelegateBridge __Hotfix_add_EventOnGetLevelDanmakuAck;
    private static DelegateBridge __Hotfix_remove_EventOnGetLevelDanmakuAck;
    private static DelegateBridge __Hotfix_add_EventOnSendLevelDanmakuAck;
    private static DelegateBridge __Hotfix_remove_EventOnSendLevelDanmakuAck;
    private static DelegateBridge __Hotfix_SendEternalShrineLevelAttackReq;
    private static DelegateBridge __Hotfix_SendEternalShrineLevelBattleFinishedReq;
    private static DelegateBridge __Hotfix_OnMessage_131;
    private static DelegateBridge __Hotfix_OnMessage_175;
    private static DelegateBridge __Hotfix_OnMessage_176;
    private static DelegateBridge __Hotfix_IsEternalShrineOpened;
    private static DelegateBridge __Hotfix_IsEternalShrineLevelOpened;
    private static DelegateBridge __Hotfix_CanAttackEternalShrineLevel;
    private static DelegateBridge __Hotfix_IsEternalShrineLevelFinished;
    private static DelegateBridge __Hotfix_GetEternalShrineMaxFinishedLevelId;
    private static DelegateBridge __Hotfix_GetEternalShrineDailyChallengeRestCount;
    private static DelegateBridge __Hotfix_GetEternalShrineDailyChallengeCountMax;
    private static DelegateBridge __Hotfix_IsEternalShrineLevelBlessing;
    private static DelegateBridge __Hotfix_add_EventOnEternalShrineLevelAttackAck;
    private static DelegateBridge __Hotfix_remove_EventOnEternalShrineLevelAttackAck;
    private static DelegateBridge __Hotfix_add_EventOnEternalShrineLevelBattleFinishedAck;
    private static DelegateBridge __Hotfix_remove_EventOnEternalShrineLevelBattleFinishedAck;
    private static DelegateBridge __Hotfix_SendFixedStoreBuyStoreItemReq;
    private static DelegateBridge __Hotfix_OnMessage_132;
    private static DelegateBridge __Hotfix_OnMessage_182;
    private static DelegateBridge __Hotfix_GetSkinTicket;
    private static DelegateBridge __Hotfix_GetMemoryEssence;
    private static DelegateBridge __Hotfix_FindFixedStoreByID;
    private static DelegateBridge __Hotfix_GetFixedStoreItemList;
    private static DelegateBridge __Hotfix_CanBuyFixedStoreGoods;
    private static DelegateBridge __Hotfix_IsOnDiscount;
    private static DelegateBridge __Hotfix_add_EventOnFixedStoreBuyStoreItemAck;
    private static DelegateBridge __Hotfix_remove_EventOnFixedStoreBuyStoreItemAck;
    private static DelegateBridge __Hotfix_SendHeroicMomentBattleReportRemoveReq;
    private static DelegateBridge __Hotfix_SendHeroicMomentBattleReportAddReq;
    private static DelegateBridge __Hotfix_SendHeroRandomActionSetReq;
    private static DelegateBridge __Hotfix_SendLikesSendReq;
    private static DelegateBridge __Hotfix_SendBusinessCardDescUpdateReq;
    private static DelegateBridge __Hotfix_SendBusinessCardHeroSetUpdateReq;
    private static DelegateBridge __Hotfix_SendBusinessCardSelectReq;
    private static DelegateBridge __Hotfix_SendGetSocialRelationReq;
    private static DelegateBridge __Hotfix_SendFriendInivteReq;
    private static DelegateBridge __Hotfix_SendFriendAcceptReq;
    private static DelegateBridge __Hotfix_SendFriendDeclineReq;
    private static DelegateBridge __Hotfix_SendFriendDeleteReq;
    private static DelegateBridge __Hotfix_SendPlayerBlockReq;
    private static DelegateBridge __Hotfix_SendPlayerUnblockReq;
    private static DelegateBridge __Hotfix_SendPlayerFindReq;
    private static DelegateBridge __Hotfix_SendSuggestedUserReq;
    private static DelegateBridge __Hotfix_SendBattlePracticeInviteReq;
    private static DelegateBridge __Hotfix_SendBattlePracticeAcceptReq;
    private static DelegateBridge __Hotfix_SendBattlePracticeDeclineReq;
    private static DelegateBridge __Hotfix_SendBattlePracticeCancelReq;
    private static DelegateBridge __Hotfix_SendFriendshipPointsSendReq;
    private static DelegateBridge __Hotfix_SendFriendshipPointsClaimReq;
    private static DelegateBridge __Hotfix_OnMessage_265;
    private static DelegateBridge __Hotfix_OnMessage_266;
    private static DelegateBridge __Hotfix_OnMessage_276;
    private static DelegateBridge __Hotfix_OnMessage_289;
    private static DelegateBridge __Hotfix_OnMessage_80;
    private static DelegateBridge __Hotfix_OnMessage_78;
    private static DelegateBridge __Hotfix_OnMessage_79;
    private static DelegateBridge __Hotfix_UpdateMineBusinessCard;
    private static DelegateBridge __Hotfix_OnMessage_133;
    private static DelegateBridge __Hotfix_OnMessage_188;
    private static DelegateBridge __Hotfix_OnMessage_197;
    private static DelegateBridge __Hotfix_OnMessage_191;
    private static DelegateBridge __Hotfix_OnMessage_194;
    private static DelegateBridge __Hotfix_OnMessage_189;
    private static DelegateBridge __Hotfix_OnMessage_190;
    private static DelegateBridge __Hotfix_OnMessage_192;
    private static DelegateBridge __Hotfix_OnMessage_193;
    private static DelegateBridge __Hotfix_OnMessage_185;
    private static DelegateBridge __Hotfix_OnMessage_184;
    private static DelegateBridge __Hotfix_OnMessage_200;
    private static DelegateBridge __Hotfix_OnMessage_186;
    private static DelegateBridge __Hotfix_OnMessage_198;
    private static DelegateBridge __Hotfix_OnMessage_199;
    private static DelegateBridge __Hotfix_OnMessage_39;
    private static DelegateBridge __Hotfix_OnMessage_34;
    private static DelegateBridge __Hotfix_OnMessage_36;
    private static DelegateBridge __Hotfix_OnMessage_35;
    private static DelegateBridge __Hotfix_OnMessage_40;
    private static DelegateBridge __Hotfix_OnMessage_38;
    private static DelegateBridge __Hotfix_OnMessage_37;
    private static DelegateBridge __Hotfix_OnMessage_196;
    private static DelegateBridge __Hotfix_OnMessage_195;
    private static DelegateBridge __Hotfix_HasBattleReportInHeroMoment;
    private static DelegateBridge __Hotfix_GetHeroMomentBattleReportIdList;
    private static DelegateBridge __Hotfix_CanSendLikes;
    private static DelegateBridge __Hotfix_CanSendFriendshipPoint;
    private static DelegateBridge __Hotfix_CanGetFriendshipPoint;
    private static DelegateBridge __Hotfix_HasSentFriendShipPoints;
    private static DelegateBridge __Hotfix_HasReceivedFriendShipPoints;
    private static DelegateBridge __Hotfix_GetSendFriendShipPointsCountToday;
    private static DelegateBridge __Hotfix_GetClaimedFriendShipPointsToday;
    private static DelegateBridge __Hotfix_CanUpdateBusinessCardDesc;
    private static DelegateBridge __Hotfix_CanUpdateBusinessCardHeroSets;
    private static DelegateBridge __Hotfix_GetFriendList;
    private static DelegateBridge __Hotfix_GetAcrossServerFriendList;
    private static DelegateBridge __Hotfix_GetRecentChatPlayerList;
    private static DelegateBridge __Hotfix_GetRecentTeamBattlePlayerList;
    private static DelegateBridge __Hotfix_GetGuildPlayerList;
    private static DelegateBridge __Hotfix_IsAcrossServerFriend;
    private static DelegateBridge __Hotfix_GetBlackList;
    private static DelegateBridge __Hotfix_GetFriendInvitedList;
    private static DelegateBridge __Hotfix_GetFriendInviteList;
    private static DelegateBridge __Hotfix_GetRecommendFriendList;
    private static DelegateBridge __Hotfix_GetFindFriendList;
    private static DelegateBridge __Hotfix_SortUserSummaryList;
    private static DelegateBridge __Hotfix_IsFriend;
    private static DelegateBridge __Hotfix_GetFriendShipPoints;
    private static DelegateBridge __Hotfix_IsFriendRedMarkShow;
    private static DelegateBridge __Hotfix_GetPVPInviteInfos;
    private static DelegateBridge __Hotfix_RemovePVPInviteInfo;
    private static DelegateBridge __Hotfix_IsNeedGetFriendSocialRelation;
    private static DelegateBridge __Hotfix_SetNeedGetFriendSocialRelation;
    private static DelegateBridge __Hotfix_get_BusinessCard;
    private static DelegateBridge __Hotfix_set_BusinessCard;
    private static DelegateBridge __Hotfix_add_EventOnBusinessCardRandomShowChangeAck;
    private static DelegateBridge __Hotfix_remove_EventOnBusinessCardRandomShowChangeAck;
    private static DelegateBridge __Hotfix_add_EventOnLikeAck;
    private static DelegateBridge __Hotfix_remove_EventOnLikeAck;
    private static DelegateBridge __Hotfix_add_EventOnBusinessCardGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnBusinessCardGetAck;
    private static DelegateBridge __Hotfix_add_EventOnBusinessCardDescUpdateAck;
    private static DelegateBridge __Hotfix_remove_EventOnBusinessCardDescUpdateAck;
    private static DelegateBridge __Hotfix_add_EventOnBusinessCardHeroSetUpdateAck;
    private static DelegateBridge __Hotfix_remove_EventOnBusinessCardHeroSetUpdateAck;
    private static DelegateBridge __Hotfix_add_EventOnFriendGetSocialRelationAck;
    private static DelegateBridge __Hotfix_remove_EventOnFriendGetSocialRelationAck;
    private static DelegateBridge __Hotfix_add_EventOnFriendInviteAck;
    private static DelegateBridge __Hotfix_remove_EventOnFriendInviteAck;
    private static DelegateBridge __Hotfix_add_EventOnFriendInviteNtf;
    private static DelegateBridge __Hotfix_remove_EventOnFriendInviteNtf;
    private static DelegateBridge __Hotfix_add_EventOnFriendInviteAcceptAck;
    private static DelegateBridge __Hotfix_remove_EventOnFriendInviteAcceptAck;
    private static DelegateBridge __Hotfix_add_EventOnFriendInviteAcceptNtf;
    private static DelegateBridge __Hotfix_remove_EventOnFriendInviteAcceptNtf;
    private static DelegateBridge __Hotfix_add_EventOnFriendInviteDeclineAck;
    private static DelegateBridge __Hotfix_remove_EventOnFriendInviteDeclineAck;
    private static DelegateBridge __Hotfix_add_EventOnFriendInviteDeclineNtf;
    private static DelegateBridge __Hotfix_remove_EventOnFriendInviteDeclineNtf;
    private static DelegateBridge __Hotfix_add_EventOnFriendDeleteAck;
    private static DelegateBridge __Hotfix_remove_EventOnFriendDeleteAck;
    private static DelegateBridge __Hotfix_add_EventOnPlayerBlockAck;
    private static DelegateBridge __Hotfix_remove_EventOnPlayerBlockAck;
    private static DelegateBridge __Hotfix_add_EventOnPlayerUnblockAck;
    private static DelegateBridge __Hotfix_remove_EventOnPlayerUnblockAck;
    private static DelegateBridge __Hotfix_add_EventOnFriendFindAck;
    private static DelegateBridge __Hotfix_remove_EventOnFriendFindAck;
    private static DelegateBridge __Hotfix_add_EventOnFriendSuggestedAck;
    private static DelegateBridge __Hotfix_remove_EventOnFriendSuggestedAck;
    private static DelegateBridge __Hotfix_add_EventOnFriendSummaryUpdateNtf;
    private static DelegateBridge __Hotfix_remove_EventOnFriendSummaryUpdateNtf;
    private static DelegateBridge __Hotfix_add_EventOnBattlePracticeInviteAck;
    private static DelegateBridge __Hotfix_remove_EventOnBattlePracticeInviteAck;
    private static DelegateBridge __Hotfix_add_EventOnBattlePracticeAcceptAck;
    private static DelegateBridge __Hotfix_remove_EventOnBattlePracticeAcceptAck;
    private static DelegateBridge __Hotfix_add_EventOnBattlePracticeDeclineAck;
    private static DelegateBridge __Hotfix_remove_EventOnBattlePracticeDeclineAck;
    private static DelegateBridge __Hotfix_add_EventOnBattlePracticeCancelAck;
    private static DelegateBridge __Hotfix_remove_EventOnBattlePracticeCancelAck;
    private static DelegateBridge __Hotfix_add_EventOnBattlePracticeInvitedNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBattlePracticeInvitedNtf;
    private static DelegateBridge __Hotfix_add_EventOnBattlePracticeFailNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBattlePracticeFailNtf;
    private static DelegateBridge __Hotfix_add_EventOnBattlePracticeDeclinedNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBattlePracticeDeclinedNtf;
    private static DelegateBridge __Hotfix_add_EventOnFriendshipPointsSendAck;
    private static DelegateBridge __Hotfix_remove_EventOnFriendshipPointsSendAck;
    private static DelegateBridge __Hotfix_add_EventOnFriendshipPointsClaimAck;
    private static DelegateBridge __Hotfix_remove_EventOnFriendshipPointsClaimAck;
    private static DelegateBridge __Hotfix_add_EventOnFriendshipPointsReceivedNtf;
    private static DelegateBridge __Hotfix_remove_EventOnFriendshipPointsReceivedNtf;
    private static DelegateBridge __Hotfix_add_EventOnHeroMomentBattleReportAddAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroMomentBattleReportAddAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroMomentBattleReportRemoveAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroMomentBattleReportRemoveAck;
    private static DelegateBridge __Hotfix_SendNoticeEventGiftReq;
    private static DelegateBridge __Hotfix_SendPullOrderRewardReq;
    private static DelegateBridge __Hotfix_SendGiftStoreCancelBuyReq;
    private static DelegateBridge __Hotfix_SendGiftStoreItemBuyReq;
    private static DelegateBridge __Hotfix_SendGiftStoreAppleSubscribeReq;
    private static DelegateBridge __Hotfix_OnMessage_301;
    private static DelegateBridge __Hotfix_OnMessage_306;
    private static DelegateBridge __Hotfix_OnMessage_347;
    private static DelegateBridge __Hotfix_OnMessage_134;
    private static DelegateBridge __Hotfix_OnMessage_207;
    private static DelegateBridge __Hotfix_OnMessage_206;
    private static DelegateBridge __Hotfix_OnMessage_209;
    private static DelegateBridge __Hotfix_OnMessage_208;
    private static DelegateBridge __Hotfix_OnMessage_205;
    private static DelegateBridge __Hotfix_GetEventGiftStoreItemList;
    private static DelegateBridge __Hotfix_GetEventGiftStoreItem;
    private static DelegateBridge __Hotfix_GetAllNotNoticedEventGifts;
    private static DelegateBridge __Hotfix_GetEventGiftExpiredTime;
    private static DelegateBridge __Hotfix_HasStrongEventGiftBox;
    private static DelegateBridge __Hotfix_EventGiftStoreItemSort;
    private static DelegateBridge __Hotfix_IsEventGiftValid;
    private static DelegateBridge __Hotfix_GetFisrtStrongEventGiftBoxItemInfoGoodsId;
    private static DelegateBridge __Hotfix_GetAllOrderRewards;
    private static DelegateBridge __Hotfix_GetGiftStoreItemList;
    private static DelegateBridge __Hotfix_GetGiftStoreItem;
    private static DelegateBridge __Hotfix_GetGiftStoreBoughtItem;
    private static DelegateBridge __Hotfix_IsGiftStoreItemSellOut;
    private static DelegateBridge __Hotfix_CanBuyGiftStoreGoods;
    private static DelegateBridge __Hotfix_GetMonthCardLeftTime;
    private static DelegateBridge __Hotfix_IsMonthCardValid;
    private static DelegateBridge __Hotfix_GetCachedMonthCardState;
    private static DelegateBridge __Hotfix_SetCachedMonthCardState;
    private static DelegateBridge __Hotfix_GetMonthCardPrefName;
    private static DelegateBridge __Hotfix_add_EventOnGiftStoreItemBuyAck;
    private static DelegateBridge __Hotfix_remove_EventOnGiftStoreItemBuyAck;
    private static DelegateBridge __Hotfix_add_EventOnGiftStoreAppleSubscribeAck;
    private static DelegateBridge __Hotfix_remove_EventOnGiftStoreAppleSubscribeAck;
    private static DelegateBridge __Hotfix_add_EventOnGiftStoreBuyGoodsNtf;
    private static DelegateBridge __Hotfix_remove_EventOnGiftStoreBuyGoodsNtf;
    private static DelegateBridge __Hotfix_add_EventOnGiftStoreOperationalGoodsUpdateNtf;
    private static DelegateBridge __Hotfix_remove_EventOnGiftStoreOperationalGoodsUpdateNtf;
    private static DelegateBridge __Hotfix_add_EventOnGiftStoreItemCancelBuyAck;
    private static DelegateBridge __Hotfix_remove_EventOnGiftStoreItemCancelBuyAck;
    private static DelegateBridge __Hotfix_add_EventOnPullOrderRewardAck;
    private static DelegateBridge __Hotfix_remove_EventOnPullOrderRewardAck;
    private static DelegateBridge __Hotfix_SendGmCommandReq;
    private static DelegateBridge __Hotfix_GenerateAddItemGmCommand;
    private static DelegateBridge __Hotfix_GenerateRemoveItemGmCommand;
    private static DelegateBridge __Hotfix_OnMessage_210;
    private static DelegateBridge __Hotfix_add_EventOnGmCommandAck;
    private static DelegateBridge __Hotfix_remove_EventOnGmCommandAck;
    private static DelegateBridge __Hotfix_GuildCreateReq;
    private static DelegateBridge __Hotfix_GuildJoinApplyReq;
    private static DelegateBridge __Hotfix_GuildJoinAdminConfirmReq;
    private static DelegateBridge __Hotfix_GuildJoinApplyRefuseReq;
    private static DelegateBridge __Hotfix_GuildJoinPlayerConfirmReq;
    private static DelegateBridge __Hotfix_GuildSearchReq;
    private static DelegateBridge __Hotfix_GuildRandomListReq;
    private static DelegateBridge __Hotfix_GuildQuitReq;
    private static DelegateBridge __Hotfix_SendGuildInvitePlayerListReq;
    private static DelegateBridge __Hotfix_SendGuildHiringDeclarationSetReq;
    private static DelegateBridge __Hotfix_SendGuildJoinInvitationGetReq;
    private static DelegateBridge __Hotfix_SendGuildJoinInvitationRefuseReq;
    private static DelegateBridge __Hotfix_SendAllGuildJoinInvitationRefuseReq;
    private static DelegateBridge __Hotfix_SendGuildGetReq;
    private static DelegateBridge __Hotfix_SendGuildBasicSetReq;
    private static DelegateBridge __Hotfix_SendGuildAnnouncementSetReq;
    private static DelegateBridge __Hotfix_SendGuildJoinApplyGetReq;
    private static DelegateBridge __Hotfix_SendGuildNameChangeReq;
    private static DelegateBridge __Hotfix_SendGuildLogGetReq;
    private static DelegateBridge __Hotfix_SendGuildJoinInviteReq;
    private static DelegateBridge __Hotfix_SendGuildVicePresidentAppointReq;
    private static DelegateBridge __Hotfix_SendGuildKickOutReq;
    private static DelegateBridge __Hotfix_SendGuildPresidentRelieveReq;
    private static DelegateBridge __Hotfix_SendGuildPresidentAppointReq;
    private static DelegateBridge __Hotfix_SendAllGuildJoinApplyRefuseReq;
    private static DelegateBridge __Hotfix_OnMessage_243;
    private static DelegateBridge __Hotfix_OnMessage_135;
    private static DelegateBridge __Hotfix_OnMessage_213;
    private static DelegateBridge __Hotfix_OnMessage_217;
    private static DelegateBridge __Hotfix_OnMessage_219;
    private static DelegateBridge __Hotfix_OnMessage_221;
    private static DelegateBridge __Hotfix_OnMessage_222;
    private static DelegateBridge __Hotfix_OnMessage_242;
    private static DelegateBridge __Hotfix_OnMessage_241;
    private static DelegateBridge __Hotfix_OnMessage_240;
    private static DelegateBridge __Hotfix_OnMessage_216;
    private static DelegateBridge __Hotfix_OnMessage_215;
    private static DelegateBridge __Hotfix_OnMessage_223;
    private static DelegateBridge __Hotfix_OnMessage_225;
    private static DelegateBridge __Hotfix_OnMessage_3;
    private static DelegateBridge __Hotfix_OnMessage_214;
    private static DelegateBridge __Hotfix_OnMessage_212;
    private static DelegateBridge __Hotfix_OnMessage_211;
    private static DelegateBridge __Hotfix_OnMessage_220;
    private static DelegateBridge __Hotfix_OnMessage_237;
    private static DelegateBridge __Hotfix_OnMessage_228;
    private static DelegateBridge __Hotfix_OnMessage_226;
    private static DelegateBridge __Hotfix_OnMessage_244;
    private static DelegateBridge __Hotfix_OnMessage_227;
    private static DelegateBridge __Hotfix_OnMessage_239;
    private static DelegateBridge __Hotfix_OnMessage_238;
    private static DelegateBridge __Hotfix_OnMessage_2;
    private static DelegateBridge __Hotfix_OnMessage_218;
    private static DelegateBridge __Hotfix_OnMessage_224;
    private static DelegateBridge __Hotfix_CheckGuildCreateCondition;
    private static DelegateBridge __Hotfix_GetGuildInfo;
    private static DelegateBridge __Hotfix_RefreshGuildListJoinState;
    private static DelegateBridge __Hotfix_CanQuitGuild;
    private static DelegateBridge __Hotfix_CanKickOutGuild;
    private static DelegateBridge __Hotfix_CheckGuildName;
    private static DelegateBridge __Hotfix_CanSetGuildHiringDeclaration;
    private static DelegateBridge __Hotfix_CanSetGuildAnnouncement;
    private static DelegateBridge __Hotfix_CheckGuildInvitePlayerList;
    private static DelegateBridge __Hotfix_GetGuildJoinInvitationList;
    private static DelegateBridge __Hotfix_GetGuildJoinApplyList;
    private static DelegateBridge __Hotfix_RemoveGuildJoinApplyListById;
    private static DelegateBridge __Hotfix_CheckGuildSearch;
    private static DelegateBridge __Hotfix_CheckGuildRandomList;
    private static DelegateBridge __Hotfix_GetGuildRecommendList;
    private static DelegateBridge __Hotfix_GetGuildId;
    private static DelegateBridge __Hotfix_GetGuildLogContent;
    private static DelegateBridge __Hotfix_AddUserIdToGuildInviteList;
    private static DelegateBridge __Hotfix_CanJoinGuildCDTime;
    private static DelegateBridge __Hotfix_HasPlayerBeenInvitedByGuild;
    private static DelegateBridge __Hotfix_ResetComponentGuildData;
    private static DelegateBridge __Hotfix_add_EventOnGuildCreateAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildCreateAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildJoinApplyAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildJoinApplyAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildJoinAdminConfirmAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildJoinAdminConfirmAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildJoinApplyRefuseAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildJoinApplyRefuseAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildJoinPlayerConfirmAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildJoinPlayerConfirmAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildSearchAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildSearchAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildRandomListAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildRandomListAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildQuitAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildQuitAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildInvitePlayerListAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildInvitePlayerListAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildHiringDeclarationSetAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildHiringDeclarationSetAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildJoinInvitationGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildJoinInvitationGetAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildJoinInvitationRefuseAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildJoinInvitationRefuseAck;
    private static DelegateBridge __Hotfix_add_EventOnAllGuildJoinInvitationRefuseAck;
    private static DelegateBridge __Hotfix_remove_EventOnAllGuildJoinInvitationRefuseAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildGetAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildBasicSetAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildBasicSetAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildAnnouncementSetAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildAnnouncementSetAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildJoinApplyGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildJoinApplyGetAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildNameChangeAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildNameChangeAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildLogGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildLogGetAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildJoinInviteAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildJoinInviteAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildVicePresidentAppointAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildVicePresidentAppointAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildKickOutAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildKickOutAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildPresidentRelieveAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildPresidentRelieveAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildPresidentAppointAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildPresidentAppointAck;
    private static DelegateBridge __Hotfix_add_EventOnAllGuildJoinApplyRefuseAck;
    private static DelegateBridge __Hotfix_remove_EventOnAllGuildJoinApplyRefuseAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildUpdateInfo;
    private static DelegateBridge __Hotfix_remove_EventOnGuildUpdateInfo;
    private static DelegateBridge __Hotfix_SendGuildMassiveCombatDataReq;
    private static DelegateBridge __Hotfix_SendGuildMassiveCombatStartReq;
    private static DelegateBridge __Hotfix_SendGuildMassiveCombatSurrenderReq;
    private static DelegateBridge __Hotfix_SendGuildMassiveCombatConquerReq;
    private static DelegateBridge __Hotfix_SendGuildMassiveCombatAttackReq;
    private static DelegateBridge __Hotfix_SendGuildMassiveCombatAttackFinishedReq;
    private static DelegateBridge __Hotfix_OnMessage_232;
    private static DelegateBridge __Hotfix_OnMessage_233;
    private static DelegateBridge __Hotfix_OnMessage_234;
    private static DelegateBridge __Hotfix_OnMessage_235;
    private static DelegateBridge __Hotfix_OnMessage_236;
    private static DelegateBridge __Hotfix_OnMessage_231;
    private static DelegateBridge __Hotfix_OnMessage_229;
    private static DelegateBridge __Hotfix_OnMessage_230;
    private static DelegateBridge __Hotfix_GetGuildMassiveCombatStrongholdEliminateRate;
    private static DelegateBridge __Hotfix_IsGuildMassiveCombatStrongholdTakeOver;
    private static DelegateBridge __Hotfix_GetStartedGuildMassiveCombatThisWeek;
    private static DelegateBridge __Hotfix_CanStartGuildMassiveCombat;
    private static DelegateBridge __Hotfix_CanAttackGuildMassiveCombatSinglePVELevel;
    private static DelegateBridge __Hotfix_GetGuildPlayerMassiveCombatInfo;
    private static DelegateBridge __Hotfix_GetUIntMassiveCombatPoint;
    private static DelegateBridge __Hotfix_IsShowGuildMassiveCombatFirstOpenRedMark;
    private static DelegateBridge __Hotfix_FillGuildMassiveCombatMemberSummary;
    private static DelegateBridge __Hotfix_OnFillGuildMassiveCombatNtfSummary;
    private static DelegateBridge __Hotfix_OnFillGuildMassiveGetDataSummary;
    private static DelegateBridge __Hotfix_GetGuildMassiveCombatInfo;
    private static DelegateBridge __Hotfix_GetGuildMassiveCombatGeneral;
    private static DelegateBridge __Hotfix_GetGuildMassiveCombatEliminateRate;
    private static DelegateBridge __Hotfix_GetGuildMassiveCombatStronghold;
    private static DelegateBridge __Hotfix_IsGuildMassiveCombatConquered;
    private static DelegateBridge __Hotfix_IsGuildMassiveCombatFinished;
    private static DelegateBridge __Hotfix_IsGuildMassiveCombatConqueredAndFinished;
    private static DelegateBridge __Hotfix_IsGuildMassiveCombatConqueredAndNotFinished;
    private static DelegateBridge __Hotfix_IsGuildMassiveCombatSurrender;
    private static DelegateBridge __Hotfix_add_EventOnGuildMassiveCombatAttackAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildMassiveCombatAttackAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildMassiveCombatAttackFinishedAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildMassiveCombatAttackFinishedAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildMassiveCombatDataAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildMassiveCombatDataAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildMassiveCombatNtf;
    private static DelegateBridge __Hotfix_remove_EventOnGuildMassiveCombatNtf;
    private static DelegateBridge __Hotfix_add_EventOnGuildMassiveCombatPlayerNtf;
    private static DelegateBridge __Hotfix_remove_EventOnGuildMassiveCombatPlayerNtf;
    private static DelegateBridge __Hotfix_add_EventOnGuildMassiveCombatStartAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildMassiveCombatStartAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildMassiveCombatSurrenderAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildMassiveCombatSurrenderAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildMassiveCombatConquerAck;
    private static DelegateBridge __Hotfix_remove_EventOnGuildMassiveCombatConquerAck;
    private static DelegateBridge __Hotfix_add_EventOnGuildMassiveCombatStartNtf;
    private static DelegateBridge __Hotfix_remove_EventOnGuildMassiveCombatStartNtf;
    private static DelegateBridge __Hotfix_add_EventOnGuildMassiveCombatSurrenderNtf;
    private static DelegateBridge __Hotfix_remove_EventOnGuildMassiveCombatSurrenderNtf;
    private static DelegateBridge __Hotfix_add_EventOnGuildMassiveCombatConqueredNtf;
    private static DelegateBridge __Hotfix_remove_EventOnGuildMassiveCombatConqueredNtf;
    private static DelegateBridge __Hotfix_SendOpenHeroJobRefineryReq;
    private static DelegateBridge __Hotfix_SendHeroJobRefineReq;
    private static DelegateBridge __Hotfix_SendHeroJobRefineSaveReq;
    private static DelegateBridge __Hotfix_SendLevelUpHeroHeartFetterLevelReq;
    private static DelegateBridge __Hotfix_SendUnlockHeroHeartFetterReq;
    private static DelegateBridge __Hotfix_SendAutoTakeOffEquipmentsReq;
    private static DelegateBridge __Hotfix_SendExchangeHeroFragmentReq;
    private static DelegateBridge __Hotfix_SendHeroFavorabilityExpAddReq;
    private static DelegateBridge __Hotfix_SendHeroInteractReq;
    private static DelegateBridge __Hotfix_SendHeroConfessReq;
    private static DelegateBridge __Hotfix_SendSetProtagonistReq;
    private static DelegateBridge __Hotfix_SendHeroJobTransferReq;
    private static DelegateBridge __Hotfix_SendHeroStarLevelUpReq;
    private static DelegateBridge __Hotfix_SendHeroJobLevelUpReq;
    private static DelegateBridge __Hotfix_SendHeroSkillsSelectReq;
    private static DelegateBridge __Hotfix_SendHeroSoldierSelectReq;
    private static DelegateBridge __Hotfix_SendHeroComposeReqq;
    private static DelegateBridge __Hotfix_SendHeroExpAddReq;
    private static DelegateBridge __Hotfix_SendHeroJobUnlockReq;
    private static DelegateBridge __Hotfix_SendEquipmentWearReq;
    private static DelegateBridge __Hotfix_SendEquipmentTakeOffReq;
    private static DelegateBridge __Hotfix_SendAutoEquipReq;
    private static DelegateBridge __Hotfix_SendHeroFetterUnlockReq;
    private static DelegateBridge __Hotfix_SendHeroFetterLevelUp;
    private static DelegateBridge __Hotfix_SendCharSkinWear;
    private static DelegateBridge __Hotfix_SendCharSkinTakeOff;
    private static DelegateBridge __Hotfix_SendModelSkinWear;
    private static DelegateBridge __Hotfix_SendModelSkinTakeOff;
    private static DelegateBridge __Hotfix_SendSoldierSkinWearReq;
    private static DelegateBridge __Hotfix_SendSoldierSkinTakeOffReq;
    private static DelegateBridge __Hotfix_OnMessage_308;
    private static DelegateBridge __Hotfix_OnMessage_269;
    private static DelegateBridge __Hotfix_OnMessage_270;
    private static DelegateBridge __Hotfix_OnMessage_411;
    private static DelegateBridge __Hotfix_OnMessage_286;
    private static DelegateBridge __Hotfix_OnMessage_27;
    private static DelegateBridge __Hotfix_OnMessage_179;
    private static DelegateBridge __Hotfix_OnMessage_139;
    private static DelegateBridge __Hotfix_OnMessage_255;
    private static DelegateBridge __Hotfix_OnMessage_267;
    private static DelegateBridge __Hotfix_OnMessage_262;
    private static DelegateBridge __Hotfix_OnMessage_374;
    private static DelegateBridge __Hotfix_OnMessage_271;
    private static DelegateBridge __Hotfix_OnMessage_279;
    private static DelegateBridge __Hotfix_OnMessage_268;
    private static DelegateBridge __Hotfix_OnMessage_277;
    private static DelegateBridge __Hotfix_OnMessage_278;
    private static DelegateBridge __Hotfix_OnMessage_254;
    private static DelegateBridge __Hotfix_OnMessage_261;
    private static DelegateBridge __Hotfix_OnMessage_272;
    private static DelegateBridge __Hotfix_OnMessage_174;
    private static DelegateBridge __Hotfix_OnMessage_173;
    private static DelegateBridge __Hotfix_OnMessage_167;
    private static DelegateBridge __Hotfix_OnMessage_263;
    private static DelegateBridge __Hotfix_OnMessage_264;
    private static DelegateBridge __Hotfix_OnMessage_83;
    private static DelegateBridge __Hotfix_OnMessage_82;
    private static DelegateBridge __Hotfix_OnMessage_298;
    private static DelegateBridge __Hotfix_OnMessage_297;
    private static DelegateBridge __Hotfix_OnMessage_377;
    private static DelegateBridge __Hotfix_OnMessage_376;
    private static DelegateBridge __Hotfix_GmResetHeroJob;
    private static DelegateBridge __Hotfix_GmLevelUpHeroFetter2SpecificLevel;
    private static DelegateBridge __Hotfix_GmLevelUpHeroHeartFetter;
    private static DelegateBridge __Hotfix_GetAdditiveHeroAddExp;
    private static DelegateBridge __Hotfix_GetAdditiveHeroFavourabilityAddExp;
    private static DelegateBridge __Hotfix_GetHeroFavorabilityUpLevel;
    private static DelegateBridge __Hotfix_GetHeroInteractionInfo;
    private static DelegateBridge __Hotfix_GetHeroInteractHeroPerformanceId;
    private static DelegateBridge __Hotfix_InitAllUseableDefaultConfigHeros;
    private static DelegateBridge __Hotfix_CreateDefaultHero;
    private static DelegateBridge __Hotfix_GetHero;
    private static DelegateBridge __Hotfix_GetHeros;
    private static DelegateBridge __Hotfix_GetAllUseableDefaultHeros;
    private static DelegateBridge __Hotfix_GetUseableJobConnectionInfos;
    private static DelegateBridge __Hotfix_GetHeroNextLevelExp;
    private static DelegateBridge __Hotfix_GetHeroSkillPointMax;
    private static DelegateBridge __Hotfix_GetHeroLevelMax;
    private static DelegateBridge __Hotfix_IsHeroShowRedMark;
    private static DelegateBridge __Hotfix_CanLevelUpHeroStarLevel;
    private static DelegateBridge __Hotfix_IfWasteAddExp;
    private static DelegateBridge __Hotfix_CanLevelUpHeroJobLevel;
    private static DelegateBridge __Hotfix_CanHeroJobTransfer;
    private static DelegateBridge __Hotfix_IsWorldHeroTabShowRedIcon;
    private static DelegateBridge __Hotfix_IsHeroHaveNewJobCanTransfer;
    private static DelegateBridge __Hotfix_IsHeroJobNeedMagicStone;
    private static DelegateBridge __Hotfix_CanComposeHero;
    private static DelegateBridge __Hotfix_IsSkillLimitToHeroJob;
    private static DelegateBridge __Hotfix_GetProtagonistId;
    private static DelegateBridge __Hotfix_IsProtagonistHero;
    private static DelegateBridge __Hotfix_IsProtagonistExist;
    private static DelegateBridge __Hotfix_IsCurrentLevelMaxHeroLevel;
    private static DelegateBridge __Hotfix_CanHeroSelectSolider;
    private static DelegateBridge __Hotfix_GetHeroInteractNums;
    private static DelegateBridge __Hotfix_GetHeroInteractNumsFlushTime;
    private static DelegateBridge __Hotfix_IsHeroCanComposed;
    private static DelegateBridge __Hotfix_IsExistSoliderId;
    private static DelegateBridge __Hotfix_IsHeroAssigned;
    private static DelegateBridge __Hotfix_GetAllStarLvlMaxHeroFragements;
    private static DelegateBridge __Hotfix_IsShowJobLevelCanUpTip;
    private static DelegateBridge __Hotfix_CanSelectSkillHero;
    private static DelegateBridge __Hotfix_HasIgnoreCostEquipment;
    private static DelegateBridge __Hotfix_IsEquipmentWeared;
    private static DelegateBridge __Hotfix_GetWearedEquipmentHeroIdByEquipmentId;
    private static DelegateBridge __Hotfix_CanWearEquipment;
    private static DelegateBridge __Hotfix_HasBetterEquipmentByHero;
    private static DelegateBridge __Hotfix_HasBetterEquipmentBySlotId;
    private static DelegateBridge __Hotfix_GetBestEquipments;
    private static DelegateBridge __Hotfix_ComputeBattlePower_3;
    private static DelegateBridge __Hotfix_ComputeBattlePower_2;
    private static DelegateBridge __Hotfix_CanAutoEquipment;
    private static DelegateBridge __Hotfix_CanTakeOffEquipment;
    private static DelegateBridge __Hotfix_CanUnlockHeroHeartFetter;
    private static DelegateBridge __Hotfix_CanLevelUpHeroHeartFetter;
    private static DelegateBridge __Hotfix_CanUnlockHeroPerformance;
    private static DelegateBridge __Hotfix_CanUnlockHeroBiography;
    private static DelegateBridge __Hotfix_CanReachFetterUnlockCondition;
    private static DelegateBridge __Hotfix_CanInteractHero;
    private static DelegateBridge __Hotfix_CanConfessHero;
    private static DelegateBridge __Hotfix_CanLevelUpHeroFetter;
    private static DelegateBridge __Hotfix_CanUnlockHeroFetter;
    private static DelegateBridge __Hotfix_ImitateUseHeroFavorabilityExpItem;
    private static DelegateBridge __Hotfix_IsFullFavorabilityExp;
    private static DelegateBridge __Hotfix_IsShowFetterRedMark;
    private static DelegateBridge __Hotfix_IsHeroFetterHasNewOrLevelUp;
    private static DelegateBridge __Hotfix_CanWearCharSkin;
    private static DelegateBridge __Hotfix_CanTakeOffCharSkin;
    private static DelegateBridge __Hotfix_CanWearModelSkin;
    private static DelegateBridge __Hotfix_CanTakeOffModelSkin;
    private static DelegateBridge __Hotfix_CanWearSoldierSkin;
    private static DelegateBridge __Hotfix_CanTakeOffSoldierSkin;
    private static DelegateBridge __Hotfix_IsShowSkinBeforeOnSale;
    private static DelegateBridge __Hotfix_CanOpenHeroJobRefineryReq;
    private static DelegateBridge __Hotfix_IsJobLevelMax;
    private static DelegateBridge __Hotfix_get_IsHideEquipMaster;
    private static DelegateBridge __Hotfix_get_DonotShowEquipMasterCancelConfirmPanelOnCloseThisLogin;
    private static DelegateBridge __Hotfix_set_DonotShowEquipMasterCancelConfirmPanelOnCloseThisLogin;
    private static DelegateBridge __Hotfix_get_DonotShowEquipMasterCancelConfirmPanelOnClickStoneThisLogin;
    private static DelegateBridge __Hotfix_set_DonotShowEquipMasterCancelConfirmPanelOnClickStoneThisLogin;
    private static DelegateBridge __Hotfix_get_DonotShowEquipMasterSaveConfirmPanelThisLogin;
    private static DelegateBridge __Hotfix_set_DonotShowEquipMasterSaveConfirmPanelThisLogin;
    private static DelegateBridge __Hotfix_add_EventOnSetProtagonistAck;
    private static DelegateBridge __Hotfix_remove_EventOnSetProtagonistAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroJobTransferAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroJobTransferAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroStarLevelUpAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroStarLevelUpAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroJobLevelUpAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroJobLevelUpAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroSkillsSelectAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroSkillsSelectAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroSoldierSelectAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroSoldierSelectAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroComposeAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroComposeAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroExpAddAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroExpAddAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroJobUnlockAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroJobUnlockAck;
    private static DelegateBridge __Hotfix_add_EventOnEquipmentWearAck;
    private static DelegateBridge __Hotfix_remove_EventOnEquipmentWearAck;
    private static DelegateBridge __Hotfix_add_EventOnEquipmentTakeOffAck;
    private static DelegateBridge __Hotfix_remove_EventOnEquipmentTakeOffAck;
    private static DelegateBridge __Hotfix_add_EventOnAutoEquipAck;
    private static DelegateBridge __Hotfix_remove_EventOnAutoEquipAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroFetterLevelUpAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroFetterLevelUpAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroConfessAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroConfessAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroFavorabilityExpAddAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroFavorabilityExpAddAck;
    private static DelegateBridge __Hotfix_add_EventOnFetterUnlockAck;
    private static DelegateBridge __Hotfix_remove_EventOnFetterUnlockAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroInteractAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroInteractAck;
    private static DelegateBridge __Hotfix_add_EventOnCharSkinWearAck;
    private static DelegateBridge __Hotfix_remove_EventOnCharSkinWearAck;
    private static DelegateBridge __Hotfix_add_EventOnCharSkinTakeOffAck;
    private static DelegateBridge __Hotfix_remove_EventOnCharSkinTakeOffAck;
    private static DelegateBridge __Hotfix_add_EventOnModelSkinWearAck;
    private static DelegateBridge __Hotfix_remove_EventOnModelSkinWearAck;
    private static DelegateBridge __Hotfix_add_EventOnModelSkinTakeOffAck;
    private static DelegateBridge __Hotfix_remove_EventOnModelSkinTakeOffAck;
    private static DelegateBridge __Hotfix_add_EventOnSoldierSkinWearAck;
    private static DelegateBridge __Hotfix_remove_EventOnSoldierSkinWearAck;
    private static DelegateBridge __Hotfix_add_EventOnSoldierSkinTakeOffAck;
    private static DelegateBridge __Hotfix_remove_EventOnSoldierSkinTakeOffAck;
    private static DelegateBridge __Hotfix_add_EventOnExchangeHeroFragementAck;
    private static DelegateBridge __Hotfix_remove_EventOnExchangeHeroFragementAck;
    private static DelegateBridge __Hotfix_add_EventOnAutoTakeOffEquipmentsAck;
    private static DelegateBridge __Hotfix_remove_EventOnAutoTakeOffEquipmentsAck;
    private static DelegateBridge __Hotfix_add_EventOnUnlockHeroHeartFetterAck;
    private static DelegateBridge __Hotfix_remove_EventOnUnlockHeroHeartFetterAck;
    private static DelegateBridge __Hotfix_add_EventOnLevelUpHeroHeartFetterLevelAck;
    private static DelegateBridge __Hotfix_remove_EventOnLevelUpHeroHeartFetterLevelAck;
    private static DelegateBridge __Hotfix_add_EventOnOpenHeroJobRefineryAck;
    private static DelegateBridge __Hotfix_remove_EventOnOpenHeroJobRefineryAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroJobRefineAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroJobRefineAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroJobRefineSaveAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroJobRefineSaveAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroChangeNtf;
    private static DelegateBridge __Hotfix_remove_EventOnHeroChangeNtf;
    private static DelegateBridge __Hotfix_SendHeroAnthemLevelAttackReq;
    private static DelegateBridge __Hotfix_SendHeroAnthemLevelBattleFinishedReq;
    private static DelegateBridge __Hotfix_OnMessage_136;
    private static DelegateBridge __Hotfix_OnMessage_246;
    private static DelegateBridge __Hotfix_OnMessage_247;
    private static DelegateBridge __Hotfix_CanAttackHeroAnthemLevel;
    private static DelegateBridge __Hotfix_IsHeroAnthemLevelFinished;
    private static DelegateBridge __Hotfix_GetHeroAnthemMaxFinishedLevelId;
    private static DelegateBridge __Hotfix_HasHeroAnthemLevelAchievement;
    private static DelegateBridge __Hotfix_GetHeroAnthemLevelAchievementCount;
    private static DelegateBridge __Hotfix_GetHeroAnthemChapterAchievementCount;
    private static DelegateBridge __Hotfix_add_EventOnHeroAnthemLevelAttackAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroAnthemLevelAttackAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroAnthemLevelBattleFinishedAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroAnthemLevelBattleFinishedAck;
    private static DelegateBridge __Hotfix_SendHeroAssistantAssignToTaskReq;
    private static DelegateBridge __Hotfix_SendHeroAssistantCancelTaskReq;
    private static DelegateBridge __Hotfix_SendHeroAssistantClaimRewardReq;
    private static DelegateBridge __Hotfix_OnMessage_137;
    private static DelegateBridge __Hotfix_OnMessage_248;
    private static DelegateBridge __Hotfix_OnMessage_249;
    private static DelegateBridge __Hotfix_OnMessage_250;
    private static DelegateBridge __Hotfix_GetAllTodayHeroAssistantsTask;
    private static DelegateBridge __Hotfix_GetHeroAssistantsTasksByWeekDay;
    private static DelegateBridge __Hotfix_GetAssignedHeroAssistantsTask;
    private static DelegateBridge __Hotfix_HaveFinishedAssistantTask;
    private static DelegateBridge __Hotfix_GetDropIdByTaskCompleteRate;
    private static DelegateBridge __Hotfix_GetDropCountByTaskWorkSeconds;
    private static DelegateBridge __Hotfix_CanAssignHero;
    private static DelegateBridge __Hotfix_CanCancelTask;
    private static DelegateBridge __Hotfix_CanClaimRewards;
    private static DelegateBridge __Hotfix_GetTaskRemainingTime;
    private static DelegateBridge __Hotfix_add_EventOnHeroAssistantAssignToTaskAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroAssistantAssignToTaskAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroAssistantCancelTaskAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroAssistantCancelTaskAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroAssistantClaimRewardAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroAssistantClaimRewardAck;
    private static DelegateBridge __Hotfix_SendHeroDungeonLevelAttackReq;
    private static DelegateBridge __Hotfix_SendHeroDungeonLevelBattleFinishedReq;
    private static DelegateBridge __Hotfix_SendHeroDungeonLevelRaidReq;
    private static DelegateBridge __Hotfix_SendHeroDungeonChapterStarRewardGainReq;
    private static DelegateBridge __Hotfix_OnMessage_138;
    private static DelegateBridge __Hotfix_OnMessage_257;
    private static DelegateBridge __Hotfix_OnMessage_258;
    private static DelegateBridge __Hotfix_OnMessage_259;
    private static DelegateBridge __Hotfix_OnMessage_256;
    private static DelegateBridge __Hotfix_CanUnLockHeroDungeonLevel;
    private static DelegateBridge __Hotfix_IsHeroDungeonLevelAttachUnlockLevel;
    private static DelegateBridge __Hotfix_CanAttackHeroDungeonLevel;
    private static DelegateBridge __Hotfix_GetHeroDungeonLevelChallengeNum;
    private static DelegateBridge __Hotfix_GetHeroDungeonLevelCanChallengeNum;
    private static DelegateBridge __Hotfix_GetHeroDungeonLevelCanChallengeMaxNum;
    private static DelegateBridge __Hotfix_CanRaidHeroDungeonLevel;
    private static DelegateBridge __Hotfix_GetHeroDungeonLevelStars;
    private static DelegateBridge __Hotfix_GetHeroDungeonLevelAchievementCount;
    private static DelegateBridge __Hotfix_HasHeroDungeonLevelAchievement;
    private static DelegateBridge __Hotfix_GetHeroDungeonChapterGainStars;
    private static DelegateBridge __Hotfix_GetHeroDungeonChapterAllStars;
    private static DelegateBridge __Hotfix_GetHeroDungeonStarRewardStatus;
    private static DelegateBridge __Hotfix_CanGainHeroDungeonStarReward;
    private static DelegateBridge __Hotfix_IsLevelChallenged;
    private static DelegateBridge __Hotfix_IsHeroDungeonNewMarkShow;
    private static DelegateBridge __Hotfix_HeroDungeonProgress;
    private static DelegateBridge __Hotfix_GetHeroDungeonDailyChallengeMaxNums;
    private static DelegateBridge __Hotfix_IsHeroDungeonLevelFinihed;
    private static DelegateBridge __Hotfix_GetHeroDungeonMaxFinishedLevelId;
    private static DelegateBridge __Hotfix_add_EventOnHeroDungeonLevelAttackAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroDungeonLevelAttackAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroDungeonLevelRaidAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroDungeonLevelRaidAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroDungeonRewardGainAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroDungeonRewardGainAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroDungeonLevelBattleFinishedAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroDungeonLevelBattleFinishedAck;
    private static DelegateBridge __Hotfix_SendHeroPhantomAttackReq;
    private static DelegateBridge __Hotfix_SendHeroPhantomBattleFinishedReq;
    private static DelegateBridge __Hotfix_OnMessage_140;
    private static DelegateBridge __Hotfix_OnMessage_274;
    private static DelegateBridge __Hotfix_OnMessage_275;
    private static DelegateBridge __Hotfix_IsHeroPhantomDisplay;
    private static DelegateBridge __Hotfix_IsHeroPhantomOpened;
    private static DelegateBridge __Hotfix_IsHeroPhantomLevelOpened;
    private static DelegateBridge __Hotfix_CanAttackHeroPhantomLevel;
    private static DelegateBridge __Hotfix_IsHeroPhantomLevelFinihed;
    private static DelegateBridge __Hotfix_GetHeroPhantomMaxFinishedLevelId;
    private static DelegateBridge __Hotfix_IsCompleteHeroPhantomLevelAchievement;
    private static DelegateBridge __Hotfix_GetHeroPhantomAchievementCount;
    private static DelegateBridge __Hotfix_GetHeroPhantomLevelAchievementCount;
    private static DelegateBridge __Hotfix_IsHeroPhantomLevelFirstCleanComplete;
    private static DelegateBridge __Hotfix_add_EventOnHeroPhantomAttackAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroPhantomAttackAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroPhantomBattleFinishedAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroPhantomBattleFinishedAck;
    private static DelegateBridge __Hotfix_SendHeroTrainningLevelAttackReq;
    private static DelegateBridge __Hotfix_SendHeroTrainningLevelBattleFinishedReq;
    private static DelegateBridge __Hotfix_OnMessage_141;
    private static DelegateBridge __Hotfix_OnMessage_280;
    private static DelegateBridge __Hotfix_OnMessage_281;
    private static DelegateBridge __Hotfix_IsHeroTrainningOpened;
    private static DelegateBridge __Hotfix_IsHeroTrainningLevelOpened;
    private static DelegateBridge __Hotfix_CanAttackHeroTrainningLevel;
    private static DelegateBridge __Hotfix_GetHeroTrainningTicketCount;
    private static DelegateBridge __Hotfix_IsHeroTrainningLevelFinished;
    private static DelegateBridge __Hotfix_GetHeroTrainningMaxFinishedLevelId;
    private static DelegateBridge __Hotfix_GetHeroTrainingDailyRewardRestCount;
    private static DelegateBridge __Hotfix_GetHeroTrainingDailyRewardCountMax;
    private static DelegateBridge __Hotfix_IsHeroTrainingLevelBlessing;
    private static DelegateBridge __Hotfix_add_EventOnHeroTrainningLevelAttackAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroTrainningLevelAttackAck;
    private static DelegateBridge __Hotfix_add_EventOnHeroTrainningLevelBattleFinishedAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeroTrainningLevelBattleFinishedAck;
    private static DelegateBridge __Hotfix_SendLevelWayPointMoveReq;
    private static DelegateBridge __Hotfix_SendLevelScenarioHandleReq;
    private static DelegateBridge __Hotfix_SendLevelWayPointBattleFinishReq;
    private static DelegateBridge __Hotfix_OnMessage_142;
    private static DelegateBridge __Hotfix_OnMessage_288;
    private static DelegateBridge __Hotfix_OnMessage_285;
    private static DelegateBridge __Hotfix_OnMessage_287;
    private static DelegateBridge __Hotfix_OnMessage_349;
    private static DelegateBridge __Hotfix_OnRandomEventUpdate;
    private static DelegateBridge __Hotfix_AppendChangedGoodsToRewards;
    private static DelegateBridge __Hotfix_AppendGoodsToRewards;
    private static DelegateBridge __Hotfix_GetNotBagItemCount;
    private static DelegateBridge __Hotfix_IsRegionOpen;
    private static DelegateBridge __Hotfix_GetCurrentWorldMapInfo;
    private static DelegateBridge __Hotfix_GetPlayerCurrentWaypointInfo;
    private static DelegateBridge __Hotfix_GetWaypointStatus;
    private static DelegateBridge __Hotfix_GetWaypointEvent;
    private static DelegateBridge __Hotfix_CanAttackEventWaypoint;
    private static DelegateBridge __Hotfix_CanAttackScenario;
    private static DelegateBridge __Hotfix_GetLastFinishedScenarioInfo;
    private static DelegateBridge __Hotfix_GetNextScenarioInfo;
    private static DelegateBridge __Hotfix_IsScenarioFinished;
    private static DelegateBridge __Hotfix_CanUnlockScenario;
    private static DelegateBridge __Hotfix_InitActiveScenarioInfo;
    private static DelegateBridge __Hotfix_CheckChangeActiveScenario;
    private static DelegateBridge __Hotfix_GetActiveNextScenarioInfo;
    private static DelegateBridge __Hotfix_GetActiveLastFinsishedScenarioInfo;
    private static DelegateBridge __Hotfix_UpdateActiveWaypointEvents;
    private static DelegateBridge __Hotfix_GetActiveWaypointEvents;
    private static DelegateBridge __Hotfix_GetActiveWaypointEventInfo;
    private static DelegateBridge __Hotfix_GetActiveWaypointRandomEvent;
    private static DelegateBridge __Hotfix_EnterWorldUITask;
    private static DelegateBridge __Hotfix_IsFirstEnterWorldUITask;
    private static DelegateBridge __Hotfix_add_EventOnLevelWayPointMoveAck;
    private static DelegateBridge __Hotfix_remove_EventOnLevelWayPointMoveAck;
    private static DelegateBridge __Hotfix_add_EventOnLevelScenarioHandleAck;
    private static DelegateBridge __Hotfix_remove_EventOnLevelScenarioHandleAck;
    private static DelegateBridge __Hotfix_add_EventOnLevelWayPointBattleFinishAck;
    private static DelegateBridge __Hotfix_remove_EventOnLevelWayPointBattleFinishAck;
    private static DelegateBridge __Hotfix_add_EventOnRandomEventUpdate;
    private static DelegateBridge __Hotfix_remove_EventOnRandomEventUpdate;
    private static DelegateBridge __Hotfix_SendMailsGetReq;
    private static DelegateBridge __Hotfix_SendMailReadReq;
    private static DelegateBridge __Hotfix_SendMailAttachmentsGetReq;
    private static DelegateBridge __Hotfix_SendMailAttachmentAutoGetReq;
    private static DelegateBridge __Hotfix_OnMessage_143;
    private static DelegateBridge __Hotfix_OnMessage_294;
    private static DelegateBridge __Hotfix_OnMessage_293;
    private static DelegateBridge __Hotfix_OnMessage_292;
    private static DelegateBridge __Hotfix_OnMessage_291;
    private static DelegateBridge __Hotfix_OnMessage_290;
    private static DelegateBridge __Hotfix_GetMails;
    private static DelegateBridge __Hotfix_IsMailReaded;
    private static DelegateBridge __Hotfix_IsExistMailAttacments;
    private static DelegateBridge __Hotfix_HasGotMailAttachments;
    private static DelegateBridge __Hotfix_GetMailExpiredTime;
    private static DelegateBridge __Hotfix_GetUnReadMailCount;
    private static DelegateBridge __Hotfix_CanGetMailAttachment;
    private static DelegateBridge __Hotfix_CanAutoGetMailAttachment;
    private static DelegateBridge __Hotfix_IsOfficialMail;
    private static DelegateBridge __Hotfix_get_IsHaveMailList;
    private static DelegateBridge __Hotfix_set_IsHaveMailList;
    private static DelegateBridge __Hotfix_add_EventOnMailsGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnMailsGetAck;
    private static DelegateBridge __Hotfix_add_EventOnMailsChangedNtf;
    private static DelegateBridge __Hotfix_remove_EventOnMailsChangedNtf;
    private static DelegateBridge __Hotfix_add_EventOnMailReadAck;
    private static DelegateBridge __Hotfix_remove_EventOnMailReadAck;
    private static DelegateBridge __Hotfix_add_EventOnMailAttachmentsGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnMailAttachmentsGetAck;
    private static DelegateBridge __Hotfix_add_EventOnMailAttachmentsAutoGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnMailAttachmentsAutoGetAck;
    private static DelegateBridge __Hotfix_SendMemoryCorridorLevelAttackReq;
    private static DelegateBridge __Hotfix_SendMemoryCorridorLevelBattleFinishedReq;
    private static DelegateBridge __Hotfix_OnMessage_144;
    private static DelegateBridge __Hotfix_OnMessage_295;
    private static DelegateBridge __Hotfix_OnMessage_296;
    private static DelegateBridge __Hotfix_IsMemoryCorridorOpened;
    private static DelegateBridge __Hotfix_IsMemoryCorridorLevelOpened;
    private static DelegateBridge __Hotfix_CanAttackMemoryCorridorLevel;
    private static DelegateBridge __Hotfix_GetMemoryCorridorTicketCount;
    private static DelegateBridge __Hotfix_IsMemoryCorridorLevelFinished;
    private static DelegateBridge __Hotfix_GetMemoryCorridorMaxFinishedLevelId;
    private static DelegateBridge __Hotfix_GetMemoryCorridorDailyRewardRestCount;
    private static DelegateBridge __Hotfix_GetMemoryCorridorDailyRewardCountMax;
    private static DelegateBridge __Hotfix_IsMemoryCorridorLevelBlessing;
    private static DelegateBridge __Hotfix_add_EventOnMemoryCorridorLevelAttackAck;
    private static DelegateBridge __Hotfix_remove_EventOnMemoryCorridorLevelAttackAck;
    private static DelegateBridge __Hotfix_add_EventOnMemoryCorridorLevelBattleFinishedAck;
    private static DelegateBridge __Hotfix_remove_EventOnMemoryCorridorLevelBattleFinishedAck;
    private static DelegateBridge __Hotfix_SendGetMissionRewardReq;
    private static DelegateBridge __Hotfix_OnMessage_119;
    private static DelegateBridge __Hotfix_OnMessage_145;
    private static DelegateBridge __Hotfix_OnMessage_203;
    private static DelegateBridge __Hotfix_add_EventOnMissionRewardGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnMissionRewardGetAck;
    private static DelegateBridge __Hotfix_GetAllProcessingMissionList;
    private static DelegateBridge __Hotfix_GetAllCompletedMissionList;
    private static DelegateBridge __Hotfix_GetAllFinishedMissionList;
    private static DelegateBridge __Hotfix_GetMissionMaxProcess;
    private static DelegateBridge __Hotfix_GetMissionCompleteProcess;
    private static DelegateBridge __Hotfix_IsExistMissionCompleted;
    private static DelegateBridge __Hotfix_CanGainMissionReward;
    private static DelegateBridge __Hotfix_CanGetMissionRewarding;
    private static DelegateBridge __Hotfix_SendNoviceClaimRewardReq;
    private static DelegateBridge __Hotfix_OnMessage_146;
    private static DelegateBridge __Hotfix_OnMessage_307;
    private static DelegateBridge __Hotfix_GetDaysAfterCreation;
    private static DelegateBridge __Hotfix_GetNoviceMissions;
    private static DelegateBridge __Hotfix_GetNoviceProcessingMissions;
    private static DelegateBridge __Hotfix_IsShowRedMarkOnOpenServiceButton;
    private static DelegateBridge __Hotfix_HaveFinishedNotGetNoviceMissions;
    private static DelegateBridge __Hotfix_HaveFinishedNotGetNoiviceMissionsByDay;
    private static DelegateBridge __Hotfix_GetFinishedAndGetRewardNoviceMissions;
    private static DelegateBridge __Hotfix_GetNoviceMissionPoints;
    private static DelegateBridge __Hotfix_GetNovicePointsRewardsConfig;
    private static DelegateBridge __Hotfix_CanClaimNoviceReward;
    private static DelegateBridge __Hotfix_GetNoviceMissionsEndTime;
    private static DelegateBridge __Hotfix_add_EventOnNoviceClaimRewardAck;
    private static DelegateBridge __Hotfix_remove_EventOnNoviceClaimRewardAck;
    private static DelegateBridge __Hotfix_SendOperationalActivityGainRewardReq;
    private static DelegateBridge __Hotfix_SendOperationalActivityExchangeItemGroupReq;
    private static DelegateBridge __Hotfix_SendRedeemInfoReq;
    private static DelegateBridge __Hotfix_SendRedeemClaimReq;
    private static DelegateBridge __Hotfix_SendFansRewardsFromPBTCBTInfoReq;
    private static DelegateBridge __Hotfix_SendFansRewardsFromPBTCBTClaimReq;
    private static DelegateBridge __Hotfix_OnMessage_314;
    private static DelegateBridge __Hotfix_OnMessage_313;
    private static DelegateBridge __Hotfix_OnMessage_1;
    private static DelegateBridge __Hotfix_OnMessage_418;
    private static DelegateBridge __Hotfix_OnMessage_147;
    private static DelegateBridge __Hotfix_OnMessage_366;
    private static DelegateBridge __Hotfix_OnMessage_122;
    private static DelegateBridge __Hotfix_OnMessage_303;
    private static DelegateBridge __Hotfix_OnMessage_300;
    private static DelegateBridge __Hotfix_OnMessage_412;
    private static DelegateBridge __Hotfix_OnMessage_302;
    private static DelegateBridge __Hotfix_OnMessage_364;
    private static DelegateBridge __Hotfix_OnMessage_363;
    private static DelegateBridge __Hotfix_OnMessage_181;
    private static DelegateBridge __Hotfix_OnMessage_180;
    private static DelegateBridge __Hotfix_GetAllCurrentAnnouncements;
    private static DelegateBridge __Hotfix_SetRedeemInfo;
    private static DelegateBridge __Hotfix_SetRedeemClaimed;
    private static DelegateBridge __Hotfix_GetRedeemInfo;
    private static DelegateBridge __Hotfix_IsRedeemClaimed;
    private static DelegateBridge __Hotfix_GetAllCurrentActivity;
    private static DelegateBridge __Hotfix_CanShowActivity;
    private static DelegateBridge __Hotfix_CanActivityGainReward;
    private static DelegateBridge __Hotfix_CanActivityGainRewardResult;
    private static DelegateBridge __Hotfix_FindOperationalActivityById;
    private static DelegateBridge __Hotfix_CanExchangeGoodsResult;
    private static DelegateBridge __Hotfix_IsTabRedPointShow;
    private static DelegateBridge __Hotfix_GetWebActivityTagNameInPDSDK;
    private static DelegateBridge __Hotfix_GetWebActivityTagIdInPDSDK;
    private static DelegateBridge __Hotfix_IsActivityRedPointShow;
    private static DelegateBridge __Hotfix_GetAllAdvertisementFlowLayouts;
    private static DelegateBridge __Hotfix_IsFirstTryToOpenActivityNoticeUI;
    private static DelegateBridge __Hotfix_HaveActivityNotice;
    private static DelegateBridge __Hotfix_SetBuyGuideActivityViewed;
    private static DelegateBridge __Hotfix_IsWebInfoEnable;
    private static DelegateBridge __Hotfix_add_EventOnActivityGainRewardAck;
    private static DelegateBridge __Hotfix_remove_EventOnActivityGainRewardAck;
    private static DelegateBridge __Hotfix_add_EventOnActivityExchangeItemGroupAck;
    private static DelegateBridge __Hotfix_remove_EventOnActivityExchangeItemGroupAck;
    private static DelegateBridge __Hotfix_add_EventOnNewMarqueeNtf;
    private static DelegateBridge __Hotfix_remove_EventOnNewMarqueeNtf;
    private static DelegateBridge __Hotfix_add_EventOnActivityRedeemClaimAck;
    private static DelegateBridge __Hotfix_remove_EventOnActivityRedeemClaimAck;
    private static DelegateBridge __Hotfix_add_EventOnActivityFansRewardsFromPBTCBT;
    private static DelegateBridge __Hotfix_remove_EventOnActivityFansRewardsFromPBTCBT;
    private static DelegateBridge __Hotfix_SendNoticeEnterPlayOffRaceReq;
    private static DelegateBridge __Hotfix_SendPeakArenaBattleLiveRoomMatchInfoGetReq;
    private static DelegateBridge __Hotfix_SendPeakArenaBattleLiveRoomJoinReq;
    private static DelegateBridge __Hotfix_SendPeakArenaBattleLiveRoomQuitReq;
    private static DelegateBridge __Hotfix_SendPeakArenaBetReq;
    private static DelegateBridge __Hotfix_SendPeakArenaBattleReportShare2FriendsReq;
    private static DelegateBridge __Hotfix_SendPeakArenaBattleReportChangeNameReq;
    private static DelegateBridge __Hotfix_SendPeakArenaPublicHeroesPoolReq;
    private static DelegateBridge __Hotfix_SendPeakArenaBattleTeamSetupReq;
    private static DelegateBridge __Hotfix_SendPeakArenaBattleTeamGetReq;
    private static DelegateBridge __Hotfix_SendPeakArenaGetTopPlayersReq;
    private static DelegateBridge __Hotfix_SendPeakArenaGetInfoReq;
    private static DelegateBridge __Hotfix_SendPeakArenaPlayOffCurrentSeasonMatchInfoGetReq;
    private static DelegateBridge __Hotfix_SendPeakArenaAcquireWinsBonusReq;
    private static DelegateBridge __Hotfix_SendPeakArenaPlayOffInfoGetReq;
    private static DelegateBridge __Hotfix_SendPeakArenaPlayOffLiveRoomGetReq;
    private static DelegateBridge __Hotfix_SendPeakArenaMyPlayOffInfoGetReq;
    private static DelegateBridge __Hotfix_SendPeakArenaBattleReportsGetReq;
    private static DelegateBridge __Hotfix_SendBattleReportBattleInfoGetReq;
    private static DelegateBridge __Hotfix_SendPeakArenaPlayOffMatchSetReadyReq;
    private static DelegateBridge __Hotfix_SendPeakArenaPlayOffBattleSetReadyReq;
    private static DelegateBridge __Hotfix_SendPeakArenaDanmakuReq;
    private static DelegateBridge __Hotfix_SendPeakArenaSeasonSimpleInfoReq;
    private static DelegateBridge __Hotfix_OnMessage_305;
    private static DelegateBridge __Hotfix_OnMessage_318;
    private static DelegateBridge __Hotfix_OnMessage_320;
    private static DelegateBridge __Hotfix_OnMessage_319;
    private static DelegateBridge __Hotfix_OnMessage_33;
    private static DelegateBridge __Hotfix_OnMessage_317;
    private static DelegateBridge __Hotfix_OnMessage_326;
    private static DelegateBridge __Hotfix_OnMessage_323;
    private static DelegateBridge __Hotfix_OnMessage_321;
    private static DelegateBridge __Hotfix_OnMessage_148;
    private static DelegateBridge __Hotfix_OnMessage_329;
    private static DelegateBridge __Hotfix_OnMessage_324;
    private static DelegateBridge __Hotfix_OnMessage_325;
    private static DelegateBridge __Hotfix_OnMessage_340;
    private static DelegateBridge __Hotfix_OnMessage_331;
    private static DelegateBridge __Hotfix_OnMessage_330;
    private static DelegateBridge __Hotfix_OnMessage_336;
    private static DelegateBridge __Hotfix_OnMessage_316;
    private static DelegateBridge __Hotfix_OnMessage_315;
    private static DelegateBridge __Hotfix_OnMessage_337;
    private static DelegateBridge __Hotfix_OnMessage_338;
    private static DelegateBridge __Hotfix_OnMessage_333;
    private static DelegateBridge __Hotfix_OnMessage_322;
    private static DelegateBridge __Hotfix_OnMessage_41;
    private static DelegateBridge __Hotfix_OnMessage_339;
    private static DelegateBridge __Hotfix_OnMessage_335;
    private static DelegateBridge __Hotfix_OnMessage_327;
    private static DelegateBridge __Hotfix_OnMessage_328;
    private static DelegateBridge __Hotfix_OnMessage_332;
    private static DelegateBridge __Hotfix_OnMessage_342;
    private static DelegateBridge __Hotfix_OnMessage_334;
    private static DelegateBridge __Hotfix_OnMessage_341;
    private static DelegateBridge __Hotfix_GetDailyJettonUseCount;
    private static DelegateBridge __Hotfix_CanBuyJetton;
    private static DelegateBridge __Hotfix_CanBetPeakArenaCare4Round;
    private static DelegateBridge __Hotfix_GetPeakArenaPublicHeroesPool;
    private static DelegateBridge __Hotfix_GetPeakArenaPublicHero;
    private static DelegateBridge __Hotfix_GetPeakArenaPublicBattleHeroEquipments;
    private static DelegateBridge __Hotfix_PeakArenaMercenaryToBattleHero;
    private static DelegateBridge __Hotfix_IsPeakArenaMercenaryInBattleTeam;
    private static DelegateBridge __Hotfix_IsPeakArenaMercenarySoldierChange;
    private static DelegateBridge __Hotfix_IsPeakArenaMercenarySkillChange;
    private static DelegateBridge __Hotfix_IsMyHeroInPeakArenaBattleTeam;
    private static DelegateBridge __Hotfix_GetPeakArenaBattleTeamAllHeroCount;
    private static DelegateBridge __Hotfix_GetPeakArenaMercenaryInBattleTeam;
    private static DelegateBridge __Hotfix_IsPeakArenaTeamManagementComplete;
    private static DelegateBridge __Hotfix_GetPeakArenaTopRankPlayerSummaries;
    private static DelegateBridge __Hotfix_GetPeakArenaTopRankPlayerInfos;
    private static DelegateBridge __Hotfix_GetPeakArenaPlayerDan;
    private static DelegateBridge __Hotfix_GetPeakArenaPlayerRank;
    private static DelegateBridge __Hotfix_GetPeakArenaPlayerScore;
    private static DelegateBridge __Hotfix_GetPeakArenaPlayerQuitCount;
    private static DelegateBridge __Hotfix_GetPeakArenaSeasonInfo;
    private static DelegateBridge __Hotfix_GetPeakArenaSeasonId;
    private static DelegateBridge __Hotfix_GetPeakArenaSeasonEndTime;
    private static DelegateBridge __Hotfix_IsFinalRoundKnockOutMatchsFinish;
    private static DelegateBridge __Hotfix_CalcPeakArenaSeasonId;
    private static DelegateBridge __Hotfix_GetCurPeakArenaStateType;
    private static DelegateBridge __Hotfix_IsPeakArenaPlayOffOverDayEndTime;
    private static DelegateBridge __Hotfix_HandleTimeIsOverEndTimeInSameDay;
    private static DelegateBridge __Hotfix_IsDuringPeakArenaSeason;
    private static DelegateBridge __Hotfix_CalcTimeBeforePeakArenaSeasonBegin;
    private static DelegateBridge __Hotfix_GetPeakArenaSeasonBeginTime;
    private static DelegateBridge __Hotfix_IsInPeakAreanPreRankingState;
    private static DelegateBridge __Hotfix_IsPlayerFinishPreRankingState;
    private static DelegateBridge __Hotfix_GetPeakArenaConsecutiveWins;
    private static DelegateBridge __Hotfix_GetPeakArenaMatchStats;
    private static DelegateBridge __Hotfix_IsAlreadyGainPeakArenaWeekFirstWinReward;
    private static DelegateBridge __Hotfix_GetPeakArenaPointRaceEndTime;
    private static DelegateBridge __Hotfix_IsPeakArenaInKnockout;
    private static DelegateBridge __Hotfix_GetPeakArenaKnockoutRound;
    private static DelegateBridge __Hotfix_IsPeakArenaKnockoutRoundAboutToStart;
    private static DelegateBridge __Hotfix_IsDuringPeakArenaKnockoutRoundMatch;
    private static DelegateBridge __Hotfix_IsPeakArenaKnockoutRoundStart;
    private static DelegateBridge __Hotfix_GetPeakArenaPlayOffRoundStartTime;
    private static DelegateBridge __Hotfix_GetPeakArenaPlayOffRoundEndTime;
    private static DelegateBridge __Hotfix_IsKnockOutMatchRoundEnd;
    private static DelegateBridge __Hotfix_GetPlayerKnockoutGroupId;
    private static DelegateBridge __Hotfix_GetPeakArenaGroupRootKnockoutInfo;
    private static DelegateBridge __Hotfix_GetAllPeakArenaKnockoutInfo;
    private static DelegateBridge __Hotfix_GetPeakArenaMatchupInfosByRound;
    private static DelegateBridge __Hotfix_GetPeakArenaNextMatchInfo;
    private static DelegateBridge __Hotfix_IsMineReadyAtKnockOutMatch;
    private static DelegateBridge __Hotfix_GetPeakArenaGroupId;
    private static DelegateBridge __Hotfix_GetPeakArenaKnockOutGroupName;
    private static DelegateBridge __Hotfix_DoesPlayerHaveNextKnockOutMatch;
    private static DelegateBridge __Hotfix_IsPlayerHaveBeenInPeakArenaKnockOutMatch;
    private static DelegateBridge __Hotfix_GetPlayerFinalRound;
    private static DelegateBridge __Hotfix_GetPlayerFinalRoundTitleName;
    private static DelegateBridge __Hotfix_GetCurPeakArenaSeasonKnockOutMatchTitleName;
    private static DelegateBridge __Hotfix_GetCurPeakArenaSeasonKnockOutMatchTime;
    private static DelegateBridge __Hotfix_GetPeakArenaSeasonKnockOutMatchTime;
    private static DelegateBridge __Hotfix_GetPeakArenaSeasonKnockoutTimeTable;
    private static DelegateBridge __Hotfix_IsPeakArenaMatchmakingAvailable;
    private static DelegateBridge __Hotfix_IsPeakArenaMatchmakingAvailableWithOutGameFunction;
    private static DelegateBridge __Hotfix_GetPeakArenaDanMaxDisplayDan;
    private static DelegateBridge __Hotfix_GetAllPeakArenaBattleReports;
    private static DelegateBridge __Hotfix_GetPeakArenaBattleReport;
    private static DelegateBridge __Hotfix_IsNeedGetPeakArenaBattleReports;
    private static DelegateBridge __Hotfix_GetPeakArenaBattleReportNameRecords;
    private static DelegateBridge __Hotfix_GetPeakArenaBattleReportNameRecord;
    private static DelegateBridge __Hotfix_GetPeakArenaBattleReportStateName;
    private static DelegateBridge __Hotfix_GetPeakArenaPlayOffRoundText;
    private static DelegateBridge __Hotfix_GetBattleReportDefaultName;
    private static DelegateBridge __Hotfix_GetBattleReportStateTypeName;
    private static DelegateBridge __Hotfix_GetBattleReportStateLevelName;
    private static DelegateBridge __Hotfix_GetNoticeEnterPlayOffRaceTag;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaBattleTeamGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaBattleTeamGetAck;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaBattleTeamSetupAck;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaBattleTeamSetupAck;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaPublicHeroesPoolAck;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaPublicHeroesPoolAck;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaGetTopPlayersAck;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaGetTopPlayersAck;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaGetInfoAck;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaGetInfoAck;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaPlayOffCurrentSeasonMatchInfoGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaPlayOffCurrentSeasonMatchInfoGetAck;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaAcquireWinsBonusAck;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaAcquireWinsBonusAck;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaAcquireRegularRewardsAck;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaAcquireRegularRewardsAck;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaPlayOffInfoGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaPlayOffInfoGetAck;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaPlayOffLiveRoomInfoGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaPlayOffLiveRoomInfoGetAck;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaMyPlayOffInfoGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaMyPlayOffInfoGetAck;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaBattleReportShare2FriendsAck;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaBattleReportShare2FriendsAck;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaBattleReportsGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaBattleReportsGetAck;
    private static DelegateBridge __Hotfix_add_EventOnBattleReportBattleInfoGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnBattleReportBattleInfoGetAck;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaPlayOffMatchSetReadyAck;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaPlayOffMatchSetReadyAck;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaPlayOffBattleSetReadyAck;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaPlayOffBattleSetReadyAck;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaBattleReportChangeNameAck;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaBattleReportChangeNameAck;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaBattleLiveRoomJoinAck;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaBattleLiveRoomJoinAck;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaBattleLiveRoomQuitAck;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaBattleLiveRoomQuitAck;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaBetAck;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaBetAck;
    private static DelegateBridge __Hotfix_add_EventOnBattleLiveRoomFinishNtf;
    private static DelegateBridge __Hotfix_remove_EventOnBattleLiveRoomFinishNtf;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaDanmakuAck;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaDanmakuAck;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaBattleLiveRoomInfoAck;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaBattleLiveRoomInfoAck;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaDanmakuNtf;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaDanmakuNtf;
    private static DelegateBridge __Hotfix_add_EventNoticeEnterPlayOffRaceAck;
    private static DelegateBridge __Hotfix_remove_EventNoticeEnterPlayOffRaceAck;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaMatchFinishNtf;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaMatchFinishNtf;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaShakeHandsNtf;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaShakeHandsNtf;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaPlayerForfeitNtf;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaPlayerForfeitNtf;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaSeasonSimpleInfoAck;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaSeasonSimpleInfoAck;
    private static DelegateBridge __Hotfix_SendGainDialogRewardReq;
    private static DelegateBridge __Hotfix_SendJettonBuyReq;
    private static DelegateBridge __Hotfix_SendOpenMemoryStoreReq;
    private static DelegateBridge __Hotfix_SendShareReq;
    private static DelegateBridge __Hotfix_SendClientDecidedAchievementReq;
    private static DelegateBridge __Hotfix_SendCheckOnlineReq;
    private static DelegateBridge __Hotfix_SendPlayerInfoInitReq;
    private static DelegateBridge __Hotfix_SendPlayerInfoReqOnReloginBySession;
    private static DelegateBridge __Hotfix_BuildPlayerInfoInitReq;
    private static DelegateBridge __Hotfix_SendCreateCharactorReq;
    private static DelegateBridge __Hotfix_SendLanguageSetReq;
    private static DelegateBridge __Hotfix_SendBuyEnergyReq;
    private static DelegateBridge __Hotfix_SendBuyArenaTicketsReq;
    private static DelegateBridge __Hotfix_SendHeadPortraitSetReq;
    private static DelegateBridge __Hotfix_SendHeadFrameSetReq;
    private static DelegateBridge __Hotfix_SendHeadPortraitAndHeadFrameSetReq;
    private static DelegateBridge __Hotfix_SendChangePlayerNameReq;
    private static DelegateBridge __Hotfix_SendExchangeGiftCDKeyReq;
    private static DelegateBridge __Hotfix_SendClientHeartBeatNtf;
    private static DelegateBridge __Hotfix_SendClientAppPauseNtf;
    private static DelegateBridge __Hotfix_SendClientAppResumeNtf;
    private static DelegateBridge __Hotfix_SendOpenGameRatingReq;
    private static DelegateBridge __Hotfix_OnMessage_201;
    private static DelegateBridge __Hotfix_OnMessage_282;
    private static DelegateBridge __Hotfix_OnMessage_260;
    private static DelegateBridge __Hotfix_WriteHeroErrorFileLog;
    private static DelegateBridge __Hotfix_OnMessage_310;
    private static DelegateBridge __Hotfix_OnMessage_117;
    private static DelegateBridge __Hotfix_OnMessage_375;
    private static DelegateBridge __Hotfix_OnMessage_102;
    private static DelegateBridge __Hotfix_OnMessage_245;
    private static DelegateBridge __Hotfix_OnMessage_371;
    private static DelegateBridge __Hotfix_OnMessage_183;
    private static DelegateBridge __Hotfix_OnMessage_178;
    private static DelegateBridge __Hotfix_OnMessage_343;
    private static DelegateBridge __Hotfix_OnMessage_116;
    private static DelegateBridge __Hotfix_OnMessage_149;
    private static DelegateBridge __Hotfix_OnMessage_344;
    private static DelegateBridge __Hotfix_OnMessage_346;
    private static DelegateBridge __Hotfix_OnMessage_81;
    private static DelegateBridge __Hotfix_OnMessage_24;
    private static DelegateBridge __Hotfix_OnMessage_345;
    private static DelegateBridge __Hotfix_OnMessage_202;
    private static DelegateBridge __Hotfix_OnMessage_100;
    private static DelegateBridge __Hotfix_SetChangedGoodsListStatus;
    private static DelegateBridge __Hotfix_SetChangedGoodsStatus;
    private static DelegateBridge __Hotfix_SetBagItemPropertyChanges;
    private static DelegateBridge __Hotfix_IsMemoryStoreOpen;
    private static DelegateBridge __Hotfix_GetEnergyReachMaxTime;
    private static DelegateBridge __Hotfix_GetArenaTicketNextGivenTime;
    private static DelegateBridge __Hotfix_GetBlackMarketNextFlushTime;
    private static DelegateBridge __Hotfix_OnMessage_414;
    private static DelegateBridge __Hotfix_InitPlayerInfo;
    private static DelegateBridge __Hotfix_UpdateServerTime;
    private static DelegateBridge __Hotfix_InitPlayerBasic;
    private static DelegateBridge __Hotfix_GetGold;
    private static DelegateBridge __Hotfix_GetDailyJettonBuyCount;
    private static DelegateBridge __Hotfix_GetRechargeRMB;
    private static DelegateBridge __Hotfix_GetCrystal;
    private static DelegateBridge __Hotfix_GetGuildMedal;
    private static DelegateBridge __Hotfix_GetMithralStone;
    private static DelegateBridge __Hotfix_GetChallengePoint;
    private static DelegateBridge __Hotfix_GetFashionPoint;
    private static DelegateBridge __Hotfix_GetBrillianceMithralStone;
    private static DelegateBridge __Hotfix_GetEnergy;
    private static DelegateBridge __Hotfix_GetEnergyMax;
    private static DelegateBridge __Hotfix_GetCreateTime;
    private static DelegateBridge __Hotfix_GetCreateTimeUtc;
    private static DelegateBridge __Hotfix_CanBuyEnergy;
    private static DelegateBridge __Hotfix_GetBuyEnergyNums;
    private static DelegateBridge __Hotfix_GetEnergyFlushTime;
    private static DelegateBridge __Hotfix_CanBuyArenaTickets;
    private static DelegateBridge __Hotfix_GetBuyArenaTicketsNums;
    private static DelegateBridge __Hotfix_GetPlayerLevel;
    private static DelegateBridge __Hotfix_IsPlayerLevelMax;
    private static DelegateBridge __Hotfix_GetPlayerExp;
    private static DelegateBridge __Hotfix_GetPlayerNextLevelExp;
    private static DelegateBridge __Hotfix_GetPlayerVip;
    private static DelegateBridge __Hotfix_GetPlayerHeadIcon;
    private static DelegateBridge __Hotfix_GetPlayerHeadPortrait;
    private static DelegateBridge __Hotfix_GetPlayerHeadFrame;
    private static DelegateBridge __Hotfix_SetUserGuide;
    private static DelegateBridge __Hotfix_GetPlayerName;
    private static DelegateBridge __Hotfix_CheckPlayerName;
    private static DelegateBridge __Hotfix_CanChangePlayerName;
    private static DelegateBridge __Hotfix_GetUserId;
    private static DelegateBridge __Hotfix_IsMe;
    private static DelegateBridge __Hotfix_SetNeedBattleReportLog;
    private static DelegateBridge __Hotfix_HasHeadFrameId;
    private static DelegateBridge __Hotfix_GetHeadFrameSource;
    private static DelegateBridge __Hotfix_GetAllValidMonthCards;
    private static DelegateBridge __Hotfix_GetAllMonthCards;
    private static DelegateBridge __Hotfix_IsMonthCardNeverBuy;
    private static DelegateBridge __Hotfix_HasValidMonthCards;
    private static DelegateBridge __Hotfix_CanGainDialogReward;
    private static DelegateBridge __Hotfix_get_IsNeedBattleReportLog;
    private static DelegateBridge __Hotfix_set_IsNeedBattleReportLog;
    private static DelegateBridge __Hotfix_get_HeroErrMsg;
    private static DelegateBridge __Hotfix_add_EventOnExchangeGiftCDKeyAck;
    private static DelegateBridge __Hotfix_remove_EventOnExchangeGiftCDKeyAck;
    private static DelegateBridge __Hotfix_add_EventOnPlayerInfoInitAck;
    private static DelegateBridge __Hotfix_remove_EventOnPlayerInfoInitAck;
    private static DelegateBridge __Hotfix_add_EventOnPlayerInfoInitEnd;
    private static DelegateBridge __Hotfix_remove_EventOnPlayerInfoInitEnd;
    private static DelegateBridge __Hotfix_add_EventOnCreateCharactorAck;
    private static DelegateBridge __Hotfix_remove_EventOnCreateCharactorAck;
    private static DelegateBridge __Hotfix_add_EventOnBuyEnergyAck;
    private static DelegateBridge __Hotfix_remove_EventOnBuyEnergyAck;
    private static DelegateBridge __Hotfix_add_EventOnHeadPortraitAndHeadFrameSetAck;
    private static DelegateBridge __Hotfix_remove_EventOnHeadPortraitAndHeadFrameSetAck;
    private static DelegateBridge __Hotfix_add_EventOnPlayerNameChangeAck;
    private static DelegateBridge __Hotfix_remove_EventOnPlayerNameChangeAck;
    private static DelegateBridge __Hotfix_add_EventOnPlayerInfoChangeNtf;
    private static DelegateBridge __Hotfix_remove_EventOnPlayerInfoChangeNtf;
    private static DelegateBridge __Hotfix_add_EventOnArenaTicketsBuyAck;
    private static DelegateBridge __Hotfix_remove_EventOnArenaTicketsBuyAck;
    private static DelegateBridge __Hotfix_add_EventOnOpenGameRatingAck;
    private static DelegateBridge __Hotfix_remove_EventOnOpenGameRatingAck;
    private static DelegateBridge __Hotfix_add_EventOnShareAck;
    private static DelegateBridge __Hotfix_remove_EventOnShareAck;
    private static DelegateBridge __Hotfix_add_EventOnClientDecidedAchievementAck;
    private static DelegateBridge __Hotfix_remove_EventOnClientDecidedAchievementAck;
    private static DelegateBridge __Hotfix_add_EventOnCheckOnlineAck;
    private static DelegateBridge __Hotfix_remove_EventOnCheckOnlineAck;
    private static DelegateBridge __Hotfix_add_EventOnSendClientHeartBeatNtfFail;
    private static DelegateBridge __Hotfix_remove_EventOnSendClientHeartBeatNtfFail;
    private static DelegateBridge __Hotfix_add_EventOnGainDialogRewardAck;
    private static DelegateBridge __Hotfix_remove_EventOnGainDialogRewardAck;
    private static DelegateBridge __Hotfix_add_EventOnSendClientAppResumeNtfFail;
    private static DelegateBridge __Hotfix_remove_EventOnSendClientAppResumeNtfFail;
    private static DelegateBridge __Hotfix_add_EventOpenMemoryStoreAck;
    private static DelegateBridge __Hotfix_remove_EventOpenMemoryStoreAck;
    private static DelegateBridge __Hotfix_add_EventOnJettonBuyAck;
    private static DelegateBridge __Hotfix_remove_EventOnJettonBuyAck;
    private static DelegateBridge __Hotfix_add_EventOnDailyFlushFinish;
    private static DelegateBridge __Hotfix_remove_EventOnDailyFlushFinish;
    private static DelegateBridge __Hotfix_add_EventOnResetPushNotification;
    private static DelegateBridge __Hotfix_remove_EventOnResetPushNotification;
    private static DelegateBridge __Hotfix_SendRaffleDrawReq;
    private static DelegateBridge __Hotfix_OnMessage_150;
    private static DelegateBridge __Hotfix_OnMessage_348;
    private static DelegateBridge __Hotfix_GetRafflePool;
    private static DelegateBridge __Hotfix_GetRaffleItem;
    private static DelegateBridge __Hotfix_GetDrawItemCost;
    private static DelegateBridge __Hotfix_CanDraw;
    private static DelegateBridge __Hotfix_IsRafflePoolOnActivityTime;
    private static DelegateBridge __Hotfix_add_EventOnRaffleDrawAck;
    private static DelegateBridge __Hotfix_remove_EventOnRaffleDrawAck;
    private static DelegateBridge __Hotfix_SendGetRandomStoreReq;
    private static DelegateBridge __Hotfix_SendFlushRandomStoreReq;
    private static DelegateBridge __Hotfix_SendBuyRandomStoreItemReq;
    private static DelegateBridge __Hotfix_OnMessage_151;
    private static DelegateBridge __Hotfix_OnMessage_352;
    private static DelegateBridge __Hotfix_OnMessage_351;
    private static DelegateBridge __Hotfix_OnMessage_350;
    private static DelegateBridge __Hotfix_GetRandomStoreItemList;
    private static DelegateBridge __Hotfix_GetNextFlushTime;
    private static DelegateBridge __Hotfix_CanManualRefreshRandomStore;
    private static DelegateBridge __Hotfix_FindRandomStoreByID;
    private static DelegateBridge __Hotfix_CanBuyRandomStoreGoods;
    private static DelegateBridge __Hotfix_GetManualFlushNums;
    private static DelegateBridge __Hotfix_add_EventOnGetRandomStoreListAck;
    private static DelegateBridge __Hotfix_remove_EventOnGetRandomStoreListAck;
    private static DelegateBridge __Hotfix_add_EventOnFlushRandomStoreAck;
    private static DelegateBridge __Hotfix_remove_EventOnFlushRandomStoreAck;
    private static DelegateBridge __Hotfix_add_EventOnRandomStoreBuyStoreItemAck;
    private static DelegateBridge __Hotfix_remove_EventOnRandomStoreBuyStoreItemAck;
    private static DelegateBridge __Hotfix_SendRankingListInfoQueryReq;
    private static DelegateBridge __Hotfix_OnMessage_353;
    private static DelegateBridge __Hotfix_GetPeakArenaSeasonRankTitleIdBySeasonAndRank;
    private static DelegateBridge __Hotfix_GetPeakArenaRankingListTitle;
    private static DelegateBridge __Hotfix_IsAbleQueryRankingListInfo;
    private static DelegateBridge __Hotfix_CheckRankingListInfoQueryCare4AncientCall;
    private static DelegateBridge __Hotfix_GetRankingListInfoByType;
    private static DelegateBridge __Hotfix_GetSingleBossRankingListInfoByBossId;
    private static DelegateBridge __Hotfix_GetFriendAndGuildSingleBossRankingListInfoByBossId;
    private static DelegateBridge __Hotfix_IsRankingListInfoValid;
    private static DelegateBridge __Hotfix_IsSingleHeroRankingListInfoValid;
    private static DelegateBridge __Hotfix_GetSingleHeroRankingListInfoByHeroId;
    private static DelegateBridge __Hotfix_add_EventOnRankingListInfoQueryAck;
    private static DelegateBridge __Hotfix_remove_EventOnRankingListInfoQueryAck;
    private static DelegateBridge __Hotfix_SendRealTimePVPWaitingForOpponentReq;
    private static DelegateBridge __Hotfix_SendRealTimePVPCancelWaitingForOpponentReq;
    private static DelegateBridge __Hotfix_SendRealTimePVPGetInfoReq;
    private static DelegateBridge __Hotfix_SendRealTimePVPGetTopPlayersReq;
    private static DelegateBridge __Hotfix_SendRealTimePVPAcquireWinsBonusReq;
    private static DelegateBridge __Hotfix_OnMessage_153;
    private static DelegateBridge __Hotfix_OnMessage_154;
    private static DelegateBridge __Hotfix_OnMessage_152;
    private static DelegateBridge __Hotfix_OnMessage_360;
    private static DelegateBridge __Hotfix_OnMessage_359;
    private static DelegateBridge __Hotfix_OnMessage_355;
    private static DelegateBridge __Hotfix_OnMessage_356;
    private static DelegateBridge __Hotfix_OnMessage_357;
    private static DelegateBridge __Hotfix_OnMessage_354;
    private static DelegateBridge __Hotfix_OnMessage_358;
    private static DelegateBridge __Hotfix_IsEmptyRealTimePVPPlayerInfo;
    private static DelegateBridge __Hotfix_IsNeedGetRealTimePVPPlayerInfo;
    private static DelegateBridge __Hotfix_GetPlayerRealTimePVPScore;
    private static DelegateBridge __Hotfix_GetPlayerRealTimePVPDan;
    private static DelegateBridge __Hotfix_GetPlayerRealTimePVPRank;
    private static DelegateBridge __Hotfix_GetRealTimePVPLeaderboardPlayerInfos;
    private static DelegateBridge __Hotfix_GetRealTimePVPLeaderboardUserSummarys;
    private static DelegateBridge __Hotfix_GetRealTimePVPBattleReports;
    private static DelegateBridge __Hotfix_GetRealTimePVPPromotionBattleReports;
    private static DelegateBridge __Hotfix_IsRealTimePVPPromotion;
    private static DelegateBridge __Hotfix_GetRealTimePVPConsecutiveWins;
    private static DelegateBridge __Hotfix_CheckRealTimePVPAcquireWinsBonus;
    private static DelegateBridge __Hotfix_GetRealTimePVPWinsBonusStatus;
    private static DelegateBridge __Hotfix_GetRealTimePVPLadderMatchStats;
    private static DelegateBridge __Hotfix_GetRealTimePVPFriendlyMatchStats;
    private static DelegateBridge __Hotfix_GetRealTimePVPHonour;
    private static DelegateBridge __Hotfix_IsRealTimePVPArenaAvailable;
    private static DelegateBridge __Hotfix_IsRealTimePVPWillMatchBot;
    private static DelegateBridge __Hotfix_add_EventOnRealTimePVPWaitingForOpponentAck;
    private static DelegateBridge __Hotfix_remove_EventOnRealTimePVPWaitingForOpponentAck;
    private static DelegateBridge __Hotfix_add_EventOnRealTimePVPMatchupNtf;
    private static DelegateBridge __Hotfix_remove_EventOnRealTimePVPMatchupNtf;
    private static DelegateBridge __Hotfix_add_EventOnRealTimePVPCancelWaitingForOpponentAck;
    private static DelegateBridge __Hotfix_remove_EventOnRealTimePVPCancelWaitingForOpponentAck;
    private static DelegateBridge __Hotfix_add_EventOnRealTimePVPGetInfoAck;
    private static DelegateBridge __Hotfix_remove_EventOnRealTimePVPGetInfoAck;
    private static DelegateBridge __Hotfix_add_EventOnRealTimePVPGetTopPlayersAck;
    private static DelegateBridge __Hotfix_remove_EventOnRealTimePVPGetTopPlayersAck;
    private static DelegateBridge __Hotfix_add_EventOnRealTimePVPAcquireWinsBonusAck;
    private static DelegateBridge __Hotfix_remove_EventOnRealTimePVPAcquireWinsBonusAck;
    private static DelegateBridge __Hotfix_add_EventOnRealTimePVPLeaderboardInfoNtf;
    private static DelegateBridge __Hotfix_remove_EventOnRealTimePVPLeaderboardInfoNtf;
    private static DelegateBridge __Hotfix_SendRechargeStoreCancelBuyReq;
    private static DelegateBridge __Hotfix_SendRechargeStoreBuyGoodsReq;
    private static DelegateBridge __Hotfix_OnMessage_362;
    private static DelegateBridge __Hotfix_OnMessage_77;
    private static DelegateBridge __Hotfix_OnMessage_361;
    private static DelegateBridge __Hotfix_OnMessage_155;
    private static DelegateBridge __Hotfix_OnMessage_413;
    private static DelegateBridge __Hotfix_IsRechargeGoodsBought;
    private static DelegateBridge __Hotfix_CaculateGotCrystalNums;
    private static DelegateBridge __Hotfix_GetRechargeCrystalOriginalNums;
    private static DelegateBridge __Hotfix_GetRechargeCrystalFirstBoughtRewardNums;
    private static DelegateBridge __Hotfix_GetRechargeCrystalRepeatlyBoughtRewardNums;
    private static DelegateBridge __Hotfix_GetRechargeGoodsIcon;
    private static DelegateBridge __Hotfix_GetRechargeGoodsIsBestValue;
    private static DelegateBridge __Hotfix_add_EventOnRechargeBoughtSuccessNtf;
    private static DelegateBridge __Hotfix_remove_EventOnRechargeBoughtSuccessNtf;
    private static DelegateBridge __Hotfix_add_EventOnRechargeStoreItemBuyAck;
    private static DelegateBridge __Hotfix_remove_EventOnRechargeStoreItemBuyAck;
    private static DelegateBridge __Hotfix_add_EventOnRechargeStoreCancelBuyAck;
    private static DelegateBridge __Hotfix_remove_EventOnRechargeStoreCancelBuyAck;
    private static DelegateBridge __Hotfix_SendRefluxClaimRewardReq;
    private static DelegateBridge __Hotfix_OnMessage_156;
    private static DelegateBridge __Hotfix_OnMessage_365;
    private static DelegateBridge __Hotfix_GetMissionDay;
    private static DelegateBridge __Hotfix_GetRefluxMissions;
    private static DelegateBridge __Hotfix_GetRefluxProcessingMissions;
    private static DelegateBridge __Hotfix_GetFinishedAndGetRewardRefluxMissions;
    private static DelegateBridge __Hotfix_GetRefluxMissionPoints;
    private static DelegateBridge __Hotfix_GetRefluxPointsRewardsConfig;
    private static DelegateBridge __Hotfix_CanClaimRefluxReward;
    private static DelegateBridge __Hotfix_IsBackFlowActivityOpen;
    private static DelegateBridge __Hotfix_GetRefluxMissionsEndTime;
    private static DelegateBridge __Hotfix_GetDaysAfterBackFlowActivityOpening;
    private static DelegateBridge __Hotfix_IsShowRedMarkOnBackFlowButton;
    private static DelegateBridge __Hotfix_HaveFinishedNotGetRefluxMissions;
    private static DelegateBridge __Hotfix_HaveFinishedNotGetRefluxMissionsByDay;
    private static DelegateBridge __Hotfix_add_EventOnRefluxClaimRewardAck;
    private static DelegateBridge __Hotfix_remove_EventOnRefluxClaimRewardAck;
    private static DelegateBridge __Hotfix_OnMessage_157;
    private static DelegateBridge __Hotfix_OnMessage_299;
    private static DelegateBridge __Hotfix_SendRiftLevelAttackReq;
    private static DelegateBridge __Hotfix_SendRiftLevelBattleFinishedReq;
    private static DelegateBridge __Hotfix_SendRiftLevelRaidReq;
    private static DelegateBridge __Hotfix_SendRiftChapterRewardGainReq;
    private static DelegateBridge __Hotfix_OnMessage_158;
    private static DelegateBridge __Hotfix_OnMessage_368;
    private static DelegateBridge __Hotfix_OnMessage_369;
    private static DelegateBridge __Hotfix_OnMessage_370;
    private static DelegateBridge __Hotfix_OnMessage_367;
    private static DelegateBridge __Hotfix_CanUnlockRiftChapter;
    private static DelegateBridge __Hotfix_GetRiftChapterTotalStars;
    private static DelegateBridge __Hotfix_GetRiftChapterTotalStarsMax;
    private static DelegateBridge __Hotfix_GetRiftChapterTotalAchievementCount;
    private static DelegateBridge __Hotfix_GetRiftChapterTotalAchievementCountMax;
    private static DelegateBridge __Hotfix_GetRiftChapterTotalBattleTreasureCount;
    private static DelegateBridge __Hotfix_GetRiftChapterTotalBattleTreasureCountMax;
    private static DelegateBridge __Hotfix_IsRiftChapterUnlockConditionCompleted;
    private static DelegateBridge __Hotfix_CanUnlockRiftLevel;
    private static DelegateBridge __Hotfix_CanAttackRiftLevel;
    private static DelegateBridge __Hotfix_CanRaidRiftLevel;
    private static DelegateBridge __Hotfix_GetRiftLevelStatus;
    private static DelegateBridge __Hotfix_GetEventRiftLevelStatus;
    private static DelegateBridge __Hotfix_IsRiftLevelUnlockConditionCompleted;
    private static DelegateBridge __Hotfix_GetRiftLevelChallengeNum;
    private static DelegateBridge __Hotfix_GetRiftLevelCanChallengeNums;
    private static DelegateBridge __Hotfix_GetRiftLevelCanChallengeMaxNums;
    private static DelegateBridge __Hotfix_IsRiftLevelChallenged;
    private static DelegateBridge __Hotfix_GetRiftLevelStars;
    private static DelegateBridge __Hotfix_GetRiftLevelAchievementCount;
    private static DelegateBridge __Hotfix_HasRiftLevelAchievement;
    private static DelegateBridge __Hotfix_CanGainRiftChapterStarReward;
    private static DelegateBridge __Hotfix_GetRiftChapterStarRewardStatus;
    private static DelegateBridge __Hotfix_GetRiftLevelFinished;
    private static DelegateBridge __Hotfix_InitOpenedRiftChapters;
    private static DelegateBridge __Hotfix_CheckChangeOpenedRiftChapter;
    private static DelegateBridge __Hotfix_CheckChangeLastOpenedRiftChapter;
    private static DelegateBridge __Hotfix_GetLastOpenedRiftChapterInfo;
    private static DelegateBridge __Hotfix_add_EventOnRiftLevelAttackAck;
    private static DelegateBridge __Hotfix_remove_EventOnRiftLevelAttackAck;
    private static DelegateBridge __Hotfix_add_EventOnRiftLevelBattleFinishedAck;
    private static DelegateBridge __Hotfix_remove_EventOnRiftLevelBattleFinishedAck;
    private static DelegateBridge __Hotfix_add_EventOnRiftLevelRaidAck;
    private static DelegateBridge __Hotfix_remove_EventOnRiftLevelRaidAck;
    private static DelegateBridge __Hotfix_add_EventOnRiftChapterRewardGainAck;
    private static DelegateBridge __Hotfix_remove_EventOnRiftChapterRewardGainAck;
    private static DelegateBridge __Hotfix_SendSelectCardReq;
    private static DelegateBridge __Hotfix_OnMessage_159;
    private static DelegateBridge __Hotfix_OnMessage_372;
    private static DelegateBridge __Hotfix_add_EventOnCardSelectAck;
    private static DelegateBridge __Hotfix_remove_EventOnCardSelectAck;
    private static DelegateBridge __Hotfix_GetSelectRewardsIsFirstGetArray;
    private static DelegateBridge __Hotfix_SetRewardFlags;
    private static DelegateBridge __Hotfix_GetIsSelectHeroRewardTransferToFragmentDict;
    private static DelegateBridge __Hotfix_SetHeroRewardToFragmentDict;
    private static DelegateBridge __Hotfix_GetCardPoolById;
    private static DelegateBridge __Hotfix_GetActivityCardPoolList;
    private static DelegateBridge __Hotfix_GetActivityTimeByPoolId;
    private static DelegateBridge __Hotfix_FindOperationalActivityByActivityCardPoolId;
    private static DelegateBridge __Hotfix_IsActivityCardPoolOnActivityTime;
    private static DelegateBridge __Hotfix_GetActivityCardPoolLastDays;
    private static DelegateBridge __Hotfix_CanSelectCard;
    private static DelegateBridge __Hotfix_SendEverydaySignReq;
    private static DelegateBridge __Hotfix_OnMessage_177;
    private static DelegateBridge __Hotfix_CanSignToday;
    private static DelegateBridge __Hotfix_GetSignDaysMonth;
    private static DelegateBridge __Hotfix_GetMonthRewardList;
    private static DelegateBridge __Hotfix_GetSignRewardMonthList;
    private static DelegateBridge __Hotfix_TodayIsSigned;
    private static DelegateBridge __Hotfix_GetTodaySignReward;
    private static DelegateBridge __Hotfix_GetOneDaySignReward;
    private static DelegateBridge __Hotfix_add_EventOnEverydaySignAck;
    private static DelegateBridge __Hotfix_remove_EventOnEverydaySignAck;
    private static DelegateBridge __Hotfix_SendOpenSurveyReq;
    private static DelegateBridge __Hotfix_SendGetSurveyRewardReq;
    private static DelegateBridge __Hotfix_OnMessage_160;
    private static DelegateBridge __Hotfix_OnMessage_304;
    private static DelegateBridge __Hotfix_OnMessage_415;
    private static DelegateBridge __Hotfix_OnMessage_204;
    private static DelegateBridge __Hotfix_OnMessage_312;
    private static DelegateBridge __Hotfix_GetCurrentSurveyStatus;
    private static DelegateBridge __Hotfix_add_EventOnOpenSurveyAck;
    private static DelegateBridge __Hotfix_remove_EventOnOpenSurveyAck;
    private static DelegateBridge __Hotfix_add_EventOnGetSurveyRewardAck;
    private static DelegateBridge __Hotfix_remove_EventOnGetSurveyRewardAck;
    private static DelegateBridge __Hotfix_CanGainSurveyReward;
    private static DelegateBridge __Hotfix_SendTeamRoomCreateReq;
    private static DelegateBridge __Hotfix_SendTeamRoomViewReq;
    private static DelegateBridge __Hotfix_SendTeamRoomAutoMatchReq;
    private static DelegateBridge __Hotfix_SendTeamRoomAutoMatchCancelReq;
    private static DelegateBridge __Hotfix_SendTeamRoomAuthorityChangeReq;
    private static DelegateBridge __Hotfix_SendTeamRoomJoinReq;
    private static DelegateBridge __Hotfix_SendTeamRoomQuitReq;
    private static DelegateBridge __Hotfix_SendTeamRoomGetReq;
    private static DelegateBridge __Hotfix_SendTeamRoomInviteReq;
    private static DelegateBridge __Hotfix_SendTeamRoomInvitationRefusedReq;
    private static DelegateBridge __Hotfix_SendTeamRoomInviteeInfoGetReq;
    private static DelegateBridge __Hotfix_SendTeamRoomPlayerPositionChangeReq;
    private static DelegateBridge __Hotfix_OnMessage_161;
    private static DelegateBridge __Hotfix_OnMessage_383;
    private static DelegateBridge __Hotfix_OnMessage_399;
    private static DelegateBridge __Hotfix_OnMessage_380;
    private static DelegateBridge __Hotfix_OnMessage_381;
    private static DelegateBridge __Hotfix_OnMessage_378;
    private static DelegateBridge __Hotfix_OnMessage_391;
    private static DelegateBridge __Hotfix_OnMessage_397;
    private static DelegateBridge __Hotfix_OnMessage_384;
    private static DelegateBridge __Hotfix_OnMessage_387;
    private static DelegateBridge __Hotfix_OnMessage_385;
    private static DelegateBridge __Hotfix_OnMessage_388;
    private static DelegateBridge __Hotfix_OnMessage_394;
    private static DelegateBridge __Hotfix_OnMessage_392;
    private static DelegateBridge __Hotfix_OnMessage_396;
    private static DelegateBridge __Hotfix_OnMessage_393;
    private static DelegateBridge __Hotfix_OnMessage_379;
    private static DelegateBridge __Hotfix_OnMessage_382;
    private static DelegateBridge __Hotfix_OnMessage_390;
    private static DelegateBridge __Hotfix_OnMessage_386;
    private static DelegateBridge __Hotfix_OnMessage_398;
    private static DelegateBridge __Hotfix_OnMessage_389;
    private static DelegateBridge __Hotfix_OnMessage_395;
    private static DelegateBridge __Hotfix_GetTeamRoom;
    private static DelegateBridge __Hotfix_IsInTeamRoom;
    private static DelegateBridge __Hotfix_IsInTeamRoomAutoMatchWaitingList;
    private static DelegateBridge __Hotfix_GetTeamRoomInviteInfos;
    private static DelegateBridge __Hotfix_RemoveTeamRoomAInviteInfo;
    private static DelegateBridge __Hotfix_CanUnlockTeamBattle;
    private static DelegateBridge __Hotfix_SetTeamRoomInviteAgain;
    private static DelegateBridge __Hotfix_IsTeamRoomInviteAgain;
    private static DelegateBridge __Hotfix_CanCreateTeam;
    private static DelegateBridge __Hotfix_CanViewTeamRoom;
    private static DelegateBridge __Hotfix_CanJoinTeamRoom;
    private static DelegateBridge __Hotfix_CanAutoMatchTeamRoom;
    private static DelegateBridge __Hotfix_CanCancelAutoMatchTeamRoom;
    private static DelegateBridge __Hotfix_add_EventOnTeamRoomCreateAck;
    private static DelegateBridge __Hotfix_remove_EventOnTeamRoomCreateAck;
    private static DelegateBridge __Hotfix_add_EventOnTeamRoomViewAck;
    private static DelegateBridge __Hotfix_remove_EventOnTeamRoomViewAck;
    private static DelegateBridge __Hotfix_add_EventOnTeamRoomAutoMatchAck;
    private static DelegateBridge __Hotfix_remove_EventOnTeamRoomAutoMatchAck;
    private static DelegateBridge __Hotfix_add_EventOnTeamRoomAutoMatchCancelAck;
    private static DelegateBridge __Hotfix_remove_EventOnTeamRoomAutoMatchCancelAck;
    private static DelegateBridge __Hotfix_add_EventOnTeamRoomAuthorityChangeAck;
    private static DelegateBridge __Hotfix_remove_EventOnTeamRoomAuthorityChangeAck;
    private static DelegateBridge __Hotfix_add_EventOnTeamRoomJoinAck;
    private static DelegateBridge __Hotfix_remove_EventOnTeamRoomJoinAck;
    private static DelegateBridge __Hotfix_add_EventOnTeamRoomQuitAck;
    private static DelegateBridge __Hotfix_remove_EventOnTeamRoomQuitAck;
    private static DelegateBridge __Hotfix_add_EventOnTeamRoomGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnTeamRoomGetAck;
    private static DelegateBridge __Hotfix_add_EventOnTeamRoomInviteAck;
    private static DelegateBridge __Hotfix_remove_EventOnTeamRoomInviteAck;
    private static DelegateBridge __Hotfix_add_EventOnTeamRoomInvitationRefusedAck;
    private static DelegateBridge __Hotfix_remove_EventOnTeamRoomInvitationRefusedAck;
    private static DelegateBridge __Hotfix_add_EventOnTeamRoomInviteeInfoGetAck;
    private static DelegateBridge __Hotfix_remove_EventOnTeamRoomInviteeInfoGetAck;
    private static DelegateBridge __Hotfix_add_EventOnTeamRoomPlayerJoinNtf;
    private static DelegateBridge __Hotfix_remove_EventOnTeamRoomPlayerJoinNtf;
    private static DelegateBridge __Hotfix_add_EventOnTeamRoomPlayerQuitNtf;
    private static DelegateBridge __Hotfix_remove_EventOnTeamRoomPlayerQuitNtf;
    private static DelegateBridge __Hotfix_add_EventOnTeamRoomPlayerLeaveWaitingListAndJoinRoomNtf;
    private static DelegateBridge __Hotfix_remove_EventOnTeamRoomPlayerLeaveWaitingListAndJoinRoomNtf;
    private static DelegateBridge __Hotfix_add_EventOnTeamRoomAuthorityChangeNtf;
    private static DelegateBridge __Hotfix_remove_EventOnTeamRoomAuthorityChangeNtf;
    private static DelegateBridge __Hotfix_add_EventOnTeamRoomAutoMatchInfoNtf;
    private static DelegateBridge __Hotfix_remove_EventOnTeamRoomAutoMatchInfoNtf;
    private static DelegateBridge __Hotfix_add_EventOnTeamRoomInviteNtf;
    private static DelegateBridge __Hotfix_remove_EventOnTeamRoomInviteNtf;
    private static DelegateBridge __Hotfix_add_EventOnTeamRoomInvitationRefusedNtf;
    private static DelegateBridge __Hotfix_remove_EventOnTeamRoomInvitationRefusedNtf;
    private static DelegateBridge __Hotfix_add_EventOnTeamRoomSelfKickOutNtf;
    private static DelegateBridge __Hotfix_remove_EventOnTeamRoomSelfKickOutNtf;
    private static DelegateBridge __Hotfix_add_EventOnTeamRoomInviteeInfoNtf;
    private static DelegateBridge __Hotfix_remove_EventOnTeamRoomInviteeInfoNtf;
    private static DelegateBridge __Hotfix_add_EventOnTeamRoomPlayerPositionChangeAck;
    private static DelegateBridge __Hotfix_remove_EventOnTeamRoomPlayerPositionChangeAck;
    private static DelegateBridge __Hotfix_add_EventOnTeamRoomPlayerPositionChangeNtf;
    private static DelegateBridge __Hotfix_remove_EventOnTeamRoomPlayerPositionChangeNtf;
    private static DelegateBridge __Hotfix_SendThearchyTrialLevelAttackReq;
    private static DelegateBridge __Hotfix_SendThearchyTrialLevelBattleFinishedReq;
    private static DelegateBridge __Hotfix_OnMessage_162;
    private static DelegateBridge __Hotfix_OnMessage_400;
    private static DelegateBridge __Hotfix_OnMessage_401;
    private static DelegateBridge __Hotfix_IsThearchyOpened;
    private static DelegateBridge __Hotfix_IsThearchyLevelOpened;
    private static DelegateBridge __Hotfix_CanAttackThearchyLevel;
    private static DelegateBridge __Hotfix_GetThearchyTicketCount;
    private static DelegateBridge __Hotfix_IsThearchyTrialLevelFinished;
    private static DelegateBridge __Hotfix_GetThearchyTrialMaxFinishedLevelId;
    private static DelegateBridge __Hotfix_GetThearchyDailyRewardRestCount;
    private static DelegateBridge __Hotfix_GetThearchyDailyRewardCountMax;
    private static DelegateBridge __Hotfix_IsThearchyLevelBlessing;
    private static DelegateBridge __Hotfix_add_EventOnThearchyTrialLevelAttackAck;
    private static DelegateBridge __Hotfix_remove_EventOnThearchyTrialLevelAttackAck;
    private static DelegateBridge __Hotfix_add_EventOnThearchyTrialLevelBattleFinishedAck;
    private static DelegateBridge __Hotfix_remove_EventOnThearchyTrialLevelBattleFinishedAck;
    private static DelegateBridge __Hotfix_SendTechLevelupReq;
    private static DelegateBridge __Hotfix_OnMessage_163;
    private static DelegateBridge __Hotfix_OnMessage_402;
    private static DelegateBridge __Hotfix_GetTrainingRoomById;
    private static DelegateBridge __Hotfix_CanTechLevelup;
    private static DelegateBridge __Hotfix_CanTrainingCourseLevelup;
    private static DelegateBridge __Hotfix_CanTrainingRoomLevelup;
    private static DelegateBridge __Hotfix_GetTrainingGroundAvailableTechs;
    private static DelegateBridge __Hotfix_GetTrainingTechById;
    private static DelegateBridge __Hotfix_GetTechMaxLevel;
    private static DelegateBridge __Hotfix_IsTechLocked;
    private static DelegateBridge __Hotfix_CanLevelup;
    private static DelegateBridge __Hotfix_GetResourceRequirementsByLevel;
    private static DelegateBridge __Hotfix_GetTrainingCourseIdByTechId;
    private static DelegateBridge __Hotfix_GetTrainingRoomIdByTechId;
    private static DelegateBridge __Hotfix_CanGoToGetSoldierTech;
    private static DelegateBridge __Hotfix_add_EventOnTrainingGroundTechLevelupAck;
    private static DelegateBridge __Hotfix_remove_EventOnTrainingGroundTechLevelupAck;
    private static DelegateBridge __Hotfix_SendTreasureLevelAttackReq;
    private static DelegateBridge __Hotfix_SendTreasureLevelBattleFinishedReq;
    private static DelegateBridge __Hotfix_OnMessage_164;
    private static DelegateBridge __Hotfix_OnMessage_403;
    private static DelegateBridge __Hotfix_OnMessage_404;
    private static DelegateBridge __Hotfix_IsTreasureLevelOpened;
    private static DelegateBridge __Hotfix_CanAttackTreasureLevel;
    private static DelegateBridge __Hotfix_GetTreasureTicketCount;
    private static DelegateBridge __Hotfix_GetTreasureTicketCountMax;
    private static DelegateBridge __Hotfix_IsTreasureLevelFinished;
    private static DelegateBridge __Hotfix_GetTreasureMapMaxFinishedLevelId;
    private static DelegateBridge __Hotfix_add_EventOnTreasureLevelAttackAck;
    private static DelegateBridge __Hotfix_remove_EventOnTreasureLevelAttackAck;
    private static DelegateBridge __Hotfix_add_EventOnTreasureLevelBattleFinishedAck;
    private static DelegateBridge __Hotfix_remove_EventOnTreasureLevelBattleFinishedAck;
    private static DelegateBridge __Hotfix_SendUnchartedLevelRaidReq;
    private static DelegateBridge __Hotfix_OnMessage_405;
    private static DelegateBridge __Hotfix_OnMessage_165;
    private static DelegateBridge __Hotfix_GetUnchartedLevelRaidSurplusTimes;
    private static DelegateBridge __Hotfix_OnMessage_406;
    private static DelegateBridge __Hotfix_GetMaxDailyUnchartedLevelRaidNum;
    private static DelegateBridge __Hotfix_CanUnchartedRaidLevel;
    private static DelegateBridge __Hotfix_GetLeftUnchartedRaidLevelDailyChallengeNum;
    private static DelegateBridge __Hotfix_add_EventOnUnchartedLevelRaidAck;
    private static DelegateBridge __Hotfix_remove_EventOnUnchartedLevelRaidAck;
    private static DelegateBridge __Hotfix_add_EventOnUnchartedRaidSurplusTimesAck;
    private static DelegateBridge __Hotfix_remove_EventOnUnchartedRaidSurplusTimesAck;
    private static DelegateBridge __Hotfix_SendUnchartedScoreChallengeLevelAttackReq;
    private static DelegateBridge __Hotfix_SendUnchartedScoreChallengeLevelBattleFinishedReq;
    private static DelegateBridge __Hotfix_SendUnchartedScoreScoreLevelAttackReq;
    private static DelegateBridge __Hotfix_SendUnchartedScoreScoreLevelBattleFinishedReq;
    private static DelegateBridge __Hotfix_OnMessage_166;
    private static DelegateBridge __Hotfix_OnMessage_407;
    private static DelegateBridge __Hotfix_OnMessage_408;
    private static DelegateBridge __Hotfix_OnMessage_409;
    private static DelegateBridge __Hotfix_OnMessage_410;
    private static DelegateBridge __Hotfix_IsUnchartedScoreOpened;
    private static DelegateBridge __Hotfix_IsUnchartedScoreDisplay;
    private static DelegateBridge __Hotfix_GetUnchartedScoreOpenTime;
    private static DelegateBridge __Hotfix_IsUnchartedScoreLevelTimeUnlock;
    private static DelegateBridge __Hotfix_IsUnchartedScoreLevelPlayerLevelVaild;
    private static DelegateBridge __Hotfix_IsUnchartedChallengePrevLevelComplete;
    private static DelegateBridge __Hotfix_GetUnchartedChallengeLevelTimeUnlockDay;
    private static DelegateBridge __Hotfix_CanAttackUnchartedScoreLevel;
    private static DelegateBridge __Hotfix_CanAttackUnchartedChallengeLevel;
    private static DelegateBridge __Hotfix_IsUnchartedScoreLevelFinished;
    private static DelegateBridge __Hotfix_IsUnchartedChallengeLevelFinished;
    private static DelegateBridge __Hotfix_GetUnchartedScoreMaxFinishedScoreLevelId;
    private static DelegateBridge __Hotfix_GetUnchartedScoreMaxFinishedChallengeLevelId;
    private static DelegateBridge __Hotfix_IsUnchartedScoreRewardGot;
    private static DelegateBridge __Hotfix_GetUnchartedScoreScore;
    private static DelegateBridge __Hotfix_GetUnchartedScoreDailyRewardRestCount;
    private static DelegateBridge __Hotfix_add_EventOnUnchartedScoreChallengeLevelAttackAck;
    private static DelegateBridge __Hotfix_remove_EventOnUnchartedScoreChallengeLevelAttackAck;
    private static DelegateBridge __Hotfix_add_EventOnUnchartedScoreChallengeLevelBattleFinishedAck;
    private static DelegateBridge __Hotfix_remove_EventOnUnchartedScoreChallengeLevelBattleFinishedAck;
    private static DelegateBridge __Hotfix_add_EventOnUnchartedScoreScoreLevelAttackAck;
    private static DelegateBridge __Hotfix_remove_EventOnUnchartedScoreScoreLevelAttackAck;
    private static DelegateBridge __Hotfix_add_EventOnUnchartedScoreScoreLevelBattleFinishedAck;
    private static DelegateBridge __Hotfix_remove_EventOnUnchartedScoreScoreLevelBattleFinishedAck;
    private static DelegateBridge __Hotfix_SendUserGuideSetReq_0;
    private static DelegateBridge __Hotfix_SendUserGuideSetReq_1;
    private static DelegateBridge __Hotfix_SendUserGuideClearReq;
    private static DelegateBridge __Hotfix_OnMessage_417;
    private static DelegateBridge __Hotfix_IsGameFunctionOpened;
    private static DelegateBridge __Hotfix_IsUserGuideComplete;
    private static DelegateBridge __Hotfix_IsRiftChapterEverOpened;
    private static DelegateBridge __Hotfix_add_EventOnUserGuideSetAck;
    private static DelegateBridge __Hotfix_remove_EventOnUserGuideSetAck;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProjectLPlayerContext()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsFullMessageLog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool SendMessage(object msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnGameServerMessage(object msg, int msgId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ServerHeartBeatNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ServerDisconnectNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ClientCheatNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T AddOwnerComponent<T>() where T : class, IComponentBase, new()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveOwnerComponent<T>() where T : class, IComponentBase
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T GetOwnerComponent<T>() where T : class, IComponentBase
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IComponentBase GetOwnerComponent(string componentName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestMarquee()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestChat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetSessionToken()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override string GetDeviceId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override string GetClientVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool IsDataCacheDirtyByPlayerInfoInitAck(
      object msg,
      out bool raiseCriticalDataCacheDirty)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool IsPlayerInfoInitAck4CheckOnly(object msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLocalGame()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetLocalTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetServerTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime LocalTimeToServerTime(DateTime t)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAssignTime(DateTime t)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime ServerTimeToLocalTime(DateTime t)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetLastReceiveMessageTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DataError(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDataError()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TestWriteMessageToFile(object msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T TestReadMessageFromFile<T>() where T : class
    {
      // ISSUE: unable to decompile the method.
    }

    public Happening Happening
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public CurrentBattle CurrentBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClientCheatNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnServerDisconnectNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendAncientCallBossAttackReq(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendAncientCallBossBattleFinishedReq(int bossId, ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSAncientCallNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(AncientCallBossAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(AncientCallBossBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(AncientCallInfoUpdateNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(AncientCallUpdateBossesPeriodFinalRankNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCall GetAncientCallInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallPeriod GetCurrentAncientCallPeriod()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataAncientCallPeriodInfo GetRecentlyAncientCallPeriodInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallBoss GetAncientCallBossByBossId(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallBossHistory GetAncientCallBossHistoryByBossId(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAncientCallBossAllMissionList(
      ConfigDataAncientCallBossInfo bossInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAncientCallBossCompletedMissionList(
      ConfigDataAncientCallBossInfo bossInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsShowAncientCallBossMissionRedMark(ConfigDataAncientCallBossInfo bossInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAncientCallBossProcessingMissionList(
      ConfigDataAncientCallBossInfo bossInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAncientCallBossCompleteMissionList(
      ConfigDataAncientCallBossInfo bossInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAncientCallCurrentPeriodExist()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetAncientCallBossIdList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAncientCallBossOpen(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCurrentAncientCallBossId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetAncientCallBossOpenTime(int selectBossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackAncientCallBoss(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetAncientCallCurrentBossStartTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetAncientCallCurrentBossEndTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetAncientCallCurrentPeriodStartTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetAncientCallCurrentPeriodEndTime()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnAncientCallBossAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event ProjectLPlayerContext.AncientCallBossBattleFinishedAckCallback EventOnAncientCallBossBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendAnikiGymLevelAttackReq(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendAnikiGymLevelBattleFinishedReq(int levelId, ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSAnikiGymNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(AnikiGymLevelAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(AnikiGymLevelBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAnikiGymOpened(int anikiGymId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAnikiLevelOpened(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackAnikiLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAnikiTicketCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAnikiLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAnikiGymMaxFinishedLevelId(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAnikiDailyRewardRestCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAnikiDailyRewardCountMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAnikiLevelBlessing(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnAnikiGymLevelAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool, BattleReward> EventOnAnikiGymLevelBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaPlayerInfoGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaFlushOpponentsReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaOpponentViewReq(int opponentIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaOpponentAttackReq(bool autoBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaRevengeOpponentGetReq(ulong arenaBattleReportInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaOpponentRevengeReq(ulong arenaBattleReportInstanceId, bool autoBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaOpponentAttackFightingReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaBattleFinishedReq(ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaPlayerSetDefensiveTeamReq(
      int arenaDefenderRuleId,
      int battleId,
      List<ArenaPlayerDefensiveHero> defensiveHeroes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaVictoryPointsRewardGainReq(int rewardIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaBattleReportBaiscDataGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaBattleReportPlayBackDataGetReq(ulong arenaBattleReportInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaTopRankPlayersGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaBattleReconnectReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSArenaNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaPlayerInfoGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaFlushOpponentsAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaOpponentViewAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaOpponentAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaRevengeOpponentGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaOpponentRevengeAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaOpponentAttackFightingAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaPlayerSetDefensiveTeamAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaVictoryPointsRewardGainAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaBattleReportBasicDataGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaBattleReportPlayBackDataGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaTopRankPlayersGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaBattleReconnectAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaBattleReportNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaFlushOpponentsNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsEmptyArenaPlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNeedGetArenaPlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNeedFlushArenaOpponents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerArenaLevelId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerArenaPoints()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerArenaVictoryPoints()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetArenaThisWeekVictoryNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetArenaThisWeekTotalBattleNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsArenaAutoBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetArenaTicketCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetArenaHonour()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGetArenaVictoryPointsReward(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GainRewardStatus GetArenaVictoryPointsRewardStatus(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetArenaThisWeekDefendBattleIds()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackArenaOpponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanRevengeArenaOpponent(ulong battleReportInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ArenaOpponent> GetArenaOpponents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetArenaOpponentNextFlushTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaOpponentDefensiveBattleInfo GetArenaOpponentDefensiveBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaPlayerDefensiveTeam GetArenaPlayerDefensiveTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsArenaInSettleTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ArenaBattleReport> GetAllArenaBattleReports()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetArenaPointChangeValue(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerArenaRank()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProArenaTopRankPlayer> GetArenaTopRankPlayers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanReconnectArenaBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnArenaPlayerInfoGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnArenaFlushOpponentsAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<ProBattleHero>, int> EventOnArenaOpponentViewAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnArenaOpponentAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, ProArenaOpponent, List<ProBattleHero>, int> EventOnArenaRevengeOpponentGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnArenaOpponentRevengeAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnArenaOpponentAttackFightingAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleReward, bool> EventOnArenaBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnArenaPlayerSetDefensiveTeamAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleReward> EventOnArenaVictoryPointsRewardGainAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnArenaBattleReportBasicDataGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnArenaBattleReportPlayBackDataGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnArenaTopRankPlayersGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnArenaBattleReconnectAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnArenaBattleReportNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnArenaFlushOpponentsNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroMaxLevelUseExpItemReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendOpenHeroSkinSelfSelectedBoxReq(List<int> indexes, int contentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendOpenSelfSelectedBoxReq(
      int index,
      GoodsType goodsTypeId,
      int contentId,
      int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendUseBagItemReq(GoodsType goodsType, int id, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendSellBagItemReq(ulong instanceId, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendLockAndUnLockEquipmentReq(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendEquipmentEnhanceReq(ulong instanceId, List<ulong> materialIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendEquipmentStarLevelUpReq(ulong instanceId, ulong materialId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBagItemDecomposeReq(List<ProGoods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBagItemComposeReq(int itemId, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendEquipmentEnchantReq(ulong equipmentInstanceId, ulong enchantStoneInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendEquipmentEnchantSaveReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroMaxLevelUseExpItemAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSBagNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSBagExtraNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(UseBagItemAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMessage(OpenHeroSkinSelfSelectedBoxAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMessage(OpenSelfSelectedBoxAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(SellBagItemAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(EquipmentLockAndUnLockAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(EquipmentEnhanceAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(EquipmentStarLevelUpAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BagItemDecomposeAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BagItemComposeAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(EquipmentEnchantAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(EquipmentEnchantSaveAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanDecomposeBagItems(List<ProGoods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanEnchantEquipment(ulong equipmentInstanceId, ulong enchantStoneInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BagItemBase> GetBagItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBagSize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase GetBagItem(GoodsType goodsType, int id, ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase GetBagItemByType(GoodsType goodsType, int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsExistBagItemIgnoreInstanceDifference(GoodsType goodsType, int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase GetBagItemByInstanceId(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBagItemCount(GoodsType goodsType, int id, ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBagItemCountByType(GoodsType goodsType, int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBagItemCountByTypeAndID(GoodsType goodsType, int goodsID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ulong> GetInstanceBagItemInstanceIdsByContentId(int contentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBagItemCountByInstanceId(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUseEnergyMedicine(int itemId, int useCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalculateEnhanceEquipmentExp(List<ulong> materialIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalculateEnhanceEquipmentGold(int exp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CaculateEquipmentNextLevelExp(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CaculateEquipmentNextLevelExp(int contentID, int Level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanEnhanceEquipment(ulong instanceId, List<ulong> materialIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanLevelUpEquipmentStar(ulong instanceId, ulong materialId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLevelTicketsMaxInMission(List<Goods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalcEquipmentDecomposeReturnGold(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEquipmentResonanceNums(ulong equipmentInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEquipmentResonanceNumsByHeroAndEquipment(
      ulong equipmentInstanceId,
      Hero hero,
      List<EquipmentBagItem> equipmentList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEquipmentResonanceNumsByHeroId(ulong equipmentInstanceId, int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int RemoveBagItemByType(
      GoodsType goodsTypeId,
      int contentId,
      int consumeNums,
      GameFunctionType caseId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsShowEnchantCancelConfirmPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEnchantCancelConfirmFlag(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsShowEnchantSaveConfirmPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEnchantSaveConfirmFlag(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasOwn(GoodsType goodsType, int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Goods> GetLevelUpEquipmentStarItems(
      int star,
      ConfigDataEquipmentInfo equipmentInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool NeedUpdatePlayerResource(GoodsType goodsType, int itemId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnBagItemChangeNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnUseBagItemAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnHeroMaxLevelUseExpItemAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnSellBagItemAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnEquipmentLockAndUnLockAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnEquipmentEnhanceAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnEquipmentStarLevelUpAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnBagItemDecomposeAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, List<CommonBattleProperty>> EventOnEquipmentEnchantAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, EquipmentBagItem> EventOnEquipmentEnchantSaveAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnOpenHeroSkinSelfSelectedBoxAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnOpenSelfSelectedBoxAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnBagItemComposeAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleCancelReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleTeamSetReq(int battleId, int gamePlayTeamTypeId, List<int> heroIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleArmyRandomSeedGetReq(int battleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSBattleNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleCancelAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleTeamSetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleArmyRandomSeedGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGameFunctionOpenByMonthCard(GameFunctionType gameFuncTypeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleBase(BattleBase battleBase)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentBattle(ConfigDataBattleInfo battleInfo, BattleType battleType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentArenaBattle(
      ConfigDataArenaBattleInfo battleInfo,
      ConfigDataArenaDefendRuleInfo arenaDefendRule,
      int defenderPlayerLevel,
      List<BattleHero> defenderHeros,
      List<TrainingTech> defenderTechs,
      bool isRevenge)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentArenaBattleReport(ArenaBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentPVPBattle(ConfigDataPVPBattleInfo battleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentRealTimePVPBattle(ConfigDataRealTimePVPBattleInfo battleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentRealTimePVPBattleReport(RealTimePVPBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentPeakArenaBattle(ConfigDataPeakArenaBattleInfo battleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentPeakArenaBattleReport(
      PeakArenaLadderBattleReport battleReport,
      string userId,
      PeakArenaBattleReportSourceType sourceType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentPeakArenaMatchGroupId(int matchGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCurrentBattleFirstBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetBattleTeam(BattleType battleType, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FindGamePlayTeamTypeId(BattleType battleType, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHero CreateBattleHeroFromMyHero(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHero CreateDefaultBattleHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SaveBeforeRewardStatus()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SaveBattleHeroIds(List<int> heroIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RewardPlayerStatus GetBeforeRewardPlayerStatus()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RewardHeroStatus GetBeforeRewardHeroStatus(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProcessingBattle GetProcessingBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaBattleStatus GetArenaBattleStatus()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetArenaBattleId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetNeedRebuildBattle(bool need)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNeedRebuildBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetGainBattleTreasrueIds()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetGainBattleTreasureCount(ConfigDataBattleInfo battleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeBattlePower(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeBattlePower(BattleHero hero, List<TrainingTech> techs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleRandomSeed(int seed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBattleRandomSeed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaBattleRandomSeed(int seed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetArenaBattleRandomSeed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleArmyRandomSeed(int seed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBattleArmyRandomSeed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleFinishedError(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleActor> GetDeadEnemyBattleActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateGeneralInviteInfos(List<GeneralInviteInfo> list)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRemainBattleRegretCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRemainBattleRegretCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UseBattleRegret()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCurrentBattleFailEnergyCost()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetChapterIdFromLevelId(GameFunctionType gameFunctionType, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareInviteTimeout(GeneralInviteInfo i1, GeneralInviteInfo i2)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnBattleCancelAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleTeamSetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleArmyRandomSeedGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTeamBattleRoomCreateReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleRoomPlayerStatusChangeReq(PlayerBattleStatus status)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomQuitNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleRoomHeroSetupReq(int heroId, int position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleRoomHeroSwapReq(int fromPosition, int toPosition)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleRoomHeroPutdownReq(int position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleRoomHeroProtectReq(int position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleRoomHeroBanReq(int position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleRoomEndCurrentBPTurnReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleRoomBattleCommandExecuteReq(List<BattleCommand> commands)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleRoomQuitReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleRoomPlayerActionBeginReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleRoomGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleRoomBattleLogReq(string log)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleRoomBPStageCommandExecuteReq(BPStageCommand cmd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomCreateAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomPlayerStatusChangeAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomHeroSetupAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomHeroSwapAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomHeroPutdownAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomHeroProtectAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomHeroBanAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomEndCurrentBPTurnAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomBattleCommandExecuteAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomQuitAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomPlayerActionBeginAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomBPStageCommandExecuteAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomTeamBattleJoinNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomGuildMassiveCombatBattleJoinNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomPVPBattleJoinNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomRealTimePVPBattleJoinNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomPeakArenaBattleJoinNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomPlayerStatusChangedNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomHeroSetupNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomTeamBattleStartNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomPVPBattleStartNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomRealTimePVPBattleStartNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomPeakArenaBattleStartNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomBattleCommandExecuteNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomBPStageCommandExecuteNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomTeamBattleFinishNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleGuildMassiveCombatBattleFinishNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomPVPBattleFinishNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomRealTimePVPBattleFinishNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomPeakArenaBattleFinishNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomPlayerBattleInfoInitNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomPlayerBPStageInfoInitNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomDataChangeNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInBattleRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInBattleLiveRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ulong GetBattleLiveRoomId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckCanJoinBattleRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoom GetBattleRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetQuitBattleRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnBattleRoomCreateAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomPlayerStatusChangeAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomHeroSetupAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomHeroSwapAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomHeroPutdownAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomHeroProtectAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomHeroBanAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomEndCurrentBPTurnAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomBattleCommandExecuteAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomQuitAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomPlayerActionBeginAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomBPStageCommandExecuteAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomTeamBattleJoinNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomGuildMassiveCombatBattleJoinNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomPVPBattleJoinNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomRealTimePVPBattleJoinNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomPeakArenaBattleJoinNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomPlayerStatusChangedNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleRoomQuitReason> EventOnBattleRoomQuitNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<List<int>> EventOnBattleRoomHeroSetupNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomTeamBattleStartNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomPVPBattleStartNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomRealTimePVPBattleStartNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomPeakArenaBattleStartNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomBattleCommandExecuteNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, ProBPStageCommand> EventOnBattleRoomBPStageCommandExecuteNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomTeamBattleFinishNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomGuildMassiveCombatBattleFinishNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomPVPBattleFinishNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomRealTimePVPBattleFinishNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomPeakArenaBattleFinishNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomPlayerBattleInfoInitNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomPlayerBPStageInfoInitNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomDataChangeNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendChatTextMessage(ChatChannel channel, string content)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendChatExpressionMessage(ChatChannel channel, string content)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendChatVoiceMessage(
      ChatChannel channel,
      byte[] voiceBytes,
      int length,
      int frequency,
      int samples,
      string translateText,
      string destId = "")
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGetVoiceContentMessage(ChatChannel channel, ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendWorldEnterNewRoomMessage(int roomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGetChatContactUserSummaryInfoMessage(List<string> playerIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendChatHeroEquipmentMessage(
      ChatChannel channel,
      string content,
      List<ulong> instanceIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendChatBattleReportMessage(
      ChatChannel channel,
      string content,
      ulong battleReportInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatContactUserSummaryInfoAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSChatNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatMessageNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatMessageSendAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatBannedNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatVoiceContentAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatWorldChannelRoomEnterAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatGuildHistoryMessageNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetChatGroupName(string groupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPrivateChatPlayeName(string gameUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatComponent GetChatComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSendChatMessage(ChatChannel channel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSendDanmaku(DateTime lastDanmakuSendTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ChatComponent.ChatMessageClient> GetChatMessageList(
      ChatChannel channel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetUnReadChatMsgCount4PointPlayerOrGroup(ChatChannel channel, string id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetGroupChatId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGroupChatTarget(string groupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearGroupChatTarget()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPrivateChatTarget(string gameUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CleanPrivateChatTarget()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPrivateChatPlayerId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<string> GetRecentPrivateChatIdList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetNewChatMsgCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetGroupUnreadChatMsgCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAssignGroupUnreadChatMsgCount(string groupID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAssignPrivateUnreadChatMsgCount(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPrivateUnreadChatMsgCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetGuildUnreadChatMsgCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRoomIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReadChat(ChatChannel type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReadChatBeforeDate(ChatChannel type, DateTime date)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshGuildChatLastReadTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetGuildChatLastReadTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public KeyValuePair<List<string>, List<string>> GetRecentChatTargetList(
      ChatChannel channel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearGuildChatContent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public NoticeText GetWaitingNoticeStringListFirstNotice()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetWaitingNoticeStringListCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddNoticeToWaitingNoticeStringList(NoticeText noticeText)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetIsShowingNoticeFlag(bool value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GetIsShowingNoticeFlag()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<ChatMessage> EventOnChatMessageNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnChatContactUserSummaryInfoAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnChatMessageAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ChatVoiceMessage> EventOnChatGetVoiceContentAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnChatEnterRoomAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendChangeOwnerReq(string chatGroupId, string targetUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendChangeNameReq(string chatGroupId, string newName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCreateChatGroupReq(string groupName, List<string> targetUserIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGetChatGroupMemberReq(string chatGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGetAllChatGroupReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendInviteToChatGroupReq(string chatGroupId, List<string> targetUserIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendLeaveChatGroupReq(string chatGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendKickUserReq(string chatGroupId, string targetUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGetUserSummaryReq(
      List<string> targetUserIds,
      Action<int, List<UserSummary>> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatGroupChangeOwnerAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatGroupChangeNameAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatGroupCreateChatGroupAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatGroupGetChatGroupAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatGroupGetChatGroupMemberAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatGroupInviteToChatGroupAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatGroupLeaveChatGroupAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatGroupKickUserAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendGetUserSummaryAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatGroupUpdateNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GetGuildMemberSummary(
      List<string> targetUserIds,
      Action<int, List<UserSummary>> action)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, ProChatGroupInfo, ProChatUserInfo> EventOnCreateChatGroupAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<ProChatGroupCompactInfo>> EventOnGetAllChatGroupAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, ProChatGroupInfo> EventOnGetChatGroupMemberAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnChatGroupChangeOwnerAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnChatGroupChangeNameAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, ProChatGroupInfo, ProChatUserInfo> EventOnInviteToChatGroupAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnLeaveChatGroupAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnKickUserFromGroupAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ProChatGroupInfo> EventOnChatGroupUpdateNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendClimbTowerGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendClimbTowerLevelAttackReq(int floorId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendClimbTowerLevelBattleFinishedReq(int floorId, ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendClimbTowerRaidReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSClimbTowerNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ClimbTowerGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ClimbTowerLevelAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ClimbTowerLevelBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ClimbTowerRaidAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackClimbTowerFloor(int floorId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetClimbTowerNextFlushTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetClimbTowerMaxFloorId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetClimbTowerFinishedFloorId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetClimbTowerRaidMaxFloorId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetClimbTowerHistoryMaxFloorId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ClimbTowerTryRaid(out int reachFloorId, out int costEnergy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GlobalClimbTowerFloor GetClimbTowerFloor(int floorId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNeedFlush()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnClimbTowerGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnClimbTowerLevelAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool, BattleReward> EventOnClimbTowerLevelBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleReward> EventOnClimbTowerRaidAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCollectionWayPointMoveReq(int collectionWayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCollectionScenarioHandleReq(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCollectionLevelAttackReq(GameFunctionType levelType, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCollectionLevelFinishReq(
      GameFunctionType levelType,
      int levelId,
      ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCollectionExchangeReq(ulong activityInstanceId, int exchangeItemId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCollectionEventHandleReq(
      ulong activityInstanceId,
      int collectionWaypointId,
      int collectionEventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCollectionEventAttackReq(
      ulong activityInstanceId,
      int collectionWaypointId,
      int collectionEventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCollectionEventBattleFinishedReq(
      ulong activityInstanceId,
      int collectionWaypointId,
      int collectionEventId,
      ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSCollectionNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(CollectionWayPointMoveAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(CollectionScenarioHandleAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(CollectionLevelAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(CollectionLevelFinishAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(CollectionExchangeAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(AddCollectionEventNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnCollectionEventAdd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DeleteCollectionEventNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnCollectionEventDelete()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(CollectionEventHandleAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(CollectionEventAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(CollectionEventBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCollectionActivityOpened(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCollectionActivityDisplay(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCollectionActivityExchangeTime(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GetCollectionActivityOpenTime(
      int collectionActivityId,
      out DateTime startTime,
      out DateTime endTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetCollectionActivityExchangeEndTime(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanMoveToCollectionActivityWaypoint(int collectionWaypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GetCollectionActivityWaypointState(
      int collectionWaypointId,
      out CollectionActivityWaypointStateType lockState,
      out string graphicState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCollectionActivityWaypointRedPointCount(int collectionWaypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetCollectionActivityPlayerGraphicResource(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanHandleCollectionActivityScenario(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackCollectionActivityLevel(GameFunctionType typeId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackCollectionActivityEvent(int collectionEventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanHandleCollectionActivityEvent(int collectionEventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityScenarioLevelInfo GetNextCollectionActivityScenarioLevelInfo(
      int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityWaypointInfo GetCollectionActivityCurrentWaypointInfo(
      int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCollectionActivityScenarioLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCollectionActivityChallengeLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCollectionActivityLootLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCollectionActivityMaxFinishedLootLevelId(int collectionWaypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCollectionActivityChallengePreLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCollectionActivityLootPreLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCollectionActivityScenarioLevelUnlockDay(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCollectionActivityChallengeLevelUnlockDay(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCollectionActivityChallengeLevelPlayerLevelVaild(
      int levelId,
      out int playerLevelRequired)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCollectionActivityLootLevelUnlockDay(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCollectionActivityLootLevelPlayerLevelVaild(
      int levelId,
      out int playerLevelRequired)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCollectionActivityLootLevelUnlock(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanCollectionActivityExchangeItem(ulong activityInstanceId, int exchangeItemID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ulong FindActivityInstanceIdByCollectionActivityId(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCollectionActivityScoreRewardGot(
      int collectionActivityId,
      int collectionActvityScoreRewardGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCollectionActivityScore(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckChangeActiveNextCollectionActivityScenarioLevelInfo(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityScenarioLevelInfo GetActiveNextCollectionActivityScenarioLevelInfo(
      int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool UpdateActiveCollectionActivityWaypointEvents(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionEvent> GetActiveCollectionActivityWaypointEvents(
      int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionEventInfo GetActiveCollectionActivityEventInfo(
      int collectionWaypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnCollectionWayPointMoveAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleReward> EventOnCollectionScenarioHandleAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnCollectionLevelAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool, BattleReward> EventOnCollectionLevelFinishAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnCollectionExchangeAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCollectionEventAdd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCollectionEventDelete
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleReward> EventOnCollectionEventHandleAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnCollectionEventAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool, BattleReward> EventOnCollectionEventBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGetHeroCommentReq(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCommentHeroReq(int heroId, string content)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPraiseHeroCommentEntryReq(int heroId, ulong entryInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendViewCommenterHeroReq(int heroId, string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroCommentGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroCommentAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroCommentEntryPraiseAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(CommenterHeroViewAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnHeroCommentGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroCommentSendAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroCommentPraisedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, Hero> EventOnCommenterHeroViewAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroComment GetHeroComments(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanPraiseHeroCommentEntry(int heroId, ulong entryInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSCooperateBattleNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCooperateBattleOpened(int cooperateBattleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCooperateBattleDisplayable(int cooperateBattleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCooperateBattleDailyRewardRestCount(int cooperateBattleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCooperateBattleDailyRewardCountMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCooperateBattleInfo GetFirstOpenedCooperateBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCooperateBattleLevelOpened(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackCooperateBattleLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCooperateBattleLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCooperateBattleMaxFinishedLevelId(int cooperateBattleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCooperateBattleMaxOpenedLevelId(int cooperateBattleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendLevelDanmakuGetReq(int gameFunctionTypeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendLevelDanmakuPostReq(
      int gameFunctionTypeId,
      int locationId,
      List<PostDanmakuEntry> entries)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(LevelDanmakuGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(LevelDanmakuPostAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LevelDanmaku GetLevelDanmaku()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<PostDanmakuEntry> GetPostedLevelDanmaku()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PostLevelDanmaku(PostDanmakuEntry entry)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGetLevelDanmaku(int gameFunctionTypeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanPostLevelDanmaku(
      int gameFunctionTypeId,
      int locationId,
      List<PostDanmakuEntry> entries)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearLevelDanmaku()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanUseDanmaku()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnGetLevelDanmakuAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnSendLevelDanmakuAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendEternalShrineLevelAttackReq(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendEternalShrineLevelBattleFinishedReq(int levelId, ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSEternalShrineNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(EternalShrineLevelAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(EternalShrineLevelBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEternalShrineOpened(int eternalShrineId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEternalShrineLevelOpened(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackEternalShrineLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEternalShrineLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEternalShrineMaxFinishedLevelId(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEternalShrineDailyChallengeRestCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEternalShrineDailyChallengeCountMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEternalShrineLevelBlessing(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnEternalShrineLevelAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool, BattleReward> EventOnEternalShrineLevelBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendFixedStoreBuyStoreItemReq(int storeId, int goodsId, int selectedIndex = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSFixedStoreNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FixedStoreBuyStoreItemAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSkinTicket()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMemoryEssence()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FixedStore FindFixedStoreByID(int storeID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<FixedStoreItem> GetFixedStoreItemList(int storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanBuyFixedStoreGoods(int storeId, int goodsId, int selfChooseItemIndex = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsOnDiscount(ConfigDataFixedStoreItemInfo itemInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnFixedStoreBuyStoreItemAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroicMomentBattleReportRemoveReq(ulong battleReportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroicMomentBattleReportAddReq(ulong battleReportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroRandomActionSetReq(bool isActionRandom)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendLikesSendReq(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBusinessCardDescUpdateReq(string desc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBusinessCardHeroSetUpdateReq(List<BusinessCardHeroSet> heroSets)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBusinessCardSelectReq(string targetUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGetSocialRelationReq(FriendSocialRelationFlag Flag = FriendSocialRelationFlag.All)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendFriendInivteReq(List<string> targetUserIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendFriendAcceptReq(List<string> inviterUserIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendFriendDeclineReq(List<string> inviterUserIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendFriendDeleteReq(string targetUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPlayerBlockReq(string targetUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPlayerUnblockReq(string targetUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPlayerFindReq(int targetBornChannelId, string partialName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendSuggestedUserReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattlePracticeInviteReq(
      string targetUserId,
      int battleId,
      PracticeMode practiceMode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattlePracticeAcceptReq(string targetUserId, PracticeMode practiceMode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattlePracticeDeclineReq(string targetUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattlePracticeCancelReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendFriendshipPointsSendReq(List<string> TargetUserIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendFriendshipPointsClaimReq(List<string> TargetUserIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroicMomentBattleReportAddAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroicMomentBattleReportRemoveAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroRandomActionSetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(LikesSendAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BusinessCardSelectAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BusinessCardDescUpdateAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BusinessCardHeroSetUpdateAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateMineBusinessCard(
      bool isDescUpdate,
      bool updateHero,
      bool updateHeroicMomentBattleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSFriendNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendInfoUpdateNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendSocialRelationUpdateNtf Ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendInviteAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendInviteNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendInviteAcceptAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendInviteAcceptNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendInviteDeclineAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendInviteDeclineNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendDeleteAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendBlockAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendUnblockAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendFindAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendSuggestedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendSummaryUpdateNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattlePracticeInviteAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattlePracticeAcceptAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattlePracticeDeclineAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattlePracticeCancelAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattlePracticeInvitedNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattlePracticeFailNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattlePracticeDeclinedNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendshipPointsSendAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendshipPointsClaimAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasBattleReportInHeroMoment(ulong id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ulong> GetHeroMomentBattleReportIdList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSendLikes(string targetUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanSendFriendshipPoint(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanGetFriendshipPoint(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasSentFriendShipPoints(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasReceivedFriendShipPoints(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSendFriendShipPointsCountToday()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetClaimedFriendShipPointsToday()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUpdateBusinessCardDesc(string desc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUpdateBusinessCardHeroSets(List<BusinessCardHeroSet> heroSets)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<UserSummary> GetFriendList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<UserSummary> GetAcrossServerFriendList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<UserSummary> GetRecentChatPlayerList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<UserSummary> GetRecentTeamBattlePlayerList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<UserSummary> GetGuildPlayerList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsAcrossServerFriend(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<UserSummary> GetBlackList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<UserSummary> GetFriendInvitedList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<UserSummary> GetFriendInviteList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<UserSummary> GetRecommendFriendList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<UserSummary> GetFindFriendList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SortUserSummaryList(List<UserSummary> Summaries)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFriend(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetFriendShipPoints()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFriendRedMarkShow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<PVPInviteInfo> GetPVPInviteInfos()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemovePVPInviteInfo(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNeedGetFriendSocialRelation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetNeedGetFriendSocialRelation(bool isNeed)
    {
      // ISSUE: unable to decompile the method.
    }

    public BusinessCard BusinessCard
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBusinessCardRandomShowChangeAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnLikeAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBusinessCardGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBusinessCardDescUpdateAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBusinessCardHeroSetUpdateAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnFriendGetSocialRelationAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, UserSummary> EventOnFriendInviteAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnFriendInviteNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, UserSummary> EventOnFriendInviteAcceptAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<UserSummary> EventOnFriendInviteAcceptNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnFriendInviteDeclineAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<UserSummary> EventOnFriendInviteDeclineNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnFriendDeleteAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPlayerBlockAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPlayerUnblockAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnFriendFindAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnFriendSuggestedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<UserSummary> EventOnFriendSummaryUpdateNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattlePracticeInviteAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattlePracticeAcceptAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattlePracticeDeclineAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattlePracticeCancelAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PVPInviteInfo> EventOnBattlePracticeInvitedNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PracticeMode> EventOnBattlePracticeFailNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattlePracticeDeclinedNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnFriendshipPointsSendAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnFriendshipPointsClaimAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnFriendshipPointsReceivedNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroMomentBattleReportAddAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroMomentBattleReportRemoveAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendNoticeEventGiftReq(List<int> goodsIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPullOrderRewardReq(string orderId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGiftStoreCancelBuyReq(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGiftStoreItemBuyReq(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGiftStoreAppleSubscribeReq(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(NewEventGiftNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(NoticeEventGiftAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PullOrderRewardAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSGiftStoreNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GiftStoreCancelBuyAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GiftStoreBuyGoodsNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GiftStoreOperationalGoodsUpdateNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GiftStoreItemBuyAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GiftStoreAppleSubscribeAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GiftStoreItem> GetEventGiftStoreItemList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GiftStoreItem GetEventGiftStoreItem(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetAllNotNoticedEventGifts()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetEventGiftExpiredTime(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasStrongEventGiftBox()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int EventGiftStoreItemSort(GiftStoreItem itemA, GiftStoreItem itemB)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEventGiftValid(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetFisrtStrongEventGiftBoxItemInfoGoodsId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<OrderReward> GetAllOrderRewards()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GiftStoreItem> GetGiftStoreItemList(bool careShowInStore = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GiftStoreItem GetGiftStoreItem(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GiftStoreItem GetGiftStoreBoughtItem(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGiftStoreItemSellOut(GiftStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanBuyGiftStoreGoods(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TimeSpan GetMonthCardLeftTime(int cardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMonthCardValid(int cardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GetCachedMonthCardState(int cardId, bool defaultState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCachedMonthCardState(int cardId, bool state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetMonthCardPrefName(int cardId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, int> EventOnGiftStoreItemBuyAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnGiftStoreAppleSubscribeAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnGiftStoreBuyGoodsNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnGiftStoreOperationalGoodsUpdateNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnGiftStoreItemCancelBuyAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, string, OrderReward> EventOnPullOrderRewardAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGmCommandReq(string cmd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GenerateAddItemGmCommand(GoodsType goodsType, int id, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GenerateRemoveItemGmCommand(GoodsType goodsType, int id, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GmCommandAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnGmCommandAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GuildCreateReq(
      string guildName,
      string hiringDeclaration,
      string announcement,
      bool autoJoin,
      int joinLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GuildJoinApplyReq(string guildId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GuildJoinAdminConfirmReq(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GuildJoinApplyRefuseReq(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GuildJoinPlayerConfirmReq(string guildID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GuildSearchReq(string searchText)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GuildRandomListReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GuildQuitReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildInvitePlayerListReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildHiringDeclarationSetReq(string hiringDeclaration)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildJoinInvitationGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildJoinInvitationRefuseReq(string guildID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendAllGuildJoinInvitationRefuseReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildGetReq(string guildId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildBasicSetReq(bool autoJoin, int joinLevel, string hiringDeclaration)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildAnnouncementSetReq(string announcement)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildJoinApplyGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildNameChangeReq(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildLogGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildJoinInviteReq(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildVicePresidentAppointReq(string userId, bool appoint)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildKickOutReq(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildPresidentRelieveReq(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildPresidentAppointReq(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendAllGuildJoinApplyRefuseReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildUpdateAnnouncementNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSGuildNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildCreateAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildJoinApplyAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildJoinApplyConfirmAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildJoinApplyRefuseAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildJoinInvitationConfirmAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildSearchAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildRandomListAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildQuitAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildInvitePlayerListAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildHiringDeclarationSetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildJoinInvitationGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildJoinInvitationRefuseAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(AllGuildJoinInvitationRefuseAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildBasicSetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildAnnouncementSetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildJoinApplyGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildNameChangeAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildLogGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildJoinInviteAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildVicePresidentAppointAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildKickOutAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildPresidentRelieveAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildPresidentAppointAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(AllGuildJoinApplyRefuseAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildJoinApplyAdminNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildJoinInvitationNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckGuildCreateCondition(string guildName, string hiringDeclaration, int joinLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Guild GetGuildInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshGuildListJoinState(string guildID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanQuitGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanKickOutGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckGuildName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSetGuildHiringDeclaration(string hiringDeclaration)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSetGuildAnnouncement(string announcement)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckGuildInvitePlayerList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GuildJoinInvitation> GetGuildJoinInvitationList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<UserSummary> GetGuildJoinApplyList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RemoveGuildJoinApplyListById(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckGuildSearch(string searchText)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckGuildRandomList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GuildSearchInfo> GetGuildRecommendList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetGuildId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetGuildLogContent(GuildLog log)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddUserIdToGuildInviteList(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TimeSpan CanJoinGuildCDTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasPlayerBeenInvitedByGuild(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetComponentGuildData()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnGuildCreateAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildJoinApplyAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildJoinAdminConfirmAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildJoinApplyRefuseAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildJoinPlayerConfirmAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<GuildSearchInfo>> EventOnGuildSearchAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<GuildSearchInfo>> EventOnGuildRandomListAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildQuitAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<UserSummary>> EventOnGuildInvitePlayerListAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildHiringDeclarationSetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<GuildJoinInvitation>> EventOnGuildJoinInvitationGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildJoinInvitationRefuseAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnAllGuildJoinInvitationRefuseAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, Guild> EventOnGuildGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildBasicSetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildAnnouncementSetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<UserSummary>> EventOnGuildJoinApplyGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildNameChangeAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<GuildLog>> EventOnGuildLogGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildJoinInviteAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildVicePresidentAppointAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildKickOutAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildPresidentRelieveAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildPresidentAppointAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnAllGuildJoinApplyRefuseAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GuildLog> EventOnGuildUpdateInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildMassiveCombatDataReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildMassiveCombatStartReq(int difficulty)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildMassiveCombatSurrenderReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildMassiveCombatConquerReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildMassiveCombatAttackReq(int levelId, List<int> heroIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildMassiveCombatAttackFinishedReq(int levelId, ProBattleReport report)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildMassiveCombatDataAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildMassiveCombatNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildMassiveCombatPlayerNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildMassiveCombatStartAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildMassiveCombatSurrenderAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildMassiveCombatConquerAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildMassiveCombatAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildMassiveCombatAttackFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetGuildMassiveCombatStrongholdEliminateRate(GuildMassiveCombatStronghold stronghold)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGuildMassiveCombatStrongholdTakeOver(int levelId, ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetStartedGuildMassiveCombatThisWeek()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanStartGuildMassiveCombat(int Difficulty)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackGuildMassiveCombatSinglePVELevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildPlayerMassiveCombatInfo GetGuildPlayerMassiveCombatInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetUIntMassiveCombatPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsShowGuildMassiveCombatFirstOpenRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FillGuildMassiveCombatMemberSummary(
      GuildMassiveCombatGeneral Info,
      Action OnComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnFillGuildMassiveCombatNtfSummary(FriendGetUserSummaryAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnFillGuildMassiveGetDataSummary(List<UserSummary> users, Action OnComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMassiveCombatInfo GetGuildMassiveCombatInfo(ulong instanceId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMassiveCombatGeneral GetGuildMassiveCombatGeneral()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetGuildMassiveCombatEliminateRate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMassiveCombatStronghold GetGuildMassiveCombatStronghold(
      int levelId,
      ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsGuildMassiveCombatConquered(GuildMassiveCombatInfo combatInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsGuildMassiveCombatFinished(GuildMassiveCombatInfo combatInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGuildMassiveCombatConqueredAndFinished(GuildMassiveCombatInfo combatInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGuildMassiveCombatConqueredAndNotFinished(GuildMassiveCombatInfo combatInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGuildMassiveCombatSurrender(GuildMassiveCombatInfo combatInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnGuildMassiveCombatAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool, BattleReward> EventOnGuildMassiveCombatAttackFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, GuildMassiveCombatGeneral> EventOnGuildMassiveCombatDataAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GuildMassiveCombatGeneral> EventOnGuildMassiveCombatNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GuildPlayerMassiveCombatInfo> EventOnGuildMassiveCombatPlayerNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildMassiveCombatStartAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildMassiveCombatSurrenderAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildMassiveCombatConquerAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GuildMassiveCombatInfo> EventOnGuildMassiveCombatStartNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GuildMassiveCombatInfo> EventOnGuildMassiveCombatSurrenderNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GuildMassiveCombatInfo> EventOnGuildMassiveCombatConqueredNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendOpenHeroJobRefineryReq(int heroId, int jobConnectionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroJobRefineReq(
      int heroId,
      int jobConnectionId,
      int slotId,
      int index,
      int stoneId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroJobRefineSaveReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendLevelUpHeroHeartFetterLevelReq(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendUnlockHeroHeartFetterReq(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendAutoTakeOffEquipmentsReq(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendExchangeHeroFragmentReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroFavorabilityExpAddReq(
      int heroId,
      GoodsType goodsType,
      int goodId,
      int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroInteractReq(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroConfessReq(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendSetProtagonistReq(int protagonistId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroJobTransferReq(int heroId, int jobConnectionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroStarLevelUpReq(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroJobLevelUpReq(int heroId, int jobConnectionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroSkillsSelectReq(
      int heroId,
      List<int> skillsId,
      SkillAndSoldierSelectMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroSoldierSelectReq(int heroId, int soldierId, SkillAndSoldierSelectMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroComposeReqq(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroExpAddReq(int heroId, GoodsType goodsType, int itemId, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroJobUnlockReq(int heroId, int jobRelateId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendEquipmentWearReq(int heroId, ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendEquipmentTakeOffReq(int heroId, int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendAutoEquipReq(int heroId, List<ulong> equipmentInstanceIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroFetterUnlockReq(int heroId, int fetterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroFetterLevelUp(int heroId, int fetterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCharSkinWear(int heroId, int charSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCharSkinTakeOff(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendModelSkinWear(int heroId, int jobRelatedId, int modelSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendModelSkinTakeOff(int heroId, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendSoldierSkinWearReq(
      int heroId,
      int soldierId,
      int soldierSkinId,
      bool isSetToAll)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendSoldierSkinTakeOffReq(int heroId, int soldierId, bool isAll)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(OpenHeroJobRefineryAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroJobRefineAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroJobRefineSaveAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(UnlockHeroHeartFetterAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(LevelUpHeroHeartFetterLevelAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(AutoTakeOffEquipmentsAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ExchangeHeroFragmentAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSHeroNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroConfessAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroInteractAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroFavorabilityExpAddAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(SetProtagonistAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroJobTransferAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroStarLevelLevelUpAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroJobLevelUpAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroSkillsSelectAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroSoldierSelectAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroComposeAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroExpAddAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroJobUnlockAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(EquipmentWearAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(EquipmentTakeOffAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(EquipmentAutoAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroFetterLevelUpAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroFetterUnlockAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(CharSkinWearAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(CharSkinTakeOffAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ModelSkinWearAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ModelSkinTakeOffAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(SoldierSkinWearAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(SoldierSkinTakeOffAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GmResetHeroJob(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GmLevelUpHeroFetter2SpecificLevel(int heroId, int fetterId, int reachLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GmLevelUpHeroHeartFetter(int heroId, int reachLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAdditiveHeroAddExp(int addExp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAdditiveHeroFavourabilityAddExp(int addExp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroFavorabilityUpLevel(
      Hero hero,
      ConfigDataHeroInformationInfo heroInformationInfo,
      int addExp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroInteractionInfo GetHeroInteractionInfo(
      int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroInteractHeroPerformanceId(int heroId, HeroInteractionResultType resultType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitAllUseableDefaultConfigHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Hero CreateDefaultHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Hero GetHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Hero> GetHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, Hero> GetAllUseableDefaultHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetUseableJobConnectionInfos(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroNextLevelExp(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroSkillPointMax(int heroLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroLevelMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroShowRedMark(int heroId, int jobConnectionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanLevelUpHeroStarLevel(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IfWasteAddExp(Hero hero, int addExp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanLevelUpHeroJobLevel(int heroId, int jobConnectionid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanHeroJobTransfer(int heroId, int jobConnectionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsWorldHeroTabShowRedIcon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroHaveNewJobCanTransfer(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroJobNeedMagicStone(int heroId, int jobConnectionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanComposeHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSkillLimitToHeroJob(int jobRelatedId, int skillId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetProtagonistId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsProtagonistHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsProtagonistExist()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCurrentLevelMaxHeroLevel(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanHeroSelectSolider(Hero hero, int soliderId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroInteractNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetHeroInteractNumsFlushTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroCanComposed(int heroID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsExistSoliderId(int soliderId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroAssigned(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<HeroFragmentBagItem> GetAllStarLvlMaxHeroFragements()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsShowJobLevelCanUpTip(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSelectSkillHero(Hero hero, List<int> skills)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasIgnoreCostEquipment(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEquipmentWeared(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetWearedEquipmentHeroIdByEquipmentId(ulong equipmentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanWearEquipment(int heroId, ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasBetterEquipmentByHero(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasBetterEquipmentBySlotId(int heroId, int slotId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ulong[] GetBestEquipments(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeBattlePower(Hero hero, ulong[] equipmentIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeBattlePower(Hero hero, List<EquipmentBagItem> quipmentList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAutoEquipment(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanTakeOffEquipment(int heroId, int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnlockHeroHeartFetter(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanLevelUpHeroHeartFetter(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnlockHeroPerformance(int heroPerformanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnlockHeroBiography(int biographyId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanReachFetterUnlockCondition(HeroFetterCompletionCondition condition)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanInteractHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanConfessHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanLevelUpHeroFetter(int heroId, int fetterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnlockHeroFetter(int heroId, int fetterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ImitateUseHeroFavorabilityExpItem(
      int heroId,
      int itemId,
      int nums,
      GoodsType goodsType = GoodsType.GoodsType_Item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFullFavorabilityExp(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsShowFetterRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroFetterHasNewOrLevelUp(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanWearCharSkin(int heroId, int charSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanTakeOffCharSkin(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanWearModelSkin(int heroId, int jobRelatedId, int modelSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanTakeOffModelSkin(int heroId, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanWearSoldierSkin(int heroId, int soldierId, int soldierSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanTakeOffSoldierSkin(int heroId, int soldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsShowSkinBeforeOnSale(int fixedStoreItemId, bool isHeroSkin)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanOpenHeroJobRefineryReq(int heroId, int jobConnectId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsJobLevelMax(HeroJob heroJob)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsHideEquipMaster
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool DonotShowEquipMasterCancelConfirmPanelOnCloseThisLogin
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool DonotShowEquipMasterCancelConfirmPanelOnClickStoneThisLogin
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool DonotShowEquipMasterSaveConfirmPanelThisLogin
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnSetProtagonistAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroJobTransferAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroStarLevelUpAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroJobLevelUpAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroSkillsSelectAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroSoldierSelectAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroComposeAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroExpAddAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroJobUnlockAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnEquipmentWearAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnEquipmentTakeOffAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnAutoEquipAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroFetterLevelUpAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnHeroConfessAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnHeroFavorabilityExpAddAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnFetterUnlockAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<HeroInteractAck, List<Goods>, int> EventOnHeroInteractAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnCharSkinWearAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnCharSkinTakeOffAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnModelSkinWearAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnModelSkinTakeOffAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnSoldierSkinWearAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnSoldierSkinTakeOffAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnExchangeHeroFragementAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnAutoTakeOffEquipmentsAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnUnlockHeroHeartFetterAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnLevelUpHeroHeartFetterLevelAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnOpenHeroJobRefineryAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, HeroJobSlotRefineryProperty> EventOnHeroJobRefineAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroJobRefineSaveAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnHeroChangeNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroAnthemLevelAttackReq(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroAnthemLevelBattleFinishedReq(int levelId, ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSHeroAnthemNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroAnthemLevelAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroAnthemLevelBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackHeroAnthemLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroAnthemLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroAnthemMaxFinishedLevelId(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasHeroAnthemLevelAchievement(int achievementRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroAnthemLevelAchievementCount(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroAnthemChapterAchievementCount(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnHeroAnthemLevelAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event ProjectLPlayerContext.HeroAnthemLevelBattleFinishedAckCallback EventOnHeroAnthemLevelBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroAssistantAssignToTaskReq(
      int TaskId,
      int Slot,
      List<int> HeroIds,
      int WorkSeconds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroAssistantCancelTaskReq(int TaskId, int Slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroAssistantClaimRewardReq(int TaskId, int Slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSHeroAssistantNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroAssistantAssignToTaskAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroAssistantCancelTaskAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroAssistantClaimRewardAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<HeroAssistantsTask> GetAllTodayHeroAssistantsTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<HeroAssistantsTask> GetHeroAssistantsTasksByWeekDay(
      int weekDay)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<HeroAssistantsTaskAssignment> GetAssignedHeroAssistantsTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HaveFinishedAssistantTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDropIdByTaskCompleteRate(int taskId, int completeRate)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDropCountByTaskWorkSeconds(int taskId, int workSeconds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAssignHero(List<int> canAssignHero, int taskId, int slot, int workSeconds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanCancelTask(int taskId, int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanClaimRewards(int taskId, int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TimeSpan GetTaskRemainingTime(int taskId, int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnHeroAssistantAssignToTaskAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroAssistantCancelTaskAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnHeroAssistantClaimRewardAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroDungeonLevelAttackReq(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroDungeonLevelBattleFinishedReq(
      int HeroDungeonLevelId,
      ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroDungeonLevelRaidReq(int levelId, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroDungeonChapterStarRewardGainReq(int chapterId, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSHeroDungeonNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroDungeonLevelAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroDungeonLevelBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroDungeonLevelRaidAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroDungeonChapterStarRewardGainAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnLockHeroDungeonLevel(int levelID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsHeroDungeonLevelAttachUnlockLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackHeroDungeonLevel(int levelID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroDungeonLevelChallengeNum(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroDungeonLevelCanChallengeNum(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroDungeonLevelCanChallengeMaxNum(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanRaidHeroDungeonLevel(int levelID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroDungeonLevelStars(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroDungeonLevelAchievementCount(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasHeroDungeonLevelAchievement(int achievementID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroDungeonChapterGainStars(int chapterID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroDungeonChapterAllStars(int chapterID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GainRewardStatus GetHeroDungeonStarRewardStatus(int chapterID, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGainHeroDungeonStarReward(int chapterID, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLevelChallenged(int levelID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroDungeonNewMarkShow(int heroDungeonChapterID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int HeroDungeonProgress(int heroDungeonChapterID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroDungeonDailyChallengeMaxNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroDungeonLevelFinihed(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroDungeonMaxFinishedLevelId(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnHeroDungeonLevelAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleReward, List<Goods>> EventOnHeroDungeonLevelRaidAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleReward> EventOnHeroDungeonRewardGainAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event ProjectLPlayerContext.HeroDungeonLevelBattleFinishedAckCallback EventOnHeroDungeonLevelBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroPhantomAttackReq(int LevelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroPhantomBattleFinishedReq(int LevelId, ProBattleReport Report)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSHeroPhantomNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroPhantomAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroPhantomBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroPhantomDisplay(int heroPhantomID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroPhantomOpened(int heroPhantomID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroPhantomLevelOpened(int levelID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackHeroPhantomLevel(int levelID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroPhantomLevelFinihed(int levelID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroPhantomMaxFinishedLevelId(int heroPhantomID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCompleteHeroPhantomLevelAchievement(int achievementRelatedInfoID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroPhantomAchievementCount(int heroPhantomID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroPhantomLevelAchievementCount(int levelID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroPhantomLevelFirstCleanComplete(int levelID)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnHeroPhantomAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event ProjectLPlayerContext.HeroPhantomBattleFinishedAckCallback EventOnHeroPhantomBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroTrainningLevelAttackReq(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroTrainningLevelBattleFinishedReq(int levelId, ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSHeroTrainningNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroTrainningLevelAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroTrainningLevelBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroTrainningOpened(int heroTrainningId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroTrainningLevelOpened(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackHeroTrainningLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroTrainningTicketCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroTrainningLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroTrainningMaxFinishedLevelId(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroTrainingDailyRewardRestCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroTrainingDailyRewardCountMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroTrainingLevelBlessing(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnHeroTrainningLevelAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool, BattleReward> EventOnHeroTrainningLevelBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendLevelWayPointMoveReq(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendLevelScenarioHandleReq(int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendLevelWayPointBattleFinishReq(int waypointId, ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSLevelNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(LevelWayPointMoveAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(LevelScenarioHandleAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(LevelWayPointBattleFinishAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RandomEventNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnRandomEventUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AppendChangedGoodsToRewards(List<ProGoods> changedGoods, List<Goods> rewards)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void AppendGoodsToRewards(
      GoodsType goodsType,
      int id,
      int count,
      List<Goods> rewards)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetNotBagItemCount(Goods goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRegionOpen(int regionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataWorldMapInfo GetCurrentWorldMapInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataWaypointInfo GetPlayerCurrentWaypointInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WayPointStatus GetWaypointStatus(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetWaypointEvent(
      int waypointId,
      out ConfigDataEventInfo eventInfo,
      out RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackEventWaypoint(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackScenario(int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ConfigDataScenarioInfo GetLastFinishedScenarioInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ConfigDataScenarioInfo GetNextScenarioInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScenarioFinished(int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnlockScenario(int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitActiveScenarioInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckChangeActiveScenario()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataScenarioInfo GetActiveNextScenarioInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataScenarioInfo GetActiveLastFinsishedScenarioInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateActiveWaypointEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, ProjectLPlayerContext.CurrentWaypointEvent> GetActiveWaypointEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEventInfo GetActiveWaypointEventInfo(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomEvent GetActiveWaypointRandomEvent(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnterWorldUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFirstEnterWorldUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, BattleReward, bool> EventOnLevelWayPointMoveAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleReward> EventOnLevelScenarioHandleAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool, BattleReward> EventOnLevelWayPointBattleFinishAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRandomEventUpdate
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendMailsGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendMailReadReq(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendMailAttachmentsGetReq(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendMailAttachmentAutoGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSMailNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(MailsGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(MailsChangedNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(MailReadAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(MailAttachmentsGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(MailAttachmentAutoGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mail> GetMails()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMailReaded(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsExistMailAttacments(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasGotMailAttachments(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public double GetMailExpiredTime(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetUnReadMailCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGetMailAttachment(ulong mailId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAutoGetMailAttachment()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsOfficialMail(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsHaveMailList
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnMailsGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnMailsChangedNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnMailReadAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnMailAttachmentsGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnMailAttachmentsAutoGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendMemoryCorridorLevelAttackReq(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendMemoryCorridorLevelBattleFinishedReq(int levelId, ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSMemoryCorridorNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(MemoryCorridorLevelAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(MemoryCorridorLevelBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMemoryCorridorOpened(int MemoryCorridorId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMemoryCorridorLevelOpened(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackMemoryCorridorLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMemoryCorridorTicketCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMemoryCorridorLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMemoryCorridorMaxFinishedLevelId(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMemoryCorridorDailyRewardRestCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMemoryCorridorDailyRewardCountMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMemoryCorridorLevelBlessing(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnMemoryCorridorLevelAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool, BattleReward> EventOnMemoryCorridorLevelBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGetMissionRewardReq(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DirectelyActivitedMissionUpdateNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSMissionNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GetMissionRewardAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, List<Goods>> EventOnMissionRewardGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAllProcessingMissionList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAllCompletedMissionList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAllFinishedMissionList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMissionMaxProcess(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public long GetMissionCompleteProcess(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsExistMissionCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGainMissionReward(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanGetMissionRewarding(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendNoviceClaimRewardReq(int Slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSNoviceNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(NoviceClaimRewardAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDaysAfterCreation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, List<int>> GetNoviceMissions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetNoviceProcessingMissions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsShowRedMarkOnOpenServiceButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HaveFinishedNotGetNoviceMissions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HaveFinishedNotGetNoiviceMissionsByDay(int day)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetFinishedAndGetRewardNoviceMissions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetNoviceMissionPoints()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataNoviceRewardInfo> GetNovicePointsRewardsConfig()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanClaimNoviceReward(int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TimeSpan GetNoviceMissionsEndTime()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, List<Goods>> EventOnNoviceClaimRewardAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendOperationalActivityGainRewardReq(
      ulong operationalActivityInstanceId,
      int rewardIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendOperationalActivityExchangeItemGroupReq(
      ulong operationalActivityInstanceId,
      int itemGroupIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRedeemInfoReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRedeemClaimReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendFansRewardsFromPBTCBTInfoReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendFansRewardsFromPBTCBTClaimReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(OperationalActivityGainRewardAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(OperationalActivityExchangeItemGroupAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(AdvertisementFlowLayoutUpdateNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(WebControlInfoUpdateNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSOperationalActivityNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RemoveAnnouncementNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSAnnouncementNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(NewOperationalActivityNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(NewAnnouncementNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(UpdateOperationalActivityNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(NewMarqueeNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RedeemInfoAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RedeemClaimAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FansRewardsFromPBTCBTInfoAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FansRewardsFromPBTCBTClaimAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Announcement> GetAllCurrentAnnouncements()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRedeemInfo(RedeemInfoAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRedeemClaimed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RedeemInfoAck GetRedeemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRedeemClaimed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<OperationalActivityBase> GetAllCurrentActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanShowActivity(OperationalActivityBase activity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanActivityGainReward(
      OperationalActivityBase operationalActivityBase,
      int rewardIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanActivityGainRewardResult(
      OperationalActivityBase operationalActivityBase,
      int rewardIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase FindOperationalActivityById(
      ulong operationalActivityInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanExchangeGoodsResult(
      OperationalActivityBase operationalActivityBase,
      int itemGroupIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTabRedPointShow(OperationalActivityBase activity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetWebActivityTagNameInPDSDK(OperationalActivityBase activity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetWebActivityTagIdInPDSDK(string tag)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsActivityRedPointShow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<AdvertisementFlowLayout> GetAllAdvertisementFlowLayouts()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFirstTryToOpenActivityNoticeUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HaveActivityNotice()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBuyGuideActivityViewed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsWebInfoEnable()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnActivityGainRewardAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnActivityExchangeItemGroupAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Marquee> EventOnNewMarqueeNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnActivityRedeemClaimAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnActivityFansRewardsFromPBTCBT
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendNoticeEnterPlayOffRaceReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaBattleLiveRoomMatchInfoGetReq(ulong roomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaBattleLiveRoomJoinReq(int matchId, ulong roomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaBattleLiveRoomQuitReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaBetReq(int matchupId, string userid, int jettonNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaBattleReportShare2FriendsReq(
      ulong battleReportId,
      List<string> friendUserIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaBattleReportChangeNameReq(ulong battleReportId, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaPublicHeroesPoolReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaBattleTeamSetupReq(PeakArenaBattleTeam battleTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaBattleTeamGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaGetTopPlayersReq(int n)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaGetInfoReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaPlayOffCurrentSeasonMatchInfoGetReq(int matchupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaAcquireWinsBonusReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaPlayOffInfoGetReq(int sequenceId, int matchIndex, int season)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaPlayOffLiveRoomGetReq(int season)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaMyPlayOffInfoGetReq(int season)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaBattleReportsGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleReportBattleInfoGetReq(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaPlayOffMatchSetReadyReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaPlayOffBattleSetReadyReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaDanmakuReq(string text)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaSeasonSimpleInfoReq(int seasonId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(NoticeEnterPlayOffRaceAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaBattleLiveRoomMatchInfoGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaBattleNameBanTimeUpdateNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaBattleLiveRoomQuitAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleLiveRoomFinishNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaBattleLiveRoomJoinAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaBetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaBattleReportShare2FriendsAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaBattleReportChangeNameAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSPeakArenaNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaDataUpdateNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaBattleTeamGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaBattleTeamSetupAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaPublicHeroesPoolAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaGetTopPlayersAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaGetInfoAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaPlayOffCurrentSeasonMatchInfoGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaAcquireWinsBonusAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaAcquireRegularRewardsAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaPlayOffInfoGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaPlayOffLiveRoomInfoGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaMyPlayOffInfoGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaBattleReportsGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleReportBattleInfoGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaPlayOffMatchSetReadyAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaPlayOffBattleSetReadyAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaDanmakuAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaDanmakuNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaMatchFinishNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaShakeHandsNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaPlayerForfeitNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaSeasonSimpleInfoAck ack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDailyJettonUseCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanBuyJetton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanBetPeakArenaCare4Round(int round, int matchupId, string userId, int jettonNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<PeakArenaHeroWithEquipments> GetPeakArenaPublicHeroesPool()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaHeroWithEquipments GetPeakArenaPublicHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleHeroEquipment> GetPeakArenaPublicBattleHeroEquipments(
      int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHero PeakArenaMercenaryToBattleHero(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPeakArenaMercenaryInBattleTeam(int heroID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPeakArenaMercenarySoldierChange(int heroID, int soldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPeakArenaMercenarySkillChange(int heroId, List<int> skillIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMyHeroInPeakArenaBattleTeam(int heroID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPeakArenaBattleTeamAllHeroCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaHiredHero GetPeakArenaMercenaryInBattleTeam(int heroID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPeakArenaTeamManagementComplete()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProUserSummary> GetPeakArenaTopRankPlayerSummaries()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProPeakArenaLeaderboardPlayerInfo> GetPeakArenaTopRankPlayerInfos()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPeakArenaPlayerDan()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPeakArenaPlayerRank()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPeakArenaPlayerScore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPeakArenaPlayerQuitCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaSeasonInfo GetPeakArenaSeasonInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPeakArenaSeasonId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetPeakArenaSeasonEndTime(int Season)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFinalRoundKnockOutMatchsFinish()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CalcPeakArenaSeasonId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public EPeakArenaDurringStateType GetCurPeakArenaStateType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPeakArenaPlayOffOverDayEndTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleTimeIsOverEndTimeInSameDay(
      DateTime srcTime,
      DateTime tarTime,
      int minutes,
      ref DateTime cachedDate,
      ref bool result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDuringPeakArenaSeason()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TimeSpan CalcTimeBeforePeakArenaSeasonBegin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetPeakArenaSeasonBeginTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInPeakAreanPreRankingState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPlayerFinishPreRankingState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPeakArenaConsecutiveWins(RealTimePVPMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaMatchStats GetPeakArenaMatchStats()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAlreadyGainPeakArenaWeekFirstWinReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetPeakArenaPointRaceEndTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPeakArenaInKnockout()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPeakArenaKnockoutRound()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPeakArenaKnockoutRoundAboutToStart(int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDuringPeakArenaKnockoutRoundMatch(int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPeakArenaKnockoutRoundStart(int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetPeakArenaPlayOffRoundStartTime(int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetPeakArenaPlayOffRoundEndTime(int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsKnockOutMatchRoundEnd(int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerKnockoutGroupId(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffMatchupInfo GetPeakArenaGroupRootKnockoutInfo(
      int groupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<PeakArenaPlayOffMatchupInfo> GetAllPeakArenaKnockoutInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<PeakArenaPlayOffMatchupInfo> GetPeakArenaMatchupInfosByRound(
      int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffMatchupInfo GetPeakArenaNextMatchInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMineReadyAtKnockOutMatch()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPeakArenaGroupId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPeakArenaKnockOutGroupName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool DoesPlayerHaveNextKnockOutMatch()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPlayerHaveBeenInPeakArenaKnockOutMatch()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerFinalRound()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPlayerFinalRoundTitleName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetCurPeakArenaSeasonKnockOutMatchTitleName(int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaKnockoutMatchTime GetCurPeakArenaSeasonKnockOutMatchTime(
      int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaKnockoutMatchTime GetPeakArenaSeasonKnockOutMatchTime(
      int round,
      int seasonID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataPeakArenaKnockoutMatchTime> GetPeakArenaSeasonKnockoutTimeTable(
      int season)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPeakArenaMatchmakingAvailable(RealTimePVPMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPeakArenaMatchmakingAvailableWithOutGameFunction(RealTimePVPMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPeakArenaDanMaxDisplayDan()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleReportHead> GetAllPeakArenaBattleReports()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleReportHead GetPeakArenaBattleReport(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNeedGetPeakArenaBattleReports()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<PeakArenaBattleReportNameRecord> GetPeakArenaBattleReportNameRecords()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBattleReportNameRecord GetPeakArenaBattleReportNameRecord(
      ulong reportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPeakArenaBattleReportStateName(
      PeakArenaLadderBattleReport battleReport,
      string sourceUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPeakArenaPlayOffRoundText(int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetBattleReportDefaultName(
      PeakArenaLadderBattleReport battleReport,
      string sourceUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetBattleReportStateTypeName(
      PeakArenaLadderBattleReport battleReport,
      string sourceUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetBattleReportStateLevelName(
      PeakArenaLadderBattleReport battleReport,
      string sourceUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GetNoticeEnterPlayOffRaceTag()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, PeakArenaBattleTeam> EventOnPeakArenaBattleTeamGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaBattleTeamSetupAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<PeakArenaHeroWithEquipments>> EventOnPeakArenaPublicHeroesPoolAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaGetTopPlayersAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaGetInfoAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, PeakArenaPlayOffMatchupInfo> EventOnPeakArenaPlayOffCurrentSeasonMatchInfoGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleReward> EventOnPeakArenaAcquireWinsBonusAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaAcquireRegularRewardsAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, int, long> EventOnPeakArenaPlayOffInfoGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<ProPeakArenaLiveRoomInfo>> EventOnPeakArenaPlayOffLiveRoomInfoGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaMyPlayOffInfoGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaBattleReportShare2FriendsAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaBattleReportsGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleReportHead> EventOnBattleReportBattleInfoGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaPlayOffMatchSetReadyAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaPlayOffBattleSetReadyAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaBattleReportChangeNameAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaBattleLiveRoomJoinAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaBattleLiveRoomQuitAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaBetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattleLiveRoomFinishReason> EventOnBattleLiveRoomFinishNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaDanmakuAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, PeakArenaBattleLiveRoomInfo> EventOnPeakArenaBattleLiveRoomInfoAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, string> EventOnPeakArenaDanmakuNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventNoticeEnterPlayOffRaceAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnPeakArenaMatchFinishNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnPeakArenaShakeHandsNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnPeakArenaPlayerForfeitNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PeakArenaSeasonSimpleInfoAck> EventOnPeakArenaSeasonSimpleInfoAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGainDialogRewardReq(int dialogId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendJettonBuyReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendOpenMemoryStoreReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendShareReq(int heroId, int archiveId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendClientDecidedAchievementReq(int param1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCheckOnlineReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool SendPlayerInfoInitReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool SendPlayerInfoReqOnReloginBySession(bool isOnlyCheck)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private PlayerInfoInitReq BuildPlayerInfoInitReq(bool sendDSVersion)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCreateCharactorReq(string nickName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendLanguageSetReq(string language)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBuyEnergyReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBuyArenaTicketsReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeadPortraitSetReq(int headPortraitId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeadFrameSetReq(int headFrameId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeadPortraitAndHeadFrameSetReq(int headPortraitId, int headFrameId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendChangePlayerNameReq(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendExchangeGiftCDKeyReq(string key)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendClientHeartBeatNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendClientAppPauseNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendClientAppResumeNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendOpenGameRatingReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GainDialogRewardAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(JettonBuyAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroErrorMsgNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WriteHeroErrorFileLog(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(OpenMemoryStoreAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DailyFlushNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ShareAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ClientDecidedAchievementAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeadPortraitAndHeadFrameSetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RMBUpdateNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FlushConfigNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ExchangeGiftCDKeyAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PlayerInfoInitAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(CreateCharactorAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSPlayerBasicNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PlayerInfoInitEndNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ProChangedGoodsNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BuyEnergyAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaTicketsBuyAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PlayerNameChangeAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GameRatingOpenAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMessage(CheckOnlineAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetChangedGoodsListStatus(List<ProGoods> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetChangedGoodsStatus(
      ProGoods goods,
      ref bool bagChange,
      ref bool playerInfoChange,
      ref bool heroChange)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase SetBagItemPropertyChanges(ProGoods bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMemoryStoreOpen()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetEnergyReachMaxTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetArenaTicketNextGivenTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetBlackMarketNextFlushTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(UpdateServerTimeNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitPlayerInfo(PlayerInfoInitAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateServerTime(long serverTimeTicks)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitPlayerBasic(DSPlayerBasicNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetGold()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDailyJettonBuyCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public long GetRechargeRMB()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCrystal()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetGuildMedal()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMithralStone()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetChallengePoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetFashionPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBrillianceMithralStone()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEnergy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEnergyMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetCreateTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetCreateTimeUtc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanBuyEnergy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBuyEnergyNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetEnergyFlushTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanBuyArenaTickets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBuyArenaTicketsNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPlayerLevelMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerExp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerNextLevelExp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerVip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerHeadIcon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerHeadPortrait()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerHeadFrame()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int SetUserGuide(List<int> completeStepIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPlayerName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckPlayerName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanChangePlayerName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetUserId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMe(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetNeedBattleReportLog(bool isNeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasHeadFrameId(int headFameId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetHeadFrameSource(int headFrameId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<MonthCard> GetAllValidMonthCards()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<MonthCard> GetAllMonthCards()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMonthCardNeverBuy(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasValidMonthCards()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGainDialogReward(int dialogId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsNeedBattleReportLog
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string HeroErrMsg
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnExchangeGiftCDKeyAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override event Action<object> EventOnPlayerInfoInitAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override event Action EventOnPlayerInfoInitEnd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnCreateCharactorAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBuyEnergyAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeadPortraitAndHeadFrameSetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPlayerNameChangeAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnPlayerInfoChangeNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnArenaTicketsBuyAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnOpenGameRatingAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShareAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnClientDecidedAchievementAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCheckOnlineAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnSendClientHeartBeatNtfFail
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnGainDialogRewardAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnSendClientAppResumeNtfFail
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOpenMemoryStoreAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnJettonBuyAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnDailyFlushFinish
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnResetPushNotification
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRaffleDrawReq(int cardPoolId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSRaffleNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RaffleDrawAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RafflePool GetRafflePool(int poolId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RaffleItem GetRaffleItem(RafflePool pool, int raffleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDrawItemCost(RafflePool pool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanDraw(int poolId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRafflePoolOnActivityTime(int poolId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, int, Goods> EventOnRaffleDrawAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGetRandomStoreReq(int storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendFlushRandomStoreReq(int storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBuyRandomStoreItemReq(int storeId, int index, int selectedIndex = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSRandomStoreNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMessage(RandomStoreGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMessage(RandomStoreFlushAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMessage(RandomStoreBuyStoreItemAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<RandomStoreItem> GetRandomStoreItemList(int storeID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetNextFlushTime(int storeID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanManualRefreshRandomStore(int storeID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomStore FindRandomStoreByID(int storeID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanBuyRandomStoreGoods(int randomStoreID, int index, int selectedIndex = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetManualFlushNums(int storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnGetRandomStoreListAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnFlushRandomStoreAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnRandomStoreBuyStoreItemAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRankingListInfoQueryReq(
      RankingListType type,
      int heroId,
      int bossId = 0,
      int seasonId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMessage(RankingListInfoQueryAck ack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPeakArenaSeasonRankTitleIdBySeasonAndRank(int rank, int season)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPeakArenaRankingListTitle(int rank, int season)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAbleQueryRankingListInfo(RankingListType rankingListType, out int errorCode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckRankingListInfoQueryCare4AncientCall(
      RankingListType rankingListType,
      int bossId,
      out int errCode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingListInfo GetRankingListInfoByType(RankingListType rankingType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingListInfo GetSingleBossRankingListInfoByBossId(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingListInfo GetFriendAndGuildSingleBossRankingListInfoByBossId(
      int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRankingListInfoValid(RankingListType rankingListType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSingleHeroRankingListInfoValid(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingListInfo GetSingleHeroRankingListInfoByHeroId(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnRankingListInfoQueryAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRealTimePVPWaitingForOpponentReq(RealTimePVPMode Mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRealTimePVPCancelWaitingForOpponentReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRealTimePVPGetInfoReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRealTimePVPGetTopPlayersReq(int topN, bool isGlobal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRealTimePVPAcquireWinsBonusReq(int bonusId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSRealTimePVPNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSRealTimePVPPromotionBattleReportNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSRealTimePVPBattleReportNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RealTimePVPWaitingForOpponentAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RealTimePVPMatchupNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RealTimePVPCancelWaitingForOpponentAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RealTimePVPGetInfoAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RealTimePVPGetTopPlayersAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RealTimePVPAcquireWinsBonusAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RealTimePVPLeaderboardInfoNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsEmptyRealTimePVPPlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNeedGetRealTimePVPPlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerRealTimePVPScore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerRealTimePVPDan()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerRealTimePVPRank(bool isGlobal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProRealTimePVPLeaderboardPlayerInfo> GetRealTimePVPLeaderboardPlayerInfos(
      bool isGlobal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProUserSummary> GetRealTimePVPLeaderboardUserSummarys(
      bool isGlobal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LinkedList<RealTimePVPBattleReport> GetRealTimePVPBattleReports()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LinkedList<RealTimePVPBattleReport> GetRealTimePVPPromotionBattleReports()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRealTimePVPPromotion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRealTimePVPConsecutiveWins(RealTimePVPMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckRealTimePVPAcquireWinsBonus(int bonusId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GainRewardStatus GetRealTimePVPWinsBonusStatus(int bonusId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RealTimePVPMatchStats GetRealTimePVPLadderMatchStats()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RealTimePVPMatchStats GetRealTimePVPFriendlyMatchStats()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRealTimePVPHonour()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsRealTimePVPArenaAvailable(RealTimePVPMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRealTimePVPWillMatchBot(RealTimePVPMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, int> EventOnRealTimePVPWaitingForOpponentAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, RealTimePVPMode> EventOnRealTimePVPMatchupNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnRealTimePVPCancelWaitingForOpponentAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnRealTimePVPGetInfoAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnRealTimePVPGetTopPlayersAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleReward> EventOnRealTimePVPAcquireWinsBonusAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ProRealTimePVPUserInfo> EventOnRealTimePVPLeaderboardInfoNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRechargeStoreCancelBuyReq(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRechargeStoreBuyGoodsReq(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RechargeStoreCancelBuyAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMessage(BoughtRechargeStoreItemNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMessage(RechargeStoreBuyGoodsAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMessage(DSRechargeStoreNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMessage(UpdateRechargeStoreStatusNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRechargeGoodsBought(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CaculateGotCrystalNums(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRechargeCrystalOriginalNums(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRechargeCrystalFirstBoughtRewardNums(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRechargeCrystalRepeatlyBoughtRewardNums(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetRechargeGoodsIcon(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GetRechargeGoodsIsBestValue(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<string> EventOnRechargeBoughtSuccessNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnRechargeStoreItemBuyAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnRechargeStoreCancelBuyAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRefluxClaimRewardReq(int Slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSRefluxNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RefluxClaimRewardAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public long GetMissionDay(ConfigDataMissionInfo Mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<long, List<long>> GetRefluxMissions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetRefluxProcessingMissions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetFinishedAndGetRewardRefluxMissions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRefluxMissionPoints()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataRefluxRewardInfo> GetRefluxPointsRewardsConfig()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanClaimRefluxReward(int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBackFlowActivityOpen()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TimeSpan GetRefluxMissionsEndTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDaysAfterBackFlowActivityOpening()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsShowRedMarkOnBackFlowButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HaveFinishedNotGetRefluxMissions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HaveFinishedNotGetRefluxMissionsByDay(int day)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, List<Goods>> EventOnRefluxClaimRewardAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSResourceNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(MonthCardUpdateNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRiftLevelAttackReq(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRiftLevelBattleFinishedReq(int riftLevelId, ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRiftLevelRaidReq(int levelId, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRiftChapterRewardGainReq(int chapterId, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSRiftNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RiftLevelAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RiftLevelBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RiftLevelRaidAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RiftChapterRewardGainAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnlockRiftChapter(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRiftChapterTotalStars(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRiftChapterTotalStarsMax(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRiftChapterTotalAchievementCount(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRiftChapterTotalAchievementCountMax(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRiftChapterTotalBattleTreasureCount(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRiftChapterTotalBattleTreasureCountMax(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRiftChapterUnlockConditionCompleted(
      int chapterId,
      RiftChapterUnlockConditionType condition,
      int param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnlockRiftLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackRiftLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanRaidRiftLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftLevelStatus GetRiftLevelStatus(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftLevelStatus GetEventRiftLevelStatus(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRiftLevelUnlockConditionCompleted(
      int levelId,
      RiftLevelUnlockConditionType condition,
      int param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRiftLevelChallengeNum(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRiftLevelCanChallengeNums(ConfigDataRiftLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRiftLevelCanChallengeMaxNums(ConfigDataRiftLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRiftLevelChallenged(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRiftLevelStars(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRiftLevelAchievementCount(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasRiftLevelAchievement(int achievementRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGainRiftChapterStarReward(int chapterId, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GainRewardStatus GetRiftChapterStarRewardStatus(int chapterId, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GetRiftLevelFinished(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitOpenedRiftChapters()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckChangeOpenedRiftChapter(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckChangeLastOpenedRiftChapter()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRiftChapterInfo GetLastOpenedRiftChapterInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnRiftLevelAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event ProjectLPlayerContext.RiftLevelBattleFinishedAckCallback EventOnRiftLevelBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleReward, List<Goods>> EventOnRiftLevelRaidAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleReward> EventOnRiftChapterRewardGainAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendSelectCardReq(int cardPoolId, bool isSingleSlect, bool isUsingTicket)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSSelectCardNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(SelectCardAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, List<Goods>, List<Goods>> EventOnCardSelectAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool[] GetSelectRewardsIsFirstGetArray()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRewardFlags(List<ProGoods> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, bool> GetIsSelectHeroRewardTransferToFragmentDict()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroRewardToFragmentDict(List<ProGoods> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CardPool GetCardPoolById(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CardPool> GetActivityCardPoolList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetActivityTimeByPoolId(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase FindOperationalActivityByActivityCardPoolId(
      int activityCardPoolId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsActivityCardPoolOnActivityTime(int activityCardPoolId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetActivityCardPoolLastDays(CardPool cardPool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSelectCard(int cardPoolId, bool isSingleSelect, bool isUsingTickets = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendEverydaySignReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(EverydaySignAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSignToday()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSignDaysMonth()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<List<Goods>> GetMonthRewardList(int month)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Goods> GetSignRewardMonthList(int month)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool TodayIsSigned()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Goods GetTodaySignReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Goods GetOneDaySignReward(int month, int day)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, List<Goods>> EventOnEverydaySignAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendOpenSurveyReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGetSurveyRewardReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSSurveyNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(NewSurveyNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(UpdateSurveyNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GetSurveyRewardAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(OpenSurveyAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SurveyStatus GetCurrentSurveyStatus()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnOpenSurveyAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGetSurveyRewardAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGainSurveyReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTeamRoomCreateReq(TeamRoomSetting setting)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTeamRoomViewReq(
      GameFunctionType gameFunctionTypeId,
      int chapterId,
      int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTeamRoomAutoMatchReq(GameFunctionType gameFunctionTypeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTeamRoomAutoMatchCancelReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTeamRoomAuthorityChangeReq(TeamRoomAuthority authority)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTeamRoomJoinReq(
      int roomId,
      GameFunctionType gameFunctionTypeId,
      int locationId,
      ulong inviterSessionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTeamRoomQuitReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTeamRoomGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTeamRoomInviteReq(List<string> inviteeUserIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTeamRoomInvitationRefusedReq(
      ulong inviterSessionId,
      int inviterChannelId,
      int roomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTeamRoomInviteeInfoGetReq(
      List<string> inviteeUserIds,
      TeamRoomInviteeInfoType infoType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTeamRoomPlayerPositionChangeReq(List<ProTeamRoomPlayerPositionInfo> positionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSTeamNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomCreateAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomViewAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomAutoMatchAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomAutoMatchCancelAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomAuthorityChangeAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomJoinAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomQuitAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomInviteAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomInvitationRefusedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomInviteeInfoGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomPlayerPositionChangeAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomPlayerJoinNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomPlayerQuitNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomPlayerLeaveWaitingListAndJoinRoomNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomAuthorityChangeNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomAutoMatchInfoNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomInviteNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomInvitationRefusedNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomSelfKickOutNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomInviteeLevelInfoNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomPlayerPositionChangeNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoom GetTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInTeamRoomAutoMatchWaitingList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<TeamRoomInviteInfo> GetTeamRoomInviteInfos()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveTeamRoomAInviteInfo(ulong sessionId, int roomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnlockTeamBattle(GameFunctionType gameFunctionType, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTeamRoomInviteAgain(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTeamRoomInviteAgain()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanCreateTeam(TeamRoomSetting setting)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanViewTeamRoom(GameFunctionType gameFunctionTypeId, int chapterId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanJoinTeamRoom(GameFunctionType gameFunctionTypeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAutoMatchTeamRoom(GameFunctionType gameFunctionTypeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanCancelAutoMatchTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnTeamRoomCreateAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<TeamRoom>> EventOnTeamRoomViewAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, bool> EventOnTeamRoomAutoMatchAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnTeamRoomAutoMatchCancelAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnTeamRoomAuthorityChangeAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnTeamRoomJoinAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnTeamRoomQuitAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnTeamRoomGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnTeamRoomInviteAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnTeamRoomInvitationRefusedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnTeamRoomInviteeInfoGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<TeamRoomPlayer> EventOnTeamRoomPlayerJoinNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<TeamRoomPlayer> EventOnTeamRoomPlayerQuitNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnTeamRoomPlayerLeaveWaitingListAndJoinRoomNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<TeamRoomAuthority> EventOnTeamRoomAuthorityChangeNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnTeamRoomAutoMatchInfoNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<TeamRoomInviteInfo> EventOnTeamRoomInviteNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, string> EventOnTeamRoomInvitationRefusedNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnTeamRoomSelfKickOutNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, int, int> EventOnTeamRoomInviteeInfoNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnTeamRoomPlayerPositionChangeAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnTeamRoomPlayerPositionChangeNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendThearchyTrialLevelAttackReq(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendThearchyTrialLevelBattleFinishedReq(int levelId, ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSThearchyTrialNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ThearchyTrialLevelAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ThearchyTrialLevelBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsThearchyOpened(int thearchyId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsThearchyLevelOpened(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackThearchyLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetThearchyTicketCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsThearchyTrialLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetThearchyTrialMaxFinishedLevelId(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetThearchyDailyRewardRestCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetThearchyDailyRewardCountMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsThearchyLevelBlessing(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnThearchyTrialLevelAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool, BattleReward> EventOnThearchyTrialLevelBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTechLevelupReq(int TechId, int DeltaLevel = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSTrainingGroundNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TrainingGroundTechLevelupAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingRoom GetTrainingRoomById(int roomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanTechLevelup(int techId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanTrainingCourseLevelup(TrainingCourse course)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanTrainingRoomLevelup(TrainingRoom room)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<TrainingTech> GetTrainingGroundAvailableTechs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingTech GetTrainingTechById(int techId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTechMaxLevel(int TechId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTechLocked(int TechId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanLevelup(int TechId, int DeltaLevel = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingTechResourceRequirements GetResourceRequirementsByLevel(
      int TechId,
      int Level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTrainingCourseIdByTechId(int techId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTrainingRoomIdByTechId(int techId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanGoToGetSoldierTech(int getSoliderTechId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnTrainingGroundTechLevelupAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTreasureLevelAttackReq(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTreasureLevelBattleFinishedReq(int levelId, ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSTreasureMapNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TreasureLevelAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TreasureLevelBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTreasureLevelOpened(int levelID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackTreasureLevel(int levelID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTreasureTicketCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTreasureTicketCountMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTreasureLevelFinished(int levelID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTreasureMapMaxFinishedLevelId()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnTreasureLevelAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool, BattleReward> EventOnTreasureLevelBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendUnchartedLevelRaidReq(int type, int levelId, int Nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(UnchartedLevelRaidAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSUnchartedLevelRaidNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetUnchartedLevelRaidSurplusTimes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(UnchartedLevelRaidSurplusTimesNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMaxDailyUnchartedLevelRaidNum()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnchartedRaidLevel(int battleType, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLeftUnchartedRaidLevelDailyChallengeNum(int battleType, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, BattleReward> EventOnUnchartedLevelRaidAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnUnchartedRaidSurplusTimesAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendUnchartedScoreChallengeLevelAttackReq(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendUnchartedScoreChallengeLevelBattleFinishedReq(
      int unchartedScoreId,
      int levelId,
      ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendUnchartedScoreScoreLevelAttackReq(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendUnchartedScoreScoreLevelBattleFinishedReq(
      int unchartedScoreId,
      int levelId,
      ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSUnchartedScoreNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(UnchartedScoreChallengeLevelAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(UnchartedScoreChallengeLevelBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(UnchartedScoreScoreLevelAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(UnchartedScoreScoreLevelBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsUnchartedScoreOpened(int unchartedScoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsUnchartedScoreDisplay(int unchartedScoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GetUnchartedScoreOpenTime(
      int unchartedScoreId,
      out DateTime startTime,
      out DateTime endTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsUnchartedScoreLevelTimeUnlock(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsUnchartedScoreLevelPlayerLevelVaild(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsUnchartedChallengePrevLevelComplete(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetUnchartedChallengeLevelTimeUnlockDay(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackUnchartedScoreLevel(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackUnchartedChallengeLevel(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsUnchartedScoreLevelFinished(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsUnchartedChallengeLevelFinished(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetUnchartedScoreMaxFinishedScoreLevelId(int unchartedScoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetUnchartedScoreMaxFinishedChallengeLevelId(int unchartedScoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsUnchartedScoreRewardGot(int unchartedScoreId, int unchartedScoreRewardGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetUnchartedScoreScore(int unchartedScoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetUnchartedScoreDailyRewardRestCount(int unchartedScoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnUnchartedScoreChallengeLevelAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool, BattleReward> EventOnUnchartedScoreChallengeLevelBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnUnchartedScoreScoreLevelAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool, BattleReward> EventOnUnchartedScoreScoreLevelBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendUserGuideSetReq(int step)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendUserGuideSetReq(int[] steps)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendUserGuideClearReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(UserGuideSetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGameFunctionOpened(GameFunctionType gameFunctionType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsUserGuideComplete(int step)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRiftChapterEverOpened(int riftChapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnUserGuideSetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public delegate void AncientCallBossBattleFinishedAckCallback(
      int result,
      bool isWin,
      BattleReward reward);

    public class CollectionActivityState
    {
      public List<CollectionEvent> m_activeWaypointEvents = new List<CollectionEvent>();
      public ConfigDataCollectionActivityScenarioLevelInfo m_activeNextScenarioLevelInfo;
      private static DelegateBridge _c__Hotfix_ctor;

      public CollectionActivityState()
      {
        ProjectLPlayerContext.CollectionActivityState._c__Hotfix_ctor?.__Gen_Delegate_Imp4((object) this);
      }
    }

    public delegate void HeroAnthemLevelBattleFinishedAckCallback(
      int result,
      bool isWin,
      BattleReward reward,
      bool isFirstWin,
      List<int> achievements);

    public delegate void HeroDungeonLevelBattleFinishedAckCallback(
      int result,
      int stars,
      BattleReward reward,
      bool isFirstWin,
      List<int> achievements);

    public delegate void HeroPhantomBattleFinishedAckCallback(
      int result,
      bool isWin,
      BattleReward reward,
      bool isFirstWin,
      List<int> achievements);

    public class CurrentWaypointEvent
    {
      public int WaypointId;
      public ConfigDataEventInfo EventInfo;
      public RandomEvent RandomEvent;
      private static DelegateBridge _c__Hotfix_ctor;

      public CurrentWaypointEvent()
      {
        ProjectLPlayerContext.CurrentWaypointEvent._c__Hotfix_ctor?.__Gen_Delegate_Imp4((object) this);
      }
    }

    public delegate void RiftLevelBattleFinishedAckCallback(
      int result,
      int stars,
      BattleReward reward,
      bool isFirstWin,
      List<int> achievements);
  }
}
