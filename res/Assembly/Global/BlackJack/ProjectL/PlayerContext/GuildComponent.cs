﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.GuildComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using BlackJack.ProjectL.UI;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class GuildComponent : GuildComponentCommon
  {
    private BattleComponent m_battle;
    public Guild m_guild;
    public List<string> m_guildInviteUserIdList;
    public List<GuildSearchInfo> m_guildSearchList;
    public List<GuildSearchInfo> m_guildRecommendList;
    public List<GuildJoinInvitation> m_guildJoinInvitationList;
    public List<UserSummary> m_guildJoinApplyList;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_GetGuildLogContent;
    private static DelegateBridge __Hotfix_SetGuildDataSection;
    private static DelegateBridge __Hotfix_RefreshGuild;
    private static DelegateBridge __Hotfix_RefreshGuildListJoinState;
    private static DelegateBridge __Hotfix_PlayerRefuseGuild;
    private static DelegateBridge __Hotfix_GetFinishedCombatThisWeek;
    private static DelegateBridge __Hotfix_FinishGuildCombat;
    private static DelegateBridge __Hotfix_CanStartMassiveCombat;
    private static DelegateBridge __Hotfix_ResetGuild;

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetGuildLogContent(GuildLog log)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGuildDataSection(DSGuildNtf dsGuildNtf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshGuild(ProGuild proGuild)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshGuildListJoinState(string id, bool isJoinRequest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayerRefuseGuild(string guildID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetFinishedCombatThisWeek(GuildMassiveCombatGeneral generalInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FinishGuildCombat(int massiveCombat, List<int> heroes = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int CanStartMassiveCombat(int Difficulty)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetGuild()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
