﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.PeakArenaPlayOffMatchupInfoExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public static class PeakArenaPlayOffMatchupInfoExtension
  {
    private static DelegateBridge __Hotfix_ToPeakArenaPlayOffMatchupInfo;
    private static DelegateBridge __Hotfix_GetMatchupInfosByRound;
    private static DelegateBridge __Hotfix_GetAllPrevMatchupInfos;
    private static DelegateBridge __Hotfix_GetMatchupInfosByGroup;
    private static DelegateBridge __Hotfix_IsMatchupFinished;
    private static DelegateBridge __Hotfix_IsLeftUserWin;
    private static DelegateBridge __Hotfix_IsRightUserWin;
    private static DelegateBridge __Hotfix_IsLeftUserForfeited;
    private static DelegateBridge __Hotfix_IsRightUserForfeited;
    private static DelegateBridge __Hotfix_GetMatchupInfoBattleScore;

    [MethodImpl((MethodImplOptions) 32768)]
    public static PeakArenaPlayOffMatchupInfo ToPeakArenaPlayOffMatchupInfo(
      this ProPeakArenaPlayOffMatchupInfo Pro)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<PeakArenaPlayOffMatchupInfo> GetMatchupInfosByRound(
      this PeakArenaSeasonInfo info,
      int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<PeakArenaPlayOffMatchupInfo> GetAllPrevMatchupInfos(
      this PeakArenaSeasonInfo info,
      int currentMatchupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<PeakArenaPlayOffMatchupInfo> GetMatchupInfosByGroup(
      this PeakArenaSeasonInfo info,
      int groupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsMatchupFinished(this PeakArenaPlayOffMatchupInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsLeftUserWin(this PeakArenaPlayOffMatchupInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsRightUserWin(this PeakArenaPlayOffMatchupInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsLeftUserForfeited(this PeakArenaPlayOffMatchupInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsRightUserForfeited(this PeakArenaPlayOffMatchupInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void GetMatchupInfoBattleScore(
      this PeakArenaPlayOffMatchupInfo info,
      out int leftWins,
      out int rightWins)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
