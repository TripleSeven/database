﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.FriendComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class FriendComponent : FriendComponentCommon
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_GetDSVersion;
    private static DelegateBridge __Hotfix_Deserialize_0;
    private static DelegateBridge __Hotfix_LoadFromPBData;
    private static DelegateBridge __Hotfix_Deserialize_1;
    private static DelegateBridge __Hotfix_GetBusinessCardHeroes;
    private static DelegateBridge __Hotfix_GetBusinessCardInfoSet;
    private static DelegateBridge __Hotfix_GetSendFriendShipPointsCount;
    private static DelegateBridge __Hotfix_GetClaimedFriendShipPointsCount;
    private static DelegateBridge __Hotfix_ReceivedFriendShipPointsCount;
    private static DelegateBridge __Hotfix_get_Friends;
    private static DelegateBridge __Hotfix_set_Friends;
    private static DelegateBridge __Hotfix_get_Blacklist;
    private static DelegateBridge __Hotfix_set_Blacklist;
    private static DelegateBridge __Hotfix_get_Invite;
    private static DelegateBridge __Hotfix_set_Invite;
    private static DelegateBridge __Hotfix_get_Invited;
    private static DelegateBridge __Hotfix_set_Invited;
    private static DelegateBridge __Hotfix_get_GuildPlayers;
    private static DelegateBridge __Hotfix_set_GuildPlayers;
    private static DelegateBridge __Hotfix_get_RecentContactsChat;
    private static DelegateBridge __Hotfix_set_RecentContactsChat;
    private static DelegateBridge __Hotfix_get_RecentContactsTeamBattle;
    private static DelegateBridge __Hotfix_set_RecentContactsTeamBattle;
    private static DelegateBridge __Hotfix_get_PVPInviteInfos;
    private static DelegateBridge __Hotfix_set_PVPInviteInfos;

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Deserialize(DSFriendNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoadFromPBData(DSFriendNtf pbFriendInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Deserialize(FriendInfoUpdateNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleHero> GetBusinessCardHeroes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BusinessCardInfoSet GetBusinessCardInfoSet()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSendFriendShipPointsCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetClaimedFriendShipPointsCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ReceivedFriendShipPointsCount()
    {
      // ISSUE: unable to decompile the method.
    }

    public List<UserSummary> Friends
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<UserSummary> Blacklist
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<UserSummary> Invite
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<UserSummary> Invited
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<UserSummary> GuildPlayers
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<UserSummary> RecentContactsChat
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<UserSummary> RecentContactsTeamBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<PVPInviteInfo> PVPInviteInfos
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
