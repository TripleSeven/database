﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.CollectionActivityExchangeModel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class CollectionActivityExchangeModel
  {
    private bool m_isInitialized;
    private IConfigDataLoader m_configDataLoader;
    private CollectionActivityComponent m_collectionComponent;
    private BagComponent m_bagComponent;
    private PlayerBasicInfoComponent m_playerBasicInfoComponent;
    private CollectionActivity m_currentActivity;
    private int m_currentActivityRid;
    private string m_currentActivityName;
    private string m_exchangePanelBgResPath;
    private string m_exchangePanelStateName;
    private Stack<CollectionActivityCurrency> m_currencyPoolStack;
    private Stack<CollectionActivityPage> m_pagePoolStack;
    private Stack<CollectionActivityMission> m_missionPoolStack;
    private Stack<CollectionActivityItem> m_itemPoolStack;
    private Dictionary<int, List<ConfigDataCollectionActivityExchangeTableInfo>> m_exchangeGroupMap;
    public List<CollectionActivityPage> pageList;
    public List<CollectionActivityCurrency> currencyList;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_UpdateExchangeGroupMap;
    private static DelegateBridge __Hotfix_UpdateCachedData;
    private static DelegateBridge __Hotfix_NewResource;
    private static DelegateBridge __Hotfix_NewPage;
    private static DelegateBridge __Hotfix_NewMission;
    private static DelegateBridge __Hotfix_NewItem;
    private static DelegateBridge __Hotfix_ClearCachedData;
    private static DelegateBridge __Hotfix_get_CurrentActivity;
    private static DelegateBridge __Hotfix_get_CurrentActivityRid;
    private static DelegateBridge __Hotfix_get_CurrentActivityName;
    private static DelegateBridge __Hotfix_get_ExchangePanelBgResPath;
    private static DelegateBridge __Hotfix_get_ExchangePanelStateName;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityExchangeModel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateExchangeGroupMap()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateCachedData(ulong activityUid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CollectionActivityCurrency NewResource()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CollectionActivityPage NewPage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CollectionActivityMission NewMission()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CollectionActivityItem NewItem()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearCachedData()
    {
      // ISSUE: unable to decompile the method.
    }

    public CollectionActivity CurrentActivity
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int CurrentActivityRid
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string CurrentActivityName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string ExchangePanelBgResPath
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string ExchangePanelStateName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
