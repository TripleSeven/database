﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.UnchartedScoreComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class UnchartedScoreComponent : UnchartedScoreComponentCommon
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_GetDSVersion;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_IsUnchartedScoreOpened;
    private static DelegateBridge __Hotfix_GetUnchartedScoreOpenTime;
    private static DelegateBridge __Hotfix_IsUnchartedScoreDisplay;
    private static DelegateBridge __Hotfix_IsUnchartedScoreLevelTimeUnlock;
    private static DelegateBridge __Hotfix_GetChallengeLevelTimeUnlockDay;
    private static DelegateBridge __Hotfix_IsScoreLevelComplete;
    private static DelegateBridge __Hotfix_IsChallengeLevelComplete;
    private static DelegateBridge __Hotfix_IsChallengePrevLevelComplete;
    private static DelegateBridge __Hotfix_IsRewardGot;
    private static DelegateBridge __Hotfix_GetScore;
    private static DelegateBridge __Hotfix_GetDailyRewardRestCount;
    private static DelegateBridge __Hotfix_FinishedChallengeLevel;
    private static DelegateBridge __Hotfix_FinishedScoreLevel;
    private static DelegateBridge __Hotfix_SetSuccessChallengeLevel;
    private static DelegateBridge __Hotfix_SetSuccessScoreLevel;
    private static DelegateBridge __Hotfix_GetMaxFinishedScoreLevelId;
    private static DelegateBridge __Hotfix_GetMaxFinishedChallengeLevelId;

    [MethodImpl((MethodImplOptions) 32768)]
    public UnchartedScoreComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSUnchartedScoreNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsUnchartedScoreOpened(int unchartedScoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GetUnchartedScoreOpenTime(
      int unchartedScoreId,
      out DateTime startTime,
      out DateTime endTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsUnchartedScoreDisplay(int unchartedScoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsUnchartedScoreLevelTimeUnlock(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetChallengeLevelTimeUnlockDay(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScoreLevelComplete(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengeLevelComplete(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengePrevLevelComplete(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRewardGot(int unchartedScoreId, int unchartedScoreRewardGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetScore(int unchartedScoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDailyRewardRestCount(int unchartedScoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FinishedChallengeLevel(
      int unchartedScoreId,
      int levelId,
      bool isWin,
      List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FinishedScoreLevel(
      int unchartedScoreId,
      int levelId,
      bool isWin,
      List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSuccessChallengeLevel(
      int unchartedScoreId,
      ConfigDataChallengeLevelInfo levelInfo,
      List<int> battleTreasures,
      List<int> heroes,
      bool isTeamBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSuccessScoreLevel(
      int unchartedScoreId,
      ConfigDataScoreLevelInfo levelInfo,
      List<int> battleTreasures,
      List<int> heroes,
      List<int> allHeroes,
      bool isTeamBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMaxFinishedScoreLevelId(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMaxFinishedChallengeLevelId(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
