﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.HappeningStep
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace BlackJack.ProjectL.PlayerContext
{
  public enum HappeningStep
  {
    None,
    Init,
    DialogBefore,
    Battle,
    DialogAfter,
    BattleLoseOrCancel,
    MoveToWaypoint,
    MoveToPrevWaypoint,
  }
}
