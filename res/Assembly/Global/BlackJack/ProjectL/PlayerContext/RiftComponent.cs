﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.RiftComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class RiftComponent : RiftComponentCommon
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_InitChapterInfos;
    private static DelegateBridge __Hotfix_GetDSVersion;
    private static DelegateBridge __Hotfix_GainChapterReward;
    private static DelegateBridge __Hotfix_SetSuccessRiftLevel;
    private static DelegateBridge __Hotfix_ComplteRiftLevel;
    private static DelegateBridge __Hotfix_CompleteAchievement;
    private static DelegateBridge __Hotfix_GetRiftLevelChallengeNum;
    private static DelegateBridge __Hotfix_GetRiftLevelCanChallengeNums;
    private static DelegateBridge __Hotfix_RaidRiftLevel;
    private static DelegateBridge __Hotfix_IsRiftLevelChallenged;
    private static DelegateBridge __Hotfix_GetRiftLevelStars;
    private static DelegateBridge __Hotfix_FinishBattleRiftLevel;
    private static DelegateBridge __Hotfix_CanGainChapterStarReward;
    private static DelegateBridge __Hotfix_IsRiftChapterUnlockConditionCompleted;
    private static DelegateBridge __Hotfix_IsRiftLevelUnlockConditionCompleted;

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSRiftNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitChapterInfos(List<ProRiftChapter> proChapters)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GainChapterReward(int chapterId, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void SetSuccessRiftLevel(
      ConfigDataRiftLevelInfo riftLevelInfo,
      List<int> newAchievementIds,
      int stars,
      List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ComplteRiftLevel(int riftLevelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void CompleteAchievement(int achievementId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRiftLevelChallengeNum(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRiftLevelCanChallengeNums(ConfigDataRiftLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int RaidRiftLevel(int riftlevelId, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRiftLevelChallenged(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRiftLevelStars(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FinishBattleRiftLevel(
      int riftLevelId,
      List<int> gotAchievementIds,
      int star,
      List<int> battleTreasureIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGainChapterStarReward(int chapterId, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRiftChapterUnlockConditionCompleted(
      int chapterId,
      RiftChapterUnlockConditionType condition,
      int param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRiftLevelUnlockConditionCompleted(
      int levelId,
      RiftLevelUnlockConditionType condition,
      int param)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
