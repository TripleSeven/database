﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.ProjectLPlayerContextWrapper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.PlayerContext;
using BlackJack.ProjectL.LibClient;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class ProjectLPlayerContextWrapper : IClientEventHandler
  {
    private IPlayerContextNetworkEventHandler m_playerContext;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_OnConnected;
    private static DelegateBridge __Hotfix_OnDisconnected;
    private static DelegateBridge __Hotfix_OnError;
    private static DelegateBridge __Hotfix_OnLoginByAuthTokenAck;
    private static DelegateBridge __Hotfix_OnLoginBySessionTokenAck;
    private static DelegateBridge __Hotfix_OnMessage;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProjectLPlayerContextWrapper(IPlayerContextNetworkEventHandler playerContext)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnConnected()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnDisconnected()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnError(int err, string excepionInfo = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnLoginByAuthTokenAck(int result, bool needRedirect, string sessionToken)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnLoginBySessionTokenAck(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMessage(object msg, int msgId)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
