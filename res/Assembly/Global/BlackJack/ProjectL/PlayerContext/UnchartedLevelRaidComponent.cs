﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.UnchartedLevelRaidComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class UnchartedLevelRaidComponent : UnchartedLevelRaidComponentCommon
  {
    public HeroTrainningComponent m_heroTrainningComponent;
    public AnikiGymComponent m_anikiGymComponent;
    public EternalShrineComponent m_eternalShrineComponent;
    public MemoryCorridorCompoment m_memoryCorridorCompoment;
    public ThearchyTrialComponent m_thearchyTrialComponent;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_Flush;
    private static DelegateBridge __Hotfix_RaidUnchartedLevel;
    private static DelegateBridge __Hotfix_CanUnchartedRaidLevel;
    private static DelegateBridge __Hotfix_GetLeftUnchartedRaidLevelDailyChallengeNum;

    [MethodImpl((MethodImplOptions) 32768)]
    public UnchartedLevelRaidComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSUnchartedLevelRaidNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Flush()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int RaidUnchartedLevel(int type, int levelId, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnchartedRaidLevel(int battleType, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLeftUnchartedRaidLevelDailyChallengeNum(int battleType, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
