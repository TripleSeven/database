﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.GmCommand
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class GmCommand
  {
    public const string AddItem = "ADD_ITEM";
    public const string RemoveItem = "REMOVE_ITEM";
    public const string ClearBag = "CLEAR_BAG";
    public const string AddHero = "ADD_HERO";
    public const string SendTemplateMail = "POST_TEMPLATEMAIL";
    public const string CleanUserGuide = "CLEAN_USER_GUIDE";
    public const string AddActivity = "POST_GLOBALOPERATIONALACTIVITY";
    public const string BattleCheat = "BATTLE_CHEAT";
    public const string BattleCheatBuff = "BATTLE_CHEAT_BUFF";
    public const string MaxTrainingTechs = "SET_MAXTRAININGTECHS";
    public const string LevelUpHeroFetter = "LevelUpHeroFetter";
    public const string LevelUpHeroHeartFetter = "Set_HeroHeartFetter";
    private static DelegateBridge _c__Hotfix_ctor;

    [MethodImpl((MethodImplOptions) 32768)]
    public GmCommand()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
