﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.HeroComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class HeroComponent : HeroComponentCommon
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_InteractHero;
    private static DelegateBridge __Hotfix_GetHeroInteractionInfo;
    private static DelegateBridge __Hotfix_GetHeroInteractHeroPerformanceId;
    private static DelegateBridge __Hotfix_CanUnlockHeroPerformance;
    private static DelegateBridge __Hotfix_CanUnlockHeroBiography;
    private static DelegateBridge __Hotfix_GetDSVersion;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_GetWearedEquipmentHeroIdByEquipmentId;
    private static DelegateBridge __Hotfix_GetHeroInteractNums;
    private static DelegateBridge __Hotfix_GetHeroInteractNumsFlushTime;
    private static DelegateBridge __Hotfix_ImitateUseHeroFavorabilityExpItem;
    private static DelegateBridge __Hotfix_AutoEquipment;
    private static DelegateBridge __Hotfix_HasIgnoreCostEquipment;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSHeroNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InteractHero(int heroId, int addFavorabilityExp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroInteractionInfo GetHeroInteractionInfo(
      int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroInteractHeroPerformanceId(int heroId, HeroInteractionResultType resultType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnlockHeroPerformance(int heroPerformanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnlockHeroBiography(int biographyId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetWearedEquipmentHeroIdByEquipmentId(ulong equipmentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroInteractNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetHeroInteractNumsFlushTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ImitateUseHeroFavorabilityExpItem(
      int heroId,
      int itemId,
      int nums,
      GoodsType goodsType = GoodsType.GoodsType_Item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AutoEquipment(int heroId, List<ulong> equipmentInstanceIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasIgnoreCostEquipment(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
