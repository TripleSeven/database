﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.CollectionActivityComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class CollectionActivityComponent : CollectionComponentCommon
  {
    private List<ConfigDataCollectionActivityScenarioLevelInfo> m_tempScenarioLevelInfos;
    protected HeroComponentCommon m_hero;
    protected CollectionActivityExchangeModel m_exchangeModel;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_GetCollectionActivityList;
    private static DelegateBridge __Hotfix_GetDSVersion;
    private static DelegateBridge __Hotfix_IsCollectionActivityOpened;
    private static DelegateBridge __Hotfix_IsCollectionActivityDisplay;
    private static DelegateBridge __Hotfix_IsCollectionActivityExchangeTime;
    private static DelegateBridge __Hotfix_GetCollectionActivityOpenTime;
    private static DelegateBridge __Hotfix_GetCollectionActivityExchangeEndTime;
    private static DelegateBridge __Hotfix_FindActivityInstanceIdByCollectionActivityId;
    private static DelegateBridge __Hotfix_FindCollectionActivityByCollectionActivityId;
    private static DelegateBridge __Hotfix_GetNextScenarioLevelId;
    private static DelegateBridge __Hotfix_GetCurrentWaypointId;
    private static DelegateBridge __Hotfix_GetFinishedScenarioLevelInfos;
    private static DelegateBridge __Hotfix_GetWaypointState;
    private static DelegateBridge __Hotfix_GetPlayerGraphicResource;
    private static DelegateBridge __Hotfix_GetWaypointRedPointCount;
    private static DelegateBridge __Hotfix_GetLevelUnlockDay;
    private static DelegateBridge __Hotfix_GetScenarioLevelUnlockDay;
    private static DelegateBridge __Hotfix_GetChallengeLevelUnlockDay;
    private static DelegateBridge __Hotfix_GetLootLevelUnlockDay;
    private static DelegateBridge __Hotfix_IsScenarioLevelFinished;
    private static DelegateBridge __Hotfix_IsChallengeLevelFinished;
    private static DelegateBridge __Hotfix_IsLootLevelFinished;
    private static DelegateBridge __Hotfix_IsChallengePreLevelFinished;
    private static DelegateBridge __Hotfix_IsChallengeLevelPlayerLevelVaild;
    private static DelegateBridge __Hotfix_IsLootPreLevelFinished;
    private static DelegateBridge __Hotfix_IsLootLevelPlayerLevelVaild;
    private static DelegateBridge __Hotfix_FinishedLevel;
    private static DelegateBridge __Hotfix_SetSuccessLevel;
    private static DelegateBridge __Hotfix_HandleScenario;
    private static DelegateBridge __Hotfix_GetLevelName;
    private static DelegateBridge __Hotfix_GetPageLockMessage;
    private static DelegateBridge __Hotfix_IsLevelFinished;
    private static DelegateBridge __Hotfix_GetScore;
    private static DelegateBridge __Hotfix_IsRewardGot;
    private static DelegateBridge __Hotfix_GetMaxFinishedLootLevelId;
    private static DelegateBridge __Hotfix_get_ExchangeModel;
    private static DelegateBridge __Hotfix_FinishEvent;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSCollectionNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionActivity> GetCollectionActivityList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCollectionActivityOpened(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCollectionActivityDisplay(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCollectionActivityExchangeTime(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GetCollectionActivityOpenTime(
      int collectionActivityId,
      out DateTime startTime,
      out DateTime endTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetCollectionActivityExchangeEndTime(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ulong FindActivityInstanceIdByCollectionActivityId(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CollectionActivity FindCollectionActivityByCollectionActivityId(
      int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetNextScenarioLevelId(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCurrentWaypointId(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetFinishedScenarioLevelInfos(
      int collectionActivityId,
      List<ConfigDataCollectionActivityScenarioLevelInfo> scenarioLevelInfos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GetWaypointState(
      int waypointId,
      out CollectionActivityWaypointStateType waypointLockState,
      out string waypointGraphicState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPlayerGraphicResource(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetWaypointRedPointCount(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetLevelUnlockDay(int activityId, int daysBeforeActivate)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetScenarioLevelUnlockDay(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetChallengeLevelUnlockDay(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLootLevelUnlockDay(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScenarioLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengeLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLootLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengePreLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengeLevelPlayerLevelVaild(int levelId, out int playerLevelRequired)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLootPreLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLootLevelPlayerLevelVaild(int levelId, out int playerLevelRequired)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FinishedLevel(GameFunctionType levelType, int levelId, bool isWin)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSuccessLevel(
      GameFunctionType levelType,
      int levelId,
      List<int> heroes,
      List<int> allHeroes,
      bool isTeamBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int HandleScenario(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetLevelName(CollectionActivityLevelType type, int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPageLockMessage(CollectionActivityPage page)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLevelFinished(LevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetScore(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRewardGot(int collectionActivityId, int collectionActvityScoreRewardGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMaxFinishedLootLevelId(int collectionWaypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    public CollectionActivityExchangeModel ExchangeModel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FinishEvent(
      ulong activityId,
      int wayPointId,
      int EventId,
      bool isWin,
      List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
