﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.BattleComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class BattleComponent : BattleComponentCommon
  {
    private BattleRoom m_battleRoom;
    private BattleReward m_battleReward;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_GetDSVersion;
    private static DelegateBridge __Hotfix_SetBattleBase;
    private static DelegateBridge __Hotfix_GetBattleBase;
    private static DelegateBridge __Hotfix_IsRiftBattling;
    private static DelegateBridge __Hotfix_IsTreasureMapBattling;
    private static DelegateBridge __Hotfix_GetBattleTeam;
    private static DelegateBridge __Hotfix_ClearFighting;
    private static DelegateBridge __Hotfix_SetBattleRandomSeed;
    private static DelegateBridge __Hotfix_SetArenaBattleRandomSeed;
    private static DelegateBridge __Hotfix_GetArenaBattleStatus;
    private static DelegateBridge __Hotfix_GetArenaBattleId;
    private static DelegateBridge __Hotfix_GetGotBattleTreasureIds;
    private static DelegateBridge __Hotfix_JoinTeamBattleRoom;
    private static DelegateBridge __Hotfix_JoinGuildMassiveCombatBattleRoom;
    private static DelegateBridge __Hotfix_JoinPVPBattleRoom;
    private static DelegateBridge __Hotfix_JoinRealtimePVPBattleRoom;
    private static DelegateBridge __Hotfix_JoinPeakArenaBattleRoom;
    private static DelegateBridge __Hotfix_JoinPeakArenaBattleLiveRoom;
    private static DelegateBridge __Hotfix_QuitBattleRoom;
    private static DelegateBridge __Hotfix_SetRealtimePVPBattleRoomData;
    private static DelegateBridge __Hotfix_SetBattleRoomPlayerStatus;
    private static DelegateBridge __Hotfix_InitBattleRoomPlayerTechs;
    private static DelegateBridge __Hotfix_BattleRoomHeroSetup;
    private static DelegateBridge __Hotfix_BattleRoomHeroSetupAll;
    private static DelegateBridge __Hotfix_BattleRoomBattleStart;
    private static DelegateBridge __Hotfix_AppendBattleRoomCommands;
    private static DelegateBridge __Hotfix_InitPeakArenaBPStageHeros;
    private static DelegateBridge __Hotfix_SetPeakAreanBPStageData;
    private static DelegateBridge __Hotfix_SetPeakArenaBPStageTimeSpans;
    private static DelegateBridge __Hotfix_SetPeakArenaBattleActionTimeSpans;
    private static DelegateBridge __Hotfix_PeakArenaBPStageExecuteCommand;
    private static DelegateBridge __Hotfix_IsInBattleRoom;
    private static DelegateBridge __Hotfix_GetBattleRoom;
    private static DelegateBridge __Hotfix_GetBattleReward;
    private static DelegateBridge __Hotfix_GetChapterIdFromLevelId;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSBattleNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleBase(BattleBase battleBase)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleBase GetBattleBase()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRiftBattling()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTreasureMapBattling()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetBattleTeam(int teamType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearFighting()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleRandomSeed(int randomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaBattleRandomSeed(int randomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaBattleStatus GetArenaBattleStatus()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetArenaBattleId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetGotBattleTreasureIds()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void JoinTeamBattleRoom(
      ulong roomId,
      int battleId,
      GameFunctionType gameFunctionType,
      int locationId,
      List<ProBattleRoomPlayer> players,
      DateTime readyTimeOut)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void JoinGuildMassiveCombatBattleRoom(
      ulong roomId,
      int battleId,
      int locationId,
      List<ProBattleRoomPlayer> players,
      DateTime readyTimeOut,
      List<int> preferredHeroTagIds,
      ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void JoinPVPBattleRoom(
      ulong roomId,
      int battleId,
      List<ProBattleRoomPlayer> players,
      DateTime readyTimeOut)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void JoinRealtimePVPBattleRoom(
      ulong roomId,
      int battleId,
      List<ProBattleRoomPlayer> players,
      DateTime readyTimeOut,
      BattleRoomType roomType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void JoinPeakArenaBattleRoom(
      ulong roomId,
      int battleId,
      List<ProBattleRoomPlayer> players,
      DateTime readyTimeOut,
      RealTimePVPMode mode,
      int round,
      int boRound,
      int randomSeed,
      ulong instanceId,
      int matchupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void JoinPeakArenaBattleLiveRoom(
      ulong roomId,
      int matchupId,
      ProPeakArenaBattleLiveRoomInfo roomInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int QuitBattleRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRealtimePVPBattleRoomData(BattleRoomDataChangeNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleRoomPlayerStatus(
      int playerIndex,
      PlayerBattleStatus status,
      bool isOffLine)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitBattleRoomPlayerTechs(int playerIndex, List<ProTrainingTech> techs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BattleRoomHeroSetup(ProBattleHeroSetupInfo heroSetup)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BattleRoomHeroSetupAll(List<ProBattleHeroSetupInfo> heroSetups)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BattleRoomBattleStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AppendBattleRoomCommands(List<ProBattleCommand> commands)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitPeakArenaBPStageHeros(int playerIndex, List<ProBPStageHero> proHeros)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPeakAreanBPStageData(ProBattleRoomBPStage proBattleRoomBPStage, int randomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPeakArenaBPStageTimeSpans(long turnTimeSpan, List<long> publicTimeSpans)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPeakArenaBattleActionTimeSpans(
      long turnActionTimeSpan,
      List<long> publicTimeSpans)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PeakArenaBPStageExecuteCommand(int playerIndex, ProBPStageCommand proCommand)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInBattleRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoom GetBattleRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleReward GetBattleReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetChapterIdFromLevelId(GameFunctionType gameFunctionType, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
