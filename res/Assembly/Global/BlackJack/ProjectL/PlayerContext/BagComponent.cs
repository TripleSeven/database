﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.BagComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class BagComponent : BagComponentCommon
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_DeSerialize_1;
    private static DelegateBridge __Hotfix_DeSerialize_0;
    private static DelegateBridge __Hotfix_GetDSVersion;
    private static DelegateBridge __Hotfix_ChangeBagItem;
    private static DelegateBridge __Hotfix_IsExistBagItemIgnoreInstanceDifference;
    private static DelegateBridge __Hotfix_GetBagItemNums;
    private static DelegateBridge __Hotfix_GetInstanceBagItemInstanceIdsByContentId;
    private static DelegateBridge __Hotfix_AddInstanceBagItemDirectly;
    private static DelegateBridge __Hotfix_AddTypeBagItemDirectly;
    private static DelegateBridge __Hotfix_DecomposeBagItems;
    private static DelegateBridge __Hotfix_EnchantEquipment;
    private static DelegateBridge __Hotfix_GetEquipmentResonanceNums;
    private static DelegateBridge __Hotfix_GetEquipmentResonanceNumsByHeroId;
    private static DelegateBridge __Hotfix_GetEquipmentResonanceNumsByHeroAndEquipment;
    private static DelegateBridge __Hotfix_CanUseEnergyMedicine;
    private static DelegateBridge __Hotfix_HasOwn;
    private static DelegateBridge __Hotfix_HandleGetRewardList;
    private static DelegateBridge __Hotfix_HandleBoxOpenNetTask;

    [MethodImpl((MethodImplOptions) 32768)]
    public BagComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSBagNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSBagExtraNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ChangeBagItem(ProGoods goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsExistBagItemIgnoreInstanceDifference(GoodsType goodsTypeId, int contentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBagItemNums(GoodsType goodsTypeId, int contentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ulong> GetInstanceBagItemInstanceIdsByContentId(int contentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase AddInstanceBagItemDirectly(ProGoods goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase AddTypeBagItemDirectly(
      GoodsType goodsTypeId,
      int contentId,
      int currentNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int DecomposeBagItems(List<ProGoods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int EnchantEquipment(ulong equipmentInstanceId, ulong enchantStoneInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEquipmentResonanceNums(ulong equipmentInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEquipmentResonanceNumsByHeroId(ulong equipmentInstanceId, int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEquipmentResonanceNumsByHeroAndEquipment(
      ulong equipmentInstanceId,
      Hero hero,
      List<EquipmentBagItem> equipmentList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUseEnergyMedicine(int itemId, int useCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasOwn(GoodsType goodsType, int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HandleGetRewardList(
      List<Goods> rewardList,
      Action<List<Goods>, List<int>> actionOnResult)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleBoxOpenNetTask(
      GoodsType type,
      int id,
      int count,
      Action<List<Goods>> successedCallback,
      Action<int> failedCallback)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
