﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.RewardPlayerStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Common;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class RewardPlayerStatus
  {
    public int Level;
    public int Exp;
    public int NextLevelExp;
    public int Energy;
    public int ArenaVictoryPoints;
    public int ArenaLevelID;
    public int RealTimePVPScore;
    public int RealTimePVPDan;
    public bool RealTimePVPIsPromotion;
    public RealTimePVPMode RealtTimePVPMode;
    public int PeakArenaScore;
    public int PeakArenaDan;
    public bool PeakArenaIsGrading;
    public bool PeakArenaIsPromotion;
    public RealTimePVPMode PeakArenaMode;
    public int ClimbTowerFinishedFloorId;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Clear;

    [MethodImpl((MethodImplOptions) 32768)]
    public RewardPlayerStatus()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
