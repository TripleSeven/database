﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.MemoryCorridorCompoment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class MemoryCorridorCompoment : MemoryCorridorCompomentCommon
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_GetDSVersion;
    private static DelegateBridge __Hotfix_GetCurrentTicketNums;
    private static DelegateBridge __Hotfix_FinishedMemoryCorridorLevel;
    private static DelegateBridge __Hotfix_SetSuccessMemoryCorridorLevelRaid;
    private static DelegateBridge __Hotfix_SetSuccessMemoryCorridorLevel;
    private static DelegateBridge __Hotfix_IsMemoryCorridorOpened;
    private static DelegateBridge __Hotfix_IsMemoryCorridorLevelOpened;
    private static DelegateBridge __Hotfix_GetMaxFinishedLevelId;
    private static DelegateBridge __Hotfix_RaidMemoryCorridorLevel;

    [MethodImpl((MethodImplOptions) 32768)]
    public MemoryCorridorCompoment()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSMemoryCorridorNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCurrentTicketNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FinishedMemoryCorridorLevel(int levelId, bool isWin, List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSuccessMemoryCorridorLevelRaid(
      ConfigDataMemoryCorridorLevelInfo levelInfo,
      List<int> battleTreasures,
      List<int> heroes,
      bool isTeamBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSuccessMemoryCorridorLevel(
      ConfigDataMemoryCorridorLevelInfo levelInfo,
      List<int> battleTreasures,
      List<int> heroes,
      bool isTeamBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMemoryCorridorOpened(int memoryCorridorId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMemoryCorridorLevelOpened(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMaxFinishedLevelId(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int RaidMemoryCorridorLevel(int levelId, int nums)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
