﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.LevelComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class LevelComponent : LevelComponentCommon
  {
    private ProjectLPlayerContext m_owner;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_GetDSVersion;
    private static DelegateBridge __Hotfix_HandleDialogEvent;
    private static DelegateBridge __Hotfix_CanAttackScenario_1;
    private static DelegateBridge __Hotfix_CanAttackScenario_0;
    private static DelegateBridge __Hotfix_CanAttackEventWayPoint;
    private static DelegateBridge __Hotfix_HandleTresureEvent;
    private static DelegateBridge __Hotfix_SetBattleWayPointSuccessful;
    private static DelegateBridge __Hotfix_IsRegionOpen;
    private static DelegateBridge __Hotfix_GetWaypointStatus;
    private static DelegateBridge __Hotfix_GetPlayerCurrentWaypointInfo;
    private static DelegateBridge __Hotfix_GetLastFinishedScenarioInfo;
    private static DelegateBridge __Hotfix_RemoveRandomEvent;
    private static DelegateBridge __Hotfix_SetFinishedScenario;
    private static DelegateBridge __Hotfix_FinishBattleWayPoint;

    [MethodImpl((MethodImplOptions) 32768)]
    public LevelComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSLevelNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override int HandleDialogEvent(
      ConfigDataWaypointInfo wayPointInfo,
      List<Goods> itemRewards,
      int playerExpReward,
      int goldReward,
      int energyCost,
      int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int CanAttackScenario(
      ConfigDataScenarioInfo secenarioInfo,
      bool scenarioFinished,
      bool checkBag = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackScenario(ConfigDataScenarioInfo secenarioInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackEventWayPoint(ConfigDataWaypointInfo wayPointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override int HandleTresureEvent(int wayPointId, ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void SetBattleWayPointSuccessful(
      ConfigDataWaypointInfo wayPointInfo,
      List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRegionOpen(int regionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WayPointStatus GetWaypointStatus(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataWaypointInfo GetPlayerCurrentWaypointInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataScenarioInfo GetLastFinishedScenarioInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void RemoveRandomEvent(RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void SetFinishedScenario(ConfigDataScenarioInfo scenarioInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FinishBattleWayPoint(
      int wayPointId,
      bool isWin,
      int result,
      List<int> battleTreasureIds)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
