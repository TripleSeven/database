﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.PeakArenaBattleLiveRoomInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class PeakArenaBattleLiveRoomInfo
  {
    public long RoomCreateTime;
    public DateTime RoomCreateDateTime;
    public List<int> WinPlayerIndexes;
    public List<UserSummary> Players;
    public List<string> FirstHandUserIds;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GatBattleCount;
    private static DelegateBridge __Hotfix_GetLeftPlayerBattleScore;
    private static DelegateBridge __Hotfix_GetRightPlayerBattleScore;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBattleLiveRoomInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GatBattleCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLeftPlayerBattleScore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRightPlayerBattleScore()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
