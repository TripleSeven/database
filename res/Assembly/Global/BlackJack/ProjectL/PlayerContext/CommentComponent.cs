﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.CommentComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Common;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class CommentComponent : CommentComponentCommon
  {
    private Dictionary<int, HeroComment> m_heroComments;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_GetHeroComment_1;
    private static DelegateBridge __Hotfix_InitPlayerHeroCommentEntry;
    private static DelegateBridge __Hotfix_AddHeroComment;
    private static DelegateBridge __Hotfix_GetHeroComment_0;
    private static DelegateBridge __Hotfix_CommentHero;
    private static DelegateBridge __Hotfix_PraiseHeroCommentEntry;
    private static DelegateBridge __Hotfix_FindHeroComment;
    private static DelegateBridge __Hotfix_GetHeroCommentEntries;
    private static DelegateBridge __Hotfix_GetLastUpdateTime;
    private static DelegateBridge __Hotfix_ClearAllComment;

    [MethodImpl((MethodImplOptions) 32768)]
    public CommentComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroComment(
      int heroId,
      HeroComment heroComment,
      PlayerHeroCommentEntry playerCommentEntry)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitPlayerHeroCommentEntry(
      HeroComment cacheHeroComment,
      PlayerHeroCommentEntry playerCommentEntry)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddHeroComment(int heroId, HeroComment cacheHeroComment, HeroComment heroComment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroComment GetHeroComment(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CommentHero(int heroId, HeroCommentEntry commentEntry, long lastUpdateTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int PraiseHeroCommentEntry(int heroId, ulong entryInstanceId, long lastUpdateTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private HeroComment FindHeroComment(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<HeroCommentEntry> GetHeroCommentEntries(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public long GetLastUpdateTime(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearAllComment()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
