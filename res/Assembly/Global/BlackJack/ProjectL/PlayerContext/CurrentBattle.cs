﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.CurrentBattle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class CurrentBattle
  {
    public BattleType BattleType;
    public ConfigDataBattleInfo BattleInfo;
    public ConfigDataPVPBattleInfo PVPBattleInfo;
    public ConfigDataRealTimePVPBattleInfo RealTimePVPBattleInfo;
    public RealTimePVPBattleReport RealTimePVPBattleReport;
    public ConfigDataPeakArenaBattleInfo PeakArenaBattleInfo;
    public PeakArenaLadderBattleReport PeakArenaBattleReport;
    public string PeakArenaBattleReportUserId;
    public PeakArenaBattleReportSourceType PeakArenaBattleReportSourceType;
    public int PeakArenaMatchGroupId;
    public ConfigDataArenaBattleInfo ArenaBattleInfo;
    public ConfigDataArenaDefendRuleInfo ArenaDefendRuleInfo;
    public int ArenaDefenderPlayerLevel;
    public List<BattleHero> ArenaDefenderHeros;
    public List<TrainingTech> ArenaDefenderTrainTechs;
    public ArenaBattleReport ArenaBattleReport;
    public bool IsArenaRevenge;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Clear;
    private static DelegateBridge __Hotfix_GetPrepareMusic;
    private static DelegateBridge __Hotfix_GetBattleMusic;
    private static DelegateBridge __Hotfix_GetDefendMusic;
    private static DelegateBridge __Hotfix_IsAnyBattleReport;

    [MethodImpl((MethodImplOptions) 32768)]
    public CurrentBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPrepareMusic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetBattleMusic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetDefendMusic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAnyBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
