﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.ChatGroupCompactInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class ChatGroupCompactInfo
  {
    public string ChatGroupId;
    public string ChatGroupName;
    public ChatUserCompactInfo Owner;
    public int UserCount;
    public int OnlineUserCount;
    private static DelegateBridge _c__Hotfix_ctor;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatGroupCompactInfo(ProChatGroupCompactInfo pbGroupInfo)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
