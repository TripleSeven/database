﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.BattleReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class BattleReward
  {
    public int PlayerExp;
    public int HeroExp;
    public int Gold;
    public int FriendshipPoints;
    public List<BlackJack.ConfigData.Goods> Goods;
    public List<BlackJack.ConfigData.Goods> TeamGoods1;
    public List<BlackJack.ConfigData.Goods> TeamGoods2;
    public List<BlackJack.ConfigData.Goods> FriendGoods;
    public List<BlackJack.ConfigData.Goods> DailyGoods;
    public List<BlackJack.ConfigData.Goods> ExtraGoods;
    public List<BlackJack.ConfigData.Goods> ScoreGoods;
    public int Score;
    public int CurrentScore;
    public bool IsDanProtectionTriggered;
    public bool IsDanGroupProtectionTriggered;
    public int DailyScoreBonus;
    public int HeroScoreBonus;
    public int ExtraScore;
    public int TotalScore;
    public int Rank;
    public int Damage;
    public int OldMaxDamage;
    public int PrevRankDamage;
    public int HistoryMaxDamage;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Clear;
    private static DelegateBridge __Hotfix_IsEmpty;
    private static DelegateBridge __Hotfix_IsChestGoodsEmpty;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEmpty()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChestGoodsEmpty()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
