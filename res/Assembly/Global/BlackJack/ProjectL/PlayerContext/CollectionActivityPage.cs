﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.CollectionActivityPage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class CollectionActivityPage
  {
    public int id;
    public string name;
    public int coverHeroId;
    public int startHour;
    public bool isLockedByTime;
    public bool isLockedByLevel;
    public string lockedLevelName;
    public bool isReserved;
    public List<CollectionActivityMission> missionList;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Sort;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityPage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int Sort(CollectionActivityPage x, CollectionActivityPage y)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
