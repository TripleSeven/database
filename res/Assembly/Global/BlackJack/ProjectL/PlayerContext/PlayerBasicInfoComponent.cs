﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.PlayerBasicInfoComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class PlayerBasicInfoComponent : PlayerBasicInfoComponentCommon
  {
    private ProjectLPlayerContext m_playerContext;
    public int m_vipLevel;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_GetCurrentTime;
    private static DelegateBridge __Hotfix_DeSerialize_1;
    private static DelegateBridge __Hotfix_DeSerialize_0;
    private static DelegateBridge __Hotfix_EveryDaySign;
    private static DelegateBridge __Hotfix_CheckPlayerName;
    private static DelegateBridge __Hotfix_GetDSVersion;
    private static DelegateBridge __Hotfix_GetPlayerBasicInfo;
    private static DelegateBridge __Hotfix_GetEnergyReachMaxTime;
    private static DelegateBridge __Hotfix_DecreaseEnergy;
    private static DelegateBridge __Hotfix_IncreamentEnergy;
    private static DelegateBridge __Hotfix_AddArenaTickets;
    private static DelegateBridge __Hotfix_OnLevelChange;

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerBasicInfoComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override DateTime GetCurrentTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(PlayerInfoInitAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSPlayerBasicNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EveryDaySign(int signDays, long lastSignTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int CheckPlayerName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionPlayerBasicInfo GetPlayerBasicInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetEnergyReachMaxTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int DecreaseEnergy(int energyCost, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int IncreamentEnergy(
      long energyAddNums,
      bool canAboveMaxEnergy,
      GameFunctionType causeId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int AddArenaTickets(
      int nums,
      bool arenaGiven = true,
      GameFunctionType causeId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnLevelChange(int upLevel, int addExp)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
