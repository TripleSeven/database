﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.PeakArenaComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class PeakArenaComponent : PeakArenaComponentCommon
  {
    public List<PeakArenaHeroWithEquipments> PeakArenaPublicHeroesPool;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_GetDSVersion;
    private static DelegateBridge __Hotfix_SetPeakArenaPublicHeroesPool;
    private static DelegateBridge __Hotfix_LocalizePeakArenaPublicHeroesPool;
    private static DelegateBridge __Hotfix_SelectHeroSkillInPublicPools;
    private static DelegateBridge __Hotfix_IsPeakArenaMatchmakingAvailable;
    private static DelegateBridge __Hotfix_IsPeakArenaMatchmakingAvailableWithOutGameFunction;
    private static DelegateBridge __Hotfix_SetPeakArenaBattleReports;
    private static DelegateBridge __Hotfix_UpdateData;
    private static DelegateBridge __Hotfix_CanBetPeakArenaCare4Round;
    private static DelegateBridge __Hotfix_UpdateMyPlayOffInfo;
    private static DelegateBridge __Hotfix_GetPeakArenaSeasonEndTime;
    private static DelegateBridge __Hotfix_get_Dan;
    private static DelegateBridge __Hotfix_set_Dan;
    private static DelegateBridge __Hotfix_get_Score;
    private static DelegateBridge __Hotfix_set_Score;
    private static DelegateBridge __Hotfix_get_MyPlayOffGroupId;
    private static DelegateBridge __Hotfix_set_MyPlayOffGroupId;
    private static DelegateBridge __Hotfix_get_MyNextPlayoffMatch;
    private static DelegateBridge __Hotfix_set_MyNextPlayoffMatch;
    private static DelegateBridge __Hotfix_get_ChampionId;
    private static DelegateBridge __Hotfix_set_ChampionId;
    private static DelegateBridge __Hotfix_get_LastPlayOffRewardSendDate;
    private static DelegateBridge __Hotfix_set_LastPlayOffRewardSendDate;
    private static DelegateBridge __Hotfix_get_LastPeakArenaSendRewardSeasonID;
    private static DelegateBridge __Hotfix_set_LastPeakArenaSendRewardSeasonID;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSPeakArenaNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPeakArenaPublicHeroesPool(List<PeakArenaHeroWithEquipments> heroesPool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LocalizePeakArenaPublicHeroesPool()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int SelectHeroSkillInPublicPools(int heroId, List<int> skillIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int IsPeakArenaMatchmakingAvailable(RealTimePVPMode Mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsPeakArenaMatchmakingAvailableWithOutGameFunction(RealTimePVPMode Mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPeakArenaBattleReports(List<ProCommonBattleReport> battleReports)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateData(ProPeakArena pro)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanBetPeakArenaCare4Round(int round, int matchupId, string userId, int jettonNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateMyPlayOffInfo(PeakArenaSeasonInfo seasonInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetPeakArenaSeasonEndTime(int Season)
    {
      // ISSUE: unable to decompile the method.
    }

    public int Dan
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Score
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int? MyPlayOffGroupId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public PeakArenaPlayOffMatchupInfo MyNextPlayoffMatch
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string ChampionId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime LastPlayOffRewardSendDate
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int LastPeakArenaSendRewardSeasonID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
