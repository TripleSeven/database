﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.UtilityTools.RandomExtentions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.UtilityTools
{
  public static class RandomExtentions
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetRandomNumberByWeight(
      this Random random,
      Dictionary<int, int> weights,
      int weightMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetRandomBumberByAccumulateWeight(
      this Random random,
      Dictionary<int, int> weights)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool CanGetSpecificRandomNumber(this Random random, int weight, int weightMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<int> GetRandomNums(
      this Random random,
      int begin,
      int end,
      int needToRandomNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<int> GetRandomNumsLessMemory(
      this Random random,
      int begin,
      int end,
      int needToRandomNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<int> GetRandomNums(
      this Random random,
      List<int> inputCollection,
      int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetRandomNumber(this Random random, List<int> inputCollection)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ShuffleWithWeight<T>(this Random rand, List<T> items, List<int> weights)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ulong NextUInt64(this Random rand, ulong inclusive_min = 0, ulong exclusive_max = 18446744073709551615)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void FisherYatesShuffle<T>(this IList<T> list, Random rand)
    {
      // ISSUE: unable to decompile the method.
    }

    public class UtilityFunction
    {
      [MethodImpl((MethodImplOptions) 32768)]
      public static void Swap<T>(ref T lhs, ref T rhs) where T : struct
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
