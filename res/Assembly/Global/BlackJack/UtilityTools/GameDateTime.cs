﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.UtilityTools.GameDateTime
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace BlackJack.UtilityTools
{
  public static class GameDateTime
  {
    public static readonly long TicksPerSecond;
    public static readonly long TicksPerMinute;
    public static readonly long TicksPerHour;
    public static readonly long TicksPerDay;
    private static TimeSpan? _Offset;

    public static TimeSpan Offset
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static long IsSameDay(DateTime x, DateTime y)
    {
      // ISSUE: unable to decompile the method.
    }

    public static DateTime BeginOfToday
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static DateTime BeginOfTomorrow
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static DateTime BeginOfYesterday
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static GameDateTime()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
