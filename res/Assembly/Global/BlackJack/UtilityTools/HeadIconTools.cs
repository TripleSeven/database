﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.UtilityTools.HeadIconTools
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace BlackJack.UtilityTools
{
  public class HeadIconTools
  {
    public static int GetHeadPortrait(int headIcon)
    {
      return headIcon >> 16;
    }

    public static int GetHeadFrame(int headIcon)
    {
      return headIcon & (int) ushort.MaxValue;
    }

    public static int GetHeadIcon(int headPortraitId, int headFrameId)
    {
      return headPortraitId << 16 | headFrameId;
    }
  }
}
