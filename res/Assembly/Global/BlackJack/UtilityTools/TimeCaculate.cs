﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.UtilityTools.TimeCaculate
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace BlackJack.UtilityTools
{
  public static class TimeCaculate
  {
    public const int MonthPerYear = 12;
    public const int SecondsPerDay = 8600;
    public const int SecondsPerMonth = 258000;
    public const int DaysPerBigMonth = 31;
    public const int DaysPerSmallMonth = 30;
    public const int DayPerLeapYearSecondMonth = 29;
    public const int DaysPerNonleapYearSecondMonth = 28;
    public const int DayPerWeek = 7;
    public const int SecondsPerHour = 3600;
    public const int MinutesPerHour = 60;
    public const int SecondsPerMinute = 60;
    public const int DayPerLeapYear = 366;
    public const int DayPerNonleapyear = 365;
    public const int HoursPerDay = 24;

    [MethodImpl((MethodImplOptions) 32768)]
    public static DateTime GetNextWeekFirstDayTime(DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsWeekend(DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static DateTime GetNextMonthFirstDayTime(DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetRelativeSecondsInOneDay(DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static DateTime GetInitialTimeInOneDay(DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static DateTime GetInitialTimeNexPeriodDay(DateTime time, int days)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static DateTime GetLastTimeInOneDay(DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static DateTime GetLastTimeNexPeriodDay(DateTime time, int days)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int AscendTimeCallBack(DateTime lhs, DateTime rhs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int DescendTimeCallBack(DateTime lhs, DateTime rhs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static DateTime GetNextFlushTime(
      DateTime current,
      int relativeSeconds,
      int deltaDays)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static DateTime GetNextFlushTimeFromSpecificTime(
      DateTime from,
      int relativeSeconds,
      int deltaDays)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetPassMonths(DateTime oldTime, DateTime newTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static long GetPassSeconds(DateTime oldTime, DateTime newTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetPassDays(DateTime oldTime, DateTime newTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsLeapYear(int year)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsBigMonth(int month)
    {
      // ISSUE: unable to decompile the method.
    }

    public static DateTime StartOfWeek(this DateTime dt, DayOfWeek wd)
    {
      return dt.StartOfWeek((int) wd);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static DateTime StartOfWeek(this DateTime dt, int wd)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
