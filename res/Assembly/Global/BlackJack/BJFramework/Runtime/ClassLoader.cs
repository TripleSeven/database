﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.ClassLoader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime
{
  public class ClassLoader
  {
    private static ClassLoader m_instance;
    private Dictionary<string, Assembly> m_assembleDict;
    private Dictionary<string, Type> m_typeDict;

    [MethodImpl((MethodImplOptions) 32768)]
    private ClassLoader()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ClassLoader CreateClassLoader()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object CreateInstance(TypeDNName typeDNName, params object[] args)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Type LoadType(TypeDNName typeDNName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddAssembly(Assembly assembly)
    {
      // ISSUE: unable to decompile the method.
    }

    public static ClassLoader Instance
    {
      get
      {
        return ClassLoader.m_instance;
      }
    }
  }
}
