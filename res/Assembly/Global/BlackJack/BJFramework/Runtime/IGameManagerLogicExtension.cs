﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.IGameManagerLogicExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;

namespace BlackJack.BJFramework.Runtime
{
  public interface IGameManagerLogicExtension
  {
    void OnLoadConfigDataEnd(Action<bool> onEnd);

    void OnLoadDynamicAssemblyEnd();

    void OnClear4Relogin();

    void Tick();
  }
}
