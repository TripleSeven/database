﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.SceneSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  [Serializable]
  public class SceneSettings
  {
    public bool UseOrthographicForUILayer;
    public int DesignScreenWidth;
    public int DesignScreenHeight;
    public int TrigerWidth2ShrinkScale;
    public int TrigerHeight2ShrinkScale;
    public Vector3 SceneLayerOffset;

    [MethodImpl((MethodImplOptions) 32768)]
    public SceneSettings()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
