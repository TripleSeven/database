﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Scene.SceneLayerBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime.Scene
{
  public class SceneLayerBase : MonoBehaviour
  {
    public SceneLayerBase.LayerState State;
    private LayerLayoutDesc m_layerLayoutDesc;
    private LayerRenderSettingDesc m_renderSetting;
    private bool m_renderSettingSearched;
    private bool m_isReserve;

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsOpaque()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFullScreen()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsStayOnTop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private LayerLayoutDesc GetDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AttachGameObject(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LayerRenderSettingDesc GetRenderSetting()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsReserve()
    {
      return this.m_isReserve;
    }

    public void SetReserve(bool value)
    {
      this.m_isReserve = value;
    }

    public string LayerName { get; private set; }

    public virtual Camera LayerCamera
    {
      get
      {
        return (Camera) null;
      }
    }

    public GameObject LayerPrefabRoot { get; private set; }

    public enum LayerState
    {
      None,
      Loading,
      Unused,
      InStack,
      WaitForFree,
    }
  }
}
