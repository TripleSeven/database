﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Scene.LayerRenderSettingDesc
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Rendering;

namespace BlackJack.BJFramework.Runtime.Scene
{
  public class LayerRenderSettingDesc : MonoBehaviour
  {
    public LayerRenderSettingDesc.EnviornmentLighting enviornmentLighting;
    public LayerRenderSettingDesc.Fog fog;

    [MethodImpl((MethodImplOptions) 32768)]
    public void CopyFrom(LayerRenderSettingDesc target)
    {
      // ISSUE: unable to decompile the method.
    }

    [Serializable]
    public class EnviornmentLighting
    {
      public Material SkyBox;
      public AmbientMode AmbientSource;
      public float AmbientIntensity;
      [Header("TrilightMode Colors")]
      public Color SkyColor;
      public Color EquatorColor;
      public Color GroundColor;
      [Header("FlatMode Color")]
      public Color AmbientColor;
    }

    [Serializable]
    public class Fog
    {
      public bool EnableFog;
      public Color FogColor;
      public FogMode FogMode;
      [Header("LinearMode")]
      public float FogStart;
      public float FogEnd;
      [Header("ExponentialMode")]
      public float FogDensity;
    }
  }
}
