﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Scene.SceneManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.Utils;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.BJFramework.Runtime.Scene
{
  public class SceneManager : ITickable
  {
    private static SceneManager m_instance;
    private TinyCorutineHelper m_corutineHelper;
    private bool m_useOrthographicForUILayer;
    private int m_designScreenWidth;
    private int m_designScreenHeight;
    private int m_trigerWidth2ShrinkScale;
    private int m_trigerHeight2ShrinkScale;
    private Vector3 m_layerOffset;
    private GameObject m_sceneRootGo;
    private GameObject m_unusedLayerRootGo;
    private GameObject m_loadingLayerRootGo;
    private GameObject m_3DSceneRootGo;
    private GameObject m_uiSceneGroup1RootCanvasGo;
    private GameObject m_uiSceneGroup2RootCanvasGo;
    public Camera m_uiSceneGroup1Camera;
    public Camera m_uiSceneGroup2Camera;
    private Canvas m_uiSceneGroup1RootCanvas;
    private Canvas m_uiSceneGroup2RootCanvas;
    private CanvasScaler m_uiSceneGroup1RootCanvasScaler;
    private CanvasScaler m_uiSceneGroup2RootCanvasScaler;
    private GameObject m_3DLayerRootPrefab;
    private GameObject m_uiLayerRootPrefab;
    private Dictionary<string, SceneLayerBase> m_layerDict;
    private List<SceneLayerBase> m_unusedLayerList;
    private List<SceneLayerBase> m_loadingLayerList;
    private List<SceneLayerBase> m_layerStack;
    private bool m_stackDirty;
    private bool m_is3DLayerBlurActive;
    private LayerRenderSettingDesc m_currRenderSetting;
    private LayerRenderSettingDesc m_defaultRenderSetting;
    private bool m_enableLayerReserve;
    private static string m_sceneRootAssetPath;
    private static string m_3DLayerRootAssetPath;
    private static string m_uiLayerRootAssetPath;
    private static int m_cameraDepthMax;

    [MethodImpl((MethodImplOptions) 32768)]
    private SceneManager()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static SceneManager CreateSceneManager()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Initlize(
      int designScreenWidth,
      int designScreenHeight,
      int trigerWidth2ShrinkScale,
      int trigerHeight2ShrinkScale,
      Vector3 layerOffset,
      bool enableLayerReserve = false,
      bool useOrthographicForUILayer = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Uninitlize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CreateSceneRoot()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateLayer(
      System.Type layerType,
      string name,
      string resPath,
      Action<SceneLayerBase> onComplete,
      bool enableReserve = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateUILayer(
      string name,
      string resPath,
      Action<SceneLayerBase> onComplete,
      bool enableReserve = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Create3DLayer(
      string name,
      string resPath,
      Action<SceneLayerBase> onComplete,
      bool enableReserve = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateUnitySceneLayer(
      string name,
      string resPath,
      Action<SceneLayerBase> onComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FreeLayer(SceneLayerBase layer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool PushLayer(SceneLayerBase layer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PopLayer(SceneLayerBase layer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BringLayerToTop(UISceneLayer layer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Enable3DLayerBlur(bool isEnable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddLayerToLoading(SceneLayerBase layer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddLayerToUnused(SceneLayerBase layer)
    {
      // ISSUE: unable to decompile the method.
    }

    public GameObject RootCanvasGo
    {
      get
      {
        return this.m_uiSceneGroup1RootCanvasGo;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLayerLoadAssetComplete(SceneLayerBase layer, GameObject asset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateLayerStack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SortLayerStack(List<SceneLayerBase> layerStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private LayerRenderSettingDesc GetDefaultRenderSetting()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ApplyRenderSetting(LayerRenderSettingDesc renderSetting)
    {
      // ISSUE: unable to decompile the method.
    }

    public void ApplyCurrRenderSetting()
    {
      this.ApplyRenderSettingImp(this.m_currRenderSetting);
    }

    public LayerRenderSettingDesc GetCurrRenderSetting()
    {
      return this.m_currRenderSetting;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ApplyRenderSettingImp(LayerRenderSettingDesc renderSetting)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GameObject GetRootCanvas(int index = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SceneLayerBase FindLayerByName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SceneLayerBase FindUILayerUnderLayer(
      string name,
      Func<SceneLayerBase, bool> filter)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Camera GetLayerCameraFromSceneByPath(string rectTransPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RectTransform GetRectTransformFromSceneByPath(string rectTransPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetObjectPath(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    public static SceneManager Instance
    {
      get
      {
        return SceneManager.m_instance;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static SceneManager()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
