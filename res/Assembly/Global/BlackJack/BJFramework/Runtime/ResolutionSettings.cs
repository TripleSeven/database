﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.ResolutionSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  [Serializable]
  public class ResolutionSettings
  {
    [Header("刷新率")]
    public int RefreshRate;
  }
}
