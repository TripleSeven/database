﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.GameManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.ConfigData;
using BlackJack.BJFramework.Runtime.Hotfix;
using BlackJack.BJFramework.Runtime.Log;
using BlackJack.BJFramework.Runtime.PlayerContext;
using BlackJack.BJFramework.Runtime.Resource;
using BlackJack.BJFramework.Runtime.Scene;
using BlackJack.BJFramework.Runtime.TaskNs;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime
{
  public abstract class GameManager : ITickable
  {
    private static GameManager m_instance;
    protected GameManager.GMState m_state;
    private TinyCorutineHelper m_corutineHelper;
    protected GameClientSetting m_gameClientSetting;
    protected LogManager m_logManager;
    protected TaskManager m_taskManager;
    protected ResourceManager m_resourceManager;
    protected IAudioManager m_audioManager;
    protected SceneManager m_sceneManager;
    protected UIManager m_uiManager;
    protected ClassLoader m_classLoader;
    protected HotfixManager m_hotfixManager;
    protected ClientConfigDataLoaderBase m_configDataLoader;
    protected StringTableManagerBase m_stringTableManager;
    protected IPlayerContextNetworkClient m_networkClient;
    protected PlayerContextBase m_playerContext;
    protected string m_currLocalization;
    protected Dictionary<string, object> m_gameManagerParamDict;
    private const string LocalizationPrefKey = "Localization";

    [MethodImpl((MethodImplOptions) 32768)]
    protected GameManager()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static T CreateAndInitGameManager<T>() where T : GameManager, new()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Assembly OnAssemblyResolve(object sender, ResolveEventArgs args)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAssemblyLoad(object sender, AssemblyLoadEventArgs args)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Initlize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitlizeAudioManager()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Uninitlize()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void OnApplicationQuit()
    {
      this.Uninitlize();
      GameManager.m_instance = (GameManager) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool InitlizeGameSetting()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected IAudioManager CreateAudioManager()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool StartLoadDynamicAssemblys(Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void OnLoadDynamicAssemblysCompleted(bool ret, Action<bool> onEnd)
    {
      onEnd(ret);
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator LoadDynamicAssemblysWorker(Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool StartHotfixManager(Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator StartHotfixManagerWorker(Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool StartLoadConfigData(Action<bool> onEnd, out int initLoadDataCount)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual bool OnLoadConfigDataEnd(Action<bool> onEnd)
    {
      return false;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool InitStringTableManager(Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T GetConfigData<T>() where T : ClientConfigDataLoaderBase
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool StartAudioManager(Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool InitlizeBeforeGameAuthLogin(string loginUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual bool InitGameNetworkClient()
    {
      throw new NotImplementedException();
    }

    public virtual bool InitPlayerContext()
    {
      throw new NotImplementedException();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T GetPlayerContext<T>() where T : PlayerContextBase
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T GetGameManagerParam<T>(string key) where T : class
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetGameManagerParam(string key, object value)
    {
      this.m_gameManagerParamDict[key] = value;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Return2Login(bool raiseCriticalDataCacheDirty)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Clear4Return2Login(bool isCacheDataDirty)
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void Clear4Relogin()
    {
    }

    public void StartCorutine(Func<IEnumerator> corutine)
    {
      this.m_corutineHelper.StartCorutine(corutine);
    }

    public void StartCorutine(IEnumerator corutine)
    {
      this.m_corutineHelper.StartCorutine(corutine);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitLocalizationInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLocalization(string localization, Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    public static GameManager Instance
    {
      get
      {
        return GameManager.m_instance;
      }
    }

    public GameClientSetting GameClientSetting
    {
      get
      {
        return this.m_gameClientSetting;
      }
    }

    public static Func<ResourceManager> CreateResourceManagerHandler { get; set; }

    public IAudioManager AudioManager
    {
      get
      {
        return this.m_audioManager;
      }
    }

    public ClientConfigDataLoaderBase ConfigDataLoader
    {
      get
      {
        return this.m_configDataLoader;
      }
    }

    public StringTableManagerBase StringTableManager
    {
      get
      {
        return this.m_stringTableManager;
      }
    }

    public IPlayerContextNetworkClient NetworkClient
    {
      get
      {
        return this.m_networkClient;
      }
    }

    public PlayerContextBase PlayerContext
    {
      get
      {
        return this.m_playerContext;
      }
    }

    public string LoginUserId { get; protected set; }

    public enum GMState
    {
      None = 0,
      Inited = 1,
      DynamicAssemblyLoading = 2,
      DynamicAssemblyLoadEnd = 3,
      HotfixAssemblyLoading = 4,
      HotfixAssemblyLoadEnd = 5,
      ConfigDataLoading = 6,
      ConfigDataLoadEnd = 7,
      AudioManagerLoading = 8,
      AudioManagerLoadEnd = 9,
      Ready = 9,
    }
  }
}
