﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Resource.StreamingAssetsFileList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime.Resource
{
  public class StreamingAssetsFileList : ScriptableObject
  {
    public int m_version;
    public List<StreamingAssetsFileList.ListItem> m_fileList;

    [MethodImpl((MethodImplOptions) 32768)]
    public StreamingAssetsFileList()
    {
      // ISSUE: unable to decompile the method.
    }

    [Serializable]
    public class ListItem
    {
      public string m_bundleName;
      public int m_bundleVersion;
      public string m_filePath;

      [MethodImpl((MethodImplOptions) 32768)]
      public ListItem(string path, int version)
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
