﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Resource.BundleData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime.Resource
{
  public class BundleData : ScriptableObject
  {
    public int m_version;
    public int m_basicVersion;
    public List<BundleData.SingleBundleData> m_bundleList;

    [MethodImpl((MethodImplOptions) 32768)]
    public BundleData()
    {
      // ISSUE: unable to decompile the method.
    }

    [Serializable]
    public class SingleBundleData
    {
      public string m_bundleName;
      public int m_version;
      public string m_bundleHash;
      public uint m_bundleCRC;
      public long m_size;
      public bool m_isInStreamingAssets;
      public bool m_isNeedPreUpdateByDefault;
      public bool m_isResaveFileBundle;
      public bool m_isLazyUpdate;
      public string m_localizationKey;
      public List<string> m_assetList;

      [MethodImpl((MethodImplOptions) 32768)]
      public SingleBundleData()
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
