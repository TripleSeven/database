﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Resource.BytesScripableObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime.Resource
{
  public class BytesScripableObject : ScriptableObject
  {
    public int m_size;
    [SerializeField]
    protected byte[] m_bytes;

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void SetBytes(byte[] bytes)
    {
      // ISSUE: unable to decompile the method.
    }

    public byte[] GetBytes()
    {
      return this.m_bytes;
    }
  }
}
