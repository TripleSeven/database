﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Resource.BundleDataHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.Resource
{
  public class BundleDataHelper
  {
    private BundleData m_internalBundleData;
    private bool m_assetPathIgnoreCase;
    private Dictionary<string, BundleData.SingleBundleData> m_bundleDataDict;
    private Dictionary<string, BundleData.SingleBundleData> m_assetPath2BundleDataDict;

    [MethodImpl((MethodImplOptions) 32768)]
    public BundleDataHelper(BundleData bundleData, bool assetPathIgnoreCase)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBundleVersionByName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BundleData.SingleBundleData GetBundleDataByName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BundleData.SingleBundleData GetBundleDataByAssetPath(string assetPath)
    {
      // ISSUE: unable to decompile the method.
    }

    public BundleData.SingleBundleData GetResaveFileBundleDataByPath(string relativePath)
    {
      return this.GetBundleDataByAssetPath(relativePath);
    }
  }
}
