﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Resource.ResourceManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.Utils;
using Boo.Lang;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BlackJack.BJFramework.Runtime.Resource
{
  public class ResourceManager : ITickable
  {
    public static string m_preUpdateVersion4BundleDataKey = "PreUpdateVersion4BundleData";
    protected static ResourceManager m_instance;
    protected TinyCorutineHelper m_corutineHelper;
    protected string m_currLocalization;
    protected List<string> m_multiLanguageList;
    protected bool m_needGC;
    protected int m_unloadUnusedAssetTimeInterval;
    protected DateTime m_nextUnloadUnusedAssetTime;
    protected DateTime m_nextCheckMemUseageTime;
    protected bool m_assetPathIgnoreCase;
    protected bool m_enableDetailResourceManagerLog;
    protected AsyncOperation m_currUnloadUnusedAssetsOperation;
    protected bool m_disableAssetBundle;
    protected bool m_loadAssetFromBundleInEditor;
    protected Dictionary<string, ResourceManager.AssetCacheItem> m_assetsCacheDict;
    protected List<ResourceManager.AssetCacheItem> m_assetsCacheToRemove;
    protected static string m_bundleDownloadUrlRoot;
    protected static string m_downloadUrlRootWithPlatform;
    protected string m_downloadBundleUrlPrefix;
    protected bool m_needUnloadAllUnusedBundles;
    protected bool m_needUnloadUnusedAssets;
    protected int m_loadingOpCount;
    protected HashSet<string> m_loadedAssetPathSet;
    protected const string m_streamingAssetsFileListVersionKey = "StreamingAssetsFileListVersion";
    protected const string m_bundleDataVersionKey = "BundleDataVersion";
    protected const string m_touchedBundleSetFileName = "TouchedBundleSet";
    public HashSet<string> m_loadFilePaths;
    private Stopwatch _stopWatch;
    public float m_loadingProgress;
    protected BundleData m_bundleData;
    protected BundleDataHelper m_bundleDataHelper;
    protected AssetBundleManifest m_assetBundleManifest;
    protected DateTime m_nextTickBundleTime;
    protected Dictionary<string, ResourceManager.BundleCacheItem> m_bundleCacheDict;
    protected List<ResourceManager.BundleCacheItem> m_bundleCachetoUnload;
    protected Dictionary<string, ResourceManager.BundleLoadingCtx> m_bundleLoadingCtxDict;
    protected HashSet<string> m_touchedBundleSet;
    protected bool m_touchedBundleSetDirty;
    protected bool m_skipAssetBundlePreUpdateing;
    protected int m_updateingWorkerCount;
    public Func<List<string>> GetCustomPreUpdateBundleListFunc;
    public Func<List<string>> GetCustomSkipPreUpdateBundleListFunc;
    protected List<BundleData.SingleBundleData> m_updateingBundleList;
    protected List<string> m_customSkipPreUpdateBundleList;
    protected List<string> m_customPreUpdateBundleList;
    protected int m_updateWorkerAliveCount;
    protected bool m_isAllUpdateWorkerSuccess;
    protected long m_assetBundleUpdateingTotalByte;
    protected long m_assetBundleUpdateingDownloadedByte;
    private bool m_disableAssetBundleDownload;
    private int m_currBundleDataVersion;
    private BundleData m_lastPreUpdateBundleData;
    private const string m_signalFileName = "dontmove.tmp";
    protected Dictionary<string, ResourceManager.ReserveItem> m_assetReserveDict;
    protected Dictionary<string, ResourceManager.ReserveItem> m_assetReserveDict4Keep;
    protected List<string> m_assetReserve4Remove;
    protected DateTime m_reserveTickDelayOutTime;
    protected const float ReserveTickDelayTime = 5f;
    private bool m_skipStreamingAssetsFileProcessing;
    private int m_currStreamingAssetsFileListVersion;
    private StreamingAssetsFileList m_streamingAssetsFileList;
    private BundleData m_streamingAssetsBundleData;
    private BundleDataHelper m_streamingAssetsBundleDataHelper;
    private List<BundleData.SingleBundleData> m_streamingAssetsMissingBundles;

    [MethodImpl((MethodImplOptions) 32768)]
    protected ResourceManager()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ResourceManager CreateResourceManager()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Initlize(ResourcesSettings settings, string currLocalization)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Uninitlize()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator LoadAsset<T>(
      string path,
      Action<string, T> onCompleted,
      bool noErrlog = false,
      bool loadAync = false)
      where T : UnityEngine.Object
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsNeedLoadAllForAssetPath(string assetPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartLoadAssetsCorutine(
      HashSet<string> pathList,
      IDictionary<string, UnityEngine.Object> assetDict,
      Action onComplete,
      bool loadAsync = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartLoadAssetCorutine(string path, Action<string, UnityEngine.Object> onComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected IEnumerator LoadAssetsCorutine(
      HashSet<string> pathList,
      int corutineId,
      IDictionary<string, UnityEngine.Object> assetDict,
      Action onComplete,
      bool loadAsync)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void LoadAssetByResourcesLoad<T>(
      string path,
      Action<string, UnityEngine.Object[]> onCompleted,
      bool noErrlog = false,
      bool isNeedLoadAllRes = false)
      where T : UnityEngine.Object
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void LoadAssetByAssetDatabase<T>(
      string path,
      Action<string, UnityEngine.Object[]> onCompleted,
      bool noErrlog = false,
      bool isNeedLoadAllRes = false)
      where T : UnityEngine.Object
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected UnityEngine.Object GetAssetFromCache(string key)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void PushAssetToCache(
      string key,
      UnityEngine.Object asset,
      ResourceManager.BundleCacheItem bundleCacheItem = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetDownloadUrlRoot(string url)
    {
      // ISSUE: unable to decompile the method.
    }

    public static string GetDownloadUrlRoot()
    {
      return ResourceManager.m_bundleDownloadUrlRoot;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLocalization(string localization, Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetLocalizationWithoutUnload(string localization)
    {
      this.m_currLocalization = localization;
    }

    public void SetMultiLanguageList(List<string> list)
    {
      this.m_multiLanguageList = list;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void TickAsset()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void TickGC()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void TickMemUseage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OutputMemUseage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool UnloadUnusedAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool UnloadUnusedResourceAll(Action onComplete = null, HashSet<string> keepReserve = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UnloadAllBundle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void CleanUnusedAssetsCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected string GetDownloadBundleUrl(string path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected string GetStreamingAssetBundleUrl(string bundleName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected string GetStreamingAssetBundlePath(string bundleName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected string GetBundleLoadingUrl(string bundleName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsBundleNeedLoadFromStreamingAssetFile(string bundleName, int version)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected IEnumerator LoadBundleFromCacheOrDownload(
      string url,
      int bundleVersion,
      uint bundleCRC,
      Action<AssetBundle> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    public HashSet<string> GetLoadedAssetPathSet()
    {
      return this.m_loadedAssetPathSet;
    }

    public event Action EventOnAssetLoaded
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static ResourceManager Instance
    {
      get
      {
        return ResourceManager.m_instance;
      }
    }

    public ResourceManager.RMState State { get; protected set; }

    protected event Action m_eventOnUnloadUnusedResourceAllCompleted
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public void SetDownloadBundleUrlPrefix(string prefix)
    {
      this.m_downloadBundleUrlPrefix = prefix;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected IEnumerator LoadAssetFromBundle(
      string path,
      Action<string, UnityEngine.Object[], ResourceManager.BundleCacheItem> onCompleted,
      bool noErrlog = false,
      bool loadAync = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected IEnumerator LoadBundle4UnityScene(
      string scenePath,
      Action<string, ResourceManager.BundleCacheItem> onComplete,
      bool noErrlog,
      bool loadAync)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected IEnumerator LoadBundle(
      BundleData.SingleBundleData bundleData,
      bool loadAync,
      Action<AssetBundle, ResourceManager.BundleCacheItem> onComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected IEnumerator LoadBundle(
      string bundleName,
      bool loadAync,
      Action<AssetBundle, ResourceManager.BundleCacheItem> onComplete,
      bool noErrlog = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected IEnumerator LoadBundleFromWWWOrStreamingImpl(
      BundleData.SingleBundleData bundleData,
      bool loadAync,
      bool ignoreCRC,
      Action<AssetBundle> onComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected string GetBundleNameByCurrLocalization(BundleData.SingleBundleData bundleData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool MakeAssetBundleDontUnload(string assetPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected ResourceManager.BundleCacheItem GetAssetBundleFromCache(
      string bundleName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected ResourceManager.BundleCacheItem PushAssetBundleToCache(
      string bundleName,
      AssetBundle bundle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnBundleCacheHit(ResourceManager.BundleCacheItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool RegBundleLoadingCtx(string bundleName, out ResourceManager.BundleLoadingCtx ctx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UnregBundleLoadingCtx(ResourceManager.BundleLoadingCtx bundleLoadingCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected BundleData.SingleBundleData GetBundleDataByAssetPath(string path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected BundleData.SingleBundleData GetResaveFileBundleDataByPath(
      string relativePath)
    {
      // ISSUE: unable to decompile the method.
    }

    protected string GetAssetNameByPath(string path)
    {
      return Path.GetFileNameWithoutExtension(path);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void TickBundle()
    {
      // ISSUE: unable to decompile the method.
    }

    protected bool UnloadAllUnusedBundles(bool onlyTimeOut = false)
    {
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int GetRealCacheVersion(BundleData.SingleBundleData realBundleData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void DealWithResaveFileInBundle(
      AssetBundle assetBundle,
      BundleDataHelper bundleHelper)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string ConvertRelativePathFile2OrginalPathFile(string relativePathFile)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetResaveRuntimePath(string srcDir, string relativePath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string TryGetResaveFile(string srcDir, string relativePath)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator DownloadResaveFile(
      string srcDir,
      HashSet<string> relativePaths,
      Action<string, bool> onComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartDownloadResaveFile(
      string srcDir,
      HashSet<string> relativePaths,
      Action<string, bool> onComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool LoadTouchedBundleSet()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SaveTouchedBundleSet(bool force = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool AddBundle2TouchedBundleSet(string bundleName, bool checkBundleData = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAssetInTouchedBundleSet(string assetPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartAssetBundleManifestLoading(Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected IEnumerator AssetBundleManifestLoadingWorker()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnAssetBundleManifestLoadingWorkerEnd(bool success)
    {
      // ISSUE: unable to decompile the method.
    }

    protected string GetAssetBundleManifestBundleName()
    {
      return Util.GetCurrentTargetPlatform();
    }

    protected event Action<bool> m_eventAssetBundleManifestLoading
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartAssetBundlePreUpdateing(Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void StartAssetBundleUpdateingWorkers(
      Action<bool> onEnd,
      Action<BundleData.SingleBundleData> onSingleBundleDownloadSuccess = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected IEnumerator AssetBundleUpdateingWorker(
      Action<bool> onEnd,
      Action<BundleData.SingleBundleData> onSingleBundleDownloadSuccess = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected IEnumerator WaitForTimeSecond(float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void CalcPreUpdateBundleListByBundleData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected BundleData.SingleBundleData GetSingleBundleData4PreUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnAssetBundlePreUpdateingWorkerEnd(bool success)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetAssetBundleUpdateingPercent()
    {
      // ISSUE: unable to decompile the method.
    }

    public long GetPreUpdateingDownloadedBytes()
    {
      return this.m_assetBundleUpdateingDownloadedByte;
    }

    public long GetTotalPreUpdateingDownloadBytes()
    {
      this.CalcPreUpdateBundleListByBundleData();
      return this.m_assetBundleUpdateingTotalByte;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartDownloadUntouchedAssets(HashSet<string> assets, Action<bool> OnEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnUntouchedAssetBundleUpdateingWorkerEnd(bool success)
    {
      // ISSUE: unable to decompile the method.
    }

    public long GetTotalUntouchedAssetBundleDownloadBytes()
    {
      return this.m_assetBundleUpdateingTotalByte;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsForceCustomPreUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsBundleInCustomPreUpdateList(string bundleName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsBundleInSkipPreUpdateList(string bundleName)
    {
      // ISSUE: unable to decompile the method.
    }

    protected event Action<bool> m_eventOnAssetBundleUpdateingEnd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartBundleDataLoading(Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator BundleDataLoadingWorker()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator CheckBundleDataVersionFromUrl(Action<bool, int, uint, bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBundleDataLoadingWorkerEnd(bool success)
    {
      // ISSUE: unable to decompile the method.
    }

    public BundleData GetBundleData()
    {
      return this.m_bundleData;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBundleDataBasicVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    private event Action<bool> EventOnBundleDataLoadingEnd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CheckBundleCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBundleCacheSignalFileExist()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateBundleCacheSignalFile()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearForCacheLost()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator ReserveAsset(
      string path,
      Action<string, UnityEngine.Object> onCompleted,
      bool noErrlog = false,
      int reserveTime = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartReserveAssetsCorutine(
      HashSet<string> pathList,
      IDictionary<string, UnityEngine.Object> assetDict,
      Action onComplete,
      bool loadAsync = false,
      int reserveTime = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ResourceManager.ReserveItem AddAsset2Reserve(
      string path,
      int reserveTime,
      UnityEngine.Object asset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetupReserveTimeOut(ResourceManager.ReserveItem reserveItem, int ReserveTime)
    {
      // ISSUE: unable to decompile the method.
    }

    protected void RemoveReserve(string path)
    {
      this.m_assetReserveDict.Remove(path);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RemoveReserve4CacheHit(string path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ClearAllReserve(HashSet<string> keepReserve = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void TickReserve()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartStreamingAssetsFilesProcessing(Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator StreamingAssetsFileProcessor()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator DealWithResaveFileInBundleAfterStreamingAssetProcessing()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStreamingAssetsFileProcessorEnd(bool success)
    {
      // ISSUE: unable to decompile the method.
    }

    private event Action<bool> m_eventOnStreamingAssetsFilesProcessingEnd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator LoadUnityScene(
      string path,
      Action<string, Scene?> onCompleted,
      bool noErrlog = false,
      bool loadAync = false)
    {
      // ISSUE: unable to decompile the method.
    }

    public enum RMState
    {
      None = 0,
      Inited = 1,
      StreamingAssetsFilesProcessing = 2,
      StreamingAssetsFilesProcessEnd = 3,
      BundleDataLoading = 4,
      BundleDataLoadEnd = 5,
      AssetBundlePreUpdateing = 6,
      AssetBundlePreUpdateEnd = 7,
      AssetBundleManifestLoading = 8,
      AssetBundleManifestLoadEnd = 9,
      Ready = 9,
    }

    protected class AssetCacheItem
    {
      public string m_cacheKey;
      public WeakReference m_weakRefrence;
      public bool m_removeReserveOnHit;
    }

    protected class BundleCacheItem
    {
      public static float s_validedHitInterval = 1f;
      public static int m_firstTimeOut = 600;
      public static int m_OnHitTimeOutDelay = 60;
      public string m_bundleName;
      public DateTime m_lastTouchTime;
      public DateTime m_timeOutTime;
      public int m_hitCount;
      public int m_refCount;
      public AssetBundle m_bundle;
      public bool m_dontUnload;
      public List<ResourceManager.BundleCacheItem> m_dependBundleCacheList;

      public void AddRefrence()
      {
        ++this.m_refCount;
      }

      public void RemoveRefrence()
      {
        --this.m_refCount;
      }
    }

    public class BundleLoadingCtx
    {
      public string m_bundleName;
      public AssetBundle m_bundle;
      public bool m_isEnd;
      public int m_ref;
    }

    protected class ReserveItem
    {
      public DateTime m_timeOut = DateTime.MinValue;
      public UnityEngine.Object m_asset;
    }
  }
}
