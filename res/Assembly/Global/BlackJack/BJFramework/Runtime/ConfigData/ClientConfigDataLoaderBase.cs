﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.ConfigData.ClientConfigDataLoaderBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Resource;
using ProtoBuf;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.ConfigData
{
  public abstract class ClientConfigDataLoaderBase
  {
    protected GameClientSetting m_gameClientSetting;
    protected Dictionary<string, UnityEngine.Object> m_assetDict;
    protected Dictionary<string, string> m_assetMD5Dict;
    private Dictionary<string, List<ClientConfigDataLoaderBase.LazyLoadConfigAssetInfo>> m_lazyLoadConfigDataAssetPathDict;

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientConfigDataLoaderBase(string luaModuleName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool StartInitializeFromAsset(Action<bool> onEnd, out int initLoadDataCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual IEnumerator OnInitLoadFromAssetEnd(
      bool ret,
      int initThreadCount,
      int loadCountForSingleYield,
      Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    protected abstract IEnumerator OnInitLoadFromAssetEndWorker(
      int totalWorkerCount,
      int workerId,
      int loadCountForSingleYield,
      Action<bool> onEnd,
      List<string> skipTypeList = null,
      List<string> filterTypeList = null);

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool OnInitLoadFromAssetEndWorkerForLuaDummyType()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void AddConfigDataItemForLuaDummyType(string typeName, DummyType dataItem)
    {
    }

    protected virtual List<string> GetAllInitLoadConfigDataTypeNameForLuaDummy()
    {
      return (List<string>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsUseMulitThreadOnInitLoadFromAssetEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    protected void ClearAsset()
    {
      this.m_assetDict.Clear();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual HashSet<string> GetAllInitLoadConfigDataAssetPath()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void LoadLazyLoadConfigDataAsset(
      string configDataName,
      string configAssetName,
      Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnLazyLoadFromAssetEnd(
      string configDataName,
      string configAssetName,
      UnityEngine.Object lasset,
      Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual string GetLazyLoadConfigAssetNameByKey(string configDataName, int key)
    {
      return string.Empty;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetConfigDataPath()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual Dictionary<string, List<ClientConfigDataLoaderBase.LazyLoadConfigAssetInfo>> GetAllLazyLoadConfigDataAssetPath()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientConfigDataLoaderBase.LazyLoadConfigAssetInfo GetLazyLoadConfigAssetInfo(
      string configDataName,
      string configAssetName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<DummyType> DeserializeExtensionTableOnLoadFromAssetEnd(
      BytesScriptableObjectMD5 dataObj,
      string assetPath,
      string typeName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReportNullAssetInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void FireEventOnConfigDataTableLoadEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnConfigDataTableLoadEnd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool ConfigDataAssetAllowNull { get; set; }

    public Dictionary<string, string> ConfigDataMD5Dict
    {
      get
      {
        return this.m_assetMD5Dict;
      }
    }

    public int InitLoadDataCount { get; private set; }

    public enum LazyLoadConfigAsssetState
    {
      Unload,
      Loading,
      Loaded,
      Loadfailed,
    }

    public class LazyLoadConfigAssetInfo
    {
      public string m_configAssetName;
      public ClientConfigDataLoaderBase.LazyLoadConfigAsssetState m_state;
    }
  }
}
