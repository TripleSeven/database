﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.HotfixSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  [Serializable]
  public class HotfixSettings
  {
    [Header("是否启用hotfix")]
    public bool EnableHotfix;
    [Header("hotfix程序集名称")]
    public string HotfixAssemblyName;
    public string HotfixAssemblyRoot;
    public string HotfixAssemblyAssetRoot;
    public string[] HotFixableTypePrefix;
    public string[] ExceptedTypePrefix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HotfixSettings()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
