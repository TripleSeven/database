﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Hotfix.HotfixManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using Assets.XIL;
using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using wxb;

namespace BlackJack.BJFramework.Runtime.Hotfix
{
  public class HotfixManager
  {
    private static HotfixManager m_instance;
    private HotfixManager.HotfixResLoader m_hotfixResLoader;

    private HotfixManager()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static HotfixManager CreateHotfixManager()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator Init()
    {
      // ISSUE: unable to decompile the method.
    }

    public static HotfixManager Instance
    {
      get
      {
        return HotfixManager.m_instance;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static TDel CallBaseMethod<T, TDel>(T obj, string methodName, ParamType[] types = null) where TDel : class
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static MethodInfo GetMethod(Type type, string methodName, ParamType[] types = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool TypeWithoutRefEquals(Type type, bool isRef, Type comparedType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static T CreateObject<T>(params object[] p)
    {
      // ISSUE: unable to decompile the method.
    }

    public class HotfixResLoader : IResLoad
    {
      private MemoryStream m_hotfixAssemblyStream;
      private MemoryStream m_hotfixAssemblyPdbStream;
      private MemoryStream m_hotfixAssemblyMdbStream;

      [MethodImpl((MethodImplOptions) 32768)]
      public Stream GetStream(string path)
      {
        // ISSUE: unable to decompile the method.
      }

      [DebuggerHidden]
      [MethodImpl((MethodImplOptions) 32768)]
      public IEnumerator Init()
      {
        // ISSUE: unable to decompile the method.
      }

      public bool IsHotfixAssemblyLoadSuccessed()
      {
        return this.m_hotfixAssemblyStream != null;
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void Clear()
      {
        // ISSUE: unable to decompile the method.
      }

      [DebuggerHidden]
      [MethodImpl((MethodImplOptions) 32768)]
      private IEnumerator LoadAssemblyFromAsset(
        string assetName,
        System.Action<MemoryStream> onEnd)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      private MemoryStream LoadAssemblyFromFile(string path)
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
