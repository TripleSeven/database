﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.PathHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime
{
  public static class PathHelper
  {
    public static string AssetBundlesBuildOutputPath;
    private static string m_assetBundlesBuildOutputPathWithPlatform;
    private static string m_assetBundlesDownloadPath4Editor;
    public static string BundleDataPath;
    private static string m_bundleDataAssetName;
    private static string m_bundleDataAssetPathNoPostfix;
    private static string m_bundleDataAssetPath;
    private static string m_streamAssetBundleDataAssetPathInEditor;
    private static string m_streamAssetBundleDataAssetPath;
    private static string m_bundleDataBundleName;
    public static string BundleDataVersionFileName;
    private static string m_bundleDataVersionFilePath;
    public static string StreamingAssetsBundlePathDirName;
    private static string m_streamingAssetsRootPath;
    private static string m_streamingAssetsBundlePath;
    private static string m_streamingAssetsFileListPath;
    private static string m_streamingAssetsFileListPathNoPostfix;
    private static string m_streamingAssetsFileListResourcesName;

    public static string AssetBundlesBuildOutputPathWithPlatform
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string AssetBundlesDownloadPath4Editor
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string BundleDataAssetName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string BundleDataAssetPathNoPostfix
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string BundleDataAssetPath
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string StreamAssetBundleDataAssetPathInEditor
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string StreamAssetBundleDataResourcesPath
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string BundleDataBundleName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string BundleDataVersionFilePath
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string StreamingAssetsRootPath
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string StreamingAssetsBundlePath
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string StreamingAssetsFileListPath
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string StreamingAssetsFileListPathNoPostfix
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string StreamingAssetsFileListResourcesName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string DynamicAssemblyRoot
    {
      get
      {
        return "Assets/plugins/BlackJack";
      }
    }

    public static string DynamicAssemblyAssetRoot
    {
      get
      {
        return "Assets/GameProject/RuntimeAssets/Assemblys";
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static PathHelper()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
