﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.AudioManager4Unity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Audio;

namespace BlackJack.BJFramework.Runtime
{
  public class AudioManager4Unity : IAudioManager, ITickable
  {
    private static AudioManager4Unity m_instance;
    private TinyCorutineHelper m_corutineHelper;
    private GameObject m_audioRoot;
    private AudioSource m_backGroundAudioSource1;
    private AudioSource m_backGroundAudioSource2;
    private AudioSource m_currBackGroundAudioSource;
    private AudioSource m_oldBackGroundAudioSource;
    private DateTime? m_bgmSwapTime;
    private AudioSource m_soundAudioSource;
    private AudioSource m_playerVoiceAudioSource;
    private AudioSource m_speechAudioSource;
    private AudioMixer m_mainAudioMixer;
    private AudioListener m_mainAudioListener;
    private bool m_muteBackGroundMusic;
    private bool m_muteMovieBackGroundMusic;
    private bool m_muteSound;
    private bool m_mutePlayerVoice;
    private bool m_muteSpeech;
    private float m_backGroundMusicVolume;
    private float m_soundVolume;
    private float m_playerVoiceVolume;
    private float m_speechVolume;
    private const float BGMSwapTime = 1f;

    [MethodImpl((MethodImplOptions) 32768)]
    private AudioManager4Unity()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static AudioManager4Unity CreateAudioManager()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Initlize()
    {
      // ISSUE: unable to decompile the method.
    }

    public void Pause(bool pause)
    {
      Debug.LogError("Do not call: AudioManager4Unity.Pause()");
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Uninitlize()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator Start(Action<bool> onEnd, string mixerAssetPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableDefaultAudioListener(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopAll()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMute(
      bool muteBackGroundMusic,
      bool muteSound,
      bool muteMovieBackGroundMusic,
      bool mutePlayerVoice,
      bool muteSpeech)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBackGroundMusicMute(bool muteBackGroundMusic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetVolume(
      float backGroundMusicVolume,
      float soundVolume,
      float playerVoiceVolume,
      float speechVolume)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetVolume(string category, float volume, bool isSmooth)
    {
    }

    public float GetVolume(string category)
    {
      return 1f;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayBackGroundMusic(string music)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopBackGroundMusic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IAudioPlayback PlaySound(string sound, float volume = 1f)
    {
      // ISSUE: unable to decompile the method.
    }

    public void PlaySound(
      string sound,
      AudioClip audioClip,
      float volume = 1f,
      bool allowRepeatedPlaying = false)
    {
    }

    public List<string> GetLanguages()
    {
      Debug.LogError("Don't call function: AudioManager4Unity.GetLanguages!");
      return (List<string>) null;
    }

    public void SetLanguage(string language)
    {
      Debug.LogError("Don't call function: AudioManager4Unity.SetLanguage!");
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetLanguage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlaySound(AudioClip sound, float volume = 1f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayPlayerVoice(AudioClip sound, float volume = 1f)
    {
      // ISSUE: unable to decompile the method.
    }

    public void StopSound(string sound)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopSound()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopPlayerVoice()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlaySpeech(AudioClip sound, float volume = 1f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlaySpeech(string sound, float volume = 1f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopSpeech()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsSpeechPlaying()
    {
      return this.m_speechAudioSource.isPlaying;
    }

    public bool IsSoundPlaying()
    {
      return this.m_soundAudioSource.isPlaying;
    }

    public bool IsPlayerVoicePlaying()
    {
      return this.m_playerVoiceAudioSource.isPlaying;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private float GetRealSoundAudioSourceVolume(float soundVolume)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private float GetPlayerVoiceAudioSourceVolume(float soundVolume)
    {
      // ISSUE: unable to decompile the method.
    }

    public static AudioManager4Unity Instance
    {
      get
      {
        return AudioManager4Unity.m_instance;
      }
    }

    private AudioListener MainAudioListener
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator CreateMainAudioMixer(Action<bool> onEnd, string mixerAssetPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopAllByAudioMixer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetMuteByAudioMixer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetVolumeByAudioMixer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopBackGroundMusicByAudioMixer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopSoundByAudioMixer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopSpeechByAudioMixer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopPlayerVoiceByAudioMixer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private float TransformNormalizeVolumeToRealAudioMixerGroupVolume(float normalizeVolume)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private AudioMixerGroup GetAudioMixerGroupBySubPathName(
      AudioMixer audioMixer,
      string subPathName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopAllByAudioSource()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetMuteByAudioSource()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetVolumeByAudioSource()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopBackGroundMusicByAudioSource()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopSoundByAudioSource()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopSpeechByAudioSource()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopPlayerVoiceByAudioSource()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
