﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.CriAudioPlayback
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime
{
  internal class CriAudioPlayback : IAudioPlayback
  {
    private CriAtomExPlayback m_playback;
    private string m_cueName;

    [MethodImpl((MethodImplOptions) 32768)]
    public CriAudioPlayback(CriAtomExPlayback playback, string cueName)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Stop()
    {
      this.m_playback.Stop();
    }

    public string CueName
    {
      get
      {
        return this.m_cueName;
      }
    }

    public float Seconds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsStoped()
    {
      return this.m_playback.status == CriAtomExPlayback.Status.Removed;
    }
  }
}
