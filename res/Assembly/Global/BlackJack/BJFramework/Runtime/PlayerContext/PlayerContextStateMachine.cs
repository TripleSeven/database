﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.PlayerContext.PlayerContextStateMachine
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.PlayerContext
{
  public class PlayerContextStateMachine : StateMachine
  {
    public PlayerContextStateMachine()
    {
      this.State = 0;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int SetStateCheck(int commingEvent, int newState = -1, bool testOnly = false)
    {
      // ISSUE: unable to decompile the method.
    }

    public PlayerContextStateMachine.StateCode SetStateCheck(
      PlayerContextStateMachine.EventCode commingEvent,
      PlayerContextStateMachine.StateCode newState = PlayerContextStateMachine.StateCode.Invalid,
      bool testOnly = false)
    {
      return (PlayerContextStateMachine.StateCode) this.SetStateCheck((int) commingEvent, (int) newState, testOnly);
    }

    public PlayerContextStateMachine.StateCode StateEnumValue
    {
      get
      {
        return (PlayerContextStateMachine.StateCode) this.State;
      }
    }

    public enum StateCode
    {
      Invalid = -1,
      None = 0,
      Disconnected = 1,
      AuthLoginStarted = 2,
      RedirectWaitDisconnect = 3,
      SessionLoginStarted = 4,
      GameLoginDone = 5,
      PlayerInitEnd = 6,
    }

    public enum EventCode
    {
      OnConnected = 1,
      OnDisconnected = 2,
      OnAuthLoginStart = 3,
      OnAuthLoginAckOk = 4,
      OnAuthLoginAckOkRedirect = 5,
      OnAuthLoginAckFail = 6,
      OnSessionLoginStart = 7,
      OnSessionLoginAckOk = 8,
      OnSessionLoginAckFail = 9,
      OnGameLogicMsgSend = 10, // 0x0000000A
      OnPlayerInitEnd = 11, // 0x0000000B
    }
  }
}
