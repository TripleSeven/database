﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.PlayerContext.PlayerContextBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.Utils;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.PlayerContext
{
  public abstract class PlayerContextBase : ITickable, IPlayerContextNetworkEventHandler
  {
    protected IPlayerContextNetworkClient m_networkClient;
    protected TinyCorutineHelper m_tinyCorutineHelper;
    protected PlayerContextStateMachine m_fsm;
    protected string m_deviceId;
    protected string m_clientVersion;
    private int m_loginChannelId;
    private int m_bornChannelId;
    private string m_localization;
    protected string m_sessionToken;
    protected bool m_inited;
    private DateTime? m_serverTimeSynced;
    private DateTime m_localTimeAtServerTimeSynced;
    private DateTime m_currTickServerTime;

    [MethodImpl((MethodImplOptions) 32768)]
    protected PlayerContextBase()
    {
      // ISSUE: unable to decompile the method.
    }

    public static PlayerContextBase GetInstance()
    {
      return GameManager.Instance.PlayerContext;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void InitWithNetworkClient(IPlayerContextNetworkClient networkClient)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsInited()
    {
      return this.m_inited;
    }

    public void BlockNetworkClient(bool isBlock)
    {
      this.m_networkClient.BlockProcessMsg(isBlock);
    }

    protected void OnPlayerInfoInitEndNtf()
    {
      this.m_inited = true;
    }

    public abstract bool IsDataCacheDirtyByPlayerInfoInitAck(
      object msg,
      out bool raiseCriticalDataCacheDirty);

    public abstract bool IsPlayerInfoInitAck4CheckOnly(object msg);

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SyncServerTime(DateTime serverTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetCurrServerTime()
    {
      // ISSUE: unable to decompile the method.
    }

    public DateTime GetCurrTickServerTime()
    {
      return this.m_currTickServerTime;
    }

    public bool Disconnect()
    {
      return this.m_networkClient.Disconnect();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool StartGameAuthLogin(
      string serverAddress,
      int serverPort,
      string authToken,
      string localization,
      int loginChannelId,
      int bornChannelId,
      string serverDomain)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSessionToken(string sessionToken, int channelId, int bornChannelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool StartGameSessionLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual bool SendPlayerInfoInitReq()
    {
      throw new NotImplementedException();
    }

    public virtual bool SendPlayerInfoReqOnReloginBySession(bool checkOnly)
    {
      throw new NotImplementedException();
    }

    public virtual bool SendWorldEnterReqOnReloginBySession()
    {
      throw new NotImplementedException();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool CheckForSessionLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    public static string CreateFakeAuthToken(string platformUserId, string password)
    {
      return string.Format("1|{0}|{1}|{2}|Self|0|0|0", (object) platformUserId, (object) platformUserId, (object) password);
    }

    protected virtual string GetDeviceId()
    {
      throw new NotImplementedException();
    }

    protected virtual string GetClientVersion()
    {
      throw new NotImplementedException();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnGameServerConnected()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnGameServerDisconnected()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnGameServerError(int err, string excepionInfo = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnLoginByAuthTokenAck(int result, bool needRedirect, string sessionToken)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnLoginBySessionTokenAck(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnGameServerMessage(object msg, int msgId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnGameServerMessageExtend(object msg, int msgId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void OnTimeJumped()
    {
    }

    public event Action EventOnGameServerConnected
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGameServerNetworkError
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnGameServerDisconnected
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Func<int, string, bool, bool> EventOnLoginByAuthTokenAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Func<int, bool> EventOnLoginBySessionTokenAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public virtual event Action EventOnGameServerDataUnsyncNotify
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public virtual event Action<object> EventOnPlayerInfoInitAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public virtual event Action EventOnPlayerInfoInitEnd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
