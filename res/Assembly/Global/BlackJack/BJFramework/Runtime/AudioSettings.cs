﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.AudioSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace BlackJack.BJFramework.Runtime
{
  [Serializable]
  public class AudioSettings
  {
    public string AudioMixerAssetPath;
    public string AudioMixerBGMGroupSubPath;
    public string AudioMixerSoundEffectGroupSubPath;
    public string AudioMixerPlayerVoiceGroupSubPath;
    public string AudioMixerSpeechGroupSubPath;
    public string AudioMixerBGMVolumeParamName;
    public string AudioMixerMovieBGMVolumeParamName;
    public string AudioMixerSoundEffectParamName;
    public string AudioMixerPlayerVoiceVolumeParamName;
    public string AudioMixerSpeechVolumeParamName;
    public bool EnableCRI;
    public string CRIAudioManagerAsset;
    public List<string> Languages;
    public int DefaultLanguageIndex;
    public string CRIVideoAssetPathRoot;
    public string CRIVideoAssetPathRootForEditor;
    public bool EnableDownload;
  }
}
