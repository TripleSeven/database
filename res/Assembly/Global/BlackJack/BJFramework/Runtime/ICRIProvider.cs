﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.ICRIProvider
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  public interface ICRIProvider
  {
    GameObject GetCRIManagerObject();

    void CriRegisterAcf(string acfFullPath);

    void AddCRIComponentSources();

    void CRIRemoveCueSheet(string sheetName);

    void CRIAddCueSheet(string sheetName, string acbFullPath, string awbFullPath);

    float GetCueLength(string sheetName, string cueName);

    void SetCategoryVolume(string category, float volume);

    float GetCategoryVolume(string category);

    IAudioPlayback PlaySound(string sound);

    void Pause(bool pause);
  }
}
