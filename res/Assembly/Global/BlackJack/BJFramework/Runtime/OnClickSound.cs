﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.OnClickSound
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BlackJack.BJFramework.Runtime
{
  public class OnClickSound : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
  {
    public string m_clickSoundName;
    private bool m_isClicked;
    private bool m_shouldLatePlay;

    [MethodImpl((MethodImplOptions) 32768)]
    public OnClickSound()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Start()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnToggleValueChanged(bool val)
    {
      if (!val)
        return;
      this.m_shouldLatePlay = true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LateUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    public void OnPointerClick(PointerEventData eventData)
    {
      this.m_isClicked = true;
    }
  }
}
