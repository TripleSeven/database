﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Lua.LuaManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 972A2846-EBE4-4323-8745-F884A18FA509
// Assembly location: D:\Games\Langrisser5\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.Lua
{
  [CustomLuaClass]
  public class LuaManager
  {
    private HashSet<string> m_bindInfoStrings;
    private static LuaManager m_instance;
    private string m_luaRootPath;
    private string m_luaInitModuleName;
    private LuaSvr m_luaSvr;
    private bool m_enableProtobufExtension;
    private Dictionary<string, LuaTable> m_hotfixDict;
    private LuaProtoBufExtensionHandler m_luaProtoBufExtensionHandler;
    public const string LuaObjHelperMemberName = "m_luaObjHelper";

    [MethodImpl((MethodImplOptions) 32768)]
    private LuaManager()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public static LuaManager CreateLuaManager()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public bool Initlize(string luaRootPath, string luaInitModule = "LuaManagerInit", bool enableProtobufExtension = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public void Uninitlize()
    {
    }

    [DoNotToLua]
    public void SetEventBindAllInPublishVersion(Action bindAction)
    {
    }

    [DebuggerHidden]
    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator LoadBindInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public void FilterBindInfo(List<Action<IntPtr>> list)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator StartLuaSvr(Action<bool> onComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public LuaState GetLuaState()
    {
      return (LuaState) LuaSvr.mainState;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LuaTable GetHotFixLuaModuleByTypeFullName(Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegHotFix(string typeFullName, LuaTable module)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public static bool TryInitHotfixForObj(object obj, string luaModuleName = null, Type objType = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LuaTable RequireModule(string moduleName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private byte[] LuaLoader(string moduleName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLuaModuleExist(string moduleName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetModulePath(string moduleName)
    {
      // ISSUE: unable to decompile the method.
    }

    public static LuaManager Instance
    {
      get
      {
        return LuaManager.m_instance;
      }
    }
  }
}
