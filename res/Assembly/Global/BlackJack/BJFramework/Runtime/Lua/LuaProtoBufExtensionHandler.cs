﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Lua.LuaProtoBufExtensionHandler
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 972A2846-EBE4-4323-8745-F884A18FA509
// Assembly location: D:\Games\Langrisser5\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.Lua
{
  public class LuaProtoBufExtensionHandler : IProtobufExensionHandler
  {
    protected LuaTable m_protocolLuaDefineModule;
    protected LuaTable m_configDataLuaDefineModule;
    protected LuaTable m_protocolLuaDefine;
    protected LuaTable m_protocolMsgIdDict;
    protected LuaTable m_configDataLuaDefine;
    protected Dictionary<string, LuaProtobufExtensionTypeInfo> m_typeInfoDict;
    protected Dictionary<int, LuaProtobufExtensionTypeInfo> m_typeInfoDictById;
    private const string ProtocolLuaDefineModuleName = "ProtoBuf.ProtocolLuaDefine";
    private const string ConfigDataLuaDefineModuleName = "ProtoBuf.ConfigDataLuaDefine";

    [MethodImpl((MethodImplOptions) 32768)]
    protected LuaProtoBufExtensionHandler()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static LuaProtoBufExtensionHandler Create()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsSystemType(string typeName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IProtobufExtensionTypeInfo GetExtTypeInfo(string typeName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IProtobufExtensionTypeInfo GetExtTypeInfo(int msgId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<IProtobufExtensionTypeInfo> GetExtTypeInfoList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object GetExtensionMember(object extObj, string extMemberName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetExtensionMember(object extObj, object memberValue, string extMemberName)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable GetExtensionMemberListIEnumerable(object list)
    {
      return (IEnumerable) (list as LuaTable);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddExtensionMemberListItem(object list, object item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object CreateExtensionList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Type ResolveTypeByName(string typeName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private LuaTable GetHotfixLuaObj(object extObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private LuaTable GetLuaTypeDefine(string typeName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private LuaTable GetLuaTypeDefineByMsgId(int msgId)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
