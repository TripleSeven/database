﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Lua.LuaExportChecker
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 972A2846-EBE4-4323-8745-F884A18FA509
// Assembly location: D:\Games\Langrisser5\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.BJFramework.Runtime.Lua
{
  [HotFix]
  public class LuaExportChecker
  {
    public static int m_publicStaticField;
    protected static int m_protectedStaticField;
    private static int m_privateStaticField;
    public int m_publicField;
    protected int m_protectedField;
    private int m_privateField;
    public Action m_publicDelegate;
    protected Action m_protectedDelegate;
    private Action m_privateDelegate;
    private LuaExportChecker.ReturnIntDelegate ReturnInt;
    [DoNotToLua]
    private LuaExportChecker.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_PublicMethod_hotfix;
    private LuaFunction m_ProtectedMethod_hotfix;
    private LuaFunction m_PrivateMethod_hotfix;
    private LuaFunction m_get_PublicProperty_hotfix;
    private LuaFunction m_set_PublicPropertyInt32_hotfix;
    private LuaFunction m_get_ProtectedProperty_hotfix;
    private LuaFunction m_set_ProtectedPropertyInt32_hotfix;
    private LuaFunction m_get_PrivateProperty_hotfix;
    private LuaFunction m_set_PrivatePropertyInt32_hotfix;
    private LuaFunction m_add_PublicEventAction_hotfix;
    private LuaFunction m_remove_PublicEventAction_hotfix;
    private LuaFunction m_add_ProtectedEventAction_hotfix;
    private LuaFunction m_remove_ProtectedEventAction_hotfix;
    private LuaFunction m_add_PrivateEventAction_hotfix;
    private LuaFunction m_remove_PrivateEventAction_hotfix;
    private LuaFunction m_add_ReturnIntEventReturnIntDelegate_hotfix;
    private LuaFunction m_remove_ReturnIntEventReturnIntDelegate_hotfix;
    private LuaFunction m_ReturnAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public LuaExportChecker()
    {
      // ISSUE: unable to decompile the method.
    }

    public static void PublicStaticMethod()
    {
    }

    protected static void ProtectedStaticMethod()
    {
    }

    private static void PrivateStaticMethod()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PublicMethod()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ProtectedMethod()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PrivateMethod()
    {
      // ISSUE: unable to decompile the method.
    }

    public static int PublicStaticProperty { get; set; }

    protected static int ProtectedStaticProperty { get; set; }

    private static int PrivateStaticProperty { get; set; }

    public int PublicProperty
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected int ProtectedProperty
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private int PrivateProperty
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static event Action PublicStaticEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected static event Action ProtectedStaticEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private static event Action PrivateStaticEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action PublicEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected event Action ProtectedEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private event Action PrivateEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event LuaExportChecker.ReturnIntDelegate ReturnIntEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Action ReturnAction()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public LuaExportChecker.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void __callDele_PublicStaticEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    private static void __clearDele_PublicStaticEvent()
    {
      LuaExportChecker.PublicStaticEvent = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void __callDele_ProtectedStaticEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    private static void __clearDele_ProtectedStaticEvent()
    {
      LuaExportChecker.ProtectedStaticEvent = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void __callDele_PrivateStaticEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    private static void __clearDele_PrivateStaticEvent()
    {
      LuaExportChecker.PrivateStaticEvent = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_PublicEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_PublicEvent()
    {
      this.PublicEvent = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_ProtectedEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_ProtectedEvent()
    {
      this.ProtectedEvent = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_PrivateEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_PrivateEvent()
    {
      this.PrivateEvent = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int __callDele_ReturnIntEvent(int a)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_ReturnIntEvent(int a)
    {
      this.ReturnIntEvent = (LuaExportChecker.ReturnIntDelegate) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_m_protectedDelegate()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_m_protectedDelegate()
    {
      this.m_protectedDelegate = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_m_privateDelegate()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_m_privateDelegate()
    {
      this.m_privateDelegate = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int __callDele_ReturnInt(int a)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_ReturnInt(int a)
    {
      this.ReturnInt = (LuaExportChecker.ReturnIntDelegate) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public delegate int ReturnIntDelegate(int a);

    public class LuaExportHelper
    {
      private LuaExportChecker m_owner;

      public LuaExportHelper(LuaExportChecker owner)
      {
        this.m_owner = owner;
      }

      public static void __callDele_PublicStaticEvent()
      {
        LuaExportChecker.__callDele_PublicStaticEvent();
      }

      public static void __clearDele_PublicStaticEvent()
      {
        LuaExportChecker.__clearDele_PublicStaticEvent();
      }

      public static void __callDele_ProtectedStaticEvent()
      {
        LuaExportChecker.__callDele_ProtectedStaticEvent();
      }

      public static void __clearDele_ProtectedStaticEvent()
      {
        LuaExportChecker.__clearDele_ProtectedStaticEvent();
      }

      public static void __callDele_PrivateStaticEvent()
      {
        LuaExportChecker.__callDele_PrivateStaticEvent();
      }

      public static void __clearDele_PrivateStaticEvent()
      {
        LuaExportChecker.__clearDele_PrivateStaticEvent();
      }

      public void __callDele_PublicEvent()
      {
        this.m_owner.__callDele_PublicEvent();
      }

      public void __clearDele_PublicEvent()
      {
        this.m_owner.__clearDele_PublicEvent();
      }

      public void __callDele_ProtectedEvent()
      {
        this.m_owner.__callDele_ProtectedEvent();
      }

      public void __clearDele_ProtectedEvent()
      {
        this.m_owner.__clearDele_ProtectedEvent();
      }

      public void __callDele_PrivateEvent()
      {
        this.m_owner.__callDele_PrivateEvent();
      }

      public void __clearDele_PrivateEvent()
      {
        this.m_owner.__clearDele_PrivateEvent();
      }

      public int __callDele_ReturnIntEvent(int a)
      {
        return this.m_owner.__callDele_ReturnIntEvent(a);
      }

      public void __clearDele_ReturnIntEvent(int a)
      {
        this.m_owner.__clearDele_ReturnIntEvent(a);
      }

      public void __callDele_m_protectedDelegate()
      {
        this.m_owner.__callDele_m_protectedDelegate();
      }

      public void __clearDele_m_protectedDelegate()
      {
        this.m_owner.__clearDele_m_protectedDelegate();
      }

      public void __callDele_m_privateDelegate()
      {
        this.m_owner.__callDele_m_privateDelegate();
      }

      public void __clearDele_m_privateDelegate()
      {
        this.m_owner.__clearDele_m_privateDelegate();
      }

      public int __callDele_ReturnInt(int a)
      {
        return this.m_owner.__callDele_ReturnInt(a);
      }

      public void __clearDele_ReturnInt(int a)
      {
        this.m_owner.__clearDele_ReturnInt(a);
      }

      public static int m_protectedStaticField
      {
        get
        {
          return LuaExportChecker.m_protectedStaticField;
        }
        set
        {
          LuaExportChecker.m_protectedStaticField = value;
        }
      }

      public static int m_privateStaticField
      {
        get
        {
          return LuaExportChecker.m_privateStaticField;
        }
        set
        {
          LuaExportChecker.m_privateStaticField = value;
        }
      }

      public int m_protectedField
      {
        get
        {
          return this.m_owner.m_protectedField;
        }
        set
        {
          this.m_owner.m_protectedField = value;
        }
      }

      public int m_privateField
      {
        get
        {
          return this.m_owner.m_privateField;
        }
        set
        {
          this.m_owner.m_privateField = value;
        }
      }

      public Action m_protectedDelegate
      {
        get
        {
          return this.m_owner.m_protectedDelegate;
        }
        set
        {
          this.m_owner.m_protectedDelegate = value;
        }
      }

      public Action m_privateDelegate
      {
        get
        {
          return this.m_owner.m_privateDelegate;
        }
        set
        {
          this.m_owner.m_privateDelegate = value;
        }
      }

      public LuaExportChecker.ReturnIntDelegate ReturnInt
      {
        get
        {
          return this.m_owner.ReturnInt;
        }
        set
        {
          this.m_owner.ReturnInt = value;
        }
      }

      public static int ProtectedStaticProperty
      {
        get
        {
          return LuaExportChecker.ProtectedStaticProperty;
        }
        set
        {
          LuaExportChecker.ProtectedStaticProperty = value;
        }
      }

      public static int PrivateStaticProperty
      {
        get
        {
          return LuaExportChecker.PrivateStaticProperty;
        }
        set
        {
          LuaExportChecker.PrivateStaticProperty = value;
        }
      }

      public int ProtectedProperty
      {
        get
        {
          return this.m_owner.ProtectedProperty;
        }
        set
        {
          this.m_owner.ProtectedProperty = value;
        }
      }

      public int PrivateProperty
      {
        get
        {
          return this.m_owner.PrivateProperty;
        }
        set
        {
          this.m_owner.PrivateProperty = value;
        }
      }

      public static void ProtectedStaticMethod()
      {
        LuaExportChecker.ProtectedStaticMethod();
      }

      public static void PrivateStaticMethod()
      {
        LuaExportChecker.PrivateStaticMethod();
      }

      public void ProtectedMethod()
      {
        this.m_owner.ProtectedMethod();
      }

      public void PrivateMethod()
      {
        this.m_owner.PrivateMethod();
      }

      public static event Action ProtectedStaticEvent = LuaExportChecker.ProtectedStaticEvent;

      public static event Action PrivateStaticEvent = LuaExportChecker.PrivateStaticEvent;

      public event Action ProtectedEvent
      {
        add
        {
          this.m_owner.ProtectedEvent += value;
        }
        remove
        {
          this.m_owner.ProtectedEvent += value;
        }
      }

      public event Action PrivateEvent
      {
        add
        {
          this.m_owner.PrivateEvent += value;
        }
        remove
        {
          this.m_owner.PrivateEvent += value;
        }
      }
    }
  }
}
