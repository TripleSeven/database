﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Lua.LuaCoroutineChecker
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 972A2846-EBE4-4323-8745-F884A18FA509
// Assembly location: D:\Games\Langrisser5\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using SLua;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime.Lua
{
  [HotFix]
  public class LuaCoroutineChecker : MonoBehaviour
  {
    [DoNotToLua]
    private LuaCoroutineChecker.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_Awake_hotfix;
    private LuaFunction m_Start_hotfix;
    private LuaFunction m_TestCoroutine_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private void Awake()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Start()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator TestCoroutine()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public LuaCoroutineChecker.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private LuaCoroutineChecker m_owner;

      public LuaExportHelper(LuaCoroutineChecker owner)
      {
        this.m_owner = owner;
      }

      public void Awake()
      {
        this.m_owner.Awake();
      }

      public void Start()
      {
        this.m_owner.Start();
      }

      public IEnumerator TestCoroutine()
      {
        return this.m_owner.TestCoroutine();
      }
    }
  }
}
