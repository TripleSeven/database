﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Lua.LuaHotFixExample
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 972A2846-EBE4-4323-8745-F884A18FA509
// Assembly location: D:\Games\Langrisser5\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.Lua
{
  [CustomLuaClass]
  public class LuaHotFixExample
  {
    private ObjectLuaHotFixState m_hotfixState;
    private BJLuaObjHelper m_luaObjHelper;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetRefParamLuaHotFixExampleInt32LuaTestStruct_hotfix;
    private LuaFunction m_GetOutParamLuaHotFixExampleInt32LuaTestStruct_hotfix;
    private LuaFunction m_WithOutParamLuaHotFixExampleInt32LuaTestStruct_hotfix;
    private LuaFunction m_ReturnVoid_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public LuaHotFixExample()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WithOutParam(
      int notOutParam,
      out LuaHotFixExample classParam,
      out LuaTestStruct valueTypeParam,
      out int intParam,
      out float floatParam,
      out bool boolParam,
      out ObjectLuaHotFixState enumParam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetOutParam(
      out LuaHotFixExample classParam,
      LuaHotFixExample notOutClassParam,
      out LuaTestStruct valueTypeParam,
      LuaTestStruct notOutvalueTypeParam,
      out int intParam,
      int notOutIntParam,
      out float floatParam,
      float notOutfloatParam,
      out bool boolParam,
      bool notOutboolParam,
      out ObjectLuaHotFixState enumParam,
      ObjectLuaHotFixState notOutEnumParam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetOutParam(
      out LuaHotFixExample classParam,
      out int intParam,
      out LuaTestStruct valueTypeParam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRefParam(
      out LuaHotFixExample classParam,
      ref int intParam,
      out LuaTestStruct valueTypeParam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReturnVoid()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix(LuaTable module)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
