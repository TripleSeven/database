﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Lua.LuaHotFixChecker
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 972A2846-EBE4-4323-8745-F884A18FA509
// Assembly location: D:\Games\Langrisser5\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using SLua;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.BJFramework.Runtime.Lua
{
  [HotFix]
  public class LuaHotFixChecker
  {
    [DoNotToLua]
    private LuaHotFixChecker.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_WithOutParamInt32LuaHotFixExample_LuaTestStruct_Int32_Single_Boolean_ObjectLuaHotFixState__hotfix;
    private LuaFunction m_GetOutParamLuaHotFixExample_Int32_LuaTestStruct__hotfix;
    private LuaFunction m_GetRefParamLuaHotFixExample_Int32_LuaTestStruct__hotfix;
    private LuaFunction m_WithOutParamAndArrayStringbeInt32__hotfix;
    private LuaFunction m_ReturnVoid_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public LuaHotFixChecker()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WithOutParam(
      int notOutParam,
      out LuaHotFixExample classParam,
      out LuaTestStruct valueTypeParam,
      out int intParam,
      out float floatParam,
      out bool boolParam,
      out ObjectLuaHotFixState enumParam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetOutParam(
      out LuaHotFixExample classParam,
      out int intParam,
      out LuaTestStruct valueTypeParam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRefParam(
      out LuaHotFixExample classParam,
      ref int intParam,
      out LuaTestStruct valueTypeParam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WithOutParamAndArray(string[] strArray, out int intParam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReturnVoid()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public LuaHotFixChecker.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private LuaHotFixChecker m_owner;

      public LuaExportHelper(LuaHotFixChecker owner)
      {
        this.m_owner = owner;
      }
    }
  }
}
