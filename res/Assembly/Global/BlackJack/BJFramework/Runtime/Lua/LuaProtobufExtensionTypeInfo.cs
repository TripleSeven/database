﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Lua.LuaProtobufExtensionTypeInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 972A2846-EBE4-4323-8745-F884A18FA509
// Assembly location: D:\Games\Langrisser5\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.Lua
{
  public class LuaProtobufExtensionTypeInfo : IProtobufExtensionTypeInfo
  {
    protected LuaProtoBufExtensionHandler m_handler;
    protected string m_typeName;
    protected TypeCode m_typeCode;
    protected LuaTable m_typeDef;
    protected bool m_isExtension;
    protected bool m_hasExtension;
    protected bool m_hasExtensionInHierarchy;
    private bool m_isEnum;
    protected Dictionary<string, LuaProtobufExtensionTypeMemberInfo> m_memberDict;

    [MethodImpl((MethodImplOptions) 32768)]
    public LuaProtobufExtensionTypeInfo(
      string typeName,
      LuaTable typeDef,
      LuaProtoBufExtensionHandler handler)
    {
      // ISSUE: unable to decompile the method.
    }

    public string GetTypeName()
    {
      return this.m_typeName;
    }

    public TypeCode GetTypeCode()
    {
      return this.m_typeCode;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable<IProtobufExtensionTypeMemberInfo> GetMembers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IProtobufExtensionTypeMemberInfo GetMember(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasExtension()
    {
      return this.m_hasExtension;
    }

    public bool HasExtensionInHierarchy()
    {
      return this.m_hasExtensionInHierarchy;
    }

    public bool IsEnum()
    {
      return this.m_isEnum;
    }

    public static bool IsSystemType(TypeCode typeCode)
    {
      return typeCode != TypeCode.Object;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static TypeCode GetTypeCode(string typeName)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
