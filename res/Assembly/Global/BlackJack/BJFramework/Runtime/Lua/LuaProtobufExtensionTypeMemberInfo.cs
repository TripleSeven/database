﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Lua.LuaProtobufExtensionTypeMemberInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 972A2846-EBE4-4323-8745-F884A18FA509
// Assembly location: D:\Games\Langrisser5\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.Lua
{
  public class LuaProtobufExtensionTypeMemberInfo : IProtobufExtensionTypeMemberInfo
  {
    private LuaProtoBufExtensionHandler m_handler;
    private string m_memberName;
    private LuaTable m_memberDefine;
    private int m_tag;
    private string m_typeName;
    private bool m_isExtension;
    private DataFormat m_dataFormat;
    private bool m_isRequire;
    private bool m_isRepeated;
    private int m_enumValue;

    [MethodImpl((MethodImplOptions) 32768)]
    public LuaProtobufExtensionTypeMemberInfo(
      string memberName,
      LuaTable memberDefine,
      LuaProtoBufExtensionHandler handler)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetTag()
    {
      return this.m_tag;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IProtobufExtensionTypeInfo GetMemberType()
    {
      // ISSUE: unable to decompile the method.
    }

    public string GetName()
    {
      return this.m_memberName;
    }

    public bool IsExtension()
    {
      return this.m_isExtension;
    }

    public DataFormat GetDataFormat()
    {
      return this.m_dataFormat;
    }

    public bool IsRequired()
    {
      return this.m_isRequire;
    }

    public bool IsRepeated()
    {
      return this.m_isRepeated;
    }

    public object GetEnumValue()
    {
      return (object) this.m_enumValue;
    }
  }
}
