﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.CRIProvider
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  public class CRIProvider : ICRIProvider
  {
    private CriAtomSource m_criBackGroundSource;
    private CriAtomSource m_criSoundSource;
    private CriAtomSource m_criSpeechSource;

    public GameObject GetCRIManagerObject()
    {
      return CriWare.managerObject;
    }

    public void Pause(bool pause)
    {
      CriAtomPlugin.Pause(pause);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CriRegisterAcf(string acfFullPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddCRIComponentSources()
    {
      // ISSUE: unable to decompile the method.
    }

    public void CRIRemoveCueSheet(string sheetName)
    {
      CriAtom.RemoveCueSheet(sheetName);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CRIAddCueSheet(string sheetName, string acbFullPath, string awbFullPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetCueLength(string sheetName, string cueName)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetCategoryVolume(string category, float volume)
    {
      CriAtom.SetCategoryVolume(category, volume);
    }

    public float GetCategoryVolume(string category)
    {
      return CriAtom.GetCategoryVolume(category);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IAudioPlayback PlaySound(string sound)
    {
      // ISSUE: unable to decompile the method.
    }

    public void StopSound()
    {
      this.m_criSoundSource.Stop();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool GetCueInfo(string sheetName, string cueName, out CriAtomEx.CueInfo cueInfo)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
