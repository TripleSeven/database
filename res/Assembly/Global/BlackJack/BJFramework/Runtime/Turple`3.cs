﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Turple`3
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime
{
  public class Turple<T1, T2, T3>
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public Turple(T1 elem1, T2 elem2, T3 elem3)
    {
      // ISSUE: unable to decompile the method.
    }

    public T1 Elem1 { get; set; }

    public T2 Elem2 { get; set; }

    public T3 Elem3 { get; set; }
  }
}
