﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.DynamicAssemblySettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime
{
  [Serializable]
  public class DynamicAssemblySettings
  {
    public bool EnableDynamicAssembly;
    public List<string> DynamicAssemblyList;

    [MethodImpl((MethodImplOptions) 32768)]
    public DynamicAssemblySettings()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
