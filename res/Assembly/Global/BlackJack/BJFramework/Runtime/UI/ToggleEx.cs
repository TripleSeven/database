﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.ToggleEx
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace BlackJack.BJFramework.Runtime.UI
{
  [AddComponentMenu("UIExtend/ToggleEx", 16)]
  public class ToggleEx : Toggle
  {
    [SerializeField]
    [Header("[按钮组件]")]
    [FormerlySerializedAs("ButtonComponent")]
    protected List<GameObject> m_toggleComponent;
    [FormerlySerializedAs("NormalStateColorList")]
    [SerializeField]
    protected List<Color> m_normalStateColorList;
    [SerializeField]
    [FormerlySerializedAs("PressedStateColorList")]
    protected List<Color> m_pressedStateColorList;
    [SerializeField]
    [FormerlySerializedAs("DisableStateColorList")]
    protected List<Color> m_disableStateColorList;
    protected Dictionary<GameObject, Color> m_baseColorList;
    [SerializeField]
    [FormerlySerializedAs("PressedAudioClip")]
    [Header("[Unity按下音效]")]
    protected AudioClip m_pressedAudioClip;
    [FormerlySerializedAs("PressedAudioCRI")]
    [SerializeField]
    [Header("[CRI按下音效]")]
    protected string m_pressedAudioCRI;
    [FormerlySerializedAs("OnStateHideList")]
    [Header("[On状态需要隐藏对象列表]")]
    [SerializeField]
    protected List<GameObject> m_onStateHideObjectList;
    [SerializeField]
    [FormerlySerializedAs("OnStateColorList")]
    [Header("[On状态颜色改变列表]")]
    protected List<ToggleEx.ColorDesc> m_onStateColorList;
    [Header("[On状态Tween列表]")]
    [FormerlySerializedAs("OnStateTweenList")]
    [SerializeField]
    protected List<TweenMain> m_onStateTweenList;
    [SerializeField]
    [FormerlySerializedAs("OffStateHideList")]
    [Header("[Off状态需要隐藏对象列表]")]
    protected List<GameObject> m_offStateHideObjectList;
    [Header("[Off状态颜色改变列表]")]
    [FormerlySerializedAs("OffStateColorList")]
    [SerializeField]
    protected List<ToggleEx.ColorDesc> m_offStateColorList;
    [Header("[Off状态Tween列表]")]
    [SerializeField]
    [FormerlySerializedAs("OffStateTweenList")]
    protected List<TweenMain> m_offStateTweenList;
    protected bool m_preIsOn;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void Awake()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetNormalState()
    {
      this.DoStateTransition(Selectable.SelectionState.Normal, false);
    }

    public void SetPressedState()
    {
      this.DoStateTransition(Selectable.SelectionState.Pressed, false);
    }

    public void SetDisableState()
    {
      this.DoStateTransition(Selectable.SelectionState.Disabled, false);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetIsOn(bool ison)
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void SetBaseColorList(Dictionary<GameObject, Color> baseColorList)
    {
      this.m_baseColorList = baseColorList;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void DoStateTransition(Selectable.SelectionState state, bool instant)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InstantClearState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetToggleComponentColor(List<Color> stateColorList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ResetToggleCompontentToBaseColor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected Color GetBaseStateColor(GameObject go, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ShowOrHideObjectWhenIsOnChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetObjectColorWhenIsOnChanged(List<ToggleEx.ColorDesc> stateColorList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void PlayTweenListWhenIsOnChanged(List<TweenMain> tweenList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetGameObjectColor(GameObject go, Color color)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    public AudioClip PressedAudioClip
    {
      get
      {
        return this.m_pressedAudioClip;
      }
    }

    public string PressedAudioCRI
    {
      get
      {
        return this.m_pressedAudioCRI;
      }
    }

    [Serializable]
    public class ColorDesc
    {
      public GameObject go;
      public Color color;
    }
  }
}
