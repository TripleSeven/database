﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.UICollider
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.BJFramework.Runtime.UI
{
  [AddComponentMenu("UIExtend/UICollider", 16)]
  public class UICollider : Graphic
  {
    public override bool Raycast(Vector2 sp, Camera eventCamera)
    {
      return true;
    }

    protected override void OnPopulateMesh(VertexHelper vh)
    {
      vh.Clear();
    }
  }
}
