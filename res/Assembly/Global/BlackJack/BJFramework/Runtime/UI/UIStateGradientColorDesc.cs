﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.UIStateGradientColorDesc
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime.UI
{
  [Serializable]
  public class UIStateGradientColorDesc
  {
    public Gradient GradientComptent;
    public Color Color1;
    public Color Color2;
  }
}
