﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.UIStateColorDesc
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.BJFramework.Runtime.UI
{
  [Serializable]
  public class UIStateColorDesc
  {
    public Color TargetColor;
    public GameObject ChangeColorGo;
    [HideInInspector]
    public List<Image> ChangeColorImageList;
    [HideInInspector]
    public List<Text> ChangeColorTextList;

    [MethodImpl((MethodImplOptions) 32768)]
    public UIStateColorDesc()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
