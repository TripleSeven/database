﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.UITaskBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Scene;
using BlackJack.BJFramework.Runtime.TaskNs;
using BlackJack.Utils;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.UI
{
  public class UITaskBase : Task
  {
    protected List<string> m_modeDefine;
    protected UIIntent m_currIntent;
    protected string m_currMode;
    protected bool m_isUIInputEnable;
    protected bool m_enableUIInputLog;
    protected int m_loadingStaticResCorutineCount;
    protected int m_loadingDynamicResCorutineCount;
    protected List<UITaskBase.PlayingUpdateViewEffectItem> m_playingUpdateViewEffectList;
    protected SceneLayerBase[] m_layerArray;
    protected UIControllerBase[] m_uiCtrlArray;
    protected UITaskPipeLineCtx m_currPipeLineCtx;
    protected Dictionary<string, UnityEngine.Object> m_dynamicResCacheDict;
    protected TinyCorutineHelper m_corutineHelper;
    protected bool m_blockPipeLine;
    private ulong m_lastStartUpdatePipeLineTime;
    private LinkedList<UITaskBase.DelayTimeExecItem> m_delayTimeExecList;
    protected List<UITaskBase.PiplineQueueItem> m_piplineQueue;
    protected List<string> m_tagList;

    [MethodImpl((MethodImplOptions) 32768)]
    public UITaskBase(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void InitlizeBeforeManagerStartIt()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
    {
    }

    protected override sealed bool OnStart(object param)
    {
      return this.OnStart(param as UIIntent);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected UITaskPipeLineCtx GetPipeLineCtx()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual UITaskPipeLineCtx CreatePipeLineCtx()
    {
      return new UITaskPipeLineCtx();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override sealed bool OnResume(object param = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual bool OnNewIntent(UIIntent intent)
    {
      return this.StartUpdatePipeLine(intent, false, false, true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool StartUpdatePipeLine(
      UIIntent intent = null,
      bool onlyUpdateView = false,
      bool canBeSkip = false,
      bool enableQueue = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool NeedSkipUpdatePipeLine(UIIntent intent, bool onlyUpdateView)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual bool IsNeedUpdateDataCache()
    {
      return false;
    }

    protected virtual void UpdateDataCache()
    {
    }

    protected virtual bool IsNeedLoadStaticRes()
    {
      return (UnityEngine.Object) this.MainLayer == (UnityEngine.Object) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void StartLoadStaticRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void CheckLayerDescArray(List<UITaskBase.LayerDesc> layerDescArray)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnLoadStaticResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual bool IsNeedLoadDynamicRes()
    {
      return false;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void StartLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual List<string> CollectAllDynamicResForLoad()
    {
      return (List<string>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected HashSet<string> CalculateDynamicResReallyNeedLoad(List<string> resPathList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnLoadDynamicResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RedirectPipLineOnAllResReady(Action callBack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReturnFromRedirectPipLineOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void InitLayerStateOnLoadAllResCompleted()
    {
      this.Base_InitLayerStateOnLoadAllResCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Base_InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void InitAllUIControllers()
    {
      this.Base_InitAllUIControllers();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Base_InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void PostOnLoadAllResCompleted()
    {
      this.StartUpdateView();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool IsLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void StartUpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void UpdateView()
    {
      throw new NotImplementedException();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void RegUpdateViewPlayingEffect(string name = null, int timeout = 0, Action<string> onTimeOut = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UnregUpdateViewPlayingEffect(string name = null, bool isTimeOut = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void PostUpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void PostUpdateViewBeforeClearContext()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void HideAllView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Base_ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void ClearAllContextAndRes()
    {
      this.Base_ClearAllContextAndRes();
    }

    protected virtual void SaveContextInIntentOnPause()
    {
    }

    protected virtual void ClearContextOnPause()
    {
    }

    protected virtual void ClearContextOnIntentChange(UIIntent newIntent)
    {
    }

    protected virtual void ClearContextOnUpdateViewEnd()
    {
      this.m_currPipeLineCtx.Clear();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void EnableUIInput(bool isEnable, bool? isGlobalEnable = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected UITaskBase.LayerDesc GetLayerDescByName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected SceneLayerBase GetLayerByName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void RegisterModesDefine(string defaultMode, params string[] modes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool SetCurrentMode(string mode)
    {
      // ISSUE: unable to decompile the method.
    }

    protected void SetIsNeedPauseTimeOut(bool isNeed)
    {
      this.IsNeedPauseTimeOut = isNeed;
    }

    protected override void OnTick()
    {
      this.Base_OnTick();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Base_OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickForDelayTimeExecuteActionList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PostDelayTimeExecuteAction(Action action, float delaySeconds)
    {
      // ISSUE: unable to decompile the method.
    }

    public void PostDelayTicksExecuteAction(Action action, ulong delayTickCount)
    {
      this.ExecAfterTicks(action, delayTickCount);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetTag(string tag)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasTag(string tag)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<UITaskBase> EventOnPostUpdateView
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public UIIntent CurrentIntent
    {
      get
      {
        return this.m_currIntent;
      }
    }

    protected bool IsUIInputEnable
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected static bool IsGlobalUIInputEnable
    {
      get
      {
        return UIManager.Instance.IsGlobalUIInputEnable();
      }
    }

    protected virtual SceneLayerBase MainLayer
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected virtual UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    protected virtual UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    public bool IsNeedPauseTimeOut { get; private set; }

    public class PlayingUpdateViewEffectItem
    {
      public string m_name;
      public DateTime? m_timeOutTime;
      public Action<string> m_onTimeOut;
    }

    public class LayerDesc
    {
      public string m_layerName;
      public string m_layerResPath;
      public bool m_isUILayer;
      public bool m_isLazyLoad;
      public int m_index;
      public bool m_isReserve;
    }

    public class UIControllerDesc
    {
      public string m_ctrlName;
      public TypeDNName m_ctrlTypeDNName;
      public string m_attachLayerName;
      public string m_attachPath;
    }

    public class DelayTimeExecItem
    {
      public DateTime m_execTargetTime;
      public Action m_action;
    }

    public class PiplineQueueItem
    {
      public UIIntent m_intent;
      public bool m_onlyUpdateView;
      public bool m_canBeSkip;
    }
  }
}
