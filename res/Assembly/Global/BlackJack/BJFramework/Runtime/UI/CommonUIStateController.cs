﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.CommonUIStateController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.BJFramework.Runtime.UI
{
  public class CommonUIStateController : MonoBehaviour
  {
    public List<UIStateDesc> UIStateDescList;
    public List<ColorSetDesc> UIStateColorSetList;
    public List<ButtonEx> UIRelateButtonList;
    public bool EnablePressColor;
    public int PressColorIndex;
    public Animator AnimationController;
    [HideInInspector]
    public List<GameObject> DisableGoList;
    [HideInInspector]
    public int LastStateIndex;
    private bool isGathed;
    private UIStateDesc m_currUIState;
    private int m_animatorTriggerNameId;
    private List<TweenMain> m_allTweens;
    private bool m_isSettingToUIState;
    private bool m_isSettingToUIStateTweenFinished;

    [MethodImpl((MethodImplOptions) 32768)]
    public CommonUIStateController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToUIState(string stateName, bool notPlayTweens = false, bool allowToRefreshSameState = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChangeToColor(List<UIStateColorDesc> colorDescList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void NotifyColorChanged(List<UIStateColorDesc> colorDescList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowNextState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReGatherDynamicGo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStateExit(string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPlaySound(string soundName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SetActionForUIStateFinshed(string stateName, Action action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UIStateDesc GetUIStateDescByName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    public UIStateDesc GetCurrentUIState()
    {
      return this.m_currUIState;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReGatherColorChangeImageAndText()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAllUIStateTweensFinished()
    {
      // ISSUE: unable to decompile the method.
    }

    [HideInInspector]
    public static event Action<GameObject, string> m_onUIStateEndEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
