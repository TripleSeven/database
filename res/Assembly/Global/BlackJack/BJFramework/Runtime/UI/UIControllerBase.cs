﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.UIControllerBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.BJFramework.Runtime.UI
{
  public class UIControllerBase : PrefabControllerBase
  {
    private static int m_lastButtonClickFrameCount = -1;
    public bool AutoInitLocalizedString = true;
    protected Dictionary<string, Action<UIControllerBase>> m_buttonClickListenerDict;
    protected Dictionary<string, Action<UIControllerBase>> m_buttonDoubleClickListenerDict;
    protected Dictionary<string, Action<UIControllerBase>> m_buttonLongPressStartListenerDict;
    protected Dictionary<string, Action<UIControllerBase>> m_buttonLongPressingListenerDict;
    protected Dictionary<string, Action<UIControllerBase>> m_buttonLongPressEndListenerDict;
    protected Dictionary<string, Action<UIControllerBase, bool>> m_toggleValueChangedListenerDict;

    public override void Initlize(string ctrlName, bool bindNow)
    {
      base.Initlize(ctrlName, bindNow);
    }

    public override void BindFields()
    {
      base.BindFields();
    }

    protected override void OnBindFiledsCompleted()
    {
      this.Base_OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Base_OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override UnityEngine.Object BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName = null,
      bool optional = false)
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void Clear()
    {
      this.m_buttonClickListenerDict = (Dictionary<string, Action<UIControllerBase>>) null;
      this.m_toggleValueChangedListenerDict = (Dictionary<string, Action<UIControllerBase, bool>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnButtonClick(Button button, string fieldName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool CheckAndUpdateCurrFrameButtonClickStateInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnButtonDoubleClick(Button button, string fieldName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnButtonLongPressStart(Button button, string fieldName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnButtonLongPressing(Button button, string fieldName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnButtonLongPressEnd(Button button, string fieldName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetButtonClickListener(string fieldName, Action<UIControllerBase> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetButtonClickListener(string[] fieldNames, Action<UIControllerBase> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetButtonDoubleClickListener(string fieldName, Action<UIControllerBase> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetButtonLongPressStartListener(string fieldName, Action<UIControllerBase> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetButtonLongPressingListener(string fieldName, Action<UIControllerBase> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetButtonLongPressEndListener(string fieldName, Action<UIControllerBase> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnToggleValueChanged(Toggle toggle, string fieldName, bool value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void OnDestroy()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void InitLocalizedString(GameObject goRoot)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
