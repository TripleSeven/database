﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.ButtonEx
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace BlackJack.BJFramework.Runtime.UI
{
  [AddComponentMenu("UIExtend/ButtonEx", 16)]
  public class ButtonEx : Button
  {
    [FormerlySerializedAs("onDoubleClick")]
    [SerializeField]
    private ButtonEx.ButtonDoubleClickedEvent m_OnDoubleClick;
    [SerializeField]
    private ButtonEx.ButtonLongPressStartEvent m_OnLongPressStart;
    [SerializeField]
    private ButtonEx.ButtonLongPressingEvent m_OnLongPressing;
    [SerializeField]
    private ButtonEx.ButtonLongPressEndEvent m_OnLongPressEnd;
    private bool m_isPointerInside;
    private bool m_isPointerDown;
    private int m_continueClickCount;
    private float m_continueClickDelayLeftTime;
    public float m_doubleClickDelayTime;
    public float m_longPressThreshhold;
    public float m_longPressingTriggerTimeGap;
    private float m_timeFromPressStarted;
    private bool m_isLongPressTriggered;
    private float m_timeFromLastPressingTrigger;
    [SerializeField]
    [FormerlySerializedAs("ButtonComponent")]
    [Header("[按钮组件]")]
    protected List<GameObject> m_buttonComponent;
    [FormerlySerializedAs("NormalStateColorList")]
    [SerializeField]
    protected List<Color> m_normalStateColorList;
    [FormerlySerializedAs("PressedStateColorList")]
    [SerializeField]
    protected List<Color> m_pressedStateColorList;
    [SerializeField]
    [FormerlySerializedAs("DisableStateColorList")]
    protected List<Color> m_disableStateColorList;
    protected Dictionary<GameObject, Color> m_baseColorList;
    [FormerlySerializedAs("PressedAudioClip")]
    [Header("[Unity按下音效]")]
    [SerializeField]
    protected AudioClip m_pressedAudioClip;
    [SerializeField]
    [FormerlySerializedAs("PressedAudioCRI")]
    [Header("[CRI按下音效]")]
    protected string m_pressedAudioCRI;
    [Header("[是否禁止音效]")]
    [SerializeField]
    protected bool m_prohibitAudio;
    [Header("按下Tween列表")]
    [SerializeField]
    [FormerlySerializedAs("PointerDownTweenList")]
    protected List<TweenMain> m_pointerDownTweenList;

    [MethodImpl((MethodImplOptions) 32768)]
    protected ButtonEx()
    {
      // ISSUE: unable to decompile the method.
    }

    public ButtonEx.ButtonDoubleClickedEvent onDoubleClick
    {
      get
      {
        return this.m_OnDoubleClick;
      }
      set
      {
        this.m_OnDoubleClick = value;
      }
    }

    public ButtonEx.ButtonLongPressStartEvent onLongPressStart
    {
      get
      {
        return this.m_OnLongPressStart;
      }
      set
      {
        this.m_OnLongPressStart = value;
      }
    }

    public ButtonEx.ButtonLongPressingEvent onLongPressing
    {
      get
      {
        return this.m_OnLongPressing;
      }
      set
      {
        this.m_OnLongPressing = value;
      }
    }

    public ButtonEx.ButtonLongPressEndEvent onLongPressEnd
    {
      get
      {
        return this.m_OnLongPressEnd;
      }
      set
      {
        this.m_OnLongPressEnd = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetNormalState()
    {
      this.DoStateTransition(Selectable.SelectionState.Normal, false);
    }

    public void SetPressedState()
    {
      this.DoStateTransition(Selectable.SelectionState.Pressed, false);
    }

    public void SetDisableState()
    {
      this.DoStateTransition(Selectable.SelectionState.Disabled, false);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnPointerDown(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnPointerUp(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
      this.m_isPointerInside = true;
      base.OnPointerEnter(eventData);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnPointerExit(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void SetBaseColorList(Dictionary<GameObject, Color> baseColorList)
    {
      this.m_baseColorList = baseColorList;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void DoStateTransition(Selectable.SelectionState state, bool instant)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InstantClearState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetButtonComponentColor(List<Color> stateColorList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ResetButtonCompontentToBaseColor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected Color GetBaseStateColor(GameObject go, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void PlayPointerDownTween(bool isforward)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetGameObjectColor(GameObject go, Color color)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ResetDoubleClickState()
    {
      // ISSUE: unable to decompile the method.
    }

    public AudioClip PressedAudioClip
    {
      get
      {
        return this.m_pressedAudioClip;
      }
    }

    public string PressedAudioCRI
    {
      get
      {
        return this.m_pressedAudioCRI;
      }
    }

    public bool ProhibitAudio
    {
      get
      {
        return this.m_prohibitAudio;
      }
    }

    [Serializable]
    public class ButtonDoubleClickedEvent : UnityEvent
    {
    }

    [Serializable]
    public class ButtonLongPressStartEvent : UnityEvent
    {
    }

    [Serializable]
    public class ButtonLongPressingEvent : UnityEvent
    {
    }

    [Serializable]
    public class ButtonLongPressEndEvent : UnityEvent
    {
    }
  }
}
