﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.UITaskPipeLineCtx
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.UI
{
  public class UITaskPipeLineCtx
  {
    public bool m_isInitPipeLine;
    public bool m_isTaskResume;
    public bool m_isRuning;
    public bool m_layerLoadedInPipe;
    public bool m_blockGlobalUIInput;
    public Action m_redirectPipLineOnAllResReady;
    protected ulong m_updateMask;

    [MethodImpl((MethodImplOptions) 32768)]
    public UITaskPipeLineCtx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetUpdateMask(ulong mask)
    {
      this.m_updateMask = mask;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUpdateMask(params int[] indexs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddUpdateMask(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearUpdateMask(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNeedUpdate(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsUpdateMaskClear()
    {
      return this.m_updateMask == 0UL;
    }
  }
}
