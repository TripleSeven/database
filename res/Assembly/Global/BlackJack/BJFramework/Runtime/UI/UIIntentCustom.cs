﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.UIIntentCustom
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.UI
{
  public class UIIntentCustom : UIIntent
  {
    private Dictionary<string, object> m_params;

    [MethodImpl((MethodImplOptions) 32768)]
    public UIIntentCustom(string targetTaskName, string targetMode = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetParam(string key, object value)
    {
      this.m_params[key] = value;
    }

    public bool TryGetParam(string key, out object value)
    {
      return this.m_params.TryGetValue(key, out value);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T GetClassParam<T>(string key) where T : class
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T GetStructParam<T>(string key) where T : struct
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
