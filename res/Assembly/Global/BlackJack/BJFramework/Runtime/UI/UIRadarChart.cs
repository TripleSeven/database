﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.UIRadarChart
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.BJFramework.Runtime.UI
{
  [AddComponentMenu("UIExtend/UIRadarChart", 16)]
  public class UIRadarChart : MaskableGraphic
  {
    public bool isFill;
    [Range(0.0f, 0.99f)]
    public float fillPercent;
    [Range(0.0f, 1f)]
    public float[] values;
    [Range(0.0f, 360f)]
    public float angleOffset;
    public bool useStateLine;
    public Color lineColor;
    public float lineWidth;
    [Range(0.0f, 1f)]
    public float lineLength;

    [MethodImpl((MethodImplOptions) 32768)]
    public UIRadarChart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPopulateMesh(VertexHelper vh)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UIVertex[] GetLine(Vector2 start, Vector2 end)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector2 GetPoint(Vector2 size, int i)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UIVertex[] GetQuad(params Vector2[] vertPos)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
