﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.UIStateDesc
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.BJFramework.Runtime.UI
{
  [Serializable]
  public class UIStateDesc
  {
    public string StateName;
    public int StateColorSetIndex;
    public List<GameObject> SetToShowGameObjectList;
    public List<TweenMain> TweenAnimationList;
    public List<UIStateGradientColorDesc> GradientDescList;
    [HideInInspector]
    private bool m_isTweenFinished;
    [HideInInspector]
    private bool m_isAnimationFinished;
    [HideInInspector]
    public Action m_eventFinished;

    [MethodImpl((MethodImplOptions) 32768)]
    public UIStateDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [HideInInspector]
    public bool AnimationFinished
    {
      get
      {
        return this.m_isAnimationFinished;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [HideInInspector]
    public bool TweenFinished
    {
      get
      {
        return this.m_isTweenFinished;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
