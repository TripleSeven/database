﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.LoginUITaskBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.UI
{
  public abstract class LoginUITaskBase : UITaskBase
  {
    protected LoginUITaskBase.LoginState m_loginTaskState;
    protected string m_authLoginServerAddress;
    protected int m_authLoginServerPort;
    protected string m_authtoken;
    protected string m_gameUserId;
    protected int m_loginChannelId;
    protected int m_bornChannelId;
    protected string m_authLoginServerDomain;
    protected string m_lastSessionToken;
    protected bool m_isWaitingForMsgAck;
    protected DateTime m_timeOutTime;
    protected float m_timeoutDelayTime;

    [MethodImpl((MethodImplOptions) 32768)]
    public LoginUITaskBase(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void OnStop()
    {
      this.UnRegisterNetEvent();
      base.OnStop();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void PostUpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool IsSDKLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool IsEditorTestLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void StartInitSDK()
    {
      this.m_loginTaskState = LoginUITaskBase.LoginState.StartSDKInit;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnSDKInitEnd(bool isSuccess)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void StartSDKLogin()
    {
      this.m_loginTaskState = LoginUITaskBase.LoginState.StartSDKLogin;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnSDKLoginEnd(bool isSuccess)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void PostSDKLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual bool IsNeedSelectServer()
    {
      throw new NotImplementedException();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void LaunchServerListUI()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual IEnumerator DownloadServerList(Action<bool> onEnd)
    {
      throw new NotImplementedException();
    }

    protected virtual void OnDownloadServerListEnd(bool isSuccess)
    {
      if (!isSuccess)
        return;
      this.ShowServerListUI();
    }

    protected virtual void ShowServerListUI()
    {
    }

    protected virtual void OnGameServerConfirmed(string serverId)
    {
      this.InitServerCtxByServerID(serverId);
      this.LaunchEnterGameUI();
    }

    protected virtual void InitServerCtxByServerID(string serverId)
    {
      throw new NotImplementedException();
    }

    protected virtual string GetLastLoginedServerID()
    {
      throw new NotImplementedException();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void LaunchEnterGameUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void LaunchEnterGameUIWithGameSettingTokenAndServer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void LaunchEnterGameUIWithUIInputAccountAndServer()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual bool LoadLastLoginWithUIInputInfo(
      out string loginUserId,
      out string authLoginServerAddress,
      out int authLoginServerPort)
    {
      throw new NotImplementedException();
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual IEnumerator DownloadGameServerLoginAnnouncement(Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnDownloadGameServerLoginAnnouncementEnd(bool isSuccess)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void ShowGameServerLoginAnnouncementUI()
    {
      throw new NotImplementedException();
    }

    protected virtual void ShowEnterGameUI()
    {
      this.m_loginTaskState = LoginUITaskBase.LoginState.ReadyForEnterGame;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnEnterGameConfirmed()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual IEnumerator StartLoginAgentLogin(Action<int, string, string> onEnd)
    {
      this.m_loginTaskState = LoginUITaskBase.LoginState.StartLoginAgentLogin;
      throw new NotImplementedException();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void LoginAgentLoginEnd(int errCode, string loginUserId, string authToken)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void StartAuthLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void StartSessionLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void StartPlayerInfoInitReq()
    {
    }

    protected virtual void LauncheMainUI()
    {
      throw new NotImplementedException();
    }

    protected virtual void OnWaitingMsgAckOutTime()
    {
      throw new NotImplementedException();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void StartWaitingMsgAck(float waitTime)
    {
      // ISSUE: unable to decompile the method.
    }

    protected void StopWaitingMsgAck()
    {
      this.m_isWaitingForMsgAck = false;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ClearGameServerLoginState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void OnGameServerConnected()
    {
      throw new NotImplementedException();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnGameServerDisconnected()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void OnGameServerNetworkError(int err)
    {
      throw new NotImplementedException();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool OnLoginByAuthTokenAck(int ret, string sessionToken, bool needRedirect)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool OnLoginBySessionTokenAck(int ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnConfigDataMD5InfoNtf(
      string fileNameWithErrorMD5,
      string localMD5,
      string serverMD5)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void OnPlayerInfoInitEnd()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RegisterNetEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnRegisterNetEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    public enum LoginState
    {
      None,
      Inited,
      StartSDKInit,
      SDKInitEnd,
      StartSDKLogin,
      SDKLoginEnd,
      ReadyForEnterGame,
      StartLoginAgentLogin,
      LoginAgentLoginEnd,
      StartAuthLogin,
      AuthLoginEnd,
      RedirectWaitDisconnect,
      StartSessionLogin,
      SessionLoginEnd,
      StartPlayerInfoInit,
      ReadyForCharacterUI,
    }
  }
}
