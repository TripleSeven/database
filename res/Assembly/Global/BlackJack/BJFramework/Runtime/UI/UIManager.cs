﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.UIManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.TaskNs;
using BlackJack.Utils;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.UI
{
  public class UIManager
  {
    private static UIManager m_instance;
    protected Dictionary<string, int> m_globalUIInputBlockDict;
    private List<UIIntent> m_uiIntentStack;
    private Dictionary<string, UITaskBase> m_uiTaskDict;
    private Dictionary<string, UIManager.UITaskRegItem> m_uiTaskRegDict;
    private List<UITaskBase> m_taskList4Stop;
    private List<List<int>> m_uiTaskGroupConflictList;
    protected DateTime m_lastTickPauseTimeOutTime;
    protected DelayExecHelper m_delayExecHelper;
    public const double UITaskPauseTimeOut = 120.0;
    protected List<UIManager.UIProcessQueueItem> m_uiProcessQueue;
    protected UIManager.UIProcessQueueItem m_currRuningUIProcess;
    protected DateTime? m_currRuningUIProcessTimeOut;
    private List<Func<UIManager.UIProcessQueueItem, bool>> m_uiProcessQueueBlockerList;
    private bool m_blockGlobalUIInputForWaitingItem;

    [MethodImpl((MethodImplOptions) 32768)]
    private UIManager()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static UIManager CreateUIManager()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool Initlize()
    {
      Debug.Log("UIManager.Initlize start");
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Uninitlize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UITaskBase StartUITask(
      UIIntent intent,
      bool pushIntentToStack = false,
      bool clearIntentStack = false,
      Action redirectPipLineOnLoadAllResCompleted = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartUITaskWithPrepare(
      UIIntent intent,
      Action<bool> onPrepareEnd,
      bool pushIntentToStack = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UITaskBase ReturnUITask(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PauseUITaskUntilTargetTaskByName(string taskName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UITaskBase ReturnUITaskWithTaskName(string taskName)
    {
      // ISSUE: unable to decompile the method.
    }

    public UITaskBase ReturnUITaskToLast()
    {
      return this.ReturnUITaskByStackIndex(-1);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UITaskBase ReturnUITaskByStackIndex(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReturnUITaskWithPrepare(UIIntent intent, Action<bool> onPrepareEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PauseUITask(string taskName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopUITask(string taskName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UITaskBase GetOrCreateUITask(string taskName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UITaskBase FindUITaskWithName(string taskName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool StartUITaskInternal<TTaskType>(
      TTaskType targetTask,
      UIIntent intent,
      bool pushIntentToStack)
      where TTaskType : UITaskBase
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool StartOrResumeTask(UITaskBase targetTask, UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool PopIntentUntilReturnTarget(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveIntentFromStack(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUITaskStop(Task task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUITaskPostUpdateView(UITaskBase task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseAllConflictUITask(string taskName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopUITaskByGroup(int group)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PauseUITaskByGroup(int group)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PauseUITaskByTag(string tag, bool isExcept = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopUITaskByTag(string tag, bool isExcept)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterUITaskWithGroup(
      string taskName,
      TypeDNName taskTypeDNName,
      int group,
      string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUITaskGroupConflict(uint group1, uint group2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReturnToLoginUI()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsGlobalUIInputEnable()
    {
      return this.m_globalUIInputBlockDict.Count == 0;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GlobalUIInputEnable(string srcName, bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    public void GlobalUIInputBlockClear(string srcName)
    {
      this.m_globalUIInputBlockDict.Remove(srcName);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GlobalUIInputBlockForTicks(int ticks)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void TickPauseTimeOut()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<string> EventOnUITaskShow
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventReturnToLoginUI
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static UIManager Instance
    {
      get
      {
        return UIManager.m_instance;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterUIProcess(UIManager.UIProcessQueueItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UnregisterUIProcess(UIManager.UIProcessQueueItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    public void RegisterUIProcessQueueBlocker(Func<UIManager.UIProcessQueueItem, bool> blocker)
    {
      this.m_uiProcessQueueBlockerList.Add(blocker);
    }

    public void UnregisterUIProcessQueueBlocker(Func<UIManager.UIProcessQueueItem, bool> blocker)
    {
      this.m_uiProcessQueueBlockerList.Remove(blocker);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void TickUIProcessQueue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnUIProcessEnd(UIManager.UIProcessQueueItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBlockGlobalUIInputForProcessQueue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsUIProcessQueueItemExist(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int UIProcessQueueComparsion(
      UIManager.UIProcessQueueItem x,
      UIManager.UIProcessQueueItem y)
    {
      // ISSUE: unable to decompile the method.
    }

    public class UITaskRegItem
    {
      public int m_taskGroup;
      public string m_taskName;
      public TypeDNName m_taskTypeDnName;
    }

    public class UIProcessQueueItem
    {
      public bool m_onlyTryOnce = true;
      public int m_priority;
      public int m_typeId;
      public string m_name;
      public object m_ctx;
      public Func<bool> m_cbCanStart;
      public Action m_cbOnStart;
      public Action m_cbOnEnd;
      public int m_waitTimeOut;
      public DateTime? m_waitTimeOutTime;
      public int m_processTimeOut;
      public bool m_blockGlobalUIInputForWaiting;
      public bool m_blockGlobalUIInputForProcessing;
      public bool m_ignoreBlockGlobalUIInputForWaitingInProcessing;

      public void OnEnd()
      {
        UIManager.Instance.OnUIProcessEnd(this);
        if (this.m_cbOnEnd == null)
          return;
        this.m_cbOnEnd();
      }
    }
  }
}
