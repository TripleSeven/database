﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.MathExtensions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  public static class MathExtensions
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public static bool AlmostEquals(
      this Vector3 target,
      Vector3 second,
      float sqrMagnitudePrecision)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool AlmostEquals(
      this Vector2 target,
      Vector2 second,
      float sqrMagnitudePrecision)
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool AlmostEquals(this Quaternion target, Quaternion second, float maxAngle)
    {
      return (double) Quaternion.Angle(target, second) < (double) maxAngle;
    }

    public static bool AlmostEquals(this float target, float second, float floatDiff)
    {
      return (double) Mathf.Abs(target - second) < (double) floatDiff;
    }
  }
}
