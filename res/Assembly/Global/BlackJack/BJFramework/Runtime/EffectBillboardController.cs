﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.EffectBillboardController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  [ExecuteInEditMode]
  public class EffectBillboardController : MonoBehaviour
  {
    [Header("编辑状态下朝向的Camera")]
    public Camera FaceToCameraInEditor;
    [Header("运行状态下朝向的Camera")]
    public Camera FaceToCameraInRuntime;
    [Header("特效对象Camera朝向模式")]
    public EffectBillboardController.FaceMode RotationFaceMode;
    public bool IsKeepSize;
    public float Width;
    public Renderer Render;

    [MethodImpl((MethodImplOptions) 32768)]
    public void Start()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected Camera GetLayerCameraFromParent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnRenderObject()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void KeepSize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private float GetScreenPortion()
    {
      // ISSUE: unable to decompile the method.
    }

    public enum FaceMode
    {
      ReverseCameraRotation,
      FaceToCamera,
    }
  }
}
