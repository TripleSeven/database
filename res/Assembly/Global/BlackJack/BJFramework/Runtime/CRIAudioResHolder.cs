﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.CRIAudioResHolder
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  [AddComponentMenu("CRIWARE/BlackJack/CRI Audio Res Holder")]
  public class CRIAudioResHolder : MonoBehaviour
  {
    [Header("播放延时")]
    public float DelayTime;
    public string m_criCueName;

    [MethodImpl((MethodImplOptions) 32768)]
    public CRIAudioResHolder()
    {
      // ISSUE: unable to decompile the method.
    }

    public void Play()
    {
      this.StartCoroutine(this.PlayWithDelay());
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator PlayWithDelay()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
