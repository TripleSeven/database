﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.ConfigDataSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  [Serializable]
  public class ConfigDataSettings
  {
    public string ConfigDataLoaderTypeDNName;
    public bool ConfigDataAssetAllowNullSetting;
    public string StringTableManagerTypeDNName;
    public bool ConfigDataAllowMD5NotMach;
    public string ConfigDataAssetTargetPath;
    [NonSerialized]
    public string RuntimeConfigDataAssetTargetPath;
    [Header("初始化的线程个数")]
    public int InitThreadCount;
    [Header("初始化时加载多少个yeildreturn一次")]
    public int InitloadCountForSingleYield;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSettings()
    {
      // ISSUE: unable to decompile the method.
    }

    public string GetConfigDataAssetTargetPath()
    {
      return this.RuntimeConfigDataAssetTargetPath;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetConfigDataAssetPathNoPostfix(string fileName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetConfigDataAssetPath(string fileName)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
