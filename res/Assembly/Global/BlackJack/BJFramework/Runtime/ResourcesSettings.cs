﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.ResourcesSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  [Serializable]
  public class ResourcesSettings
  {
    public bool SkipStreamingAssetsFileProcessing;
    public bool DisableAssetBundle;
    public bool DisableAssetBundleDownload;
    public string AssetBundleDownloadUrlRoot;
    public bool SkipAssetBundlePreUpdateing;
    public int PreUpdateWorkerCount;
    public bool LoadAssetFromBundleInEditor;
    public int UnloadUnusedAssetTimeInterval;
    public bool AssetPathIgnoreCase;
    public List<string> ResaveFileDirRoots;
    public string ResaveAssetRoot;
    public string ResaveFileDestDir;
    [Header("是否开启资源管理器的详细log")]
    public bool EnableDetailResourceManagerLog;
    [Header("Bundle预更新重试次数")]
    public int BundlePreUpdateRetryCount;
    [Header("Bundle预更新重试时间间隔(秒)")]
    public float BundlePreUpdateRetryTimeDelay;

    [MethodImpl((MethodImplOptions) 32768)]
    public ResourcesSettings()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
