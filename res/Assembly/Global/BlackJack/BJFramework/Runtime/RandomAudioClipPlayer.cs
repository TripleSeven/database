﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.RandomAudioClipPlayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  [RequireComponent(typeof (AudioSource))]
  [Serializable]
  public class RandomAudioClipPlayer : MonoBehaviour
  {
    private AudioSource m_audioSource;
    [Header("播放延时")]
    public float DelayTime;
    [Header("播放速度范围最小值")]
    public float MinPitch;
    [Header("播放速度范围最大值")]
    public float MaxPitch;
    [Header("待播放的AudioClip列表")]
    public List<AudioClip> AudioClipList;

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomAudioClipPlayer()
    {
      // ISSUE: unable to decompile the method.
    }

    private void Awake()
    {
      this.m_audioSource = this.GetComponent<AudioSource>();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Play(float volume = -1f)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator PlayWithDelay()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
