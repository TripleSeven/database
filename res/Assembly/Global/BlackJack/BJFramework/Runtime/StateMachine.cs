﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.StateMachine
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime
{
  public class StateMachine
  {
    public virtual int SetStateCheck(int commingEvent, int newState = -1, bool testOnly = false)
    {
      this.State = newState;
      return this.State;
    }

    public virtual void SetStateWithoutCheck(int newState)
    {
      this.State = newState;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool EventCheck(int commingEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    public int State { get; protected set; }
  }
}
