﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.LuaSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 972A2846-EBE4-4323-8745-F884A18FA509
// Assembly location: D:\Games\Langrisser5\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;

namespace BlackJack.BJFramework.Runtime
{
  [Serializable]
  public class LuaSettings
  {
    public bool EnableLua;
    public string LuaRootPath;
    public string LuaInitModule;
  }
}
