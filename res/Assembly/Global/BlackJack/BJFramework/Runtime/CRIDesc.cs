﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.CRIDesc
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime
{
  public class CRIDesc
  {
    public string m_acfFullPath;
    public List<CRIDesc.SheetDesc> m_sheetList;

    [MethodImpl((MethodImplOptions) 32768)]
    public CRIDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    public class SheetDesc
    {
      public string m_sheetName;
      public string m_acbFullPath;
      public string m_awbFullPath;
    }
  }
}
