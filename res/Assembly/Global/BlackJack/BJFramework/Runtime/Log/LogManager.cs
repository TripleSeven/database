﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Log.LogManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime.Log
{
  public class LogManager
  {
    private static LogManager m_instance;
    public bool NeedFileLog;
    public bool NeedEngineLog;
    public bool IsCallingEngineLog;

    [MethodImpl((MethodImplOptions) 32768)]
    private LogManager()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReceiveUnityEngineLog(string log, string stackTrace, LogType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static LogManager CreateLogManager()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Initlize(bool needEngineLog, bool needFileLog, string logFileRoot, string logName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Uninitlize()
    {
      // ISSUE: unable to decompile the method.
    }

    public static LogManager Instance
    {
      get
      {
        return LogManager.m_instance;
      }
    }

    public FileLogger FileLogger { get; private set; }
  }
}
