﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.LoginSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime
{
  [Serializable]
  public class LoginSettings
  {
    public bool LoginUseSDK;
    public bool LoginUseSettings;
    public string GameServerAddress;
    public int GameServerPort;
    public string LoginAccount;
    public string ClientVersion;
    public int AndroidBasicVersion;
    public int IOSBasicVersion;

    [MethodImpl((MethodImplOptions) 32768)]
    public LoginSettings()
    {
      // ISSUE: unable to decompile the method.
    }

    public int BasicVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
