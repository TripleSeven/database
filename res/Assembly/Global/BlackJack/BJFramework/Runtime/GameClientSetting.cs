﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.GameClientSetting
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  public class GameClientSetting : ScriptableObject
  {
    public static string defaultClientPrefPath = "Assets/GameProject/Resources/GameClientSetting";
    public bool DisableUnityEngineLog;
    public bool Log2Persistent;
    public ResourcesSettings ResourcesSetting;
    public ResolutionSettings ResolutionSetting;
    public SceneSettings SceneSetting;
    public DynamicAssemblySettings DynamicAssemblySetting;
    public HotfixSettings HotfixSettings;
    public ConfigDataSettings ConfigDataSetting;
    public AudioSettings AudioSetting;
    public StringTableSettings StringTableSetting;
    public bool DisableUserGuide;
    public string UITaskRegisterTypeDNName;
    public LoginSettings LoginSetting;

    [MethodImpl((MethodImplOptions) 32768)]
    public GameClientSetting()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
