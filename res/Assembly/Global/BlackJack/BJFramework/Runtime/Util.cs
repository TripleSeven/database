﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Util
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Profiling;

namespace BlackJack.BJFramework.Runtime
{
  public class Util
  {
    private static Dictionary<string, string> m_dnNameDict = new Dictionary<string, string>();
    private static string m_platform;
    private static string m_platformSuffix;
    private static string m_persistentPath;
    private static string m_streamingAssetsPath;

    public static long GetRuntimeMemorySize()
    {
      return Profiler.GetTotalAllocatedMemoryLong();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void LogMemorySize(string prefix)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator DownloadHttpFile(
      string url,
      Action<bool, WWW> onReceive,
      Action<WWW> onUpdate = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetCurrentTargetPlatform()
    {
      // ISSUE: unable to decompile the method.
    }

    public static string PlatformSuffix
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static Func<string> GetCurrentTargetPlatformInEditor { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetFilePersistentPath(string assetPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetFileStreamingAssetsPath(string assetPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetFileStreamingAssetsPath4WWW(string assetPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string ReformPathString(string path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetAssetDNNameFromPath(
      string path,
      string replaceLastFolderNameStr = "",
      bool autoExtension = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string ReplaceLastFolderName(string path, string replaceStr)
    {
      // ISSUE: unable to decompile the method.
    }

    public static string GetBundleNameByAssetPath(
      string path,
      string replaceLastFolderNameStr = "",
      bool autoExtension = true)
    {
      return Util.GetAssetDNNameFromPath(path, replaceLastFolderNameStr, autoExtension).ToLower();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static long GetFileSize(string path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void RemoveCloneFromGameObjectName(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetTransformParent(Transform child, Transform parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<long> GetAllFilesSize(List<string> fileList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<string> CreateMd5FromFileList(List<string> fileList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string CreateMd5FromFile(string filePath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string CreateMd5FromBytes(byte[] bytes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string TimeSpanToString(TimeSpan timeSpan)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetTypeDefaultValueString(System.Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static System.Type FindType(string typeName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetTypeDefinitionString(System.Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetGenericTypeFriendlyName(System.Type type)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
