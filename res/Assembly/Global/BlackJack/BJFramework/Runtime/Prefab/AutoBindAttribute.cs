﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Prefab.AutoBindAttribute
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.Prefab
{
  [AttributeUsage(AttributeTargets.Field)]
  public class AutoBindAttribute : Attribute
  {
    public readonly string m_path;
    public readonly AutoBindAttribute.InitState m_initState;
    public readonly bool m_optional;

    [MethodImpl((MethodImplOptions) 32768)]
    public AutoBindAttribute(string path, AutoBindAttribute.InitState initState = AutoBindAttribute.InitState.NotInit, bool optional = false)
    {
      // ISSUE: unable to decompile the method.
    }

    public enum InitState
    {
      NotInit,
      Active,
      Inactive,
    }
  }
}
