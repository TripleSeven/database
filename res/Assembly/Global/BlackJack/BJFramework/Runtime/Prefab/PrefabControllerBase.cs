﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Prefab.PrefabControllerBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime.Prefab
{
  public class PrefabControllerBase : MonoBehaviour
  {
    protected bool m_inited;
    protected string m_ctrlName;
    public PrefabResourceContainer m_resContainer;
    protected PrefabControllerNextUpdateExecutor m_nextUpdateExecutor;

    [MethodImpl((MethodImplOptions) 32768)]
    public PrefabControllerBase()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Awake()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Initlize(string ctrlName, bool bindNow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T GetAssetInContainer<T>(string resName) where T : UnityEngine.Object
    {
      // ISSUE: unable to decompile the method.
    }

    public UnityEngine.Object GetAssetInContainer(string resName)
    {
      return this.GetAssetInContainer<UnityEngine.Object>(resName);
    }

    public void BindResContainer()
    {
      this.m_resContainer = this.GetComponent<PrefabResourceContainer>();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void BindFields()
    {
      // ISSUE: unable to decompile the method.
    }

    public PrefabControllerNextUpdateExecutor GetNextUpdateExecutor()
    {
      return this.m_nextUpdateExecutor;
    }

    protected virtual void OnBindFiledsCompleted()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual UnityEngine.Object BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName = null,
      bool optional = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GameObject GetChildByPath(string path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PrefabControllerBase AddControllerToGameObject(
      GameObject root,
      string path,
      TypeDNName ctrlTypeDNName,
      string ctrlName,
      bool autoBind = false)
    {
      // ISSUE: unable to decompile the method.
    }

    public string CtrlName
    {
      get
      {
        return this.m_ctrlName;
      }
    }

    public static event Action<GameObject> m_onAwake
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
