﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Prefab.PrefabResourceContainerBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime.Prefab
{
  public abstract class PrefabResourceContainerBase : MonoBehaviour
  {
    public List<PrefabResourceContainerBase.AssetCacheItem> AssetList;
    public List<PrefabResourceContainerBase.AssetCacheItem> m_assetCacheList;
    protected Dictionary<string, UnityEngine.Object> m_rawAssetCacheDict;

    [MethodImpl((MethodImplOptions) 32768)]
    protected PrefabResourceContainerBase()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool TryGetAsset(string assetName, out UnityEngine.Object asset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UnityEngine.Object GetAsset(string assetName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T GetAsset<T>(string assetName) where T : UnityEngine.Object
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable GetAssetAsync(
      string assetName,
      Action<UnityEngine.Object> onEnd,
      bool isLoadAssetAsync = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable StartLazyLoadBackground(bool isLoadAssetAsync = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable StartLazyLoadBackground(
      List<string> assetNames,
      bool isLoadAssetAsync = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator RecacheAllAssetsFromResourceManager()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected IEnumerator LoadAndCacheAsset(
      HashSet<string> assetPathSet,
      bool isLoadAssetAsync)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private PrefabResourceContainerBase.AssetCacheItem GetCacheItemFromList(
      string assetName)
    {
      // ISSUE: unable to decompile the method.
    }

    [Serializable]
    public class AssetCacheItem
    {
      public string Name;
      [Header("[不要手动设置!]")]
      public string AssetPath;
      public UnityEngine.Object Asset;
      [Header("[是否延时加载!]")]
      public bool LazyLoad;
      [Header("[不要手动设置!]")]
      public bool IsAssetBundleNotUnload;
      [Header("[不要手动设置!]")]
      public UnityEngine.Object RuntimeAssetCache;
    }
  }
}
