﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Prefab.PrefabControllerNextUpdateExecutor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.Prefab
{
  public class PrefabControllerNextUpdateExecutor
  {
    private List<Action> m_nextUpdateExecutionList;

    [MethodImpl((MethodImplOptions) 32768)]
    public PrefabControllerNextUpdateExecutor()
    {
      // ISSUE: unable to decompile the method.
    }

    public void AddNextUpdateExecution(Action action)
    {
      this.m_nextUpdateExecutionList.Add(action);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
