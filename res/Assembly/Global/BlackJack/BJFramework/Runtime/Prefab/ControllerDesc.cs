﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Prefab.ControllerDesc
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;

namespace BlackJack.BJFramework.Runtime.Prefab
{
  [Serializable]
  public struct ControllerDesc
  {
    public string m_ctrlName;
    public TypeDNName m_ctrlTypeDNName;
    public string m_attachPath;
  }
}
