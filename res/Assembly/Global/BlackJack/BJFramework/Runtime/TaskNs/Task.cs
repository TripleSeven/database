﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.TaskNs.Task
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.TaskNs
{
  public class Task : ITickable
  {
    private TaskManager m_taskManager;
    private ulong m_currTickCount;
    private LinkedList<Task.DelayExecItem> m_delayExecList;

    [MethodImpl((MethodImplOptions) 32768)]
    public Task(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Start(object param = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Stop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Pause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Resume(object param = null)
    {
      // ISSUE: unable to decompile the method.
    }

    protected void ClearOnStopEvent()
    {
      this.EventOnStop = (Action<Task>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    void ITickable.Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ExecAfterTicks(Action action, ulong delayTickCount = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual bool OnStart(object param)
    {
      return true;
    }

    protected virtual void OnPause()
    {
    }

    protected virtual bool OnResume(object param = null)
    {
      return true;
    }

    protected virtual void OnStop()
    {
    }

    protected virtual void OnTick()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }

    public string Name { get; private set; }

    public Task.TaskState State { get; private set; }

    public DateTime PauseStartTime { get; private set; }

    public event Action<Task> EventOnStart
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Task> EventOnStop
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Task> EventOnPause
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Task> EventOnResume
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public enum TaskState
    {
      Init,
      Running,
      Paused,
      Stopped,
    }

    public class DelayExecItem
    {
      public ulong m_execTargetTick;
      public Action m_action;
    }
  }
}
