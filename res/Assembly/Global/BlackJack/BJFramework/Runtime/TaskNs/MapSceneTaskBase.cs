﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.TaskNs.MapSceneTaskBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Scene;
using BlackJack.Utils;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.TaskNs
{
  public class MapSceneTaskBase : Task, IDynamicAssetProvider
  {
    protected MapSceneUpdatePipeLineCtxBase m_pipeCtxDefault;
    protected List<MapSceneUpdatePipeLineCtxBase> m_runingPipeLineCtxList;
    protected List<MapSceneUpdatePipeLineCtxBase> m_unusingPipeLineCtxList;
    protected List<MapSceneUpdatePipeLineCtxBase> m_pipeLineWait2Start;
    protected SceneLayerBase[] m_layerArray;
    protected Dictionary<string, MapSceneTaskBase.DynamicResCacheItem> m_dynamicResCacheDict;
    protected List<string> m_wait2ReleaseResList;
    protected TinyCorutineHelper m_corutineHelper;
    private int m_unloadDynamicResTimeInterval;
    private DateTime m_nextUnLoadDynamicResTime;

    [MethodImpl((MethodImplOptions) 32768)]
    public MapSceneTaskBase(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void InitPipeCtxDefault()
    {
      this.m_pipeCtxDefault = new MapSceneUpdatePipeLineCtxBase();
    }

    protected override bool OnStart(object param)
    {
      return this.StartUpdatePipeLine((MapSceneUpdatePipeLineCtxBase) null);
    }

    protected override void OnStop()
    {
      this.ClearAllContextAndRes();
    }

    protected override void OnPause()
    {
      this.HideAllView();
      this.ClearContextOnPause();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool StartUpdatePipeLine(MapSceneUpdatePipeLineCtxBase pipeCtx = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual MapSceneUpdatePipeLineCtxBase AllocPipeLineCtx()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual MapSceneUpdatePipeLineCtxBase CreatePipeLineCtx()
    {
      return new MapSceneUpdatePipeLineCtxBase();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool StartPipeLineCtx(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void ReleasePipeLineCtx(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual bool IsNeedUpdateDataCache(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      return false;
    }

    protected virtual void UpdateDataCache(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
    }

    protected virtual void PreProcessBeforeResLoad(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
    }

    protected virtual bool IsNeedLoadStaticRes()
    {
      return (UnityEngine.Object) this.m_mainLayer == (UnityEngine.Object) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void StartLoadStaticRes(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnLoadStaticResCompleted(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual bool IsNeedCollectAllDynamicResForLoad(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      return false;
    }

    protected virtual List<string> CollectAllDynamicResForLoad(
      MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      return (List<string>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void StartLoadDynamicRes(
      List<string> resPathList,
      MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected HashSet<string> CalculateDynamicResReallyNeedLoad(List<string> resPathList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnLoadDynamicResCompleted(
      Dictionary<string, UnityEngine.Object> resDict,
      MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool IsLoadAllResCompleted(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnLoadAllResCompleted(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void InitLayerStateOnLoadAllResCompleted(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void StartUpdateView(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      this.UpdateView(pipeCtx);
      this.PostUpdateView(pipeCtx);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void UpdateView(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void PostUpdateView(MapSceneUpdatePipeLineCtxBase pipeCtx)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void HideAllView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void ClearContextOnPause()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ReleaseDynamicResCache(string path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void LockDynamicResCache(string path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UnlockDynamicResCache(string path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UnlockAllDynamicResCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void TickDynamicResCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    UnityEngine.Object IDynamicAssetProvider.AllocDynamicAsset(
      string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    T IDynamicAssetProvider.AllocDynamicAsset<T>(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    void IDynamicAssetProvider.ReleaseDynamicAsset(string name)
    {
      this.ReleaseDynamicResCache(name);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    bool IDynamicAssetProvider.CheckAsset(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    protected SceneLayerBase m_mainLayer
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected virtual MapSceneTaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    public struct LayerDesc
    {
      public string m_layerName;
      public string m_layerResPath;
      public bool m_isUILayer;
    }

    public class DynamicResCacheItem
    {
      public UnityEngine.Object m_res;
      public uint m_ref;
      public bool m_waitForRelease;
      public bool m_isLocked;
    }
  }
}
