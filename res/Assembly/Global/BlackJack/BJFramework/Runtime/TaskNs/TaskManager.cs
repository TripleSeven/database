﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.TaskNs.TaskManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.TaskNs
{
  public class TaskManager : ITickable
  {
    private static TaskManager m_instance;
    private List<Task> m_taskList;
    private List<Task> m_taskList4TickLoop;
    private Dictionary<string, Task> m_taskRegDict;

    [MethodImpl((MethodImplOptions) 32768)]
    private TaskManager()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static TaskManager CreateTaskManager()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool Initlize()
    {
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Uninitlize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool RegisterTask(Task task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UnregisterTask(Task task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopTaskWithTaskName(string taskName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T FindTaskByName<T>(string taskName) where T : Task
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    public static TaskManager Instance
    {
      get
      {
        return TaskManager.m_instance;
      }
    }
  }
}
