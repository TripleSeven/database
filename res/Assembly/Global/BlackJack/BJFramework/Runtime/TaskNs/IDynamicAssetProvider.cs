﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.TaskNs.IDynamicAssetProvider
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace BlackJack.BJFramework.Runtime.TaskNs
{
  public interface IDynamicAssetProvider
  {
    Object AllocDynamicAsset(string name);

    T AllocDynamicAsset<T>(string name) where T : Object;

    void ReleaseDynamicAsset(string name);

    bool CheckAsset(string resString);
  }
}
