﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.TaskNs.MapSceneUpdatePipeLineCtxBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.TaskNs
{
  public class MapSceneUpdatePipeLineCtxBase
  {
    protected bool m_runing;
    public bool m_lockResCache;
    public int m_loadingStaticResCorutineCount;
    public int m_loadingDynamicResCorutineCount;
    public Action m_updateViewAction;

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Start()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsRunning()
    {
      return this.m_runing;
    }
  }
}
