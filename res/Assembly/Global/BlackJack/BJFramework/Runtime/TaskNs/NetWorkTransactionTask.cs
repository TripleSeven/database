﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.TaskNs.NetWorkTransactionTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.TaskNs
{
  public class NetWorkTransactionTask : Task
  {
    public static int m_delayTimeForUIWaiting = 1;
    protected UITaskBase m_blockedUITask;
    protected bool m_autoRetry;
    protected float m_timeout;
    protected bool m_isTimeOuted;
    protected DateTime? m_timeoutTime;
    protected DateTime m_showWaitingUITime;
    protected bool m_isUIWaitingShown;
    protected bool m_isReturnToLoginByDirtyData;
    protected bool m_isLaunchedReloginUITask;
    public const float TimeOutForEditor = 60f;

    [MethodImpl((MethodImplOptions) 32768)]
    public NetWorkTransactionTask(float timeout = 10f, UITaskBase blockedUITask = null, bool autoRetry = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(object param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(object param = null)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual bool StartNetWorking()
    {
      return this.Base_StartNetWorking();
    }

    private bool Base_StartNetWorking()
    {
      return true;
    }

    protected virtual void OnTransactionComplete()
    {
      this.Stop();
    }

    protected virtual void RegisterNetworkEvent()
    {
      this.Base_RegisterNetworkEvent();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Base_RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void UnregisterNetworkEvent()
    {
      this.Base_UnregisterNetworkEvent();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Base_UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void OnTimeOut()
    {
      this.OnNetworkError();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnGameServerDataUnsyncNotify()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void OnGameServerNetworkError(int err)
    {
      this.OnNetworkError();
    }

    protected virtual void OnGameServerDisconnected()
    {
      this.OnNetworkError();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnNetworkError()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnReLoginSuccess()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void OnUIManagerReturnToLoginUI(bool obj)
    {
      this.Stop();
    }

    public static event Action<bool> EventShowUIWaiting
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static event Action<bool> EventReturnToLoginUI
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static event Action<Action> EventReLoginBySession
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
