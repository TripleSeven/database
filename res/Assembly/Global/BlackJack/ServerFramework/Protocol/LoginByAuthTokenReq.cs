﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ServerFramework.Protocol.LoginByAuthTokenReq
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.ComponentModel;

namespace BlackJack.ServerFramework.Protocol
{
  [ProtoContract(Name = "LoginByAuthTokenReq")]
  [Serializable]
  public class LoginByAuthTokenReq : IExtensible
  {
    private string _AuthToken;
    private string _ClientVersion;
    private string _ClientDeviceId;
    private string _Localization;
    private int _CurrChannelId;
    private int _BornChannelId;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "AuthToken")]
    public string AuthToken
    {
      get
      {
        return this._AuthToken;
      }
      set
      {
        this._AuthToken = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "ClientVersion")]
    public string ClientVersion
    {
      get
      {
        return this._ClientVersion;
      }
      set
      {
        this._ClientVersion = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "ClientDeviceId")]
    public string ClientDeviceId
    {
      get
      {
        return this._ClientDeviceId;
      }
      set
      {
        this._ClientDeviceId = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Localization")]
    public string Localization
    {
      get
      {
        return this._Localization;
      }
      set
      {
        this._Localization = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "CurrChannelId")]
    [DefaultValue(0)]
    public int CurrChannelId
    {
      get
      {
        return this._CurrChannelId;
      }
      set
      {
        this._CurrChannelId = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "BornChannelId")]
    public int BornChannelId
    {
      get
      {
        return this._BornChannelId;
      }
      set
      {
        this._BornChannelId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
