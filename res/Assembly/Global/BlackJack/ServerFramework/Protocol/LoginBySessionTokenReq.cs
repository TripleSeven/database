﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ServerFramework.Protocol.LoginBySessionTokenReq
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.ComponentModel;

namespace BlackJack.ServerFramework.Protocol
{
  [ProtoContract(Name = "LoginBySessionTokenReq")]
  [Serializable]
  public class LoginBySessionTokenReq : IExtensible
  {
    private string _SessionToken;
    private string _ClientVersion;
    private string _Localization;
    private int _CurrChannelId;
    private int _BornChannelId;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "SessionToken")]
    public string SessionToken
    {
      get
      {
        return this._SessionToken;
      }
      set
      {
        this._SessionToken = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "ClientVersion")]
    public string ClientVersion
    {
      get
      {
        return this._ClientVersion;
      }
      set
      {
        this._ClientVersion = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Localization")]
    public string Localization
    {
      get
      {
        return this._Localization;
      }
      set
      {
        this._Localization = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "CurrChannelId")]
    [DefaultValue(0)]
    public int CurrChannelId
    {
      get
      {
        return this._CurrChannelId;
      }
      set
      {
        this._CurrChannelId = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "BornChannelId")]
    [DefaultValue(0)]
    public int BornChannelId
    {
      get
      {
        return this._BornChannelId;
      }
      set
      {
        this._BornChannelId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
