﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ServerFramework.Protocol.LoginByAuthTokenAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BlackJack.ServerFramework.Protocol
{
  [ProtoContract(Name = "LoginByAuthTokenAck")]
  [Serializable]
  public class LoginByAuthTokenAck : IExtensible
  {
    private int _Result;
    private bool _NeedRedirect;
    private string _SessionToken;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public LoginByAuthTokenAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      get
      {
        return this._Result;
      }
      set
      {
        this._Result = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "NeedRedirect")]
    public bool NeedRedirect
    {
      get
      {
        return this._NeedRedirect;
      }
      set
      {
        this._NeedRedirect = value;
      }
    }

    [DefaultValue("")]
    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = false, Name = "SessionToken")]
    public string SessionToken
    {
      get
      {
        return this._SessionToken;
      }
      set
      {
        this._SessionToken = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
