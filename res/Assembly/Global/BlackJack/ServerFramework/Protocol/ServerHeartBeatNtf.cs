﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ServerFramework.Protocol.ServerHeartBeatNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ServerFramework.Protocol
{
  [ProtoContract(Name = "ServerHeartBeatNtf")]
  [Serializable]
  public class ServerHeartBeatNtf : IExtensible
  {
    private IExtension extensionObject;

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
