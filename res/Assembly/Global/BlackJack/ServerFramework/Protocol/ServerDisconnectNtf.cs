﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ServerFramework.Protocol.ServerDisconnectNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.ComponentModel;

namespace BlackJack.ServerFramework.Protocol
{
  [ProtoContract(Name = "ServerDisconnectNtf")]
  [Serializable]
  public class ServerDisconnectNtf : IExtensible
  {
    private int _ErrorCode;
    private IExtension extensionObject;

    [DefaultValue(0)]
    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "ErrorCode")]
    public int ErrorCode
    {
      get
      {
        return this._ErrorCode;
      }
      set
      {
        this._ErrorCode = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
