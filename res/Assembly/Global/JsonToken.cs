﻿// Decompiled with JetBrains decompiler
// Type: JsonToken
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

public enum JsonToken
{
  None,
  ObjectStart,
  PropertyName,
  ObjectEnd,
  ArrayStart,
  ArrayEnd,
  Int,
  Long,
  Double,
  String,
  Boolean,
  Null,
}
