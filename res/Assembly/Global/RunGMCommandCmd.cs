﻿// Decompiled with JetBrains decompiler
// Type: RunGMCommandCmd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.UI;

public class RunGMCommandCmd : IDebugCmd
{
  public void Execute(string strParams)
  {
    TestUI.SendGMCommand(strParams);
  }

  public string GetHelpDesc()
  {
    return "Rgc CLEAN_USER_GUIDE 1,2,3 : 执行管理员指令";
  }

  public string GetName()
  {
    return "Rgc";
  }
}
