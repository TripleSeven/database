﻿// Decompiled with JetBrains decompiler
// Type: GoogleTranslationSetting
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

public class GoogleTranslationSetting
{
  public const string API_KEY = "AIzaSyDkr9VY-Rje0Qqn5LGDj5ZVDiY7nnVp6iE";
  public const string POST_TRANSLATE_REQUEST_URL = "https://translation.googleapis.com/language/translate/v2?key=";
  public const string POST_DETECT_REQUEST_URL = "https://translation.googleapis.com/language/translate/v2/detect?key=";
  public const bool ENABLE_CACHE = true;
  public const int CACHE_MAX_LENGTH = 300;
  public const int MaxLanguageRepeatTimes = 3;
}
