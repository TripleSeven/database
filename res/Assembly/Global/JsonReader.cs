﻿// Decompiled with JetBrains decompiler
// Type: JsonReader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;

public class JsonReader
{
  private static IDictionary<int, IDictionary<int, int[]>> parse_table;
  private Stack<int> automaton_stack;
  private int current_input;
  private int current_symbol;
  private bool end_of_json;
  private bool end_of_input;
  private Lexer lexer;
  private bool parser_in_string;
  private bool parser_return;
  private bool read_started;
  private TextReader reader;
  private bool reader_is_owned;
  private object token_value;
  private JsonToken token;

  static JsonReader()
  {
    JsonReader.PopulateParseTable();
  }

  public JsonReader(string json_text)
    : this((TextReader) new StringReader(json_text), true)
  {
  }

  public JsonReader(TextReader reader)
    : this(reader, false)
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private JsonReader(TextReader reader, bool owned)
  {
    // ISSUE: unable to decompile the method.
  }

  public bool AllowComments
  {
    get
    {
      return this.lexer.AllowComments;
    }
    set
    {
      this.lexer.AllowComments = value;
    }
  }

  public bool AllowSingleQuotedStrings
  {
    get
    {
      return this.lexer.AllowSingleQuotedStrings;
    }
    set
    {
      this.lexer.AllowSingleQuotedStrings = value;
    }
  }

  public bool EndOfInput
  {
    get
    {
      return this.end_of_input;
    }
  }

  public bool EndOfJson
  {
    get
    {
      return this.end_of_json;
    }
  }

  public JsonToken Token
  {
    get
    {
      return this.token;
    }
  }

  public object Value
  {
    get
    {
      return this.token_value;
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void PopulateParseTable()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void TableAddCol(ParserToken row, int col, params int[] symbols)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void TableAddRow(ParserToken rule)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void ProcessNumber(string number)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void ProcessSymbol()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private bool ReadToken()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Close()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public bool Read()
  {
    // ISSUE: unable to decompile the method.
  }
}
