﻿// Decompiled with JetBrains decompiler
// Type: DynamicGridCellController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using System.Runtime.CompilerServices;

public abstract class DynamicGridCellController : UIControllerBase
{
  protected bool m_isRecycled = true;
  protected DynamicGrid m_ownerGrid;
  protected int m_index;
  protected object m_info;

  public event DynamicGridCellEventHappenedDelegate eventOnCellEventHappened
  {
    [MethodImpl((MethodImplOptions) 32768)] add
    {
      // ISSUE: unable to decompile the method.
    }
    [MethodImpl((MethodImplOptions) 32768)] remove
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public bool isRecycled
  {
    get
    {
      return this.m_isRecycled;
    }
  }

  public DynamicGrid ownerGrid
  {
    get
    {
      return this.m_ownerGrid;
    }
    set
    {
      this.m_ownerGrid = value;
    }
  }

  public int index
  {
    get
    {
      return this.m_index;
    }
    set
    {
      this.m_index = value;
    }
  }

  public object info
  {
    get
    {
      return this.m_info;
    }
  }

  protected virtual void OnUpdateCell()
  {
  }

  protected virtual void OnRenovate()
  {
  }

  protected virtual void OnRecycle()
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  protected void Select()
  {
    // ISSUE: unable to decompile the method.
  }

  protected void SendEvent(int eventId)
  {
    this.SendEvent(eventId, (object) null);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  protected void SendEvent(int eventId, object obj)
  {
    // ISSUE: unable to decompile the method.
  }

  public void UpdateCell(object info)
  {
    this.m_info = info;
    this.OnUpdateCell();
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Renovate()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Recycle()
  {
    // ISSUE: unable to decompile the method.
  }
}
