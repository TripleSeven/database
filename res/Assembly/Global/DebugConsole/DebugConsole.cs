﻿// Decompiled with JetBrains decompiler
// Type: DebugConsole.DebugConsole
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;
using UnityEngine;

namespace DebugConsole
{
  public class DebugConsole : MonoBehaviour
  {
    private DebugConsoleView _consoleView;
    public static DebugConsole.DebugConsole instance;

    [MethodImpl((MethodImplOptions) 32768)]
    private static void LogPlatformDefine()
    {
      // ISSUE: unable to decompile the method.
    }

    private void Awake()
    {
      DebugConsole.DebugConsole.instance = this;
      this.Init();
    }

    private void Start()
    {
      DebugConsole.DebugConsole.LogPlatformDefine();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    protected void OnGUI()
    {
      this._consoleView.Show();
    }

    private void OnDestroy()
    {
      DebugConsole.DebugConsole.instance = (DebugConsole.DebugConsole) null;
    }
  }
}
