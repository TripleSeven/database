﻿// Decompiled with JetBrains decompiler
// Type: JsonMapper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;

public class JsonMapper
{
  private static int max_nesting_depth;
  private static IFormatProvider datetime_format;
  private static IDictionary<System.Type, ExporterFunc> base_exporters_table;
  private static IDictionary<System.Type, ExporterFunc> custom_exporters_table;
  private static IDictionary<System.Type, IDictionary<System.Type, ImporterFunc>> base_importers_table;
  private static IDictionary<System.Type, IDictionary<System.Type, ImporterFunc>> custom_importers_table;
  private static IDictionary<System.Type, ArrayMetadata> array_metadata;
  private static readonly object array_metadata_lock;
  private static IDictionary<System.Type, IDictionary<System.Type, MethodInfo>> conv_ops;
  private static readonly object conv_ops_lock;
  private static IDictionary<System.Type, ObjectMetadata> object_metadata;
  private static readonly object object_metadata_lock;
  private static IDictionary<System.Type, IList<PropertyMetadata>> type_properties;
  private static readonly object type_properties_lock;
  private static JsonWriter static_writer;
  private static readonly object static_writer_lock;

  [MethodImpl((MethodImplOptions) 32768)]
  static JsonMapper()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void AddArrayMetadata(System.Type type)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void AddObjectMetadata(System.Type type)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void AddTypeProperties(System.Type type)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static MethodInfo GetConvOp(System.Type t1, System.Type t2)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static object ReadValue(System.Type inst_type, JsonReader reader)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static IJsonWrapper ReadValue(WrapperFactory factory, JsonReader reader)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void RegisterBaseExporters()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void RegisterBaseImporters()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void RegisterImporter(
    IDictionary<System.Type, IDictionary<System.Type, ImporterFunc>> table,
    System.Type json_type,
    System.Type value_type,
    ImporterFunc importer)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void WriteValue(object obj, JsonWriter writer, bool writer_is_private, int depth)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static string ToJson(object obj)
  {
    // ISSUE: unable to decompile the method.
  }

  public static void ToJson(object obj, JsonWriter writer)
  {
    JsonMapper.WriteValue(obj, writer, false, 0);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static JsonData ToObject(JsonReader reader)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static JsonData ToObject(TextReader reader)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static JsonData ToObject(string json)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static T ToObject<T>(JsonReader reader)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static T ToObject<T>(TextReader reader)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static T ToObject<T>(string json)
  {
    // ISSUE: unable to decompile the method.
  }

  public static IJsonWrapper ToWrapper(WrapperFactory factory, JsonReader reader)
  {
    return JsonMapper.ReadValue(factory, reader);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static IJsonWrapper ToWrapper(WrapperFactory factory, string json)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void RegisterExporter<T>(ExporterFunc<T> exporter)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void RegisterImporter<TJson, TValue>(ImporterFunc<TJson, TValue> importer)
  {
    // ISSUE: unable to decompile the method.
  }

  public static void UnregisterExporters()
  {
    JsonMapper.custom_exporters_table.Clear();
  }

  public static void UnregisterImporters()
  {
    JsonMapper.custom_importers_table.Clear();
  }
}
