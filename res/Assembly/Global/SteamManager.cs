﻿// Decompiled with JetBrains decompiler
// Type: SteamManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using Steamworks;
using System.Runtime.CompilerServices;
using System.Text;
using UnityEngine;

[DisallowMultipleComponent]
public class SteamManager : MonoBehaviour
{
  private static SteamManager s_instance;
  private static bool s_EverInitialized;
  private bool m_bInitialized;
  private SteamAPIWarningMessageHook_t m_SteamAPIWarningMessageHook;
  protected Callback<GameOverlayActivated_t> m_GameOverlayActivated;
  protected Callback<MicroTxnAuthorizationResponse_t> m_MicroTxnAuthorizationResponse;

  private static SteamManager Instance
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public static bool Initialized
  {
    get
    {
      return SteamManager.Instance.m_bInitialized;
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void SteamAPIDebugTextHook(int nSeverity, StringBuilder pchDebugText)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void Awake()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void OnEnable()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void OnGameOverlayActivated(GameOverlayActivated_t pCallback)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void OnMicroTxnAuthorizationResponse(MicroTxnAuthorizationResponse_t pCallback)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void OnDestroy()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void Update()
  {
    // ISSUE: unable to decompile the method.
  }
}
