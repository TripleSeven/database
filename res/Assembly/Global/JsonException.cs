﻿// Decompiled with JetBrains decompiler
// Type: JsonException
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

public class JsonException : ApplicationException
{
  public JsonException()
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  internal JsonException(ParserToken token)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  internal JsonException(ParserToken token, Exception inner_exception)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  internal JsonException(int c)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  internal JsonException(int c, Exception inner_exception)
  {
    // ISSUE: unable to decompile the method.
  }

  public JsonException(string message)
    : base(message)
  {
  }

  public JsonException(string message, Exception inner_exception)
    : base(message, inner_exception)
  {
  }
}
