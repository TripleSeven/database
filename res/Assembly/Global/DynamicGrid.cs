﻿// Decompiled with JetBrains decompiler
// Type: DynamicGrid
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

[AddComponentMenu("Layout/Dynamic Grid", 152)]
public class DynamicGrid : LayoutGroup
{
  [SerializeField]
  protected RectTransform.Axis m_axis;
  [SerializeField]
  protected DynamicGrid.Corner m_startCorner;
  [SerializeField]
  protected RectTransform.Axis m_startAxis;
  [SerializeField]
  protected Vector2 m_cellSize;
  [SerializeField]
  protected Vector2 m_spacing;
  [SerializeField]
  protected int m_constraintCount;
  [SerializeField]
  protected ScrollRect m_scrollRect;
  [SerializeField]
  protected GameObject m_dynamicCell;
  [SerializeField]
  protected int m_dynamicCellCapacity;
  [SerializeField]
  protected List<GameObject> m_headStaticObjList;
  [SerializeField]
  protected List<GameObject> m_tailStaticObjList;
  [SerializeField]
  protected bool m_isStaticInRule;
  [SerializeField]
  private DynamicGrid.AutoCollectStaticType m_autoCollectType;
  [SerializeField]
  protected DynamicGrid.EmptyFillType m_emptyFillType;
  [SerializeField]
  protected GameObject m_emptyCell;
  [SerializeField]
  protected int m_emptyCellCapacity;
  [SerializeField]
  protected int m_lazyCount;
  [SerializeField]
  protected bool m_disableScrollWhenLessCell;
  [SerializeField]
  protected List<GameObject> m_ignoreList;
  [SerializeField]
  protected DynamicGrid.SelectType m_selectType;
  [SerializeField]
  protected bool m_allowSelectSameAgain;
  [SerializeField]
  protected int m_selectLimitedCount;
  private int m_cellsPerMainAxis;
  private int m_actualCellCountX;
  private int m_actualCellCountY;
  private Vector2 m_startOffset;
  private float m_triggerPosMin;
  private float m_triggerPosMax;
  private int m_visiblePosMin;
  private int m_visiblePosMax;
  private List<DynamicGridCellController> m_cellList;
  private List<DynamicGridCellController> m_emptyCellList;
  private List<DynamicGridCellController> m_headStaticCellList;
  private List<DynamicGridCellController> m_tailStaticCellList;
  private IList m_infoList;
  private int m_infoCount;
  private Func<GameObject, DynamicGridCellController> m_funcOnInitCell;
  private Func<GameObject, DynamicGridCellController> m_funcOnInitEmptyCell;
  private Func<int, int, object> m_funcOnUpdateEmptyCell;
  private Vector2 m_targetLerpPos;
  private Vector2 m_startLerpPos;
  private bool m_startLerp;
  private float m_lerpTime;
  private float m_curTime;
  private Action m_actionOnScrollEnd;
  private List<object> m_selectedInfoList;
  private bool m_isInitialized;
  private Stack<DynamicGrid.DummyInfo> m_dummyInfoStack;
  private List<DynamicGrid.DummyInfo> m_dummyInfoList;
  private List<DynamicGridCellController> m_hideCellList;
  private bool m_needUpdateGridPlaying;

  [MethodImpl((MethodImplOptions) 32768)]
  public DynamicGrid()
  {
    // ISSUE: unable to decompile the method.
  }

  public event Action eventOnSelectedListChanged
  {
    [MethodImpl((MethodImplOptions) 32768)] add
    {
      // ISSUE: unable to decompile the method.
    }
    [MethodImpl((MethodImplOptions) 32768)] remove
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public event DynamicGridCellUpdateDelegate eventOnCellUpdate
  {
    [MethodImpl((MethodImplOptions) 32768)] add
    {
      // ISSUE: unable to decompile the method.
    }
    [MethodImpl((MethodImplOptions) 32768)] remove
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public event DynamicGridCellSelectChangedDelegate eventOnSelectedStateChanged
  {
    [MethodImpl((MethodImplOptions) 32768)] add
    {
      // ISSUE: unable to decompile the method.
    }
    [MethodImpl((MethodImplOptions) 32768)] remove
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public event DynamicGridCellEventHappenedDelegate eventOnCellEventHappened
  {
    [MethodImpl((MethodImplOptions) 32768)] add
    {
      // ISSUE: unable to decompile the method.
    }
    [MethodImpl((MethodImplOptions) 32768)] remove
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public DynamicGrid.Corner startCorner
  {
    get
    {
      return this.m_startCorner;
    }
    set
    {
      this.SetProperty<DynamicGrid.Corner>(ref this.m_startCorner, value);
    }
  }

  public Vector2 cellSize
  {
    get
    {
      return this.m_cellSize;
    }
    set
    {
      this.SetProperty<Vector2>(ref this.m_cellSize, value);
    }
  }

  public Vector2 spacing
  {
    get
    {
      return this.m_spacing;
    }
    set
    {
      this.SetProperty<Vector2>(ref this.m_spacing, value);
    }
  }

  public int constraintCount
  {
    get
    {
      return this.m_constraintCount;
    }
    [MethodImpl((MethodImplOptions) 32768)] set
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public ScrollRect scrollRect
  {
    get
    {
      return this.m_scrollRect;
    }
    set
    {
      this.SetProperty<ScrollRect>(ref this.m_scrollRect, value);
    }
  }

  public int selectedIndex
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public object selectedInfo
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public List<object> selectedInfoList
  {
    get
    {
      return this.m_selectedInfoList;
    }
  }

  protected override void OnEnable()
  {
    base.OnEnable();
    this.SetDirty();
  }

  [MethodImpl((MethodImplOptions) 32768)]
  protected override void OnDisable()
  {
    // ISSUE: unable to decompile the method.
  }

  protected override void OnRectTransformDimensionsChange()
  {
    base.OnRectTransformDimensionsChange();
    this.SetDirty();
  }

  public void Initialize(
    Func<GameObject, DynamicGridCellController> funcOnInitCell)
  {
    this.Initialize(funcOnInitCell, (Func<GameObject, DynamicGridCellController>) null, (Func<int, int, object>) null);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Initialize(
    Func<GameObject, DynamicGridCellController> funcOnInitCell,
    Func<GameObject, DynamicGridCellController> funcOnInitEmptyCell,
    Func<int, int, object> funcOnUpdateEmptyCell)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  protected void InitDynamicCells()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  protected void InitEmptyCells()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private bool IsUnknownChild(GameObject go)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void InitStaticCellList(
    List<GameObject> objList,
    List<DynamicGridCellController> cellList,
    DynamicGrid.AutoCollectStaticType collectType)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void HideUnknownChild()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override void CalculateLayoutInputHorizontal()
  {
    // ISSUE: unable to decompile the method.
  }

  public override void SetLayoutHorizontal()
  {
    this.UpdateContentSize(RectTransform.Axis.Horizontal);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override void CalculateLayoutInputVertical()
  {
    // ISSUE: unable to decompile the method.
  }

  public override void SetLayoutVertical()
  {
    this.UpdateContentSize(RectTransform.Axis.Vertical);
    this.UpdateGrid(false);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void RebuildImmediately()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void UpdateGrid(bool immediately)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void UpdateContentSize(RectTransform.Axis axis)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void UpdateGridFieldData()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void UpdateGridPlaying()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void UpdateGridPreview()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void OnScrollRectValueChanged(Vector2 value)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void OnCellEventHappened(int eventId, int index, object obj)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private RectTransform.Axis GetAxis()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private Rect GetViewportRect()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public int GetChildCount(bool ignoreEmpty = false)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private bool IsIndexNeedCheckSkip(int index)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private bool IsIndexVisible(RectTransform.Axis axis, int positionX, int positionY)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private DynamicGrid.EIndexPart GetIndexPart(int index)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void UpdateVisibleRange()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void GetPosByIndex(int index, out int positionX, out int positionY)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private bool CheckInScroll(bool needLogOut)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private Rect GetCutRect()
  {
    // ISSUE: unable to decompile the method.
  }

  public int GetHeadStaticCellCount()
  {
    return this.m_headStaticObjList.Count;
  }

  public int GetTailStaticCellCount()
  {
    return this.m_tailStaticObjList.Count;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public int GetEmptyCount(DynamicGrid.EmptyFillType emptyFillType)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private int GetViewportLineCount()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private Vector2 GetDefaultNormalizedPosition()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private DynamicGrid.ScrollEndAnchor GetAutoAnchor()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private DynamicGridCellController InstantiateCell()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void BindCell(DynamicGridCellController cell)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void RenovateCell(DynamicGridCellController cell)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void RecycleCell(DynamicGridCellController cell)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void UpdateHideCells()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private DynamicGridCellController InstantiateEmptyCell()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void ClearDummyInfoList()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private DynamicGrid.DummyInfo NewDummyInfo()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private bool SetCellListSelectState(List<DynamicGridCellController> list, object info, bool flag)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private bool SetSelectState(object info, bool flag)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void Update()
  {
    // ISSUE: unable to decompile the method.
  }

  public void Refresh()
  {
    this.Refresh(false);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Refresh(bool resetSelect)
  {
    // ISSUE: unable to decompile the method.
  }

  public void Refresh(int count)
  {
    this.Refresh(count, false);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Refresh(int count, bool resetSelect)
  {
    // ISSUE: unable to decompile the method.
  }

  public void Refresh(IList infoList)
  {
    this.Refresh(infoList, false);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Refresh(IList infoList, bool resetSelect)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public bool IsDrawingInfo(object info)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public bool IsDrawingIndex(int index)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public DynamicGridCellController GetCellByInfo(object info)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public DynamicGridCellController GetCellByIndex(int index)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void UpdateCell(int index)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void UpdateCell(int index, object info)
  {
    // ISSUE: unable to decompile the method.
  }

  public void ScrollToIndex(int index, DynamicGrid.ScrollEndAnchor anchor = DynamicGrid.ScrollEndAnchor.Auto)
  {
    this.ScrollToIndex(index, 0.0f, (Action) null, anchor);
  }

  public void ScrollToIndex(int index, float time, DynamicGrid.ScrollEndAnchor anchor = DynamicGrid.ScrollEndAnchor.Auto)
  {
    this.ScrollToIndex(index, time, (Action) null, anchor);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void ScrollToIndex(
    int index,
    float time,
    Action actionOnScrollEnd,
    DynamicGrid.ScrollEndAnchor anchor = DynamicGrid.ScrollEndAnchor.Auto)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void ScrollEnd()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void ScrollToInfo(int index, DynamicGrid.ScrollEndAnchor anchor = DynamicGrid.ScrollEndAnchor.Auto)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void ScrollToInfo(int index, float time, DynamicGrid.ScrollEndAnchor anchor = DynamicGrid.ScrollEndAnchor.Auto)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void ScrollToInfo(
    object info,
    float time,
    Action actionOnScrollEnd,
    DynamicGrid.ScrollEndAnchor anchor)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void ClearExcludedSelection()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void ResetAllSelect()
  {
    // ISSUE: unable to decompile the method.
  }

  public bool IsInfoSelected(object info)
  {
    return this.m_selectedInfoList.Contains(info);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public bool IsIndexSelected(int index)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void SelectIndex(int index)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void SelectInfo(object info)
  {
    // ISSUE: unable to decompile the method.
  }

  public enum Corner
  {
    UpperLeft,
    UpperRight,
    LowerLeft,
    LowerRight,
  }

  public enum ScrollEndAnchor
  {
    Auto,
    Center,
    UpperOrRight,
    LowerOrLeft,
  }

  public enum EmptyFillType
  {
    None,
    Line,
    Viewport,
  }

  public enum SelectType
  {
    None,
    One,
    OneJust,
    More,
    MoreLimited,
  }

  public enum AutoCollectStaticType
  {
    None,
    Head,
    Tail,
  }

  public enum EIndexPart
  {
    None,
    StaticHead,
    Dynamic,
    StaticTail,
    Empty,
  }

  private class DummyInfo
  {
  }
}
