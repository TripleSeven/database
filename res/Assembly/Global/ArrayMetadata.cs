﻿// Decompiled with JetBrains decompiler
// Type: ArrayMetadata
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

internal struct ArrayMetadata
{
  private System.Type element_type;
  private bool is_array;
  private bool is_list;

  public System.Type ElementType
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
    set
    {
      this.element_type = value;
    }
  }

  public bool IsArray
  {
    get
    {
      return this.is_array;
    }
    set
    {
      this.is_array = value;
    }
  }

  public bool IsList
  {
    get
    {
      return this.is_list;
    }
    set
    {
      this.is_list = value;
    }
  }
}
