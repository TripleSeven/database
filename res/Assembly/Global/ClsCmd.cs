﻿// Decompiled with JetBrains decompiler
// Type: ClsCmd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

public class ClsCmd : IDebugCmd
{
  private const string _NAME = "cls";
  private const string _DESC = "cls: clear console screen.";

  public void Execute(string strParams)
  {
    DebugConsoleMode.instance.ClearLog();
  }

  public string GetHelpDesc()
  {
    return "cls: clear console screen.";
  }

  public string GetName()
  {
    return "cls";
  }
}
