﻿// Decompiled with JetBrains decompiler
// Type: wxb.ULongType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace wxb
{
  internal class ULongType : Serialize<ulong>
  {
    protected override int CalculateSize(ulong value)
    {
      return 8;
    }

    protected override void Write(WRStream stream, ulong value)
    {
      stream.WriteInt64(value);
    }

    protected override ulong Read(WRStream stream)
    {
      return stream.ReadUInt64();
    }
  }
}
