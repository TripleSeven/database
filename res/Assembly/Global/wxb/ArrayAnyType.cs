﻿// Decompiled with JetBrains decompiler
// Type: wxb.ArrayAnyType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;

namespace wxb
{
  internal class ArrayAnyType : IListAnyType
  {
    public ArrayAnyType(Type arrayType)
      : base(arrayType, arrayType.GetElementType())
    {
    }

    protected override IList Create(int lenght)
    {
      return (IList) Array.CreateInstance(this.elementType, lenght);
    }
  }
}
