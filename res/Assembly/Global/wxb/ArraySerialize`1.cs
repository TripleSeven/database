﻿// Decompiled with JetBrains decompiler
// Type: wxb.ArraySerialize`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace wxb
{
  internal abstract class ArraySerialize<T> : IListSerialize<T>
  {
    protected override IList<T> Create(int lenght)
    {
      return (IList<T>) new T[lenght];
    }
  }
}
