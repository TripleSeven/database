﻿// Decompiled with JetBrains decompiler
// Type: wxb.IListSerialize`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace wxb
{
  internal abstract class IListSerialize<T> : ITypeSerialize
  {
    [MethodImpl((MethodImplOptions) 32768)]
    int ITypeSerialize.CalculateSize(object value)
    {
      // ISSUE: unable to decompile the method.
    }

    protected abstract int GetElementSize();

    protected abstract void Write(WRStream stream, T value);

    [MethodImpl((MethodImplOptions) 32768)]
    void ITypeSerialize.WriteTo(object value, MonoStream ms)
    {
      // ISSUE: unable to decompile the method.
    }

    protected abstract IList<T> Create(int lenght);

    [MethodImpl((MethodImplOptions) 32768)]
    void ITypeSerialize.MergeFrom(ref object value, MonoStream ms)
    {
      // ISSUE: unable to decompile the method.
    }

    protected abstract T Read(WRStream stream);
  }
}
