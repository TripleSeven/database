﻿// Decompiled with JetBrains decompiler
// Type: wxb.ArrayFloatType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace wxb
{
  internal class ArrayFloatType : ArraySerialize<float>
  {
    protected override int GetElementSize()
    {
      return 4;
    }

    protected override void Write(WRStream stream, float value)
    {
      stream.WriteFloat(value);
    }

    protected override float Read(WRStream stream)
    {
      return stream.ReadFloat();
    }
  }
}
