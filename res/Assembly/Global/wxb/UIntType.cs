﻿// Decompiled with JetBrains decompiler
// Type: wxb.UIntType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace wxb
{
  internal class UIntType : Serialize<uint>
  {
    protected override int CalculateSize(uint value)
    {
      return 4;
    }

    protected override void Write(WRStream stream, uint value)
    {
      stream.WriteInt32(value);
    }

    protected override uint Read(WRStream stream)
    {
      return stream.ReadUInt32();
    }
  }
}
