﻿// Decompiled with JetBrains decompiler
// Type: wxb.CharType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace wxb
{
  internal class CharType : Serialize<char>
  {
    protected override int CalculateSize(char value)
    {
      return 2;
    }

    protected override void Write(WRStream stream, char value)
    {
      stream.WriteInt16((ushort) value);
    }

    protected override char Read(WRStream stream)
    {
      return (char) stream.ReadInt16();
    }
  }
}
