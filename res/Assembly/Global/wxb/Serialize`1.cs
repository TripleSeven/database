﻿// Decompiled with JetBrains decompiler
// Type: wxb.Serialize`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace wxb
{
  internal abstract class Serialize<T> : ITypeSerialize
  {
    int ITypeSerialize.CalculateSize(object value)
    {
      return this.CalculateSize((T) value);
    }

    protected abstract int CalculateSize(T value);

    protected abstract void Write(WRStream stream, T value);

    [MethodImpl((MethodImplOptions) 32768)]
    void ITypeSerialize.WriteTo(object obj, MonoStream ms)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    void ITypeSerialize.MergeFrom(ref object value, MonoStream ms)
    {
      // ISSUE: unable to decompile the method.
    }

    protected abstract T Read(WRStream stream);
  }
}
