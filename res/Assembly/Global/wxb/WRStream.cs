﻿// Decompiled with JetBrains decompiler
// Type: wxb.WRStream
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;
using System.Text;

namespace wxb
{
  public class WRStream
  {
    internal static readonly Encoding Utf8Encoding = Encoding.UTF8;
    protected int mSize;
    protected byte[] mBuffer;
    protected int mReadPos;
    protected int mWritePos;

    [MethodImpl((MethodImplOptions) 32768)]
    public WRStream(int size)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WRStream(byte[] bytes)
    {
      // ISSUE: unable to decompile the method.
    }

    public static int ComputeLengthSize(int length)
    {
      return WRStream.ComputeRawVarint32Size((uint) length);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int ComputeRawVarint32Size(uint value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int ComputeStringSize(string value)
    {
      // ISSUE: unable to decompile the method.
    }

    public int WriteRemain
    {
      get
      {
        return this.mSize - this.mWritePos;
      }
    }

    public int ReadPos
    {
      get
      {
        return this.mReadPos;
      }
      set
      {
        this.mReadPos = value;
      }
    }

    public int WritePos
    {
      get
      {
        return this.mWritePos;
      }
      set
      {
        this.mWritePos = value;
      }
    }

    public int ReadSize
    {
      get
      {
        return this.WritePos - this.ReadPos;
      }
    }

    public int size
    {
      get
      {
        return this.mSize;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public byte[] GetBytes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ensureCapacity(int length)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckReadSize(int size)
    {
      // ISSUE: unable to decompile the method.
    }

    public void WriteLength(int length)
    {
      this.WriteRawVarint32((uint) length);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void WriteRawVarint32(uint value)
    {
      // ISSUE: unable to decompile the method.
    }

    public int ReadLength()
    {
      return (int) this.ReadRawVarint32();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal byte ReadRawByte()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private uint SlowReadRawVarint32()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal uint ReadRawVarint32()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WriteString(string value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string ReadString()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WriteSByte(sbyte value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public sbyte ReadSByte()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WriteByte(byte value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public byte ReadByte()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WriteChar(char value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public char ReadChar()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WriteInt16(short value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public short ReadInt16()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WriteInt16(ushort value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort ReadUInt16()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WriteInt32(int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ReadInt32()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WriteInt32(uint value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public uint ReadUInt32()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WriteInt64(long value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public long ReadInt64()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WriteInt64(ulong value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ulong ReadUInt64()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void Reverse(byte[] bytes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void Copy(byte[] src, int srcOffset, byte[] dst, int dstOffset, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void WriteRawBytes(byte[] value, int offset, int length)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WriteFloat(float value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal byte[] ReadRawBytes(int size)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float ReadFloat()
    {
      // ISSUE: unable to decompile the method.
    }

    public void WriteDouble(double value)
    {
      this.WriteInt64(BitConverter.DoubleToInt64Bits(value));
    }

    public double ReadDouble()
    {
      return BitConverter.Int64BitsToDouble(this.ReadInt64());
    }
  }
}
