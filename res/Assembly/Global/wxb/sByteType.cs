﻿// Decompiled with JetBrains decompiler
// Type: wxb.sByteType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace wxb
{
  internal class sByteType : Serialize<sbyte>
  {
    protected override int CalculateSize(sbyte value)
    {
      return 1;
    }

    protected override void Write(WRStream stream, sbyte value)
    {
      stream.WriteSByte(value);
    }

    protected override sbyte Read(WRStream stream)
    {
      return stream.ReadSByte();
    }
  }
}
