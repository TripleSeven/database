﻿// Decompiled with JetBrains decompiler
// Type: wxb.ArrayULongType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace wxb
{
  internal class ArrayULongType : ArraySerialize<ulong>
  {
    protected override int GetElementSize()
    {
      return 8;
    }

    protected override void Write(WRStream stream, ulong value)
    {
      stream.WriteInt64(value);
    }

    protected override ulong Read(WRStream stream)
    {
      return stream.ReadUInt64();
    }
  }
}
