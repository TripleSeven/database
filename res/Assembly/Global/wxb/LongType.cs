﻿// Decompiled with JetBrains decompiler
// Type: wxb.LongType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace wxb
{
  internal class LongType : Serialize<long>
  {
    protected override int CalculateSize(long value)
    {
      return 8;
    }

    protected override void Write(WRStream stream, long value)
    {
      stream.WriteInt64(value);
    }

    protected override long Read(WRStream stream)
    {
      return stream.ReadInt64();
    }
  }
}
