﻿// Decompiled with JetBrains decompiler
// Type: wxb.ByteType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace wxb
{
  internal class ByteType : Serialize<byte>
  {
    protected override int CalculateSize(byte value)
    {
      return 1;
    }

    protected override void Write(WRStream stream, byte value)
    {
      stream.WriteByte(value);
    }

    protected override byte Read(WRStream stream)
    {
      return stream.ReadByte();
    }
  }
}
