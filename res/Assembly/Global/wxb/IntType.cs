﻿// Decompiled with JetBrains decompiler
// Type: wxb.IntType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace wxb
{
  internal class IntType : Serialize<int>
  {
    protected override int CalculateSize(int value)
    {
      return 4;
    }

    protected override void Write(WRStream stream, int value)
    {
      stream.WriteInt32(value);
    }

    protected override int Read(WRStream stream)
    {
      return stream.ReadInt32();
    }
  }
}
