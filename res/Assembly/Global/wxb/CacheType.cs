﻿// Decompiled with JetBrains decompiler
// Type: wxb.CacheType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace wxb
{
  public class CacheType
  {
    private static readonly BindingFlags Flags = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;
    private Dictionary<string, MemberInfo> NameToMethodBase;
    private List<FieldInfo> serializes;
    private CacheType.Ctor[] Ctors;
    private ConstructorInfo default_ctor;
    private bool is_default_ctor;

    [MethodImpl((MethodImplOptions) 32768)]
    public CacheType(Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    public Type type { get; private set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConstructorInfo GetCtor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConstructorInfo GetCtor(Type param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static MethodInfo GetMethodInfo(Type type, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MethodInfo GetMethod(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PropertyInfo GetProperty(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FieldInfo GetField(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<FieldInfo> GetSerializeField()
    {
      // ISSUE: unable to decompile the method.
    }

    private struct Ctor
    {
      public Type param;
      public ConstructorInfo info;
    }
  }
}
