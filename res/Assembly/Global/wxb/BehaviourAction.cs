﻿// Decompiled with JetBrains decompiler
// Type: wxb.BehaviourAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace wxb
{
  public class BehaviourAction : MonoBehaviour
  {
    public Action<BehaviourAction, int> OnAnimatorIKAction;
    public Action<BehaviourAction> OnAnimatorMoveAction;
    public Action<BehaviourAction, bool> OnApplicationFocusAction;
    public Action<BehaviourAction, bool> OnApplicationPauseAction;
    public Action<BehaviourAction> OnApplicationQuitAction;
    public Action<BehaviourAction, float[], int> OnAudioFilterReadAction;
    public Action<BehaviourAction> OnBecameInvisibleAction;
    public Action<BehaviourAction> OnBecameVisibleAction;
    public Action<BehaviourAction> OnBeforeTransformParentChangedAction;
    public Action<BehaviourAction> OnCanvasGroupChangedAction;
    public Action<BehaviourAction, Collision> OnCollisionEnterAction;
    public Action<BehaviourAction, Collision2D> OnCollisionEnter2DAction;
    public Action<BehaviourAction, Collision> OnCollisionExitAction;
    public Action<BehaviourAction, Collision2D> OnCollisionExit2DAction;
    public Action<BehaviourAction, Collision> OnCollisionStayAction;
    public Action<BehaviourAction, Collision2D> OnCollisionStay2DAction;
    public Action<BehaviourAction> OnConnectedToServerAction;
    public Action<BehaviourAction, ControllerColliderHit> OnControllerColliderHitAction;
    public Action<BehaviourAction> OnDestroyAction;
    public Action<BehaviourAction> OnDisableAction;
    public Action<BehaviourAction> OnEnableAction;
    public Action<BehaviourAction, float> OnJointBreakAction;
    public Action<BehaviourAction, Joint2D> OnJointBreak2DAction;
    public Action<BehaviourAction> OnMouseDownAction;
    public Action<BehaviourAction> OnMouseDragAction;
    public Action<BehaviourAction> OnMouseEnterAction;
    public Action<BehaviourAction> OnMouseExitAction;
    public Action<BehaviourAction> OnMouseOverAction;
    public Action<BehaviourAction> OnMouseUpAction;
    public Action<BehaviourAction> OnMouseUpAsButtonAction;
    public Action<BehaviourAction, GameObject> OnParticleCollisionAction;
    public Action<BehaviourAction> OnParticleTriggerAction;
    public Action<BehaviourAction> OnPostRenderAction;
    public Action<BehaviourAction> OnPreCullAction;
    public Action<BehaviourAction> OnPreRenderAction;
    public Action<BehaviourAction> OnRectTransformDimensionsChangeAction;
    public Action<BehaviourAction> OnRectTransformRemovedAction;
    public Action<BehaviourAction, RenderTexture, RenderTexture> OnRenderImageAction;
    public Action<BehaviourAction> OnRenderObjectAction;
    public Action<BehaviourAction> OnTransformChildrenChangedAction;
    public Action<BehaviourAction> OnTransformParentChangedAction;
    public Action<BehaviourAction, Collider> OnTriggerEnterAction;
    public Action<BehaviourAction, Collider2D> OnTriggerEnter2DAction;
    public Action<BehaviourAction, Collider> OnTriggerExitAction;
    public Action<BehaviourAction, Collider2D> OnTriggerExit2DAction;
    public Action<BehaviourAction, Collider> OnTriggerStayAction;
    public Action<BehaviourAction, Collider2D> OnTriggerStay2DAction;
    public Action<BehaviourAction> OnValidateAction;
    public Action<BehaviourAction> OnWillRenderObjectAction;
    public Action<BehaviourAction> StartAction;
    public Action<BehaviourAction> AwakeAction;
    public Action<BehaviourAction> FixedUpdateAction;
    public Action<BehaviourAction> LateUpdateAction;
    public Action<BehaviourAction> UpdateAction;

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAnimatorIK(int layerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAnimatorMove()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnApplicationFocus(bool focus)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnApplicationPause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnApplicationQuit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAudioFilterRead(float[] data, int channels)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBecameInvisible()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBecameVisible()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBeforeTransformParentChanged()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCanvasGroupChanged()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCollisionEnter(Collision collision)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCollisionEnter2D(Collision2D collision)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCollisionExit(Collision collision)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCollisionExit2D(Collision2D collision)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCollisionStay(Collision collision)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCollisionStay2D(Collision2D collision)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConnectedToServer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDestroy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJointBreak(float breakForce)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJointBreak2D(Joint2D joint)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMouseDown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMouseDrag()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMouseEnter()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMouseExit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMouseOver()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMouseUp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMouseUpAsButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnParticleCollision(GameObject other)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnParticleTrigger()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPostRender()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPreCull()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPreRender()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRectTransformDimensionsChange()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRectTransformRemoved()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRenderObject()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTransformChildrenChanged()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTransformParentChanged()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTriggerEnter(Collider other)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTriggerEnter2D(Collider2D collision)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTriggerExit(Collider other)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTriggerExit2D(Collider2D collision)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTriggerStay(Collider other)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTriggerStay2D(Collider2D collision)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnValidate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWillRenderObject()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Start()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Awake()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FixedUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LateUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
