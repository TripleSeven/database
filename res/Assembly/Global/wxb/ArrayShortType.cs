﻿// Decompiled with JetBrains decompiler
// Type: wxb.ArrayShortType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace wxb
{
  internal class ArrayShortType : ArraySerialize<short>
  {
    protected override int GetElementSize()
    {
      return 2;
    }

    protected override void Write(WRStream stream, short value)
    {
      stream.WriteInt16(value);
    }

    protected override short Read(WRStream stream)
    {
      return stream.ReadInt16();
    }
  }
}
