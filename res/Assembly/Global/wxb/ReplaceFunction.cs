﻿// Decompiled with JetBrains decompiler
// Type: wxb.ReplaceFunction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;

namespace wxb
{
  [AttributeUsage(AttributeTargets.Method)]
  public class ReplaceFunction : Attribute
  {
    public ReplaceFunction(Type type)
    {
    }

    public ReplaceFunction()
    {
    }

    public ReplaceFunction(string fieldNameOrTypeName)
    {
    }

    public ReplaceFunction(Type type, string fieldName)
    {
    }

    public ReplaceFunction(string typeName, string fieldName)
    {
    }
  }
}
