﻿// Decompiled with JetBrains decompiler
// Type: wxb.DoubleType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace wxb
{
  internal class DoubleType : Serialize<double>
  {
    protected override int CalculateSize(double value)
    {
      return 8;
    }

    protected override void Write(WRStream stream, double value)
    {
      stream.WriteDouble(value);
    }

    protected override double Read(WRStream stream)
    {
      return stream.ReadDouble();
    }
  }
}
