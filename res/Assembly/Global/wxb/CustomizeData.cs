﻿// Decompiled with JetBrains decompiler
// Type: wxb.CustomizeData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace wxb
{
  [Serializable]
  public class CustomizeData
  {
    [SerializeField]
    [HideInInspector]
    private string typeName;
    [HideInInspector]
    [SerializeField]
    private List<UnityEngine.Object> objs;
    [HideInInspector]
    [SerializeField]
    private byte[] bytes;
    private RefType refType_;

    public string TypeName
    {
      get
      {
        return this.typeName;
      }
    }

    public RefType refType
    {
      get
      {
        return this.refType_;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnAfterDeserialize(object self)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
