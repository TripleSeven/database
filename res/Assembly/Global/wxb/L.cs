﻿// Decompiled with JetBrains decompiler
// Type: wxb.L
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace wxb
{
  public static class L
  {
    private static L.I active = (L.I) new L.UnityLog();

    public static void Set(L.I l)
    {
      L.active = l;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Log(object obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [Conditional("DEBUGLOG")]
    [MethodImpl((MethodImplOptions) 32768)]
    public static void LogFormat(string format, params object[] objs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void LogWarningFormat(string format, params object[] objs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void LogError(object obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void LogErrorFormat(string format, params object[] objs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void LogException(Exception ex)
    {
      // ISSUE: unable to decompile the method.
    }

    public interface I
    {
      void Log(object obj);

      void LogFormat(string format, params object[] objs);

      void LogWarningFormat(string format, params object[] objs);

      void LogError(object obj);

      void LogErrorFormat(string format, params object[] objs);

      void LogException(Exception ex);
    }

    private class UnityLog : L.I
    {
      void L.I.Log(object obj)
      {
        Debug.Log(obj);
      }

      void L.I.LogFormat(string format, params object[] objs)
      {
        Debug.Log(string.Format(format, objs));
      }

      void L.I.LogWarningFormat(string format, params object[] objs)
      {
        Debug.LogWarning(string.Format(format, objs));
      }

      void L.I.LogError(object obj)
      {
        Debug.LogError(obj);
      }

      void L.I.LogErrorFormat(string format, params object[] objs)
      {
        Debug.LogError(string.Format(format, objs));
      }

      void L.I.LogException(Exception ex)
      {
        Debug.LogError((object) ex);
      }
    }
  }
}
