﻿// Decompiled with JetBrains decompiler
// Type: wxb.StrType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace wxb
{
  internal class StrType : Serialize<string>
  {
    protected override int CalculateSize(string value)
    {
      return WRStream.ComputeStringSize(value);
    }

    protected override void Write(WRStream stream, string value)
    {
      stream.WriteString(value);
    }

    protected override string Read(WRStream stream)
    {
      return stream.ReadString();
    }
  }
}
