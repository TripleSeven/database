﻿// Decompiled with JetBrains decompiler
// Type: wxb.CoroutineAdapter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace wxb
{
  public class CoroutineAdapter : CrossBindingAdaptor
  {
    public override Type BaseCLRType
    {
      get
      {
        return (Type) null;
      }
    }

    public override Type[] BaseCLRTypes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override Type AdaptorType
    {
      get
      {
        return typeof (CoroutineAdapter.Adaptor);
      }
    }

    public override object CreateCLRInstance(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance)
    {
      return (object) new CoroutineAdapter.Adaptor(appdomain, instance);
    }

    internal class Adaptor : IEnumerator<object>, IEnumerator, IDisposable, CrossBindingAdaptorType
    {
      private ILTypeInstance instance;
      private ILRuntime.Runtime.Enviorment.AppDomain appdomain;
      private IMethod mCurrentMethod;
      private bool mCurrentMethodGot;
      private IMethod mDisposeMethod;
      private bool mDisposeMethodGot;
      private IMethod mMoveNextMethod;
      private bool mMoveNextMethodGot;
      private IMethod mResetMethod;
      private bool mResetMethodGot;

      public Adaptor()
      {
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public Adaptor(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance)
      {
        // ISSUE: unable to decompile the method.
      }

      public ILTypeInstance ILInstance
      {
        get
        {
          return this.instance;
        }
      }

      public object Current
      {
        [MethodImpl((MethodImplOptions) 32768)] get
        {
          // ISSUE: unable to decompile the method.
        }
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void Dispose()
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public bool MoveNext()
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void Reset()
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public override string ToString()
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
