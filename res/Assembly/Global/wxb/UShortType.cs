﻿// Decompiled with JetBrains decompiler
// Type: wxb.UShortType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace wxb
{
  internal class UShortType : Serialize<ushort>
  {
    protected override int CalculateSize(ushort value)
    {
      return 2;
    }

    protected override void Write(WRStream stream, ushort value)
    {
      stream.WriteInt16(value);
    }

    protected override ushort Read(WRStream stream)
    {
      return stream.ReadUInt16();
    }
  }
}
