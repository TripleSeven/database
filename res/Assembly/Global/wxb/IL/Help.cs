﻿// Decompiled with JetBrains decompiler
// Type: wxb.IL.Help
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace wxb.IL
{
  public static class Help
  {
    private static Dictionary<Type, wxb.CacheType> Caches;
    public static readonly object[] EmptyObj;
    public static readonly object[] OneObj;
    public static readonly Type[] OneType;
    public static readonly Type[] EmptyType;
    private static HashSet<Type> BaseTypes;
    private static Dictionary<string, Type> AllTypesByFullName;

    [MethodImpl((MethodImplOptions) 32768)]
    private static void X()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static wxb.CacheType GetOrCreate(Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static FieldInfo GetField(Type type, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static MethodInfo GetMethod(Type type, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PropertyInfo GetProperty(Type type, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void Reg<T>()
    {
      // ISSUE: unable to decompile the method.
    }

    private static ConstructorInfo GetConstructor(Type instanceType, Type param)
    {
      return Help.GetOrCreate(instanceType).GetCtor(param);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static object CreateInstaince(Type instanceType, object parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ReleaseAll()
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool IsBaseType(Type type)
    {
      return Help.BaseTypes.Contains(type);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ForEach(Action<Type> fun)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool TryGetTypeByFullName(string name, out Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Type GetTypeByFullName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    public static Type GetType(string name)
    {
      return Help.GetTypeByFullName(name);
    }

    public static List<FieldInfo> GetSerializeField(Type type)
    {
      return Help.GetOrCreate(type).GetSerializeField();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void GetSerializeField(Type type, List<FieldInfo> fieldinfos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool isListType(Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Type GetElementByList(FieldInfo fieldInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Type GetElementByList(Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<FieldInfo> GetSerializeField(object obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool isType(Type src, Type baseType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static object Create(Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static Help()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
