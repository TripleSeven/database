﻿// Decompiled with JetBrains decompiler
// Type: wxb.ResLoad
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.IO;

namespace wxb
{
  public static class ResLoad
  {
    private static IResLoad current;

    public static void Set(IResLoad load)
    {
      ResLoad.current = load;
    }

    public static Stream GetStream(string path)
    {
      return ResLoad.current.GetStream(path);
    }
  }
}
