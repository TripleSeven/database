﻿// Decompiled with JetBrains decompiler
// Type: wxb.hotMgr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ILRuntime.CLR.TypeSystem;
using ILRuntime.Mono.Cecil;
using ILRuntime.Mono.Collections.Generic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using UnityEngine;

namespace wxb
{
  public static class hotMgr
  {
    private static HashSet<FieldInfo> Fields = new HashSet<FieldInfo>();
    private static RefType refType;
    public const BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;
    private static Stream DllStream;

    [MethodImpl((MethodImplOptions) 32768)]
    public static StringBuilder AppendFormatLine(
      this StringBuilder sb,
      string format,
      params object[] objs)
    {
      // ISSUE: unable to decompile the method.
    }

    public static ILRuntime.Runtime.Enviorment.AppDomain appdomain { get; private set; }

    public static RefType RefType
    {
      get
      {
        return hotMgr.refType;
      }
    }

    public static List<IType> AllTypes { get; private set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Init(string assemblyName, IResLoad resLoad = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static MemoryStream CopyStream(Stream input)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void InitHotModule(string assemblyName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void RegDelegate(ILRuntime.Runtime.Enviorment.AppDomain appdomain)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool GetReplaceFunction(
      Collection<ILRuntime.Mono.Cecil.CustomAttribute> CustomAttributes,
      out System.Type type,
      out string fieldName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void GetPlatform(
      Collection<ILRuntime.Mono.Cecil.CustomAttribute> CustomAttributes,
      Action<RuntimePlatform> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static System.Type GetTypeByName(string fullName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static System.Type GetReplaceType(Collection<ILRuntime.Mono.Cecil.CustomAttribute> CustomAttributes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool ReplaceField(System.Type type, string fieldName, MethodInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsStatic(MethodInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool ReplaceFunc(System.Type type, string name, MethodInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static FieldDefinition FindStaticField(ILType type, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string GetKey(MethodInfo method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static List<MethodInfo> FindMethodInfo(System.Type type, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    private static RuntimePlatform GetCurrentPlatform()
    {
      return Application.platform;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void AutoReplace(List<IType> types)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void FindAttribute(System.Type attType, Action<System.Type> on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void InitByProperty(System.Type attType, string name, List<IType> types)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void AutoSetFieldMethodValue(
      System.Type srcType,
      FieldInfo srcFieldInfo,
      ILType type,
      string fieldName,
      DelegateBridge bridge,
      Dictionary<string, List<MethodInfo>> NameToSorted)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ReleaseAll()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
