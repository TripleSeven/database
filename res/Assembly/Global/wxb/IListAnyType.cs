﻿// Decompiled with JetBrains decompiler
// Type: wxb.IListAnyType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Runtime.CompilerServices;

namespace wxb
{
  internal abstract class IListAnyType : ITypeSerialize
  {
    protected Type arrayType;
    protected Type elementType;
    protected ITypeSerialize elementTypeSerialize;

    [MethodImpl((MethodImplOptions) 32768)]
    public IListAnyType(Type arrayType, Type elementType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    int ITypeSerialize.CalculateSize(object value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    void ITypeSerialize.WriteTo(object value, MonoStream ms)
    {
      // ISSUE: unable to decompile the method.
    }

    protected abstract IList Create(int lenght);

    [MethodImpl((MethodImplOptions) 32768)]
    void ITypeSerialize.MergeFrom(ref object value, MonoStream ms)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
