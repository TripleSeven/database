﻿// Decompiled with JetBrains decompiler
// Type: wxb.Hotfix
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace wxb
{
  public class Hotfix
  {
    public FieldInfo field;
    public MethodInfo method;
    public DelegateBridge bridge;

    [MethodImpl((MethodImplOptions) 32768)]
    public Hotfix(FieldInfo field, MethodInfo method, DelegateBridge bridge)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Hotfix(Type type, string name, Type hotfixType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Hotfix(
      Type type,
      string fieldName,
      string funName,
      Type hotfixType,
      string hotfixFunName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Run(Action action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object Run(Func<object> fun)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object Invoke(object obj, object[] parameters)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object Invoke(object obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object Invoke(object obj, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object Invoke(object obj, object p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object Invoke(object obj, object p1, object p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object Invoke(object obj, object p1, object p2, object p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object Invoke(object obj, object p1, object p2, object p3, object p4, object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object Invoke(
      object obj,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object Invoke(
      object obj,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6,
      object p7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object Invoke(
      object obj,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6,
      object p7,
      object p8)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object Invoke(
      object obj,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6,
      object p7,
      object p8,
      object p9)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object Invoke(
      object obj,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6,
      object p7,
      object p8,
      object p9,
      object p10)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Release()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
