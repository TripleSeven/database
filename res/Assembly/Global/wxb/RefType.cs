﻿// Decompiled with JetBrains decompiler
// Type: wxb.RefType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace wxb
{
  public class RefType
  {
    private object instance;
    private string fullType;
    private Type type;

    [MethodImpl((MethodImplOptions) 32768)]
    public RefType(string fullType, object param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RefType(string fullType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RefType(Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RefType(object instance)
    {
      // ISSUE: unable to decompile the method.
    }

    public object Instance
    {
      get
      {
        return this.instance;
      }
    }

    public Type Type
    {
      get
      {
        return this.type;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetField(string name, object value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object GetField(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object TryGetField(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TrySetField(string name, object value)
    {
      // ISSUE: unable to decompile the method.
    }

    public void InvokeMethod(string name, object[] param)
    {
      this.InvokeMethodReturn(name, param);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object InvokeMethodReturn(string name, object[] param)
    {
      // ISSUE: unable to decompile the method.
    }

    public void TryInvokeMethod(string name, object[] param)
    {
      this.TryInvokeMethodReturn(name, param);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object TryInvokeMethodReturn(string name, object[] param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object GetProperty(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetProperty(string name, object value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object TryGetProperty(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TrySetProperty(string name, object value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InvokeMethod(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InvokeMethod(string name, object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InvokeMethod(string name, object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InvokeMethod(string name, object p0, object p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InvokeMethod(string name, object p0, object p1, object p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InvokeMethod(string name, object p0, object p1, object p2, object p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InvokeMethod(
      string name,
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InvokeMethod(
      string name,
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InvokeMethod(
      string name,
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6,
      object p7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InvokeMethod(
      string name,
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6,
      object p7,
      object p8)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InvokeMethod(
      string name,
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6,
      object p7,
      object p8,
      object p9)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object InvokeMethodReturn(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object InvokeMethodReturn(string name, object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object InvokeMethodReturn(string name, object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object InvokeMethodReturn(string name, object p0, object p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object InvokeMethodReturn(string name, object p0, object p1, object p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object InvokeMethodReturn(
      string name,
      object p0,
      object p1,
      object p2,
      object p3,
      object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object InvokeMethodReturn(
      string name,
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object InvokeMethodReturn(
      string name,
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object InvokeMethodReturn(
      string name,
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6,
      object p7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object InvokeMethodReturn(
      string name,
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6,
      object p7,
      object p8)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object InvokeMethodReturn(
      string name,
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6,
      object p7,
      object p8,
      object p9)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TryInvokeMethod(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TryInvokeMethod(string name, object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TryInvokeMethod(string name, object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TryInvokeMethod(string name, object p0, object p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TryInvokeMethod(string name, object p0, object p1, object p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TryInvokeMethod(
      string name,
      object p0,
      object p1,
      object p2,
      object p3,
      object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TryInvokeMethod(
      string name,
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TryInvokeMethod(
      string name,
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TryInvokeMethod(
      string name,
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6,
      object p7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TryInvokeMethod(
      string name,
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6,
      object p7,
      object p8)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TryInvokeMethod(
      string name,
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6,
      object p7,
      object p8,
      object p9)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object TryInvokeMethodReturn(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object TryInvokeMethodReturn(string name, object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object TryInvokeMethodReturn(string name, object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object TryInvokeMethodReturn(string name, object p0, object p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object TryInvokeMethodReturn(string name, object p0, object p1, object p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object TryInvokeMethodReturn(
      string name,
      object p0,
      object p1,
      object p2,
      object p3,
      object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object TryInvokeMethodReturn(
      string name,
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object TryInvokeMethodReturn(
      string name,
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object TryInvokeMethodReturn(
      string name,
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6,
      object p7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object TryInvokeMethodReturn(
      string name,
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6,
      object p7,
      object p8)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object TryInvokeMethodReturn(
      string name,
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6,
      object p7,
      object p8,
      object p9)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
