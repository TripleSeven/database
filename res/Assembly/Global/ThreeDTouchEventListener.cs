﻿// Decompiled with JetBrains decompiler
// Type: ThreeDTouchEventListener
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;

public class ThreeDTouchEventListener : MonoBehaviour, IUpdateSelectedHandler, IEventSystemHandler
{
  public float ThreeDTouchThreshhold;
  public bool IsThreeDTouchTriggered;

  [MethodImpl((MethodImplOptions) 32768)]
  public ThreeDTouchEventListener()
  {
    // ISSUE: unable to decompile the method.
  }

  public void OnDisable()
  {
    this.IsThreeDTouchTriggered = false;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void OnUpdateSelected(BaseEventData data)
  {
    // ISSUE: unable to decompile the method.
  }

  public event Action EventOnThreeDTouchTriggered
  {
    [MethodImpl((MethodImplOptions) 32768)] add
    {
      // ISSUE: unable to decompile the method.
    }
    [MethodImpl((MethodImplOptions) 32768)] remove
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
