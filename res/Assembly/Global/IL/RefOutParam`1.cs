﻿// Decompiled with JetBrains decompiler
// Type: IL.RefOutParam`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace IL
{
  public class RefOutParam<T>
  {
    public T value;

    public RefOutParam(T value)
    {
      this.value = value;
    }
  }
}
