﻿// Decompiled with JetBrains decompiler
// Type: IL.Objects
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace IL
{
  public struct Objects : IDisposable
  {
    public object[] objs;

    [MethodImpl((MethodImplOptions) 32768)]
    public Objects(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Objects(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Objects(object p0, object p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Objects(object p0, object p1, object p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Objects(object p0, object p1, object p2, object p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Objects(object p0, object p1, object p2, object p3, object p4, object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Objects(object p0, object p1, object p2, object p3, object p4, object p5, object p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Objects(
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6,
      object p7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Objects(
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6,
      object p7,
      object p8)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Objects(
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6,
      object p7,
      object p8,
      object p9)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Objects(
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6,
      object p7,
      object p8,
      object p9,
      object p10)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Objects(
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6,
      object p7,
      object p8,
      object p9,
      object p10,
      object p11)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Objects(
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6,
      object p7,
      object p8,
      object p9,
      object p10,
      object p11,
      object p12)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Objects(
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6,
      object p7,
      object p8,
      object p9,
      object p10,
      object p11,
      object p12,
      object p13)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Objects(
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6,
      object p7,
      object p8,
      object p9,
      object p10,
      object p11,
      object p12,
      object p13,
      object p14,
      object p15,
      object p16,
      object p17,
      object p18)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Objects(
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      object p6,
      object p7,
      object p8,
      object p9,
      object p10,
      object p11,
      object p12,
      object p13,
      object p14,
      object p15,
      object p16,
      object p17,
      object p18,
      object p19,
      object p20,
      object p21)
    {
      // ISSUE: unable to decompile the method.
    }

    void IDisposable.Dispose()
    {
      ObjectArrayPools.Delete(this.objs);
    }
  }
}
