﻿// Decompiled with JetBrains decompiler
// Type: IL.EmptyObjs
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.InteropServices;

namespace IL
{
  [StructLayout(LayoutKind.Sequential, Size = 1)]
  public struct EmptyObjs : IDisposable
  {
    private static object[] Emptys = new object[0];

    public object[] objs
    {
      get
      {
        return EmptyObjs.Emptys;
      }
    }

    void IDisposable.Dispose()
    {
    }
  }
}
