﻿// Decompiled with JetBrains decompiler
// Type: IL.ObjectArrayPools
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace IL
{
  public static class ObjectArrayPools
  {
    private static ObjectArrayPools.IPool current = (ObjectArrayPools.IPool) new ObjectArrayPools.QueuePool();

    public static void SetDefault()
    {
      ObjectArrayPools.current = (ObjectArrayPools.IPool) new ObjectArrayPools.DefaultPool();
    }

    public static void SetCurrent(ObjectArrayPools.IPool pool)
    {
      ObjectArrayPools.current = pool;
    }

    public static object[] New(int length)
    {
      return ObjectArrayPools.current.New(length);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Delete(object[] objs)
    {
      // ISSUE: unable to decompile the method.
    }

    public interface IPool
    {
      object[] New(int length);

      void Delete(object[] objs);
    }

    private class DefaultPool : ObjectArrayPools.IPool
    {
      object[] ObjectArrayPools.IPool.New(int length)
      {
        return new object[length];
      }

      void ObjectArrayPools.IPool.Delete(object[] objs)
      {
      }
    }

    private class QueuePool : ObjectArrayPools.IPool
    {
      private object[] Pools = new object[20];

      object[] ObjectArrayPools.IPool.New(int length)
      {
        int index = length - 1;
        object pool = this.Pools[index];
        if (pool == null)
          return new object[length];
        if (pool is object[])
        {
          object[] objArray = (object[]) pool;
          this.Pools[index] = (object) null;
          return objArray;
        }
        Queue<object[]> objArrayQueue = pool as Queue<object[]>;
        if (objArrayQueue.Count != 0)
          return objArrayQueue.Dequeue();
        return new object[length];
      }

      void ObjectArrayPools.IPool.Delete(object[] objs)
      {
        int index = objs.Length - 1;
        object pool = this.Pools[index];
        if (pool == null)
        {
          this.Pools[index] = (object) objs;
        }
        else
        {
          Queue<object[]> objArrayQueue;
          if (pool is object[])
          {
            objArrayQueue = new Queue<object[]>();
            objArrayQueue.Enqueue((object[]) pool);
            this.Pools[index] = (object) objArrayQueue;
          }
          else
            objArrayQueue = pool as Queue<object[]>;
          objArrayQueue.Enqueue(objs);
        }
      }
    }

    private class ListPool : ObjectArrayPools.IPool
    {
      private List<object[]> objs = new List<object[]>();

      object[] ObjectArrayPools.IPool.New(int length)
      {
        int count = this.objs.Count;
        for (int index = 0; index < this.objs.Count; ++index)
        {
          object[] objArray = this.objs[index];
          if (objArray != null && objArray.Length == length)
          {
            this.objs[index] = (object[]) null;
            return objArray;
          }
        }
        return new object[length];
      }

      void ObjectArrayPools.IPool.Delete(object[] v)
      {
        int count = this.objs.Count;
        for (int index = 0; index < count; ++index)
        {
          if (this.objs[index] == null)
          {
            this.objs[index] = v;
            return;
          }
        }
        this.objs.Add(v);
      }
    }
  }
}
