﻿// Decompiled with JetBrains decompiler
// Type: IL.DelegateBridge
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime;
using BlackJack.BJFramework.Runtime.PlayerContext;
using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.AR;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.LibClient;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Protocol;
using BlackJack.ProjectL.Scene;
using BlackJack.ProjectL.UI;
using BlackJack.ProjectLBasic;
using FixMath.NET;
using PD.SDK;
using ProtoBuf;
using SimpleJson;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.XR.iOS;

namespace IL
{
  public class DelegateBridge
  {
    private MethodInfo methodInfo;
    private List<object> paramList;
    private object result;

    public DelegateBridge(MethodInfo methodInfo)
    {
      this.methodInfo = methodInfo;
    }

    public void InvokeSessionStart()
    {
      this.paramList = new List<object>();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Invoke(int nRet)
    {
      // ISSUE: unable to decompile the method.
    }

    public void InvokeSessionEnd()
    {
      this.paramList = (List<object>) null;
      this.result = (object) null;
    }

    public TResult InvokeSessionEndWithResult<TResult>()
    {
      return (TResult) this.result;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InParam<T>(T p)
    {
      // ISSUE: unable to decompile the method.
    }

    public void InParams<T>(T[] ps)
    {
      this.paramList.Add((object) ps);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OutParam<TResult>(int pos, out TResult ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GDBStarfieldsSimpleInfo> __Gen_Delegate_Imp1(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GDBLinkInfo> __Gen_Delegate_Imp2(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IExtension __Gen_Delegate_Imp3(object p0, bool p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp4(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp5(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp6(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp7(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp8(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float __Gen_Delegate_Imp9(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp10(object p0, float p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GDBStargroupInfo> __Gen_Delegate_Imp11(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp12(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp13(object p0, bool p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GDBSolarSystemSimpleInfo> __Gen_Delegate_Imp14(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GDBStarInfo __Gen_Delegate_Imp15(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GDBPlanetInfo> __Gen_Delegate_Imp16(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GDBAsteroidBeltInfo __Gen_Delegate_Imp17(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GDBSpaceStationInfo> __Gen_Delegate_Imp18(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GDBStargateInfo> __Gen_Delegate_Imp19(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GDBSceneDummyInfo> __Gen_Delegate_Imp20(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PVector3D __Gen_Delegate_Imp21(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public uint __Gen_Delegate_Imp22(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp23(object p0, uint p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public double __Gen_Delegate_Imp24(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp25(object p0, double p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ulong __Gen_Delegate_Imp26(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp27(object p0, ulong p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GDBMoonInfo> __Gen_Delegate_Imp28(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GDBAsteroidGatherPointPositionInfo> __Gen_Delegate_Imp29(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<SceneObject3DInfo> __Gen_Delegate_Imp30(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<SceneDummyObjectInfo> __Gen_Delegate_Imp31(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollisionInfo __Gen_Delegate_Imp32(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<SphereCollisionInfo> __Gen_Delegate_Imp33(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CapsuleCollisionInfo> __Gen_Delegate_Imp34(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBattleHeroEquipment __Gen_Delegate_Imp35(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHeroEquipment __Gen_Delegate_Imp36(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEquipmentInfo __Gen_Delegate_Imp37(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHeroJob __Gen_Delegate_Imp38(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHeroJob __Gen_Delegate_Imp39(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCharImageSkinResourceInfo __Gen_Delegate_Imp40(
      object p0,
      object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataModelSkinResourceInfo __Gen_Delegate_Imp41(
      object p0,
      object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHero __Gen_Delegate_Imp42(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBattleHero __Gen_Delegate_Imp43(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BPStageHero __Gen_Delegate_Imp44(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBPStageHero __Gen_Delegate_Imp45(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroInfo __Gen_Delegate_Imp46(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSoldierInfo __Gen_Delegate_Imp47(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroJob __Gen_Delegate_Imp48(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBattleHeroJob __Gen_Delegate_Imp49(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataJobConnectionInfo __Gen_Delegate_Imp50(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp51(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp52(object p0, int p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp53(object p0, int p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp54(object p0, int p1, int p2, object p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp55(object p0, int p1, int p2, int p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp56(object p0, int p1, int p2, SetupBattleHeroFlag p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHeroSetupInfo __Gen_Delegate_Imp57(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp58(object p0, int p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp59(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> __Gen_Delegate_Imp60(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleHeroSetupInfo> __Gen_Delegate_Imp61(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp62(object p0, SetupBattleHeroFlag p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBattleHeroSetupInfo __Gen_Delegate_Imp63(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHeroSetupInfo __Gen_Delegate_Imp64(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleCommand __Gen_Delegate_Imp65(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBattleCommand __Gen_Delegate_Imp66(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBattlePosition __Gen_Delegate_Imp67(GridPosition p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition __Gen_Delegate_Imp68(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PropertyModifyType __Gen_Delegate_Imp69(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp70(object p0, PropertyModifyType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProCommonBattleProperty __Gen_Delegate_Imp71(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CommonBattleProperty __Gen_Delegate_Imp72(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProCommonBattleReport __Gen_Delegate_Imp73(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleReportHead __Gen_Delegate_Imp74(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime __Gen_Delegate_Imp75(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp76(object p0, DateTime p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RealTimePVPMode __Gen_Delegate_Imp77(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp78(object p0, RealTimePVPMode p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBattleReportPlayerSummaryInfo __Gen_Delegate_Imp79(
      object p0,
      int p1,
      int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBattleReportPlayerSummaryInfo __Gen_Delegate_Imp80(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArenaBattleReportPlayerSummaryInfo __Gen_Delegate_Imp81(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBattleReportPlayerSummaryInfo __Gen_Delegate_Imp82(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArenaBattleReportBattleInfo __Gen_Delegate_Imp83(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBattleReportBattleInfo __Gen_Delegate_Imp84(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArenaBattleReportBattleInitInfo __Gen_Delegate_Imp85(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBattleReportBattleInitInfo __Gen_Delegate_Imp86(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp87(
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      ConfigDataSkillInfo[] p5,
      BattleActorMasterJob[] p6,
      BattleActorEquipment[] p7,
      ConfigDataSkillInfo[] p8,
      ConfigDataSkillInfo[] p9,
      int p10,
      int p11,
      int p12,
      int p13,
      GridPosition p14,
      int p15,
      bool p16,
      int p17,
      int p18,
      int p19,
      BattleActorSourceType p20,
      int p21)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp88(object p0, object p1, object p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp89(object p0, object p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp90(object p0, bool p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp91(object p0, object p1, bool p2, int p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp92(object p0, GridPosition p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTerrainInfo __Gen_Delegate_Imp93(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTerrainEffectInfo __Gen_Delegate_Imp94(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp95(object p0, int p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleCommand __Gen_Delegate_Imp96(object p0, BattleCommandType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp97(
      object p0,
      GridPosition p1,
      GridPosition p2,
      int p3,
      int p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp98(
      object p0,
      GridPosition p1,
      GridPosition p2,
      int p3,
      FindPathIgnoreTeamType p4,
      int p5,
      object p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp99(object p0, GridPosition p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp100(object p0, BattleCommandType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp101(object p0, BattleCommandType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp102(object p0, GridPosition p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp103(object p0, GridPosition p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp104(object p0, object p1, object p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp105(object p0, int p1, GridPosition p2, GridPosition p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp106(
      object p0,
      object p1,
      GridPosition p2,
      GridPosition p3,
      bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp107(object p0, int p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp108(object p0, int p1, object p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo __Gen_Delegate_Imp109(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp110(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MoveType __Gen_Delegate_Imp111(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MoveType __Gen_Delegate_Imp112(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MoveType __Gen_Delegate_Imp113(MoveType p0, MoveType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp114(MoveType p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public short __Gen_Delegate_Imp115(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor __Gen_Delegate_Imp116(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActorSourceType __Gen_Delegate_Imp117(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePlayer __Gen_Delegate_Imp118(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleBase __Gen_Delegate_Imp119(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleTeam __Gen_Delegate_Imp120(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleProperty __Gen_Delegate_Imp121(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActorSummonData __Gen_Delegate_Imp122(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArmyInfo __Gen_Delegate_Imp123(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataJobInfo __Gen_Delegate_Imp124(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCharImageSkinResourceInfo __Gen_Delegate_Imp125(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataModelSkinResourceInfo __Gen_Delegate_Imp126(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCharImageInfo __Gen_Delegate_Imp127(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp128(object p0, int p1, int p2, ref GridPosition p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition __Gen_Delegate_Imp129(
      object p0,
      int p1,
      int p2,
      GridPosition p3,
      bool p4,
      bool p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp130(object p0, object p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor __Gen_Delegate_Imp131(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomNumber __Gen_Delegate_Imp132(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleTeam __Gen_Delegate_Imp133(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BehaviorGroup __Gen_Delegate_Imp134(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp135(object p0, BattleActor.BehaviorState p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp136(object p0, BehaviorCondition p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition __Gen_Delegate_Imp137(
      object p0,
      GridPosition p1,
      int p2,
      int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleActor> __Gen_Delegate_Imp138(
      object p0,
      object p1,
      GridPosition p2,
      int p3,
      int p4,
      bool p5,
      bool p6,
      int p7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition __Gen_Delegate_Imp139(object p0, object p1, GridPosition p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor __Gen_Delegate_Imp140(object p0, object p1, GridPosition p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BehaviorTarget __Gen_Delegate_Imp141(
      object p0,
      SelectTarget p1,
      object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleActor> __Gen_Delegate_Imp142(
      object p0,
      object p1,
      int[] p2,
      int[] p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp143(int[] p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp144(object p0, GridPosition p1, GridPosition p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition __Gen_Delegate_Imp145(object p0, GridPosition p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SpecialMoveCostType __Gen_Delegate_Imp146(object p0, out int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition __Gen_Delegate_Imp147(
      object p0,
      GridPosition p1,
      out BattleActor p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp148(object p0, object p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp149(object p0, ArmyTag p1, ArmyTag p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Fix64 __Gen_Delegate_Imp150(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleActor> __Gen_Delegate_Imp151(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleActor> __Gen_Delegate_Imp152(object p0, object p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition __Gen_Delegate_Imp153(
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor __Gen_Delegate_Imp154(
      object p0,
      object p1,
      object p2,
      object p3,
      object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BehaviorTarget __Gen_Delegate_Imp155(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GridPosition> __Gen_Delegate_Imp156(
      object p0,
      GridPosition p1,
      int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleActor> __Gen_Delegate_Imp157(
      object p0,
      object p1,
      GridPosition p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleActor> __Gen_Delegate_Imp158(
      object p0,
      object p1,
      GridPosition p2,
      int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor __Gen_Delegate_Imp159(object p0, object p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleActor> __Gen_Delegate_Imp160(object p0, object p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp161(int p0, int[] p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp162(object p0, int[] p1, int[] p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition __Gen_Delegate_Imp163(
      object p0,
      GridPosition p1,
      AttackDirection p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp164(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition __Gen_Delegate_Imp165(object p0, object p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp166(object p0, int p1, int p2, GridPosition p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor __Gen_Delegate_Imp167(object p0, object p1, object p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BuffState> __Gen_Delegate_Imp168(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp169(object p0, object p1, BuffSourceType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp170(
      object p0,
      object p1,
      object p2,
      BuffSourceType p3,
      object p4,
      object p5,
      bool p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp171(
      object p0,
      object p1,
      object p2,
      BuffSourceType p3,
      object p4,
      object p5,
      bool p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BuffState __Gen_Delegate_Imp172(object p0, int p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp173(object p0, object p1, ref uint p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp174(object p0, object p1, ref uint p2, bool p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp175(
      object p0,
      object p1,
      PropertyModifyType p2,
      int p3,
      bool p4,
      bool p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp176(
      object p0,
      object p1,
      ref uint p2,
      object p3,
      bool p4,
      int p5,
      object p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp177(
      object p0,
      object p1,
      object p2,
      bool p3,
      int p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp178(
      object p0,
      object p1,
      object p2,
      int p3,
      int p4,
      DamageNumberType p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp179(object p0, FightTag p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp180(object p0, BuffType p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp181(object p0, BuffType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp182(object p0, int p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp183(
      object p0,
      object p1,
      bool p2,
      int p3,
      int p4,
      int p5,
      int p6,
      object p7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp184(
      object p0,
      object p1,
      bool p2,
      bool p3,
      int p4,
      bool p5,
      object p6,
      object p7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp185(object p0, object p1, int p2, out int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleActor.HealChangeDamageResult> __Gen_Delegate_Imp186(
      object p0,
      int p1,
      int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp187(object p0, object p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp188(
      object p0,
      int p1,
      bool p2,
      ref ArmyRelationData p3,
      bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp189(object p0, int p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp190(object p0, bool p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp191(object p0, BuffSourceType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp192(object p0, bool p1, bool p2, bool p3, GridPosition p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp193(object p0, bool p1, bool p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp194(object p0, bool p1, bool p2, bool p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp195(object p0, bool p1, bool p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp196(
      object p0,
      object p1,
      bool p2,
      bool p3,
      int p4,
      bool p5,
      int p6,
      int p7,
      int p8,
      int p9)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp197(
      object p0,
      object p1,
      bool p2,
      bool p3,
      int p4,
      bool p5,
      int p6,
      int p7,
      int p8)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp198(
      object p0,
      object p1,
      bool p2,
      bool p3,
      int p4,
      bool p5,
      object p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp199(
      object p0,
      object p1,
      bool p2,
      bool p3,
      int p4,
      bool p5,
      bool p6,
      object p7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp200(int p0, int p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp201(object p0, object p1, object p2, ref int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp202(
      object p0,
      object p1,
      int p2,
      int p3,
      object p4,
      out int p5,
      out int p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp203(object p0, bool p1, out int p2, out int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp204(object p0, object p1, bool p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp205(
      object p0,
      object p1,
      bool p2,
      bool p3,
      int p4,
      object p5,
      bool p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp206(object p0, object p1, bool p2, bool p3, int p4, bool p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp207(object p0, object p1, bool p2, bool p3, bool p4, bool p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp208(object p0, object p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp209(object p0, object p1, AttackDirection p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp210(
      object p0,
      object p1,
      object p2,
      object p3,
      bool p4,
      bool p5,
      int p6,
      bool p7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BuffState __Gen_Delegate_Imp211(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp212(object p0, bool p1, bool p2, int p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo __Gen_Delegate_Imp213(object p0, bool p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp214(object p0, object p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleSkillState __Gen_Delegate_Imp215(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleSkillState> __Gen_Delegate_Imp216(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp217(object p0, int p1, GridPosition p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp218(
      object p0,
      object p1,
      GridPosition p2,
      GridPosition p3,
      object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp219(object p0, object p1, GridPosition p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp220(
      object p0,
      object p1,
      GridPosition p2,
      GridPosition p3,
      bool p4,
      bool p5,
      bool p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp221(
      object p0,
      object p1,
      object p2,
      int p3,
      int p4,
      DamageNumberType p5,
      bool p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp222(object p0, object p1, GridPosition p2, object p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp223(object p0, object p1, GridPosition p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp224(
      object p0,
      object p1,
      object p2,
      object p3,
      GridPosition p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp225(object p0, object p1, object p2, GridPosition p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo __Gen_Delegate_Imp226(
      object p0,
      MoveType p1,
      int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp227(
      KeyValuePair<BattleActor, int[]> p0,
      KeyValuePair<BattleActor, int[]> p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp228(KeyValuePair<BattleActor, int[]> p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BuffSourceType __Gen_Delegate_Imp229(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp230(
      object p0,
      int p1,
      object p2,
      object p3,
      BuffSourceType p4,
      object p5,
      object p6,
      bool p7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor __Gen_Delegate_Imp231(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleTeam __Gen_Delegate_Imp232(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePlayer __Gen_Delegate_Imp233(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp234(object p0, int p1, object p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp235(object p0, object p1, object p2, int p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp236(object p0, object p1, int p2, object p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> __Gen_Delegate_Imp237(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GridPosition> __Gen_Delegate_Imp238(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataBuffInfo> __Gen_Delegate_Imp239(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BuffState> __Gen_Delegate_Imp240(object p0, object p1, BuffType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePropertyModifier __Gen_Delegate_Imp241(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp242(object p0, object p1, int p2, object p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArmyRelationData __Gen_Delegate_Imp243(
      object p0,
      object p1,
      bool p2,
      object p3,
      bool p4,
      bool p5,
      bool p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp244(
      object p0,
      GridPosition p1,
      GridPosition p2,
      int p3,
      MoveType p4,
      FindPathIgnoreTeamType p5,
      int p6,
      int p7,
      SpecialMoveCostType p8,
      int p9,
      object p10)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp245(
      object p0,
      GridPosition p1,
      int p2,
      MoveType p3,
      FindPathIgnoreTeamType p4,
      int p5,
      int p6,
      SpecialMoveCostType p7,
      int p8,
      object p9)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp246(object p0, GridPosition p1, int p2, int p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp247(
      object p0,
      GridPosition p1,
      AttackDirection p2,
      int p3,
      int p4,
      int p5,
      object p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AttackDirection __Gen_Delegate_Imp248(GridPosition p0, GridPosition p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp249(object p0, GridPosition p1, int p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp250(GridPosition p0, GridPosition p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleActor> __Gen_Delegate_Imp251(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, List<BattleActorLogInfo>> __Gen_Delegate_Imp252(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IConfigDataLoader __Gen_Delegate_Imp253(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IBattleListener __Gen_Delegate_Imp254(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleInfo __Gen_Delegate_Imp255(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArenaBattleInfo __Gen_Delegate_Imp256(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPVPBattleInfo __Gen_Delegate_Imp257(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRealTimePVPBattleInfo __Gen_Delegate_Imp258(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaBattleInfo __Gen_Delegate_Imp259(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattlefieldInfo __Gen_Delegate_Imp260(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleType __Gen_Delegate_Imp261(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ActionOrderType __Gen_Delegate_Imp262(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePlayer[] __Gen_Delegate_Imp263(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPlayerLevelInfo __Gen_Delegate_Imp264(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleState __Gen_Delegate_Imp265(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleMap __Gen_Delegate_Imp266(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Combat __Gen_Delegate_Imp267(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp268(object p0, object p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp269(object p0, object p1, object p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp270(object p0, object p1, NpcCondition p2, object p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp271(object p0, int p1, NpcCondition p2, object p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp272(object p0, int p1, NpcCondition p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleAchievementState> __Gen_Delegate_Imp273(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp274(object p0, BattleCommandType p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LinkedList<BattleCommand> __Gen_Delegate_Imp275(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp276(object p0, int p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp277(object p0, object p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleCommand> __Gen_Delegate_Imp278(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleEventTriggerState __Gen_Delegate_Imp279(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<RandomArmyActor> __Gen_Delegate_Imp280(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp281(
      object p0,
      GridPosition p1,
      int p2,
      int p3,
      bool p4,
      int p5,
      int p6,
      int p7,
      int p8,
      int p9,
      object p10,
      BattleActorSourceType p11)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp282(object p0, object p1, int p2, int p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition __Gen_Delegate_Imp283(
      object p0,
      GridPosition p1,
      int p2,
      MoveType p3,
      bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition __Gen_Delegate_Imp284(
      object p0,
      GridPosition p1,
      GridPosition p2,
      int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition __Gen_Delegate_Imp285(
      object p0,
      GridPosition p1,
      MoveType p2,
      bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleEventTriggerState> __Gen_Delegate_Imp286(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, int> __Gen_Delegate_Imp287(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp288(
      object p0,
      object p1,
      BattleType p2,
      object p3,
      object p4,
      BattlePlayer[] p5,
      int p6,
      int p7,
      int p8)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp289(
      object p0,
      object p1,
      object p2,
      object p3,
      BattlePlayer[] p4,
      int p5,
      object p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp290(
      object p0,
      object p1,
      object p2,
      object p3,
      BattlePlayer[] p4,
      int p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp291(object p0, BattlePlayer[] p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp292(object p0, int p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp293(
      object p0,
      object p1,
      BattleType p2,
      object p3,
      object p4,
      BattlePlayer[] p5,
      int p6,
      int p7,
      int p8,
      object p9)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp294(
      object p0,
      object p1,
      object p2,
      object p3,
      BattlePlayer[] p4,
      int p5,
      object p6,
      object p7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp295(object p0, int p1, int p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp296()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Fix64 __Gen_Delegate_Imp297(
      object p0,
      object p1,
      object p2,
      int p3,
      bool p4,
      int p5,
      int p6,
      int p7,
      int p8,
      bool p9)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Fix64 __Gen_Delegate_Imp298(Fix64 p0, int p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Fix64 __Gen_Delegate_Imp299(object p0, object p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Fix64 __Gen_Delegate_Imp300(object p0, object p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Fix64 __Gen_Delegate_Imp301(object p0, object p1, object p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp302(
      object p0,
      object p1,
      ArmyRelationData p2,
      object p3,
      int p4,
      bool p5,
      bool p6,
      bool p7,
      bool p8,
      object p9,
      int p10,
      bool p11,
      object p12)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp303(int p0, int p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp304(int p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp305(
      int p0,
      int p1,
      int p2,
      int p3,
      int p4,
      int p5,
      int p6,
      int p7,
      int p8,
      int p9)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp306(
      int p0,
      int p1,
      int p2,
      int p3,
      int p4,
      int p5,
      int p6,
      int p7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp307(int p0, int p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp308(
      object p0,
      int p1,
      int p2,
      int p3,
      int p4,
      int p5,
      int p6,
      int p7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp309(object p0, int p1, int p2, int p3, int p4, int p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DamageNumberType __Gen_Delegate_Imp310(
      object p0,
      bool p1,
      ArmyRelationData p2,
      bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor __Gen_Delegate_Imp311(object p0, GridPosition p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTerrainInfo __Gen_Delegate_Imp312(
      object p0,
      GridPosition p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTerrainEffectInfo __Gen_Delegate_Imp313(
      object p0,
      GridPosition p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp314(object p0, GridPosition p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp315(object p0, GridPosition p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp316(object p0, GridPosition p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp317(object p0, GridPosition p1, MoveType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp318(object p0, MoveType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp319(object p0, SpecialMoveCostType p1, GridPosition p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleMapCell __Gen_Delegate_Imp320(object p0, GridPosition p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataSkillInfo> __Gen_Delegate_Imp321(
      object p0,
      object p1,
      object p2,
      out int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataSkillInfo> __Gen_Delegate_Imp322(
      object p0,
      object p1,
      object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp323(
      object p0,
      object p1,
      object p2,
      object p3,
      int p4,
      int p5,
      int p6,
      object p7,
      bool p8)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp324(int p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp325(
      object p0,
      object p1,
      object p2,
      object p3,
      int p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp326(object p0, PropertyModifyType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp327(object p0, PropertyModifyType p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor __Gen_Delegate_Imp328(object p0, int p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp329(object p0, object p1, NpcCondition p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp330(object p0, object p1, NpcCondition p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BehaviorGroup __Gen_Delegate_Imp331(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGroupBehavior __Gen_Delegate_Imp332(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp333(object p0, BehaviorCondition p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp334(object p0, bool p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp335(object p0, object p1, GridPosition p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp336(object p0, object p1, GridPosition p2, int p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp337(object p0, object p1, object p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp338(object p0, object p1, object p2, int p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp339(object p0, object p1, object p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp340(object p0, object p1, object p2, GridPosition p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp341(object p0, object p1, int p2, int p3, DamageNumberType p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp342(object p0, object p1, int p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp343(
      object p0,
      int p1,
      int p2,
      bool p3,
      int p4,
      int p5,
      bool p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp344(
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      int p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatTeam __Gen_Delegate_Imp345(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort __Gen_Delegate_Imp346(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatState __Gen_Delegate_Imp347(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp348(object p0, Vector2i p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp349(object p0, Vector2i p1, Fix64 p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp350(object p0, int p1, Fix64 p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp351(
      object p0,
      object p1,
      float p2,
      float p3,
      float p4,
      object p5,
      object p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp352(object p0, GraphicEffect p1, float p2, float p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp353(object p0, GraphicEffect p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IBattleGraphic __Gen_Delegate_Imp354(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2i __Gen_Delegate_Imp355(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatActorState __Gen_Delegate_Imp356(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatTeam __Gen_Delegate_Imp357(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp358(object p0, ushort p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp359(object p0, ushort p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatSkillState __Gen_Delegate_Imp360(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatActor __Gen_Delegate_Imp361(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatActor __Gen_Delegate_Imp362(object p0, Vector2i p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatActor __Gen_Delegate_Imp363(object p0, int p1, ushort p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatActor __Gen_Delegate_Imp364(object p0, ushort p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp365(Vector2i p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp366(object p0, CombatActorState p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp367(object p0, int p1, Vector2i p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp368(object p0, object p1, object p2, object p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp369(Fix64 p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatActor __Gen_Delegate_Imp370(object p0, Fix64 p1, Fix64 p2, ushort p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp371(object p0, object p1, Fix64 p2, Fix64 p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp372(object p0, Fix64 p1, out Vector2i p2, out Fix64 p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp373(object p0, object p1, float p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Colori __Gen_Delegate_Imp374(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatActor __Gen_Delegate_Imp375(object p0, bool p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatFlyObject __Gen_Delegate_Imp376(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp377(
      object p0,
      object p1,
      object p2,
      object p3,
      int p4,
      bool p5,
      ArmyRelationData p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp378(
      object p0,
      object p1,
      object p2,
      object p3,
      int p4,
      int p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Position2i __Gen_Delegate_Imp379(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CombatActor> __Gen_Delegate_Imp380(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp381(
      object p0,
      CombatActorState p1,
      CombatActorState p2,
      CombatActorState p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp382(
      object p0,
      object p1,
      object p2,
      GridPosition p3,
      object p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp383(object p0, object p1, int p2, int p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp384(
      object p0,
      object p1,
      object p2,
      object p3,
      int p4,
      int p5,
      DamageNumberType p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp385(object p0, object p1, object p2, object p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp386(object p0, object p1, GridPosition p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IBattleGraphic __Gen_Delegate_Imp387(object p0, object p1, float p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp388(object p0, Vector2i p1, Vector2i p2, Colori p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp389(
      object p0,
      Vector2i p1,
      Fix64 p2,
      Vector2i p3,
      Fix64 p4,
      Colori p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp390(object p0, object p1, GridPosition p2, GridPosition p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp391(object p0, object p1, int p2, int p3, GridPosition p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Pathfinder.Node __Gen_Delegate_Imp392(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PathNode __Gen_Delegate_Imp393(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Pathfinder.SearchState __Gen_Delegate_Imp394(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FindPathIgnoreTeamType __Gen_Delegate_Imp395(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp396(
      object p0,
      object p1,
      GridPosition p2,
      GridPosition p3,
      int p4,
      MoveType p5,
      FindPathIgnoreTeamType p6,
      int p7,
      int p8,
      SpecialMoveCostType p9,
      int p10,
      object p11)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp397(
      object p0,
      object p1,
      GridPosition p2,
      int p3,
      MoveType p4,
      FindPathIgnoreTeamType p5,
      int p6,
      int p7,
      SpecialMoveCostType p8,
      int p9,
      object p10)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp398(object p0, RandomNumberState p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomNumberState __Gen_Delegate_Imp399(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> __Gen_Delegate_Imp400(object p0, int p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> __Gen_Delegate_Imp401(object p0, object p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RealTimePVPInfo __Gen_Delegate_Imp402(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerBattleStatus __Gen_Delegate_Imp403(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp404(object p0, PlayerBattleStatus p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public long __Gen_Delegate_Imp405(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp406(object p0, long p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHero __Gen_Delegate_Imp407(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBattleRoomPlayer __Gen_Delegate_Imp408(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomPlayer __Gen_Delegate_Imp409(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActorSetup __Gen_Delegate_Imp410(
      object p0,
      object p1,
      int p2,
      int p3,
      int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataSkillInfo> __Gen_Delegate_Imp411(
      object p0,
      object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataSkillInfo> __Gen_Delegate_Imp412(
      object p0,
      int p1,
      int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActorSetup __Gen_Delegate_Imp413(
      object p0,
      object p1,
      int p2,
      object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActorSetup __Gen_Delegate_Imp414(
      object p0,
      int p1,
      object p2,
      int p3,
      object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp415(object p0, object p1, int p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp416(object p0, object p1, int p2, int p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp417(
      object p0,
      object p1,
      object p2,
      out List<RandomArmyActor> p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataSkillInfo> __Gen_Delegate_Imp418(
      object p0,
      int p1,
      object p2,
      object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePlayer __Gen_Delegate_Imp419(object p0, int p1, object p2, ulong p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCharImageSkinResourceInfo __Gen_Delegate_Imp420(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataModelSkinResourceInfo __Gen_Delegate_Imp421(
      object p0,
      int p1,
      int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp422(object p0, int p1, int p2, int p3, object p4, object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp423(object p0, BattleType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp424(object p0, BattleType p1, int p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBPStageCommand __Gen_Delegate_Imp425(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BPStageCommand __Gen_Delegate_Imp426(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBPStage __Gen_Delegate_Imp427(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BPStage __Gen_Delegate_Imp428(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp429(object p0, int p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BPStageCommand __Gen_Delegate_Imp430(
      object p0,
      int p1,
      BPStageCommandType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BPStageHeroSetupInfo> __Gen_Delegate_Imp431(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BPStageHeroSetupInfo> __Gen_Delegate_Imp432(
      object p0,
      object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BPStageHeroSetupInfo __Gen_Delegate_Imp433(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IBPStageRule __Gen_Delegate_Imp434(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakAreanaBPFlowInfo __Gen_Delegate_Imp435(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArenaBattleReportNameRecord __Gen_Delegate_Imp436(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProPeakArenaBattleReportNameRecord> __Gen_Delegate_Imp437(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBattleReportNameRecord __Gen_Delegate_Imp438(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<PeakArenaBattleReportNameRecord> __Gen_Delegate_Imp439(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaStatus __Gen_Delegate_Imp440(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp441(object p0, PeakArenaStatus p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BusinessCardPeakArenaData __Gen_Delegate_Imp442(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBusinessCardPeakArenaData __Gen_Delegate_Imp443(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BusinessCardInfoSet __Gen_Delegate_Imp444(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BusinessCardStatisticalData __Gen_Delegate_Imp445(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBusinessCard __Gen_Delegate_Imp446(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BusinessCard __Gen_Delegate_Imp447(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBusinessCardStatisticalData __Gen_Delegate_Imp448(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBusinessCardInfoSet __Gen_Delegate_Imp449(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDirectionType __Gen_Delegate_Imp450(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp451(object p0, HeroDirectionType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroActionType __Gen_Delegate_Imp452(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp453(object p0, HeroActionType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBusinessCardHeroSet __Gen_Delegate_Imp454(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProBusinessCardHeroSet> __Gen_Delegate_Imp455(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BusinessCardHeroSet __Gen_Delegate_Imp456(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BusinessCardHeroSet> __Gen_Delegate_Imp457(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GuildMemberCacheObject> __Gen_Delegate_Imp458(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp459(object p0, DateTime p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMemberCacheObject __Gen_Delegate_Imp460(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMemberCacheObject __Gen_Delegate_Imp461(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<string> __Gen_Delegate_Imp462(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProGuild __Gen_Delegate_Imp463(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Guild __Gen_Delegate_Imp464(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildTitle __Gen_Delegate_Imp465(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp466(object p0, GuildTitle p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProGuildMember __Gen_Delegate_Imp467(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMember __Gen_Delegate_Imp468(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProGuildLog __Gen_Delegate_Imp469(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProGuildLog> __Gen_Delegate_Imp470(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GuildLog> __Gen_Delegate_Imp471(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildLog __Gen_Delegate_Imp472(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProGuildSearchInfo __Gen_Delegate_Imp473(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildSearchInfo __Gen_Delegate_Imp474(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMassiveCombatStronghold __Gen_Delegate_Imp475(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMassiveCombatInfo __Gen_Delegate_Imp476(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProGuildMassiveCombatStrongholdInfo __Gen_Delegate_Imp477(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMassiveCombatStronghold __Gen_Delegate_Imp478(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProGuildMassiveCombatMemberInfo __Gen_Delegate_Imp479(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMassiveCombatMemberInfo __Gen_Delegate_Imp480(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProGuildMassiveCombatInfo __Gen_Delegate_Imp481(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProGuildMassiveCombatGeneralInfo __Gen_Delegate_Imp482(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMassiveCombatGeneral __Gen_Delegate_Imp483(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProGuildPlayerMassiveCombatInfo __Gen_Delegate_Imp484(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildPlayerMassiveCombatInfo __Gen_Delegate_Imp485(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Hero __Gen_Delegate_Imp486(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroJob __Gen_Delegate_Imp487(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProHero __Gen_Delegate_Imp488(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataModelSkinResourceInfo __Gen_Delegate_Imp489(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroJobRefineryProperty __Gen_Delegate_Imp490(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProHeroJob __Gen_Delegate_Imp491(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroJobSlotRefineryProperty __Gen_Delegate_Imp492(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp493(object p0, PropertyModifyType p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProHeroJobRefineryProperty __Gen_Delegate_Imp494(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroJobRefineryProperty __Gen_Delegate_Imp495(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroJobSlotRefineryProperty __Gen_Delegate_Imp496(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProHeroJobSlotRefineryProperty __Gen_Delegate_Imp497(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProRandomEvent __Gen_Delegate_Imp498(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProRandomEvent> __Gen_Delegate_Imp499(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomEvent __Gen_Delegate_Imp500(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<RandomEvent> __Gen_Delegate_Imp501(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp502(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp503(object p0, object p1, object p2, object p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp504(CommonBattleProperty[] p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp505(object p0, BattleType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleActorSetup> __Gen_Delegate_Imp506(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattlePlayer> __Gen_Delegate_Imp507(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProSoldierSkin __Gen_Delegate_Imp508(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SoldierSkin __Gen_Delegate_Imp509(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomPlayer __Gen_Delegate_Imp510(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<TeamRoomPlayer> __Gen_Delegate_Imp511(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomSetting __Gen_Delegate_Imp512(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp513(object p0, ulong p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomPlayer __Gen_Delegate_Imp514(object p0, ulong p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomPlayer __Gen_Delegate_Imp515(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProTeamRoom __Gen_Delegate_Imp516(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoom __Gen_Delegate_Imp517(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GameFunctionType __Gen_Delegate_Imp518(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp519(object p0, GameFunctionType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomAuthority __Gen_Delegate_Imp520(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp521(object p0, TeamRoomAuthority p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProTeamRoomSetting __Gen_Delegate_Imp522(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomInviteInfo __Gen_Delegate_Imp523(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProTeamRoomInviteInfo __Gen_Delegate_Imp524(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProTeamRoomPlayer __Gen_Delegate_Imp525(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingRoom __Gen_Delegate_Imp526(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingTech __Gen_Delegate_Imp527(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<TrainingTech> __Gen_Delegate_Imp528(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable<TrainingTech> __Gen_Delegate_Imp529(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingGround __Gen_Delegate_Imp530(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTrainingRoomInfo __Gen_Delegate_Imp531(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingRoom __Gen_Delegate_Imp532(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTrainingCourseInfo __Gen_Delegate_Imp533(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<TrainingTechInfo> __Gen_Delegate_Imp534(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTrainingTechInfo __Gen_Delegate_Imp535(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingTech __Gen_Delegate_Imp536(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProTrainingTech __Gen_Delegate_Imp537(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable<TrainingTechInfo> __Gen_Delegate_Imp538(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UserSummary __Gen_Delegate_Imp539(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProUserSummary __Gen_Delegate_Imp540(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime __Gen_Delegate_Imp541(BuyRuleType p0, int p1, DateTime p2, long p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IComponentOwner __Gen_Delegate_Imp542(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataAncientCallPeriodInfo __Gen_Delegate_Imp543(
      DateTime p0,
      object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallPeriod __Gen_Delegate_Imp544(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp545(
      object p0,
      int p1,
      int p2,
      object p3,
      DateTime p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallBoss __Gen_Delegate_Imp546(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCall __Gen_Delegate_Imp547(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp548(object p0, int p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp549(object p0, object p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp550(
      object p0,
      object p1,
      object p2,
      object p3,
      int p4,
      bool p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HashSet<int> __Gen_Delegate_Imp551(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaOpponentDefensiveBattleInfo __Gen_Delegate_Imp552(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp553(object p0, int p1, ushort p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public byte __Gen_Delegate_Imp554(byte p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp555(object p0, ulong p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp556(object p0, int p1, DateTime p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp557(DateTime p0, DateTime p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime __Gen_Delegate_Imp558(DateTime p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp559(object p0, GameFunctionStatus p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaPlayerInfo __Gen_Delegate_Imp560(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ArenaBattleReport> __Gen_Delegate_Imp561(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionArenaBattleReport __Gen_Delegate_Imp562(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp563(object p0, object p1, out List<BagItemBase> p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp564(
      object p0,
      object p1,
      object p2,
      object p3,
      GameFunctionType p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp565(object p0, int p1, object p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Goods> __Gen_Delegate_Imp566(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Goods> __Gen_Delegate_Imp567(object p0, int p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp568(object p0, GoodsType p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp569(
      object p0,
      GoodsType p1,
      int p2,
      int p3,
      ulong p4,
      GameFunctionType p5,
      object p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp570(
      object p0,
      object p1,
      int p2,
      GameFunctionType p3,
      object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp571(
      object p0,
      GoodsType p1,
      int p2,
      int p3,
      GameFunctionType p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp572(object p0, ulong p1, GameFunctionType p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase __Gen_Delegate_Imp573(
      object p0,
      object p1,
      int p2,
      GameFunctionType p3,
      object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase __Gen_Delegate_Imp574(object p0, GoodsType p1, int p2, ulong p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase __Gen_Delegate_Imp575(object p0, ulong p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase __Gen_Delegate_Imp576(object p0, GoodsType p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UseableBagItem __Gen_Delegate_Imp577(object p0, GoodsType p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BagItemBase> __Gen_Delegate_Imp578(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable<BagItemBase> __Gen_Delegate_Imp579(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BagItemBase> __Gen_Delegate_Imp580(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp581(object p0, GoodsType p1, int p2, int p3, object[] p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp582(object p0, object p1, int p2, object[] p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp583(object p0, int p1, GoodsType p2, int p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp584(object p0, object p1, GoodsType p2, int p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp585(object p0, GoodsType p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp586(object p0, GameFunctionType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp587(
      object p0,
      GameFunctionType p1,
      GoodsType p2,
      int p3,
      int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp588(object p0, object p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp589(object p0, ulong p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp590(object p0, GoodsType p1, int p2, int p3, ulong p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp591(object p0, PropertyModifyType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp592(object p0, ulong p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp593(object p0, object p1, EquipmentType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp594(object p0, ulong p1, ulong p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp595(
      object p0,
      object p1,
      object p2,
      int p3,
      int p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Goods> __Gen_Delegate_Imp596(object p0, int p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp597(
      object p0,
      GoodsType p1,
      int p2,
      int p3,
      GameFunctionType p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemFactory __Gen_Delegate_Imp598(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp599(object p0, GameFunctionType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp600(object p0, GameFunctionType p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp601(object p0, GameFunctionType p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GameFunctionType __Gen_Delegate_Imp602(BattleType p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleType __Gen_Delegate_Imp603(GameFunctionType p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp604(object p0, GameFunctionStatus p1, bool p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProcessingBattle __Gen_Delegate_Imp605(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp606(object p0, BattleType p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp607(object p0, BattleType p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp608(object p0, BattleRoomType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp609(object p0, PlayerBattleStatus p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp610(object p0, BattleType p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp611(object p0, int p1, int p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGamePlayTeamTypeInfo __Gen_Delegate_Imp612(
      object p0,
      BattleType p1,
      int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> __Gen_Delegate_Imp613(object p0, BattleType p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleHeroEquipment> __Gen_Delegate_Imp614(
      object p0,
      ulong[] p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHero __Gen_Delegate_Imp615(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp616(object p0, object p1, ulong[] p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp617(object p0, int p1, object p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp618(object p0, PropertyModifyType p1, int p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClimbTower __Gen_Delegate_Imp619(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GlobalClimbTowerInfo __Gen_Delegate_Imp620(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GlobalClimbTowerFloor __Gen_Delegate_Imp621(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GlobalClimbTowerInfo __Gen_Delegate_Imp622(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp623(
      object p0,
      object p1,
      object p2,
      int p3,
      int p4,
      int p5,
      int p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp624(object p0, object p1, ulong p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable<ConfigDataCollectionActivityExchangeTableInfo> __Gen_Delegate_Imp625(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityInfo __Gen_Delegate_Imp626(
      object p0,
      ulong p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ulong __Gen_Delegate_Imp627(object p0, GameFunctionType p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityInfo __Gen_Delegate_Imp628(
      object p0,
      GameFunctionType p1,
      int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public long __Gen_Delegate_Imp629(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivity __Gen_Delegate_Imp630(
      object p0,
      GameFunctionType p1,
      int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp631(object p0, GameFunctionType p1, int p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp632(object p0, object p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp633(object p0, object p1, int p2, int p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp634(object p0, object p1, object p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionEvent> __Gen_Delegate_Imp635(object p0, ulong p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp636(
      object p0,
      object p1,
      GameFunctionType p2,
      int p3,
      object p4,
      object p5,
      bool p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp637(object p0, ulong p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp638(object p0, ulong p1, int p2, object p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp639(object p0, ulong p1, int p2, int p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionEvent> __Gen_Delegate_Imp640(
      object p0,
      object p1,
      int p2,
      int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionEvent> __Gen_Delegate_Imp641(
      object p0,
      object p1,
      int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityWaypointStateType __Gen_Delegate_Imp642(
      object p0,
      object p1,
      int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataCollectionActivityScenarioLevelInfo> __Gen_Delegate_Imp643(
      object p0,
      object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp644(
      object p0,
      object p1,
      int p2,
      object p3,
      GameFunctionType p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivity __Gen_Delegate_Imp645(object p0, ulong p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp646(object p0, int p1, ulong p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerHeroCommentEntry __Gen_Delegate_Imp647(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp648(object p0, int p1, ref int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp649(object p0, int p1, int p2, ref int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp650(object p0, ref int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp651(object p0, int p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp652(object p0, int p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FixedStore __Gen_Delegate_Imp653(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp654(object p0, int p1, object p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<string> __Gen_Delegate_Imp655(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ulong> __Gen_Delegate_Imp656(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp657(object p0, EventGiftUnlockConditionType p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime __Gen_Delegate_Imp658(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp659(object p0, int p1, DateTime p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GiftStoreItem> __Gen_Delegate_Imp660(
      object p0,
      bool p1,
      GiftType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp661(object p0, DateTime p1, DateTime p2, DateTime p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<OrderReward> __Gen_Delegate_Imp662(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OrderReward __Gen_Delegate_Imp663(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp664(object p0, RankingListType p1, out int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp665(object p0, RankingListType p1, int p2, out int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp666(object p0, RankingListType p1, int p2, DateTime p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime __Gen_Delegate_Imp667(object p0, RankingListType p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Hero> __Gen_Delegate_Imp668(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionGuild __Gen_Delegate_Imp669(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp670(object p0, object p1, int p2, int p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp671(object p0, object p1, int p2, int p3, int p4, bool p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp672(object p0, int p1, int p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TimeSpan __Gen_Delegate_Imp673(object p0, int p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<HeroFragmentBagItem> __Gen_Delegate_Imp674(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp675(int p0, int p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroJob __Gen_Delegate_Imp676(object p0, object p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroJob __Gen_Delegate_Imp677(object p0, int p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Hero __Gen_Delegate_Imp678(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Hero __Gen_Delegate_Imp679(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LinkedList<Hero> __Gen_Delegate_Imp680(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp681(object p0, int p1, int p2, int p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp682(object p0, object p1, int p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Hero __Gen_Delegate_Imp683(object p0, ulong p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp684(object p0, object p1, int p2, ulong p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ulong[] __Gen_Delegate_Imp685(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, List<HeroComponentCommon.EquipmentBattlePowerInfo>> __Gen_Delegate_Imp686(
      object p0,
      object p1,
      int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp687(object p0, int p1, int p2, int p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp688(
      object p0,
      int p1,
      int p2,
      int p3,
      int p4,
      int p5,
      object p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp689(object p0, object p1, DateTime p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp690(object p0, int p1, int p2, int p3, int p4, object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp691(object p0, int p1, int p2, int p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> __Gen_Delegate_Imp692(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp693(int p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp694(object p0, int p1, int p2, int p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDungeonLevel __Gen_Delegate_Imp695(object p0, int p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Goods> __Gen_Delegate_Imp696(object p0, object p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroPhantom __Gen_Delegate_Imp697(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroPhantomLevel __Gen_Delegate_Imp698(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp699(object p0, int p1, RandomEventStatus p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomEvent __Gen_Delegate_Imp700(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp701(object p0, object p1, bool p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WayPointStatus __Gen_Delegate_Imp702(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mail> __Gen_Delegate_Imp703(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Mail __Gen_Delegate_Imp704(object p0, ulong p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Mail __Gen_Delegate_Imp705(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp706(object p0, ulong p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TimeSpan __Gen_Delegate_Imp707(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> __Gen_Delegate_Imp708(object p0, MissionType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp709(object p0, MissionPeriodType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DirectlyActivatedMissionSatatus __Gen_Delegate_Imp710(
      object p0,
      object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Mission __Gen_Delegate_Imp711(object p0, MissionPeriodType p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataMissionInfo> __Gen_Delegate_Imp712(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> __Gen_Delegate_Imp713(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp714(object p0, object p1, long p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp715(object p0, StatisticalDataType p1, long p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp716(object p0, StatisticalDataType p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public long __Gen_Delegate_Imp717(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp718(object p0, RealTimePVPMode p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataNoviceRewardInfo> __Gen_Delegate_Imp719(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, List<int>> __Gen_Delegate_Imp720(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TimeSpan __Gen_Delegate_Imp721(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<OperationalActivityBase> __Gen_Delegate_Imp722(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase __Gen_Delegate_Imp723(object p0, ulong p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<OperationalActivityBase> __Gen_Delegate_Imp724(
      object p0,
      OperationalActivityType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase __Gen_Delegate_Imp725(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp726(object p0, OperationalActivityType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<AdvertisementFlowLayout> __Gen_Delegate_Imp727(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp728(object p0, int p1, object p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp729(object p0, int p1, int p2, object p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp730(object p0, object p1, DateTime p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp731(DateTime p0, object p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBetInfo __Gen_Delegate_Imp732(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaSeasonInfo __Gen_Delegate_Imp733(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp734(object p0, RealTimePVPMode p1, bool p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp735(object p0, RealTimePVPMode p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleReportHead> __Gen_Delegate_Imp736(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaMatchStats __Gen_Delegate_Imp737(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBattleTeam __Gen_Delegate_Imp738(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp739(object p0, ulong p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp740(object p0, ulong p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaDanInfo __Gen_Delegate_Imp741(
      int p0,
      object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaDanInfo __Gen_Delegate_Imp742(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp743(int p0, bool p1, object p2, out int p3, out int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp744(object p0, int p1, out int p2, out int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> __Gen_Delegate_Imp745(object p0, int p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp746(DateTime p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime __Gen_Delegate_Imp747(DateTime p0, object p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaSeasonInfo __Gen_Delegate_Imp748(
      DateTime p0,
      object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp749(DateTime p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp750(object p0, int p1, GameFunctionType p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public long __Gen_Delegate_Imp751(object p0, int p1, DateTime p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp752(object p0, long p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp753(object p0, long p1, bool p2, GameFunctionType p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBuyEnergyInfo __Gen_Delegate_Imp754(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBuyArenaTicketInfo __Gen_Delegate_Imp755(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp756(object p0, int p1, bool p2, GameFunctionType p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingPlayerInfo __Gen_Delegate_Imp757(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RafflePool __Gen_Delegate_Imp758(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RaffleItem __Gen_Delegate_Imp759(object p0, object p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LinkedList<RealTimePVPBattleReport> __Gen_Delegate_Imp760(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RealTimePVPMatchStats __Gen_Delegate_Imp761(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp762(
      object p0,
      RealTimePVPMode p1,
      int p2,
      out int p3,
      out int p4,
      out int p5,
      out int p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp763(
      object p0,
      RealTimePVPMode p1,
      int p2,
      int p3,
      out int p4,
      out int p5,
      out int p6,
      out int p7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataRefluxRewardInfo> __Gen_Delegate_Imp764(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<long, List<long>> __Gen_Delegate_Imp765(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<MonthCard> __Gen_Delegate_Imp766(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp767(object p0, int p1, DateTime p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MonthCard __Gen_Delegate_Imp768(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp769(object p0, int p1, GameFunctionType p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp770(object p0, int p1, GameFunctionType p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp771(object p0, GoodsType p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp772(object p0, int p1, int p2, int p3, int p4, bool p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CardPool __Gen_Delegate_Imp773(object p0, int p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp774(object p0, object p1, bool p2, out bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp775(object p0, CardPoolType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SurveyStatus __Gen_Delegate_Imp776(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp777(object p0, SurveyStatus p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SurveyStatus __Gen_Delegate_Imp778(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp779(object p0, int p1, GameFunctionType p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp780(object p0, GameFunctionType p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp781(object p0, out GameFunctionType p1, out int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp782(object p0, ulong p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp783(object p0, ulong p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<TeamRoomInviteInfo> __Gen_Delegate_Imp784(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingTechResourceRequirements __Gen_Delegate_Imp785(
      object p0,
      int p1,
      int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp786(object p0, int p1, int p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp787(object p0, object p1, object p2, DateTime p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp788(object p0, object p1, object p2, object p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp789(object p0, object p1, object p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp790(object p0, object p1, int p2, DateTime p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp791(
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      int p5,
      bool p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp792(
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      int p6,
      bool p7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp793(object p0, object p1, GameFunctionType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UnchartedScore __Gen_Delegate_Imp794(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object __Gen_Delegate_Imp795(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp796(object p0, int p1, int p2, object p3, DateTime p4, int p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Announcement> __Gen_Delegate_Imp797(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Announcement __Gen_Delegate_Imp798(object p0, ulong p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp799(object p0, byte p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaOpponent __Gen_Delegate_Imp800(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaBattleReport __Gen_Delegate_Imp801(object p0, ulong p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public byte __Gen_Delegate_Imp802(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaBattleReportUpdateCache __Gen_Delegate_Imp803(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<object> __Gen_Delegate_Imp804(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase __Gen_Delegate_Imp805(object p0, object p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UpdateCache<BagItemBase> __Gen_Delegate_Imp806(
      object p0,
      object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<ulong, int> __Gen_Delegate_Imp807(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemUpdateCache __Gen_Delegate_Imp808(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp809(object p0, ushort p1, ushort p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaBattleStatus __Gen_Delegate_Imp810(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp811(object p0, ArenaBattleStatus p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionActivityPlayerExchangeInfo> __Gen_Delegate_Imp812(
      object p0,
      object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionActivity> __Gen_Delegate_Imp813(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp814(object p0, int p1, ulong p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<PlayerHeroCommentEntry> __Gen_Delegate_Imp815(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CooperateBattle __Gen_Delegate_Imp816(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CooperateBattleLevel __Gen_Delegate_Imp817(
      object p0,
      int p1,
      int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CooperateBattleLevel __Gen_Delegate_Imp818(
      object p0,
      int p1,
      bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FixedStoreItem __Gen_Delegate_Imp819(object p0, object p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, FixedStore> __Gen_Delegate_Imp820(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GiftStoreItem __Gen_Delegate_Imp821(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp822(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GiftStoreItem> __Gen_Delegate_Imp823(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, string> __Gen_Delegate_Imp824(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, DateTime> __Gen_Delegate_Imp825(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GiftStoreOperationalGoods __Gen_Delegate_Imp826(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GiftStoreOperationalGoods> __Gen_Delegate_Imp827(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<string, OrderReward> __Gen_Delegate_Imp828(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> __Gen_Delegate_Imp829(object p0, DateTime p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public EventGift __Gen_Delegate_Imp830(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, EventGift> __Gen_Delegate_Imp831(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Hero __Gen_Delegate_Imp832(object p0, int p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UpdateCache<Hero> __Gen_Delegate_Imp833(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp834(object p0, int p1, DateTime p2, int p3, DateTime p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp835(object p0, int p1, DateTime p2, int p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp836(object p0, DateTime p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroUpdateCache __Gen_Delegate_Imp837(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp838(object p0, object p1, DateTime p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp839(object p0, int p1, int p2, DateTime p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp840(object p0, DateTime p1, int p2, DateTime p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<HeroAnthemHasAttackLevelInfo> __Gen_Delegate_Imp841(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDungeonChapter __Gen_Delegate_Imp842(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp843(object p0, object p1, int p2, int p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, HeroDungeonChapter> __Gen_Delegate_Imp844(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp845(object p0, int p1, WayPointStatus p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, WayPointStatus> __Gen_Delegate_Imp846(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataScenarioInfo __Gen_Delegate_Imp847(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataWaypointInfo __Gen_Delegate_Imp848(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Mail __Gen_Delegate_Imp849(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Mail __Gen_Delegate_Imp850(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WebInfoControl __Gen_Delegate_Imp851(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, RafflePool> __Gen_Delegate_Imp852(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, RandomEventGroup> __Gen_Delegate_Imp853(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, RandomEventGroup> __Gen_Delegate_Imp854(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomEventLevelZone __Gen_Delegate_Imp855(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, RandomEventLevelZone> __Gen_Delegate_Imp856(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomStore __Gen_Delegate_Imp857(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomStoreItem __Gen_Delegate_Imp858(object p0, object p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<RandomStore> __Gen_Delegate_Imp859(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerOutOfBagItem __Gen_Delegate_Imp860(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftChapter __Gen_Delegate_Imp861(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftLevel __Gen_Delegate_Imp862(object p0, int p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftLevel __Gen_Delegate_Imp863(object p0, object p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, RiftChapter> __Gen_Delegate_Imp864(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CardPool __Gen_Delegate_Imp865(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CardPool __Gen_Delegate_Imp866(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp867(object p0, SelectCardGuaranteedStatus p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, CardPool> __Gen_Delegate_Imp868(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SelectCardGuaranteedStatus __Gen_Delegate_Imp869(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp870(object p0, int p1, long p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, long> __Gen_Delegate_Imp871(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Survey __Gen_Delegate_Imp872(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp873(object p0, ulong p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp874(object p0, DateTime p1, int p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, UnchartedScore> __Gen_Delegate_Imp875(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProAncientCall __Gen_Delegate_Imp876(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp877(object p0, int p1, int p2, object p3, DateTime p4, int p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProAncientCallPeriod __Gen_Delegate_Imp878(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProAncientCallBoss __Gen_Delegate_Imp879(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallBoss __Gen_Delegate_Imp880(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp881(object p0, int p1, object p2, DateTime p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallBossHistory __Gen_Delegate_Imp882(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProAncientCallBossHistory __Gen_Delegate_Imp883(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Announcement __Gen_Delegate_Imp884(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProAnnouncement __Gen_Delegate_Imp885(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaPlayerDefensiveTeam __Gen_Delegate_Imp886(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ArenaOpponent> __Gen_Delegate_Imp887(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaOpponent __Gen_Delegate_Imp888(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProArenaPlayerInfo __Gen_Delegate_Imp889(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProArenaPlayerDefensiveTeam __Gen_Delegate_Imp890(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaPlayerDefensiveHero __Gen_Delegate_Imp891(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProArenaPlayerDefensiveHero __Gen_Delegate_Imp892(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaBattleReportStatus __Gen_Delegate_Imp893(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp894(object p0, ArenaBattleReportStatus p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProArenaDefensiveBattleInfo __Gen_Delegate_Imp895(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProArenaOpponent __Gen_Delegate_Imp896(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProArenaTopRankPlayer __Gen_Delegate_Imp897(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaTopRankPlayer __Gen_Delegate_Imp898(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaBattleReport __Gen_Delegate_Imp899(object p0, bool p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProArenaBattleReport __Gen_Delegate_Imp900(object p0, bool p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase __Gen_Delegate_Imp901(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp902(GoodsType p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp903(object p0, GoodsType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GoodsType __Gen_Delegate_Imp904(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp905(object p0, GoodsType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProGoods __Gen_Delegate_Imp906(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Goods __Gen_Delegate_Imp907(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase __Gen_Delegate_Imp908(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProGoods> __Gen_Delegate_Imp909(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Goods> __Gen_Delegate_Imp910(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataItemInfo __Gen_Delegate_Imp911(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataJobMaterialInfo __Gen_Delegate_Imp912(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEnchantStoneInfo __Gen_Delegate_Imp913(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRefineryStoneInfo __Gen_Delegate_Imp914(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp915(object p0, GoodsType p1, int p2, int p3, ulong p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Goods __Gen_Delegate_Imp916(GoodsType p0, int p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ulong __Gen_Delegate_Imp917(GoodsType p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase __Gen_Delegate_Imp918(object p0, GoodsType p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase __Gen_Delegate_Imp919(
      object p0,
      GoodsType p1,
      int p2,
      int p3,
      ulong p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp920(object p0, object p1, object[] p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBattleProcessing __Gen_Delegate_Imp921(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCardPoolInfo __Gen_Delegate_Imp922(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CardPool __Gen_Delegate_Imp923(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CardPool> __Gen_Delegate_Imp924(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProCardPool __Gen_Delegate_Imp925(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProCardPool> __Gen_Delegate_Imp926(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatMessage __Gen_Delegate_Imp927(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatChannel __Gen_Delegate_Imp928(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp929(object p0, ChatChannel p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatSrcType __Gen_Delegate_Imp930(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp931(object p0, ChatSrcType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatContentType __Gen_Delegate_Imp932(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp933(object p0, ChatContentType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatMessageNtf __Gen_Delegate_Imp934(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public byte[] __Gen_Delegate_Imp935(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp936(object p0, byte[] p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ChatBagItemMessage.ChatBagItemData> __Gen_Delegate_Imp937(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProClimbTower __Gen_Delegate_Imp938(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProClimbTowerFloor __Gen_Delegate_Imp939(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GlobalClimbTowerFloor __Gen_Delegate_Imp940(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionEvent> __Gen_Delegate_Imp941(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionActivityPlayerExchangeInfo> __Gen_Delegate_Imp942(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProCollectionActivity __Gen_Delegate_Imp943(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivity __Gen_Delegate_Imp944(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProCollectionEvent __Gen_Delegate_Imp945(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProCollectionEvent> __Gen_Delegate_Imp946(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionEvent __Gen_Delegate_Imp947(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<HeroCommentEntry> __Gen_Delegate_Imp948(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProHeroComment __Gen_Delegate_Imp949(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroComment __Gen_Delegate_Imp950(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProHeroCommentEntry __Gen_Delegate_Imp951(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroCommentEntry __Gen_Delegate_Imp952(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPlayerHeroCommentEntry __Gen_Delegate_Imp953(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerHeroCommentEntry __Gen_Delegate_Imp954(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCooperateBattleLevelInfo __Gen_Delegate_Imp955(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<DayOfWeek> __Gen_Delegate_Imp956(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCooperateBattleInfo __Gen_Delegate_Imp957(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LevelDanmaku __Gen_Delegate_Imp958(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProLevelDanmaku __Gen_Delegate_Imp959(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TurnDanmaku __Gen_Delegate_Imp960(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProTurnDanmaku __Gen_Delegate_Imp961(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DanmakuEntry __Gen_Delegate_Imp962(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProDanmakuEntry __Gen_Delegate_Imp963(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PostDanmakuEntry __Gen_Delegate_Imp964(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPostDanmakuEntry __Gen_Delegate_Imp965(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGiftStoreItemInfo __Gen_Delegate_Imp966(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProGiftStoreItem __Gen_Delegate_Imp967(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GiftStoreItem __Gen_Delegate_Imp968(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProGiftStoreOperationalGoods __Gen_Delegate_Imp969(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GiftStoreOperationalGoods __Gen_Delegate_Imp970(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProOrderReward __Gen_Delegate_Imp971(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OrderReward __Gen_Delegate_Imp972(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProEventGift __Gen_Delegate_Imp973(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public EventGift __Gen_Delegate_Imp974(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroAnthemHasAttackLevelInfo __Gen_Delegate_Imp975(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProHeroAnthemSuccessLevel __Gen_Delegate_Imp976(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CompleteValueDropID> __Gen_Delegate_Imp977(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroAssistantTaskInfo __Gen_Delegate_Imp978(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProHeroDungeonChapter __Gen_Delegate_Imp979(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProHeroDungeonLevel __Gen_Delegate_Imp980(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleLevelAchievement[] __Gen_Delegate_Imp981(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroPhantomLevelInfo __Gen_Delegate_Imp982(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataHeroPhantomLevelInfo> __Gen_Delegate_Imp983(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroPhantomCollections __Gen_Delegate_Imp984(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroPhantomInfo __Gen_Delegate_Imp985(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MailType __Gen_Delegate_Imp986(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp987(object p0, MailType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMailInfo __Gen_Delegate_Imp988(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProMail __Gen_Delegate_Imp989(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProMail> __Gen_Delegate_Imp990(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LinkedList<string> __Gen_Delegate_Imp991(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Marquee __Gen_Delegate_Imp992(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProMarquee __Gen_Delegate_Imp993(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMissionInfo __Gen_Delegate_Imp994(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Mission __Gen_Delegate_Imp995(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProMission __Gen_Delegate_Imp996(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProMission> __Gen_Delegate_Imp997(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase __Gen_Delegate_Imp998(
      ulong p0,
      int p1,
      OperationalActivityType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProOperationalActivityBasicInfo __Gen_Delegate_Imp999(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1000(
      object p0,
      DateTime p1,
      DateTime p2,
      DateTime p3,
      DateTime p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime __Gen_Delegate_Imp1001(object p0, DateTime p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityType __Gen_Delegate_Imp1002(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1003(object p0, OperationalActivityType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataOperationalActivityInfo __Gen_Delegate_Imp1004(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1005(object p0, ulong p1, int p2, OperationalActivityType p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp1006(object p0, int p1, DateTime p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPlayerLevelUpOperationalActivity __Gen_Delegate_Imp1007(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProSpecificDaysLoginOperationalActivity __Gen_Delegate_Imp1008(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<long> __Gen_Delegate_Imp1009(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProAccumulateLoginOperationalActivity __Gen_Delegate_Imp1010(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProAccumulateRechargeOperationalActivity __Gen_Delegate_Imp1011(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProAccumulateConsumeCrystalOperationalActivity __Gen_Delegate_Imp1012(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProLimitedTimeExchangeOperationActivity __Gen_Delegate_Imp1013(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProEffectOperationActivity __Gen_Delegate_Imp1014(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProEventOperationalActivity __Gen_Delegate_Imp1015(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProAdvertisementFlowLayout __Gen_Delegate_Imp1016(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AdvertisementFlowLayout __Gen_Delegate_Imp1017(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProWebControlInfo __Gen_Delegate_Imp1018(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArenaMatchStats __Gen_Delegate_Imp1019(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArenaHiredHero __Gen_Delegate_Imp1020(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaHiredHero __Gen_Delegate_Imp1021(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<PeakArenaHiredHero> __Gen_Delegate_Imp1022(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArenaBattleTeam __Gen_Delegate_Imp1023(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaHeroWithEquipments __Gen_Delegate_Imp1024(
      object p0,
      object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<PeakArenaHeroWithEquipments> __Gen_Delegate_Imp1025(
      object p0,
      object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArena __Gen_Delegate_Imp1026(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArenaBetInfo __Gen_Delegate_Imp1027(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArenaMatchupBet __Gen_Delegate_Imp1028(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaMatchupBet __Gen_Delegate_Imp1029(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProResource __Gen_Delegate_Imp1030(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProMonthCard __Gen_Delegate_Imp1031(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MonthCard __Gen_Delegate_Imp1032(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProRafflePool> __Gen_Delegate_Imp1033(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProRafflePool __Gen_Delegate_Imp1034(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RafflePool __Gen_Delegate_Imp1035(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<RafflePool> __Gen_Delegate_Imp1036(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRafflePoolInfo __Gen_Delegate_Imp1037(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProRankingPlayerInfo __Gen_Delegate_Imp1038(
      object p0,
      int p1,
      long p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingPlayerInfo __Gen_Delegate_Imp1039(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingListType __Gen_Delegate_Imp1040(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1041(object p0, RankingListType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<RankingTargetPlayerInfo> __Gen_Delegate_Imp1042(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProRankingListInfo __Gen_Delegate_Imp1043(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingListInfo __Gen_Delegate_Imp1044(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RealTimePVPBattleReport __Gen_Delegate_Imp1045(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RealTimePVPBattleReportType __Gen_Delegate_Imp1046(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1047(object p0, RealTimePVPBattleReportType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomBPRule __Gen_Delegate_Imp1048(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1049(object p0, BattleRoomBPRule p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RealTimePVPBattleReportPlayerData __Gen_Delegate_Imp1050(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProRealTimePVPBattleReportPlayerData __Gen_Delegate_Imp1051(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProRealTimePVPBattleReport __Gen_Delegate_Imp1052(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProRealTimePVPMatchStats __Gen_Delegate_Imp1053(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftChapter __Gen_Delegate_Imp1054(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProRiftChapter __Gen_Delegate_Imp1055(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftLevel __Gen_Delegate_Imp1056(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProRiftLevel __Gen_Delegate_Imp1057(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProRandomStore __Gen_Delegate_Imp1058(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProRandomStore> __Gen_Delegate_Imp1059(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomStore __Gen_Delegate_Imp1060(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProRandomStoreItem __Gen_Delegate_Imp1061(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomStoreItem __Gen_Delegate_Imp1062(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FixedStore __Gen_Delegate_Imp1063(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<FixedStore> __Gen_Delegate_Imp1064(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProFixedStore __Gen_Delegate_Imp1065(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProFixedStore> __Gen_Delegate_Imp1066(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProFixedStoreItem __Gen_Delegate_Imp1067(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProFixedStoreItem> __Gen_Delegate_Imp1068(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FixedStoreItem __Gen_Delegate_Imp1069(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<FixedStoreItem> __Gen_Delegate_Imp1070(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProSurvey __Gen_Delegate_Imp1071(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProUnchartedScore> __Gen_Delegate_Imp1072(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProUnchartedScore __Gen_Delegate_Imp1073(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<UnchartedScore> __Gen_Delegate_Imp1074(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UnchartedScore __Gen_Delegate_Imp1075(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataUnchartedScoreInfo __Gen_Delegate_Imp1076(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1077(int p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1078(int p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp1079(
      StopwatchManager.StatisticResult p0,
      StopwatchManager.StatisticResult p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1080(bool p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1081(
      object p0,
      object p1,
      int p2,
      object p3,
      object p4,
      object p5,
      object p6,
      int p7,
      int p8,
      object p9)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1082(
      object p0,
      object p1,
      object p2,
      object p3,
      int p4,
      int p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1083(int p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IClient __Gen_Delegate_Imp1084(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SessionToken __Gen_Delegate_Imp1085(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBattleBeginInfo __Gen_Delegate_Imp1086(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBattleReport __Gen_Delegate_Imp1087(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProAncientCallBossPeriodFinalRank> __Gen_Delegate_Imp1088(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProChangedGoodsNtf __Gen_Delegate_Imp1089(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProBattleHero> __Gen_Delegate_Imp1090(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProArenaTopRankPlayer> __Gen_Delegate_Imp1091(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProArenaBattleReport __Gen_Delegate_Imp1092(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProArenaBattleReport> __Gen_Delegate_Imp1093(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProArenaOpponent> __Gen_Delegate_Imp1094(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProCommonBattleProperty> __Gen_Delegate_Imp1095(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProBattleCommand> __Gen_Delegate_Imp1096(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProBattleRoomPlayer> __Gen_Delegate_Imp1097(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProBattleHeroSetupInfo> __Gen_Delegate_Imp1098(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomDataChangeNtf __Gen_Delegate_Imp1099(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBattleRoomBPStage __Gen_Delegate_Imp1100(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProBattleRoomPlayerBattleInfoInitInfo> __Gen_Delegate_Imp1101(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBattleSyncCommand __Gen_Delegate_Imp1102(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBPStageSyncCommand __Gen_Delegate_Imp1103(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProGuildMassiveCombatAttackResultNtf __Gen_Delegate_Imp1104(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBattleRoomPlayerBattleInfoInitInfo __Gen_Delegate_Imp1105(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProBPStageHero> __Gen_Delegate_Imp1106(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProChatGroupCompactInfo> __Gen_Delegate_Imp1107(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProChatGroupInfo __Gen_Delegate_Imp1108(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProChatUserInfo __Gen_Delegate_Imp1109(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatGroupCreateChatGroupReq __Gen_Delegate_Imp1110(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProChatInfo __Gen_Delegate_Imp1111(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProChatCententInfo __Gen_Delegate_Imp1112(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProChatContentVoice __Gen_Delegate_Imp1113(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProChatEnterRoomInfo __Gen_Delegate_Imp1114(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProChatVoiceSimpleInfo __Gen_Delegate_Imp1115(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProUserSummary> __Gen_Delegate_Imp1116(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ChatMessageNtf> __Gen_Delegate_Imp1117(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProChatContentText __Gen_Delegate_Imp1118(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProGoodsWithInstanceId> __Gen_Delegate_Imp1119(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArenaBattleReportShareInfo __Gen_Delegate_Imp1120(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionExchangeReq __Gen_Delegate_Imp1121(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProHeroJob> __Gen_Delegate_Imp1122(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProHeroFetter> __Gen_Delegate_Imp1123(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProSoldierSkin> __Gen_Delegate_Imp1124(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProHeroJobRefineryProperty> __Gen_Delegate_Imp1125(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArenaLadderBattleReport __Gen_Delegate_Imp1126(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProCommonBattleReportHead __Gen_Delegate_Imp1127(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProPeakArenaBattleReportPlayerSummaryInfo> __Gen_Delegate_Imp1128(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProPeakArenaBattleReportBattleInfo> __Gen_Delegate_Imp1129(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProTrainingTech> __Gen_Delegate_Imp1130(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProPeakArenaBattleReportBattleInitInfo> __Gen_Delegate_Imp1131(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProBPStageCommand> __Gen_Delegate_Imp1132(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProEquipment __Gen_Delegate_Imp1133(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProRiftLevel> __Gen_Delegate_Imp1134(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBattlePosition __Gen_Delegate_Imp1135(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProRandomStoreItem> __Gen_Delegate_Imp1136(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProHeroCommentEntry> __Gen_Delegate_Imp1137(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProArenaPlayerDefensiveHero> __Gen_Delegate_Imp1138(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProBattleHeroJob> __Gen_Delegate_Imp1139(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProBattleHeroEquipment> __Gen_Delegate_Imp1140(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProLimitedTimeExchangeOperationActivityItemGroup> __Gen_Delegate_Imp1141(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProTurnDanmaku> __Gen_Delegate_Imp1142(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProDanmakuEntry> __Gen_Delegate_Imp1143(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProHeroDungeonLevel> __Gen_Delegate_Imp1144(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProTeamRoomPlayer> __Gen_Delegate_Imp1145(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProRankingPlayerAncientCallBossInfo> __Gen_Delegate_Imp1146(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProRankingPlayerInfo> __Gen_Delegate_Imp1147(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProCommonBattleReport> __Gen_Delegate_Imp1148(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProChatUserCompactInfo __Gen_Delegate_Imp1149(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProChatGroupCompactInfo __Gen_Delegate_Imp1150(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProChatUserInfo> __Gen_Delegate_Imp1151(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProRealTimePVPUserInfo __Gen_Delegate_Imp1152(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProCooperateBattleLevel> __Gen_Delegate_Imp1153(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProHeroPhantomLevel> __Gen_Delegate_Imp1154(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProMonthCard> __Gen_Delegate_Imp1155(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProRealTimePVPBattleReport> __Gen_Delegate_Imp1156(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<bool> __Gen_Delegate_Imp1157(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProPeakArenaHiredHero> __Gen_Delegate_Imp1158(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProRealTimePVPBattleReportPlayerData> __Gen_Delegate_Imp1159(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProGuildMember> __Gen_Delegate_Imp1160(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProClimbTowerFloor> __Gen_Delegate_Imp1161(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProGuildMassiveCombatStrongholdInfo> __Gen_Delegate_Imp1162(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProGuildMassiveCombatMemberInfo> __Gen_Delegate_Imp1163(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProGuildMassiveCombatInfo> __Gen_Delegate_Imp1164(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProCollectionActivityPlayerExchangeInfo> __Gen_Delegate_Imp1165(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArenaPlayOffPlayerInfo __Gen_Delegate_Imp1166(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProPeakArenaPlayOffBattleInfo> __Gen_Delegate_Imp1167(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProPeakArenaPlayOffMatchupInfo> __Gen_Delegate_Imp1168(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProPeakArenaMatchupBet> __Gen_Delegate_Imp1169(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProAncientCallBoss> __Gen_Delegate_Imp1170(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProAncientCallBossHistory> __Gen_Delegate_Imp1171(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProHeroJobSlotRefineryProperty> __Gen_Delegate_Imp1172(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProPostDanmakuEntry> __Gen_Delegate_Imp1173(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendInviteReq __Gen_Delegate_Imp1174(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendInviteAcceptReq __Gen_Delegate_Imp1175(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendInviteDeclineReq __Gen_Delegate_Imp1176(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendDeleteReq __Gen_Delegate_Imp1177(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendBlockReq __Gen_Delegate_Imp1178(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendUnblockReq __Gen_Delegate_Imp1179(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendGetUserSummaryReq __Gen_Delegate_Imp1180(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePracticeAcceptReq __Gen_Delegate_Imp1181(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendshipPointsSendReq __Gen_Delegate_Imp1182(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendshipPointsClaimReq __Gen_Delegate_Imp1183(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<RedeemCrystalInfo> __Gen_Delegate_Imp1184(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RedeemBundleInfo __Gen_Delegate_Imp1185(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProHero> __Gen_Delegate_Imp1186(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProRiftChapter> __Gen_Delegate_Imp1187(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProTeam> __Gen_Delegate_Imp1188(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProPlayerLevelUpOperationalActivity> __Gen_Delegate_Imp1189(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProSpecificDaysLoginOperationalActivity> __Gen_Delegate_Imp1190(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProAccumulateLoginOperationalActivity> __Gen_Delegate_Imp1191(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProLimitedTimeExchangeOperationActivity> __Gen_Delegate_Imp1192(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProEffectOperationActivity> __Gen_Delegate_Imp1193(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProAccumulateRechargeOperationalActivity> __Gen_Delegate_Imp1194(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProAccumulateConsumeCrystalOperationalActivity> __Gen_Delegate_Imp1195(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProEventOperationalActivity> __Gen_Delegate_Imp1196(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProAnnouncement> __Gen_Delegate_Imp1197(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProHeroDungeonChapter> __Gen_Delegate_Imp1198(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProHeroAssistantTask> __Gen_Delegate_Imp1199(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProTeamRoomInviteInfo> __Gen_Delegate_Imp1200(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProCooperateBattle> __Gen_Delegate_Imp1201(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProHeroPhantom> __Gen_Delegate_Imp1202(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProNovice __Gen_Delegate_Imp1203(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProReflux __Gen_Delegate_Imp1204(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProRealTimePVP __Gen_Delegate_Imp1205(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProGiftStoreItem> __Gen_Delegate_Imp1206(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProGiftStoreFirstBoughtRecord> __Gen_Delegate_Imp1207(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProOrderReward> __Gen_Delegate_Imp1208(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProEventGift> __Gen_Delegate_Imp1209(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProCollectionActivity> __Gen_Delegate_Imp1210(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProHeroAnthemSuccessLevel> __Gen_Delegate_Imp1211(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProGiftStoreOperationalGoods> __Gen_Delegate_Imp1212(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProGuildJoinInvitation> __Gen_Delegate_Imp1213(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProGuildSearchInfo> __Gen_Delegate_Imp1214(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMassiveCombatAttackReq __Gen_Delegate_Imp1215(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMassiveCombatAttackFinishedReq __Gen_Delegate_Imp1216(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroAssistantAssignToTaskReq __Gen_Delegate_Imp1217(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroAssistantCancelTaskReq __Gen_Delegate_Imp1218(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroAssistantClaimRewardReq __Gen_Delegate_Imp1219(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroPhantomAttackReq __Gen_Delegate_Imp1220(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroPhantomBattleFinishedReq __Gen_Delegate_Imp1221(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public NoviceClaimRewardReq __Gen_Delegate_Imp1222(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProAdvertisementFlowLayout> __Gen_Delegate_Imp1223(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProHeroWithEquipments> __Gen_Delegate_Imp1224(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBattleTeamSetupReq __Gen_Delegate_Imp1225(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProPeakArenaLeaderboardPlayerInfo> __Gen_Delegate_Imp1226(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArenaSeasonInfo __Gen_Delegate_Imp1227(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArenaLeaderboardPlayerInfo __Gen_Delegate_Imp1228(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaAcquireWinsBonusReq __Gen_Delegate_Imp1229(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaAcquireRegularRewardsReq __Gen_Delegate_Imp1230(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffInfoGetReq __Gen_Delegate_Imp1231(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProPeakArenaLiveRoomInfo> __Gen_Delegate_Imp1232(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffLiveRoomGetReq __Gen_Delegate_Imp1233(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaMyPlayOffInfoGetReq __Gen_Delegate_Imp1234(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArenaPlayOffMatchupInfo __Gen_Delegate_Imp1235(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffCurrentSeasonMatchInfoGetReq __Gen_Delegate_Imp1236(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBattleLiveRoomJoinReq __Gen_Delegate_Imp1237(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArenaBattleLiveRoomInfo __Gen_Delegate_Imp1238(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaSeasonSimpleInfoReq __Gen_Delegate_Imp1239(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public System.Type __Gen_Delegate_Imp1240(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<System.Type, int> __Gen_Delegate_Imp1241(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RealTimePVPWaitingForOpponentReq __Gen_Delegate_Imp1242(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProRealTimePVPLeaderboardPlayerInfo __Gen_Delegate_Imp1243(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProRealTimePVPLeaderboardPlayerInfo> __Gen_Delegate_Imp1244(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RealTimePVPAcquireWinsBonusReq __Gen_Delegate_Imp1245(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RefluxClaimRewardReq __Gen_Delegate_Imp1246(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProTeamRoom> __Gen_Delegate_Imp1247(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProTeamRoomPlayerPositionInfo> __Gen_Delegate_Imp1248(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingGroundTechLevelupReq __Gen_Delegate_Imp1249(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Sprite __Gen_Delegate_Imp1250(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UnityEngine.Object __Gen_Delegate_Imp1251(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LruAssetCache __Gen_Delegate_Imp1252(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AssetUtility __Gen_Delegate_Imp1253()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1254()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float[] __Gen_Delegate_Imp1255(float[] p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AudioClip __Gen_Delegate_Imp1256(object p0, float p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IAudioPlayback __Gen_Delegate_Imp1257(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<string> __Gen_Delegate_Imp1258()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp1259()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp1260(SoundTableId p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1261(SoundTableId p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1262(object p0, float p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1263(object p0, byte[] p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IPlayerContextNetworkClient __Gen_Delegate_Imp1264(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GameObject __Gen_Delegate_Imp1265()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GameObject __Gen_Delegate_Imp1266(object p0, string[] p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1267(object p0, string[] p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GameObject __Gen_Delegate_Imp1268(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PrefabControllerBase __Gen_Delegate_Imp1269(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GameObject __Gen_Delegate_Imp1270(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GameObject> __Gen_Delegate_Imp1271(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public JsonObject __Gen_Delegate_Imp1272(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public JsonArray __Gen_Delegate_Imp1273(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public JsonArray __Gen_Delegate_Imp1274(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public JsonObject __Gen_Delegate_Imp1275(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object __Gen_Delegate_Imp1276(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1277(object p0, object p1, ref long p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1278(object p0, object p1, ref int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1279(object p0, object p1, uint p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1280(object p0, object p1, ref uint p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1281(object p0, object p1, short p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1282(object p0, object p1, ref short p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1283(object p0, object p1, ushort p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1284(object p0, object p1, ref ushort p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1285(object p0, object p1, sbyte p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1286(object p0, object p1, ref sbyte p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1287(object p0, object p1, byte p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1288(object p0, object p1, ref byte p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1289(object p0, object p1, double p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1290(object p0, object p1, ref double p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1291(object p0, object p1, ref float p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1292(object p0, object p1, ref bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1293(object p0, object p1, ref string p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1294(object p0, object p1, ref DateTime p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1295(object p0, object p1, ref PLong p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1296(object p0, object p1, ref PInt p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1297(object p0, object p1, ref PUInt p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1298(object p0, object p1, ref PShort p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1299(object p0, object p1, ref PUShort p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1300(object p0, object p1, ref PSByte p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1301(object p0, object p1, ref PByte p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1302(object p0, ulong p1, DateTime p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LocalAccountConfigData __Gen_Delegate_Imp1303(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LocalAccountConfig __Gen_Delegate_Imp1304()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LocalConfigData __Gen_Delegate_Imp1305(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LocalConfig __Gen_Delegate_Imp1306()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LocalProcessingBattleData __Gen_Delegate_Imp1307(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LocalProcessingBattle __Gen_Delegate_Imp1308()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IMonoBehaviourEventListener __Gen_Delegate_Imp1309()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float __Gen_Delegate_Imp1310()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1311(float p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataDeviceSetting __Gen_Delegate_Imp1312()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BatteryStatus __Gen_Delegate_Imp1313()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1314(object p0, int p1, bool p2, int p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp1315(object p0, int p1, bool p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1316(object p0, object p1, object p2, object p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1317(object p0, ulong p1, bool p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ulong> __Gen_Delegate_Imp1318(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp1319(object p0, ulong p1, object p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1320(
      object p0,
      GoodsType p1,
      int p2,
      int p3,
      object p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1321(
      object p0,
      ulong p1,
      int p2,
      GameFunctionType p3,
      int p4,
      object p5,
      DateTime p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1322(
      object p0,
      ulong p1,
      int p2,
      int p3,
      object p4,
      DateTime p5,
      object p6,
      ulong p7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1323(object p0, ulong p1, int p2, object p3, DateTime p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1324(
      object p0,
      ulong p1,
      int p2,
      object p3,
      DateTime p4,
      BattleRoomType p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1325(
      object p0,
      ulong p1,
      int p2,
      object p3,
      DateTime p4,
      RealTimePVPMode p5,
      int p6,
      int p7,
      int p8,
      ulong p9,
      int p10)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1326(object p0, ulong p1, int p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1327(object p0, int p1, PlayerBattleStatus p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1328(object p0, long p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoom __Gen_Delegate_Imp1329(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleReward __Gen_Delegate_Imp1330(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomPlayer __Gen_Delegate_Imp1331(object p0, ulong p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomPlayer __Gen_Delegate_Imp1332(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp1333(object p0, int p1, SetupBattleHeroFlag p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1334(object p0, TimeSpan p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1335(object p0, TimeSpan p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1336(object p0, ChatChannel p1, DateTime p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp1337(object p0, DateTime p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatComponent.ChatMessageClient __Gen_Delegate_Imp1338(
      object p0,
      ChatChannel p1,
      object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1339(object p0, ChatChannel p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UserSummary __Gen_Delegate_Imp1340(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ChatComponent.ChatMessageClient> __Gen_Delegate_Imp1341(
      object p0,
      ChatChannel p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp1342(object p0, ChatChannel p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public KeyValuePair<List<string>, List<string>> __Gen_Delegate_Imp1343(
      object p0,
      ChatChannel p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp1344(object p0, int p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatGroupCompactInfo __Gen_Delegate_Imp1345(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp1346(object p0, int p1, int p2, bool p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp1347(object p0, out int p1, out int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1348(object p0, int p1, out DateTime p2, out DateTime p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ulong __Gen_Delegate_Imp1349(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivity __Gen_Delegate_Imp1350(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1351(
      object p0,
      int p1,
      out CollectionActivityWaypointStateType p2,
      out string p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1352(object p0, int p1, out int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1353(
      object p0,
      GameFunctionType p1,
      int p2,
      object p3,
      object p4,
      bool p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp1354(object p0, CollectionActivityLevelType p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityExchangeModel __Gen_Delegate_Imp1355(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp1356(object p0, ulong p1, int p2, int p3, bool p4, object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroComment __Gen_Delegate_Imp1357(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp1358(object p0, int p1, object p2, long p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp1359(object p0, int p1, ulong p2, long p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<HeroCommentEntry> __Gen_Delegate_Imp1360(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LevelDanmaku __Gen_Delegate_Imp1361(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DanmakuEntry __Gen_Delegate_Imp1362(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<PostDanmakuEntry> __Gen_Delegate_Imp1363(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1364(object p0, int p1, int p2, long p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleHero> __Gen_Delegate_Imp1365(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<UserSummary> __Gen_Delegate_Imp1366(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<PVPInviteInfo> __Gen_Delegate_Imp1367(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1368(object p0, int p1, object p2, int p3, long p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1369(object p0, RankingListType p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingListInfo __Gen_Delegate_Imp1370(object p0, RankingListType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1371(object p0, RankingListType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingListInfo __Gen_Delegate_Imp1372(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp1373(object p0, int p1, object p2, bool p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<HeroAssistantsTask> __Gen_Delegate_Imp1374(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<HeroAssistantsTask> __Gen_Delegate_Imp1375(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<HeroAssistantsTaskAssignment> __Gen_Delegate_Imp1376(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroInteractionInfo __Gen_Delegate_Imp1377(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp1378(object p0, int p1, HeroInteractionResultType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp1379(object p0, int p1, int p2, int p3, GoodsType p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp1380(object p0, int p1, object p2, int p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp1381(
      object p0,
      object p1,
      bool p2,
      object p3,
      object p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp1382(object p0, int p1, bool p2, int p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1383(object p0, int p1, long p2, long p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Announcement> __Gen_Delegate_Imp1384(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp1385(object p0, int p1, int p2, object p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int? __Gen_Delegate_Imp1386(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1387(object p0, int? p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffMatchupInfo __Gen_Delegate_Imp1388(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionPlayerBasicInfo __Gen_Delegate_Imp1389(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1390(
      object p0,
      int p1,
      RiftChapterUnlockConditionType p2,
      int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1391(object p0, int p1, RiftLevelUnlockConditionType p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1392(object p0, int p1, bool p2, bool p3, int p4, int p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp1393(object p0, int p1, bool p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1394(object p0, GameFunctionType p1, int p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1395(object p0, object p1, ulong p2, long p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp1396(object p0, int p1, bool p2, object p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1397(
      object p0,
      int p1,
      object p2,
      object p3,
      object p4,
      bool p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1398(
      object p0,
      int p1,
      object p2,
      object p3,
      object p4,
      object p5,
      bool p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IComponentBase __Gen_Delegate_Imp1399(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1400(object p0, object p1, out bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Happening __Gen_Delegate_Imp1401(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CurrentBattle __Gen_Delegate_Imp1402(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataAncientCallPeriodInfo __Gen_Delegate_Imp1403(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallBossHistory __Gen_Delegate_Imp1404(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> __Gen_Delegate_Imp1405(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1406(object p0, ulong p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1407(object p0, int p1, int p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GainRewardStatus __Gen_Delegate_Imp1408(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1409(object p0, int p1, GoodsType p2, int p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1410(object p0, ulong p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1411(object p0, ulong p1, ulong p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp1412(object p0, GoodsType p1, int p2, ulong p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1413(object p0, object p1, BattleType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1414(
      object p0,
      object p1,
      object p2,
      int p3,
      object p4,
      object p5,
      bool p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1415(
      object p0,
      object p1,
      object p2,
      PeakArenaBattleReportSourceType p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RewardPlayerStatus __Gen_Delegate_Imp1416(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RewardHeroStatus __Gen_Delegate_Imp1417(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1418(object p0, PlayerBattleStatus p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1419(
      object p0,
      ChatChannel p1,
      byte[] p2,
      int p3,
      int p4,
      int p5,
      object p6,
      object p7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1420(object p0, ChatChannel p1, ulong p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1421(object p0, ChatChannel p1, object p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1422(object p0, ChatChannel p1, object p2, ulong p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatComponent __Gen_Delegate_Imp1423(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp1424(object p0, ChatChannel p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public NoticeText __Gen_Delegate_Imp1425(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1426(object p0, GameFunctionType p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1427(object p0, GameFunctionType p1, int p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1428(object p0, ulong p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1429(object p0, ulong p1, int p2, int p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityScenarioLevelInfo __Gen_Delegate_Imp1430(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityWaypointInfo __Gen_Delegate_Imp1431(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionEvent> __Gen_Delegate_Imp1432(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionEventInfo __Gen_Delegate_Imp1433(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1434(object p0, int p1, ulong p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<FixedStoreItem> __Gen_Delegate_Imp1435(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1436(object p0, FriendSocialRelationFlag p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1437(object p0, object p1, int p2, PracticeMode p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1438(object p0, object p1, PracticeMode p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GiftStoreItem> __Gen_Delegate_Imp1439(object p0, bool p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TimeSpan __Gen_Delegate_Imp1440(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp1441(GoodsType p0, int p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1442(
      object p0,
      object p1,
      object p2,
      object p3,
      bool p4,
      int p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1443(object p0, bool p1, int p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1444(object p0, object p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GuildJoinInvitation> __Gen_Delegate_Imp1445(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GuildSearchInfo> __Gen_Delegate_Imp1446(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMassiveCombatInfo __Gen_Delegate_Imp1447(object p0, ulong p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMassiveCombatStronghold __Gen_Delegate_Imp1448(
      object p0,
      int p1,
      ulong p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1449(object p0, int p1, int p2, int p3, int p4, int p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1450(object p0, int p1, object p2, SkillAndSoldierSelectMode p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1451(object p0, int p1, int p2, SkillAndSoldierSelectMode p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1452(object p0, int p1, int p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, Hero> __Gen_Delegate_Imp1453(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ulong[] __Gen_Delegate_Imp1454(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1455(object p0, int p1, int p2, object p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GainRewardStatus __Gen_Delegate_Imp1456(object p0, int p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1457(GoodsType p0, int p1, int p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataWorldMapInfo __Gen_Delegate_Imp1458(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1459(
      object p0,
      int p1,
      out ConfigDataEventInfo p2,
      out RandomEvent p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, ProjectLPlayerContext.CurrentWaypointEvent> __Gen_Delegate_Imp1460(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEventInfo __Gen_Delegate_Imp1461(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public double __Gen_Delegate_Imp1462(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RedeemInfoAck __Gen_Delegate_Imp1463(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1464(object p0, int p1, object p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<PeakArenaHeroWithEquipments> __Gen_Delegate_Imp1465(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaHeroWithEquipments __Gen_Delegate_Imp1466(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleHeroEquipment> __Gen_Delegate_Imp1467(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaHiredHero __Gen_Delegate_Imp1468(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public EPeakArenaDurringStateType __Gen_Delegate_Imp1469(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1470(
      object p0,
      DateTime p1,
      DateTime p2,
      int p3,
      ref DateTime p4,
      ref bool p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffMatchupInfo __Gen_Delegate_Imp1471(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<PeakArenaPlayOffMatchupInfo> __Gen_Delegate_Imp1472(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<PeakArenaPlayOffMatchupInfo> __Gen_Delegate_Imp1473(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaKnockoutMatchTime __Gen_Delegate_Imp1474(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaKnockoutMatchTime __Gen_Delegate_Imp1475(
      object p0,
      int p1,
      int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataPeakArenaKnockoutMatchTime> __Gen_Delegate_Imp1476(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1477(object p0, RealTimePVPMode p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleReportHead __Gen_Delegate_Imp1478(object p0, ulong p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBattleReportNameRecord __Gen_Delegate_Imp1479(
      object p0,
      ulong p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp1480(object p0, object p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerInfoInitReq __Gen_Delegate_Imp1481(object p0, bool p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1482(
      object p0,
      object p1,
      ref bool p2,
      ref bool p3,
      ref bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<RandomStoreItem> __Gen_Delegate_Imp1483(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1484(object p0, RankingListType p1, int p2, int p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProRealTimePVPLeaderboardPlayerInfo> __Gen_Delegate_Imp1485(
      object p0,
      bool p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProUserSummary> __Gen_Delegate_Imp1486(object p0, bool p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftLevelStatus __Gen_Delegate_Imp1487(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRiftChapterInfo __Gen_Delegate_Imp1488(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1489(object p0, int p1, bool p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool[] __Gen_Delegate_Imp1490(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, bool> __Gen_Delegate_Imp1491(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<List<Goods>> __Gen_Delegate_Imp1492(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Goods> __Gen_Delegate_Imp1493(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Goods __Gen_Delegate_Imp1494(object p0, int p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1495(object p0, GameFunctionType p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1496(object p0, TeamRoomAuthority p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1497(object p0, int p1, GameFunctionType p2, int p3, ulong p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1498(object p0, object p1, TeamRoomInviteeInfoType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1499(object p0, int[] p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1500(ulong p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1501(
      object p0,
      BattleType p1,
      object p2,
      int p3,
      object p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1502(object p0, out GameFunctionType p1, out int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1503(object p0, out int p1, out int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataBattleAchievementRelatedInfo> __Gen_Delegate_Imp1504(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1505(object p0, int p1, bool p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 __Gen_Delegate_Imp1506(Vector2 p0, float p1, float p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1507(object p0, Vector2 p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 __Gen_Delegate_Imp1508(object p0, Vector2 p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1509(object p0, Vector2 p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1510(object p0, Vector2 p1, float p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 __Gen_Delegate_Imp1511(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1512(object p0, object p1, BattleType p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1513(
      object p0,
      object p1,
      object p2,
      BattlePlayer[] p3,
      int p4,
      int p5,
      int p6,
      int p7,
      int p8,
      object p9,
      object p10)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1514(
      object p0,
      object p1,
      object p2,
      BattlePlayer[] p3,
      int p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1515(
      object p0,
      object p1,
      object p2,
      BattlePlayer[] p3,
      int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1516(
      object p0,
      object p1,
      object p2,
      BattlePlayer[] p3,
      int p4,
      int p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1517(object p0, bool p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition __Gen_Delegate_Imp1518(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ClientBattleActor> __Gen_Delegate_Imp1519(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientBattleActor __Gen_Delegate_Imp1520(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientBattleActor __Gen_Delegate_Imp1521(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 __Gen_Delegate_Imp1522(object p0, GridPosition p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition __Gen_Delegate_Imp1523(object p0, Vector2 p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1524(object p0, Vector3 p1, Vector3 p2, Color p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1525(object p0, GridPosition p1, object p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1526(object p0, GridPosition p1, float p2, Color p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GenericGraphic __Gen_Delegate_Imp1527(object p0, object p1, float p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CameraBase __Gen_Delegate_Imp1528(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientBattleTreasure __Gen_Delegate_Imp1529(
      object p0,
      object p1,
      bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientBattleTreasure __Gen_Delegate_Imp1530(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActorUIController __Gen_Delegate_Imp1531(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1532(object p0, SkipCombatMode p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp1533(object p0, object p1, object p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1534(ArmyRelationData p0, bool p1, ref int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleCamera __Gen_Delegate_Imp1535(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatCamera __Gen_Delegate_Imp1536(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientBattleState __Gen_Delegate_Imp1537(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SkipCombatMode __Gen_Delegate_Imp1538(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FxPlayer __Gen_Delegate_Imp1539(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IClientBattleListener __Gen_Delegate_Imp1540(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1541(object p0, object p1, bool p2, int p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1542(object p0, SoundTableId p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 __Gen_Delegate_Imp1543(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 __Gen_Delegate_Imp1544(object p0, Vector2i p1, Fix64 p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1545(object p0, Vector2 p1, Vector2 p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 __Gen_Delegate_Imp1546(object p0, Vector2 p1, float p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1547(object p0, object p1, GridPosition p2, float p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1548(object p0, Colori p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientBattleActorActionState __Gen_Delegate_Imp1549(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 __Gen_Delegate_Imp1550(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 __Gen_Delegate_Imp1551(int p0, int p1, int p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GenericGraphic __Gen_Delegate_Imp1552(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1553(object p0, GridPosition p1, int p2, bool p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp1554(object p0, GridPosition p1, GridPosition p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleTreasureInfo __Gen_Delegate_Imp1555(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1556(object p0, Vector2 p1, Vector2 p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1557(object p0, out float p1, out float p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Camera __Gen_Delegate_Imp1558(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1559(object p0, Vector2 p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1560(object p0, object p1, Vector3 p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityWaypointInfo __Gen_Delegate_Imp1561(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionEventInfo __Gen_Delegate_Imp1562(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1563(object p0, CollectionActivityWaypointStateType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityWaypointStateType __Gen_Delegate_Imp1564(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RectTransform __Gen_Delegate_Imp1565(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityWaypoint __Gen_Delegate_Imp1566(
      object p0,
      object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityPlayerActor __Gen_Delegate_Imp1567(
      object p0,
      object p1,
      int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityEventActor __Gen_Delegate_Imp1568(
      object p0,
      object p1,
      object p2,
      int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityWaypoint __Gen_Delegate_Imp1569(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityEventActor __Gen_Delegate_Imp1570(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionActivityEventActor> __Gen_Delegate_Imp1571(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityPlayerActor __Gen_Delegate_Imp1572(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1573(object p0, Vector2 p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WorldCamera __Gen_Delegate_Imp1574(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Quaternion __Gen_Delegate_Imp1575(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ICollectionActivityWorldListener __Gen_Delegate_Imp1576(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProjectLPlayerContext __Gen_Delegate_Imp1577(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GenericGraphic __Gen_Delegate_Imp1578(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float __Gen_Delegate_Imp1579(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1580(object p0, float p1, float p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1581(object p0, Vector2i p1, Fix64 p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1582(object p0, Vector2i p1, Fix64 p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1583(object p0, Quaternion p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1584(object p0, Vector3 p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1585(object p0, Vector3 p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1586(object p0, Colori p1, float p2, float p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1587(object p0, int p1, float p2, float p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1588(
      object p0,
      GraphicEffect p1,
      Colori p2,
      float p3,
      float p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1589(object p0, Color p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1590(object p0, object p1, bool p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float __Gen_Delegate_Imp1591(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1592(object p0, int p1, float p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldRegion __Gen_Delegate_Imp1593(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldWaypoint __Gen_Delegate_Imp1594(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldPlayerActor __Gen_Delegate_Imp1595(
      object p0,
      object p1,
      int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldEventActor __Gen_Delegate_Imp1596(
      object p0,
      object p1,
      object p2,
      int p3,
      object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldRegion __Gen_Delegate_Imp1597(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldWaypoint __Gen_Delegate_Imp1598(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldEventActor __Gen_Delegate_Imp1599(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ClientWorldEventActor> __Gen_Delegate_Imp1600(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldPlayerActor __Gen_Delegate_Imp1601(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldState __Gen_Delegate_Imp1602(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IClientWorldListener __Gen_Delegate_Imp1603(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEventInfo __Gen_Delegate_Imp1604(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRegionInfo __Gen_Delegate_Imp1605(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1606(object p0, WayPointStatus p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WayPointStatus __Gen_Delegate_Imp1607(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1608(object p0, Vector2 p1, object p2, float p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float __Gen_Delegate_Imp1609(object p0, object p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WorldPathfinder.Node __Gen_Delegate_Imp1610(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WorldPathNode __Gen_Delegate_Imp1611(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WorldPathfinder.SearchState __Gen_Delegate_Imp1612(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1613(object p0, int p1, out Vector2 p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1614(object p0, object p1, int p2, int p3, object p4, bool p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp1615(object p0, double p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, List<RaffleItem>> __Gen_Delegate_Imp1616(
      object p0,
      object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp1617(ulong p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1618(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1619(object p0, GoodsType p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UITaskBase.LayerDesc[] __Gen_Delegate_Imp1620(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UITaskBase.UIControllerDesc[] __Gen_Delegate_Imp1621(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1622(object p0, DialogBoxResult p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRefluxRewardInfo __Gen_Delegate_Imp1623(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1624(
      object p0,
      ulong p1,
      int p2,
      long p3,
      long p4,
      OperationalActivityType p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataNoviceRewardInfo __Gen_Delegate_Imp1625(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1626(object p0, int p1, object p2, int p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1627(object p0, int p1, object p2, object p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1628(object p0, float p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallBossAchievementUITask __Gen_Delegate_Imp1629(
      int p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallBossDetailUITask __Gen_Delegate_Imp1630(int p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallRankingListUITask __Gen_Delegate_Imp1631(int p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Sprite __Gen_Delegate_Imp1632(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1633(object p0, object p1, int p2, int p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1634(object p0, bool p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataAnikiGymInfo __Gen_Delegate_Imp1635(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataAnikiLevelInfo __Gen_Delegate_Imp1636(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataJobConnectionInfo> __Gen_Delegate_Imp1637(
      object p0,
      object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1638(object p0, HeroBelongProduction p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1639(object p0, object p1, HeroBelongProduction p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1640(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1641(object p0, GridPosition p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 __Gen_Delegate_Imp1642(object p0, GridPosition p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 __Gen_Delegate_Imp1643(object p0, GridPosition p1, float p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1644(object p0, int p1, object p2, float p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 __Gen_Delegate_Imp1645(object p0, Vector2 p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 __Gen_Delegate_Imp1646(object p0, int p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1647(object p0, int p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDragButton __Gen_Delegate_Imp1648(object p0, object p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaDefendActor __Gen_Delegate_Imp1649(
      object p0,
      object p1,
      GridPosition p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaDefendActor __Gen_Delegate_Imp1650(object p0, GridPosition p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ArenaDefendActor> __Gen_Delegate_Imp1651(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaActionOrderButton __Gen_Delegate_Imp1652(
      object p0,
      object p1,
      object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1653(object p0, PointerEventData.InputButton p1, Vector2 p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1654(object p0, int p1, object p2, object p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1655(object p0, ulong p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1656(object p0, PeakArenaPanelType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1657(object p0, ArenaUIType p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1658(object p0, ArenaUIType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UISpineGraphic __Gen_Delegate_Imp1659(object p0, object p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1660(object p0, int p1, int p2, GainRewardStatus p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1661(object p0, OfflineArenaPanelType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1662(object p0, object p1, object p2, int p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1663(object p0, int p1, object p2, object p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1664(object p0, OnlineArenaPanelType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaBattleReport __Gen_Delegate_Imp1665(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1666(object p0, out int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1667(object p0, int p1, RealTimePVPMode p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaChangeReportNameUITask __Gen_Delegate_Imp1668(
      ulong p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaSeasonInfo __Gen_Delegate_Imp1669(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1670(object p0, out int p1, out int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffMatchupInfo.MatchState __Gen_Delegate_Imp1671(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffMatchupInfo.PlayerMatchResult __Gen_Delegate_Imp1672(
      object p0,
      object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffMatchupInfo.PlayerMatchResult __Gen_Delegate_Imp1673(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffPlayerInfo __Gen_Delegate_Imp1674(
      object p0,
      object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffPlayerInfo __Gen_Delegate_Imp1675(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaLiveRoomInfo __Gen_Delegate_Imp1676(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffBattleInfo __Gen_Delegate_Imp1677(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1678(object p0, PropertyModifyType p1, int p2, int p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public EquipmentBagItem __Gen_Delegate_Imp1679(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CommonUIStateController> __Gen_Delegate_Imp1680(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataPeakArenaKnockoutMatchTime> __Gen_Delegate_Imp1681(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public EquipmentBagItem __Gen_Delegate_Imp1682(object p0, ulong p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1683(object p0, int p1, int p2, int p3, long p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaSeasonSimpleInfoAck __Gen_Delegate_Imp1684(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaShareReportToBusinessCardUITask __Gen_Delegate_Imp1685(
      object p0,
      ulong p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1686(
      object p0,
      object p1,
      object p2,
      object p3,
      PeakArenaReportListItemUIController.ReportType p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1687(object p0, PeakArenaReportListItemUIController.ReportType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp1688(object p0, bool p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1689(object p0, PeakArenaBattleReportType p1, ulong p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1690(ulong p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BagItemBase> __Gen_Delegate_Imp1691(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1692(object p0, BagListUIController.DisplayType p1, ulong p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<UITaskBase.LayerDesc> __Gen_Delegate_Imp1693(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1694(
      object p0,
      int p1,
      ulong p2,
      BagListUIController.DisplayType p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1695(
      object p0,
      GoodsType p1,
      int p2,
      int p3,
      BagListUIController.DisplayType p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1696(object p0, object p1, BagListUIController.DisplayType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1697(object p0, GoodsType p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1698(object p0, GoodsType p1, int p2, int p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1699(object p0, object p1, StoreId p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1700(object p0, GoodsType p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1701(DialogBoxResult p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SelfSelectedItemBoxUITask __Gen_Delegate_Imp1702(
      StoreId p0,
      int p1,
      SelfSelectedItemBoxUITask.SelfSelectedItemBoxMode p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SelfSelectedItemBoxUITask __Gen_Delegate_Imp1703(int p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1704(
      object p0,
      ref UISpineGraphic p1,
      object p2,
      object p3,
      int p4,
      Vector2 p5,
      float p6,
      object p7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SelfSelectedSkinBoxUITask __Gen_Delegate_Imp1705(
      int p0,
      SelfSelectedSkinBoxUITask.SelfSelectedSkinBoxMode p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp1706(object p0, int p1, int p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UISpineGraphic __Gen_Delegate_Imp1707(
      object p0,
      object p1,
      float p2,
      Vector2 p3,
      object p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1708(object p0, StageActorTagType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IAudioPlayback __Gen_Delegate_Imp1709(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<DialogDetailUITask.LogData> __Gen_Delegate_Imp1710(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleLoseUITask __Gen_Delegate_Imp1711()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1712(int p0, int p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1713(object p0, StagePositionType p1, GridPosition p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1714(object p0, StagePositionType p1, GridPosition p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1715(object p0, GridPosition p1, float p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1716(object p0, BattleType p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1717(object p0, BattleLevelAchievement[] p1, BattleType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1718(
      object p0,
      object p1,
      object p2,
      int p3,
      bool p4,
      bool p5,
      bool p6,
      object p7,
      int p8,
      int p9)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RectTransform __Gen_Delegate_Imp1719(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1720(object p0, object p1, char p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1721(
      object p0,
      object p1,
      int p2,
      StagePositionType p3,
      StageActorTagType p4,
      int p5,
      int p6,
      int p7,
      bool p8)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public StageActorTagType __Gen_Delegate_Imp1722(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public StagePositionType __Gen_Delegate_Imp1723(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1724(object p0, object p1, StageActorTagType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1725(object p0, GridPosition p1, int p2, StagePositionType p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GridPosition> __Gen_Delegate_Imp1726(
      object p0,
      StagePositionType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp1727(object p0, GridPosition p1, StagePositionType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePrepareStageActor __Gen_Delegate_Imp1728(
      object p0,
      object p1,
      GridPosition p2,
      int p3,
      int p4,
      StagePositionType p5,
      StageActorTagType p6,
      int p7,
      int p8,
      int p9)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePrepareStageActor __Gen_Delegate_Imp1729(
      object p0,
      object p1,
      GridPosition p2,
      int p3,
      StagePositionType p4,
      int p5,
      StageActorTagType p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePrepareStageActor __Gen_Delegate_Imp1730(
      object p0,
      GridPosition p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePrepareStageActor __Gen_Delegate_Imp1731(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattlePrepareStageActor> __Gen_Delegate_Imp1732(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1733(object p0, BattleRoomType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1734(object p0, float p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleReportEndUITask __Gen_Delegate_Imp1735(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleReportEndUITask __Gen_Delegate_Imp1736(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1737(
      object p0,
      object p1,
      int p2,
      int p3,
      float p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1738(object p0, object p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleResultBossUITask __Gen_Delegate_Imp1739(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleResultScoreUITask __Gen_Delegate_Imp1740(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataUnchartedScoreInfo __Gen_Delegate_Imp1741()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataUnchartedScoreRewardGroupInfo __Gen_Delegate_Imp1742(
      int p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGuildMassiveCombatLevelInfo __Gen_Delegate_Imp1743()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGuildMassiveCombatIndividualPointsRewardsInfo __Gen_Delegate_Imp1744(
      int p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityInfo __Gen_Delegate_Imp1745()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityScoreRewardGroupInfo __Gen_Delegate_Imp1746(
      int p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1747(
      object p0,
      BattleType p1,
      object p2,
      int p3,
      int p4,
      int p5,
      int p6,
      object p7,
      object p8,
      BattleLevelAchievement[] p9)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1748(
      object p0,
      BattleType p1,
      object p2,
      int p3,
      int p4,
      int p5,
      int p6,
      object p7,
      object p8,
      BattleLevelAchievement[] p9)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1749(
      object p0,
      BattleType p1,
      int p2,
      int p3,
      int p4,
      int p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1750(
      object p0,
      object p1,
      object p2,
      BattleType p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1751(
      object p0,
      object p1,
      BattleLevelAchievement[] p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleResultUITask __Gen_Delegate_Imp1752(
      BattleType p0,
      object p1,
      object p2,
      int p3,
      int p4,
      int p5,
      int p6,
      object p7,
      bool p8,
      object p9,
      BattleLevelAchievement[] p10,
      bool p11)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleTeamPlayerUIController __Gen_Delegate_Imp1753(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1754(object p0, BattleRoomType p1, TimeSpan p2, TimeSpan p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1755(object p0, PlayerBattleStatus p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GameObject __Gen_Delegate_Imp1756(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1757(
      object p0,
      object p1,
      int p2,
      DamageNumberType p3,
      bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 __Gen_Delegate_Imp1758(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1759(
      object p0,
      object p1,
      GridPosition p2,
      object p3,
      GridPosition p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1760(object p0, GridPosition p1, int p2, float p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1761(
      object p0,
      GridPosition p1,
      object p2,
      GridPosition p3,
      float p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1762(object p0, int p1, int p2, ClientBattleActorActionState p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1763(object p0, ActionOrderType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1764(int p0, out Vector2 p1, out float p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1765(object p0, StringTableId p1, float p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1766(
      object p0,
      bool p1,
      bool p2,
      bool p3,
      bool p4,
      bool p5,
      bool p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleReportPlayerPanelUIController __Gen_Delegate_Imp1767(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1768(object p0, bool p1, bool p2, bool p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1769(object p0, TimeSpan p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1770(object p0, int p1, TimeSpan p2, TimeSpan p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1771(object p0, TimeSpan p1, TimeSpan p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallBossBehavioralDesc __Gen_Delegate_Imp1772(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleTeamSetup __Gen_Delegate_Imp1773(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GridPosition> __Gen_Delegate_Imp1774(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1775()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1776(object p0, StringTableId p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1777(object p0, BattleType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataBattleDialogInfo> __Gen_Delegate_Imp1778(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientBattle __Gen_Delegate_Imp1779(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition __Gen_Delegate_Imp1780(object p0, int p1, GridPosition p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MoveType __Gen_Delegate_Imp1781(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1782(object p0, object p1, int p2, GridPosition p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1783(object p0, int p1, TimeSpan p2, TimeSpan p3, TimeSpan p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1784(object p0, int p1, BattleRoomQuitReason p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1785(object p0, BattleLiveRoomFinishReason p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1786(
      object p0,
      BattleType p1,
      int p2,
      object p3,
      bool p4,
      object p5,
      BattleLevelAchievement[] p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1787(object p0, object p1, object p2, GridPosition p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1788(
      object p0,
      object p1,
      object p2,
      int p3,
      int p4,
      DamageNumberType p5,
      int p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1789(object p0, BattleType p1, int p2, int p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1790(
      object p0,
      int p1,
      bool p2,
      object p3,
      int p4,
      bool p5,
      object p6,
      BattleLevelAchievement[] p7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1791(object p0, PlayerBattleStatus p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GridPosition> __Gen_Delegate_Imp1792(object p0, bool p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> __Gen_Delegate_Imp1793(
      object p0,
      object p1,
      int p2,
      BattleType p3,
      int p4,
      bool p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTowerBattleRuleInfo __Gen_Delegate_Imp1794(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTowerBonusHeroGroupInfo __Gen_Delegate_Imp1795(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public StageActorTagType __Gen_Delegate_Imp1796(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition __Gen_Delegate_Imp1797(object p0, StagePositionType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<TrainingTech> __Gen_Delegate_Imp1798(
      object p0,
      int p1,
      int p2,
      bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<RegretStep> __Gen_Delegate_Imp1799(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1800(object p0, object p1, object p2, bool p3, int p4, int p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1801(
      object p0,
      object p1,
      object p2,
      object p3,
      int p4,
      DamageNumberType p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1802(object p0, object p1, out Vector3 p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1803(object p0, int p1, int p2, int p3, int p4, int p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1804(
      object p0,
      int p1,
      bool p2,
      int p3,
      int p4,
      int p5,
      int p6,
      int p7,
      DamageNumberType p8)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBPStagePanelUIController __Gen_Delegate_Imp1805(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1806(object p0, int p1, object p2, bool p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1807(
      object p0,
      object p1,
      object p2,
      PeakArenaBPStageHeroItemState p3,
      bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1808(object p0, int p1, int p2, BPStageCommandType p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1809(
      object p0,
      object p1,
      int p2,
      int p3,
      BPStageCommandType p4,
      bool p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1810(
      object p0,
      object p1,
      int p2,
      int p3,
      BPStageCommandType p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1811(object p0, int p1, object p2, BPStageCommandType p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1812(
      object p0,
      object p1,
      int p2,
      PeakArenaBPStageHeroItemState p3,
      bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1813(object p0, BPStageCommandType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1814(object p0, PeakArenaBPStageHeroItemState p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBPStageHeroItemState __Gen_Delegate_Imp1815(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1816(
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      bool p5,
      bool p6,
      int p7,
      object p8,
      int p9,
      bool p10)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1817(object p0, int p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1818(
      object p0,
      object p1,
      int p2,
      int p3,
      int p4,
      int p5,
      bool p6,
      int p7,
      bool p8)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1819(
      object p0,
      object p1,
      int p2,
      int p3,
      int p4,
      int p5,
      bool p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ARPlaneTrace.EFocusState __Gen_Delegate_Imp1820(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Quaternion __Gen_Delegate_Imp1821(object p0, Vector3 p1, Vector3 p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1822(object p0, Vector3 p1, out Vector3 p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1823(object p0, object p1, HeroActionType p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Renderer __Gen_Delegate_Imp1824(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ARPlaneTrace __Gen_Delegate_Imp1825(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1826(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Texture2D __Gen_Delegate_Imp1827(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1828(object p0, object p1, bool p2, bool p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UISpineGraphic __Gen_Delegate_Imp1829(
      object p0,
      object p1,
      object p2,
      bool p3,
      HeroDirectionType p4,
      HeroActionType p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1830(
      object p0,
      object p1,
      HeroDirectionType p2,
      HeroActionType p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1831(
      object p0,
      ARPoint p1,
      ARHitTestResultType p2,
      out Vector3 p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMusicAppreciateTable __Gen_Delegate_Imp1832(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataBigExpressionInfo> __Gen_Delegate_Imp1833(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp1834(object p0, object p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1835(
      ulong p0,
      object p1,
      out ChatBagItemMessage.ChatBagItemData p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1836(object p0, object p1, object p2, out string p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp1837(object p0, DateTime p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CommonUIStateController __Gen_Delegate_Imp1838(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1839(object p0, ChatChannel p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1840(
      object p0,
      ChatChannel p1,
      byte[] p2,
      int p3,
      int p4,
      int p5,
      object p6,
      object p7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1841(object p0, ChatChannel p1, object p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1842(object p0, ChatChannel p1, object p2, ulong p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1843(
      object p0,
      object p1,
      ChatChannel p2,
      bool p3,
      bool p4,
      bool p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1844(object p0, ChatChannel p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Button __Gen_Delegate_Imp1845(object p0, ChatChannel p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatComponent.ChatMessageClient __Gen_Delegate_Imp1846(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatUITask __Gen_Delegate_Imp1847(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatUITask __Gen_Delegate_Imp1848(ChatChannel p0, object p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1849(ulong p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1850(object p0, ChatUITask.PipeLineStateMaskType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1851(object p0, ChatUITask.PipeLineStateMaskType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ChatComponent.ChatMessageClient> __Gen_Delegate_Imp1852(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ulong> __Gen_Delegate_Imp1853(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1854(object p0, float[] p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerVoiceHandleThread.VoicePacket __Gen_Delegate_Imp1855(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public byte[] __Gen_Delegate_Imp1856(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerVoiceHandleThread __Gen_Delegate_Imp1857()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public byte[] __Gen_Delegate_Imp1858(object p0, float[] p1, VoiceChatCompressionType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float[] __Gen_Delegate_Imp1859(
      object p0,
      byte[] p1,
      int p2,
      VoiceChatCompressionType p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1860(object p0, float[] p1, short[] p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1861(object p0, short[] p1, float[] p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float[] __Gen_Delegate_Imp1862(object p0, byte[] p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public byte[] __Gen_Delegate_Imp1863(object p0, float[] p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public VoiceChatCompression __Gen_Delegate_Imp1864()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatVoiceMessage __Gen_Delegate_Imp1865(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1866(object p0, ChatChannel p1, ulong p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public VoiceRecordHelper __Gen_Delegate_Imp1867()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1868(
      object p0,
      object p1,
      int p2,
      float p3,
      float p4,
      float p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ExpressionParseController.PosStringTuple> __Gen_Delegate_Imp1869(
      object p0,
      object p1,
      out string p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public VoiceRecordUITask __Gen_Delegate_Imp1870(object p0, ChatChannel p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1871(object p0, bool p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1872(
      object p0,
      int p1,
      object p2,
      object p3,
      bool p4,
      bool p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1873(object p0, bool p1, bool p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityMission __Gen_Delegate_Imp1874(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityCurrency __Gen_Delegate_Imp1875(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityPage __Gen_Delegate_Imp1876(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityItem __Gen_Delegate_Imp1877(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityExchangePageUIController __Gen_Delegate_Imp1878(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionAcitivityExchangeConsumeItemUIController __Gen_Delegate_Imp1879(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1880(object p0, ulong p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1881(object p0, object p1, bool p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityRecommendHeroUIController __Gen_Delegate_Imp1882(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1883(object p0, object p1, bool p2, int p3, bool p4, int p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DynamicGridCellController __Gen_Delegate_Imp1884(
      object p0,
      object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1885(object p0, StringTableId p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityScenarioLevelInfo __Gen_Delegate_Imp1886(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityChallengeLevelInfo __Gen_Delegate_Imp1887(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityLootLevelInfo __Gen_Delegate_Imp1888(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1889(object p0, object p1, bool p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ChestUITask __Gen_Delegate_Imp1890(object p0, bool p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1891(object p0, object p1, float p2, object p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1892(
      object p0,
      object p1,
      float p2,
      object p3,
      bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1893(object p0, StringTableId p1, float p2, object p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1894(object p0, int p1, float p2, object p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1895(
      object p0,
      StringTableId p1,
      object p2,
      StringTableId p3,
      StringTableId p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1896(object p0, ExplanationId p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1897(object p0, FadeStyle p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1898(object p0, object p1, FadeStyle p2, float p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1899(object p0, object p1, FadeStyle p2, float p3, float p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1900(
      object p0,
      object p1,
      FadeStyle p2,
      float p3,
      float p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1901(object p0, float p1, Color p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1902(object p0, object p1, float p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CommonUIController __Gen_Delegate_Imp1903()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1904(object p0, object p1, object p2, LogType p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1905(object p0, PracticeMode p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp1906(object p0, BattleType p1, int p2, ulong p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1907(int p0, ulong p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GetPathData __Gen_Delegate_Imp1908(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GetPathUITask __Gen_Delegate_Imp1909(
      GoodsType p0,
      int p1,
      object p2,
      int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GetRewardGoodsUITask __Gen_Delegate_Imp1910(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public InstructionUITask __Gen_Delegate_Imp1911(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1912(
      object p0,
      float p1,
      float p2,
      object p3,
      object p4,
      object p5,
      bool p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MatchingNowUITask __Gen_Delegate_Imp1913(DateTime p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MonthCardInfoUITask __Gen_Delegate_Imp1914(int p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerLevelUpUITask __Gen_Delegate_Imp1915(int p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Rect __Gen_Delegate_Imp1916(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1917(
      object p0,
      Vector3 p1,
      PlayerSimpleInfoUITask.PostionType p2,
      object p3,
      bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerSimpleInfoUITask __Gen_Delegate_Imp1918(
      Vector3 p0,
      PlayerSimpleInfoUITask.PostionType p1,
      object p2,
      bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1919(object p0, object p1, bool p2, object p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RecommendHeroUITask __Gen_Delegate_Imp1920(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1921(object p0, GoodsType p1, int p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1922(
      object p0,
      object p1,
      GoodsType p2,
      int p3,
      int p4,
      object p5,
      bool p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1923(object p0, object p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RewardGoodsDescUITask __Gen_Delegate_Imp1924(
      object p0,
      GoodsType p1,
      int p2,
      int p3,
      object p4,
      bool p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RewardGoodsUIController __Gen_Delegate_Imp1925(
      object p0,
      object p1,
      object p2,
      bool p3,
      int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1926(
      object p0,
      object p1,
      object p2,
      object p3,
      bool p4,
      int p5,
      bool p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp1927(object p0, float p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UISpineGraphic __Gen_Delegate_Imp1928(
      object p0,
      object p1,
      float p2,
      Vector2 p3,
      int p4,
      object p5,
      object p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1929(object p0, int p1, PracticeMode p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1930(object p0, object p1, int p2, PracticeMode p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1931(object p0, Color p1, float p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1932(
      object p0,
      Color p1,
      Color p2,
      float p3,
      object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1933(object p0, object p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1934(object p0, float p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1935(object p0, ref string p1, object p2, object p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1936(
      object p0,
      DialogDetailUITask.LogData.LogDataType p1,
      object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DialogDetailCharDialogInfoUIController __Gen_Delegate_Imp1937(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DialogDetailChoiceUIController __Gen_Delegate_Imp1938(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1939(object p0, int p1, object p2, int p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1940(object p0, int p1, TimeSpan p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1941(object p0, int p1, int p2, ArmyTag p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEternalShrineLevelInfo __Gen_Delegate_Imp1942(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroTagInfo __Gen_Delegate_Imp1943(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UITaskBase __Gen_Delegate_Imp1944()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1945(object p0, int p1, int p2, bool p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroFetterInfo __Gen_Delegate_Imp1946(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1947(object p0, GoodsType p1, int p2, int p3, out bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1948(
      object p0,
      object p1,
      int p2,
      HeroInteractionResultType p3,
      int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1949(object p0, int p1, int p2, float p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1950(object p0, int p1, GoodsType p2, int p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1951(object p0, HeroInteractionResultType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroInteractionResultType __Gen_Delegate_Imp1952(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1953(object p0, GoodsType p1, int p2, int p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1954(object p0, FriendSocialRelationFlag p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1955(object p0, object p1, PracticeMode p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1956(
      object p0,
      object p1,
      object p2,
      FriendInfoType p3,
      bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1957(object p0, object p1, object p2, FriendInfoType p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1958(object p0, object p1, FriendInfoType p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerSimpleInfoUITask.PostionType __Gen_Delegate_Imp1959(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1960(object p0, object p1, FriendInfoType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1961(object p0, FriendPanelType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1962(
      object p0,
      object p1,
      Vector3 p2,
      PlayerSimpleInfoUITask.PostionType p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1963(
      object p0,
      Vector3 p1,
      PlayerSimpleInfoUITask.PostionType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1964(object p0, object p1, object p2, bool p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1965(
      object p0,
      object p1,
      float p2,
      Vector2 p3,
      object p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1966(object p0, int p1, GoodsType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1967(
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5,
      GoodsType p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildJoinInvitation __Gen_Delegate_Imp1968(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1969(object p0, GuildManagementUIController.GuildListSortType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp1970(object p0, GuildTitle p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildManagementUITask __Gen_Delegate_Imp1971(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1972(object p0, bool p1, int p2, object p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Hero> __Gen_Delegate_Imp1973(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1974(object p0, bool p1, object p2, float p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1975(object p0, bool p1, object p2, float p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1976(object p0, object p1, bool p2, float p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1977(
      object p0,
      object p1,
      object p2,
      object p3,
      bool p4,
      int p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1978(object p0, bool p1, int p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1979(object p0, StoreId p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1980(object p0, StoreId p1, int? p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1981(object p0, StoreId p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1982(object p0, object p1, GoodsType p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public NormalItemBuyUITask __Gen_Delegate_Imp1983(StoreId p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GameObject __Gen_Delegate_Imp1984(object p0, object p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1985(object p0, object p1, EquipMasterEquipType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1986(object p0, object p1, PropertyModifyType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1987(
      object p0,
      object p1,
      EquipMasterEquipType p2,
      int p3,
      object p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1988(
      object p0,
      object p1,
      object p2,
      PropertyModifyType p3,
      int p4,
      int p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp1989(
      object p0,
      object p1,
      object p2,
      PropertyModifyType p3,
      int p4,
      int p5,
      object p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1990(
      object p0,
      EquipMasterRefineOpenPanelUIController.TempPropertyCancelType p1,
      object p2,
      object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1991(object p0, bool p1, EquipMasterEquipPropertyFilterType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<RefineryStoneBagItem> __Gen_Delegate_Imp1992(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RefineryStoneBagItem __Gen_Delegate_Imp1993(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp1994(EquipMasterEquipType p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<PropertyModifyType, int> __Gen_Delegate_Imp1995(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp1996(int p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1997(object p0, EquipMasterEquipType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1998(object p0, EquipMasterEquipType p1, int p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp1999(object p0, PropertyModifyType p1, int p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2000(object p0, int p1, ulong p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2001(
      object p0,
      EquipmentForgeUIController.ForgeState p1,
      int p2,
      ulong p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2002(
      object p0,
      PropertyModifyType p1,
      int p2,
      int p3,
      int p4,
      int p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp2003(
      object p0,
      object p1,
      object p2,
      int p3,
      PropertyModifyType p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float __Gen_Delegate_Imp2004(object p0, PropertyModifyType p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2005(object p0, ulong p1, object p2, object p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2006(object p0, ulong p1, ulong p2, object p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2007(
      object p0,
      ulong p1,
      int p2,
      EquipmentForgeUIController.ForgeState p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2008(object p0, ulong p1, ulong p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2009(
      object p0,
      ulong p1,
      EquipmentForgeUIController.ForgeState p2,
      object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2010(
      object p0,
      object p1,
      HeroCharUIController.PerformanceState p2,
      bool p3,
      object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2011(
      object p0,
      object p1,
      HeroCharUIController.PerformanceState p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float __Gen_Delegate_Imp2012(object p0, int p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IAudioPlayback __Gen_Delegate_Imp2013(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float __Gen_Delegate_Imp2014(object p0, int p1, float p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float __Gen_Delegate_Imp2015(object p0, bool p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp2016(
      object p0,
      int p1,
      object p2,
      HeroCharUIController.PerformanceState p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<HeroCommentEntry> __Gen_Delegate_Imp2017(
      object p0,
      object p1,
      object p2,
      int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<HeroCommentEntry> __Gen_Delegate_Imp2018(
      object p0,
      object p1,
      int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2019(object p0, ref UISpineGraphic p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp2020(object p0, float p1, float p2, float p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2021(object p0, object p1, float p2, float p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2022(object p0, object p1, object p2, bool p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2023(
      ref UISpineGraphic p0,
      object p1,
      object p2,
      int p3,
      Vector2 p4,
      float p5,
      object p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2024(ref UISpineGraphic p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp2025(object p0, int p1, int p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2026(
      object p0,
      ref UISpineGraphic p1,
      object p2,
      object p3,
      float p4,
      object p5,
      int p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2027(
      object p0,
      ref UISpineGraphic p1,
      object p2,
      object p3,
      float p4,
      int p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataJobUnlockConditionInfo __Gen_Delegate_Imp2028(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp2029(object p0, float p1, float p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UITaskBase.LayerDesc __Gen_Delegate_Imp2030(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2031(object p0, int p1, bool p2, bool p3, int p4, bool p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2032(object p0, HeroListUIController.HeroSortType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2033(object p0, int p1, object p2, SkillAndSoldierSelectMode p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2034(object p0, int p1, int p2, SkillAndSoldierSelectMode p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2035(object p0, ulong p1, ulong p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2036(object p0, int p1, int p2, int p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2037(
      object p0,
      object p1,
      int p2,
      int p3,
      int p4,
      int p5,
      int p6,
      int p7,
      int p8)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataSkillInfo> __Gen_Delegate_Imp2038(
      object p0,
      ulong[] p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataSkillInfo> __Gen_Delegate_Imp2039(
      object p0,
      ulong[] p1,
      object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2040(object p0, int p1, int p2, object p3, int p4, object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroSkinInfo __Gen_Delegate_Imp2041(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroSkinChangeUITask __Gen_Delegate_Imp2042(
      object p0,
      StoreId p1,
      int p2,
      object p3,
      object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSoldierSkinInfo __Gen_Delegate_Imp2043(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroSoldierSkinUITask __Gen_Delegate_Imp2044(
      object p0,
      bool p1,
      object p2,
      object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroAnthemLevelInfo __Gen_Delegate_Imp2045(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2046(
      object p0,
      int p1,
      bool p2,
      object p3,
      bool p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroAnthemInfo __Gen_Delegate_Imp2047(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2048(object p0, int p1, int p2, object p3, bool p4, object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Vector2> __Gen_Delegate_Imp2049(
      object p0,
      double p1,
      double p2,
      double p3,
      double p4,
      double p5,
      double p6,
      double p7,
      double p8)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp2050(
      object p0,
      double p1,
      double p2,
      double p3,
      double p4,
      double p5,
      double p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 __Gen_Delegate_Imp2051(
      object p0,
      Vector2 p1,
      Vector2 p2,
      Vector2 p3,
      Vector2 p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroDungeonLevelInfo __Gen_Delegate_Imp2052(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2053(object p0, BattleLevelAchievement[] p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroTrainningLevelInfo __Gen_Delegate_Imp2054(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2055(object p0, StringTableId p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerInfoInitNetTask.ResultState __Gen_Delegate_Imp2056(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2057(object p0, StringTableId p1, int p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LoginAnnouncement.AnnounceType __Gen_Delegate_Imp2058(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2059(object p0, LoginAnnouncement.AnnounceType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2060(
      object p0,
      LoginAnnouncement.AnnounceType p1,
      object p2,
      object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<LoginUITask.ServerInfo> __Gen_Delegate_Imp2061()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LoginUITask.ServerInfo __Gen_Delegate_Imp2062(int p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp2063()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LoginUITask.ServerInfo __Gen_Delegate_Imp2064()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp2065(object p0, int p1, object p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp2066(object p0, bool p1, float p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2067(LoginSuccessMsg p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2068(object p0, LoginSuccessMsg p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp2069(
      object p0,
      object p1,
      ref string p2,
      ref string p3,
      ref string p4,
      ref string p5,
      ref string p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp2070(
      object p0,
      object p1,
      object p2,
      object p3,
      object p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LoginUITask __Gen_Delegate_Imp2071()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2072(bool p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2073(
      object p0,
      object p1,
      int p2,
      int p3,
      int p4,
      int p5,
      object p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2074(
      object p0,
      int p1,
      int p2,
      object p3,
      LoginUITask.ServerInfo.State p4,
      bool p5,
      object p6,
      object p7,
      int p8,
      object p9,
      int p10,
      int p11,
      float p12,
      object p13,
      int p14,
      bool p15,
      object p16,
      bool p17,
      object p18)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public long __Gen_Delegate_Imp2075(DateTime p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UserBindUITask __Gen_Delegate_Imp2076(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp2077(object p0, MailUITask.PipeLineStateMaskType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2078(object p0, MailUITask.PipeLineStateMaskType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMemoryCorridorLevelInfo __Gen_Delegate_Imp2079(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2080(
      object p0,
      object p1,
      object p2,
      ChatChannel p3,
      ChatContentType p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2081(object p0, object p1, ChatChannel p2, ChatContentType p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2082(object p0, bool p1, bool? p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp2083(object p0, object p1, ChatChannel p2, ChatSrcType p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2084(object p0, int p1, bool p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2085(object p0, PlayerInfoHeadIconPanelType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerInfoHeadIconPanelType __Gen_Delegate_Imp2086(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2087(object p0, UIGroup p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2088(UIGroup p0, UIGroup p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2089(object p0, UIGroup4LowMem p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2090(UIGroup4LowMem p0, UIGroup4LowMem p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public StringTableId __Gen_Delegate_Imp2091(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CommonUIStateController __Gen_Delegate_Imp2092(object p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp2093(object p0, RaffleUITask.PipeLineStateMaskType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2094(object p0, RaffleUITask.PipeLineStateMaskType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2095(
      object p0,
      RankingListType p1,
      int p2,
      int p3,
      int p4,
      object p5,
      int p6,
      object p7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2096(object p0, int p1, int p2, int p3, object p4, int p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2097(object p0, RankingListType p1, object p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2098(object p0, RankingListType p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2099(object p0, RankingListType p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingUIController.MainMenuSelectState __Gen_Delegate_Imp2100(
      object p0,
      RankingListType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2101(object p0, RankingUIController.MainMenuSelectState p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2102(object p0, object p1, RankingListType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp2103(object p0, RankingUITask.PipeLineStateMaskType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2104(object p0, RankingUITask.PipeLineStateMaskType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGiftStoreItemInfo __Gen_Delegate_Imp2105()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2106(object p0, RiftLevelType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2107(
      object p0,
      object p1,
      int p2,
      int p3,
      int p4,
      int p5,
      int p6,
      int p7,
      int p8,
      int p9,
      int p10,
      int p11,
      int p12,
      int p13)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2108(object p0, RiftChapterUnlockConditionType p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2109(object p0, int p1, GainRewardStatus p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2110(
      object p0,
      object p1,
      RiftLevelStatus p2,
      int p3,
      int p4,
      bool p5,
      int p6,
      int p7,
      int p8,
      int p9)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2111(object p0, object p1, RiftLevelStatus p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRiftLevelInfo __Gen_Delegate_Imp2112(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2113(object p0, RiftLevelStatus p1, bool p2, bool p3, bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftLevelStatus __Gen_Delegate_Imp2114(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2115(object p0, RiftLevelStatus p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2116(object p0, GainRewardStatus p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GainRewardStatus __Gen_Delegate_Imp2117(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2118(object p0, RiftLevelUnlockConditionType p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2119(object p0, bool p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp2120(
      object p0,
      object p1,
      bool p2,
      int p3,
      int p4,
      int p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp2121(object p0, object p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2122(object p0, object p1, ref UISpineGraphic p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp2123(object p0, EquipmentType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public EquipmentBuyUITask __Gen_Delegate_Imp2124(StoreId p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GoodsUIController __Gen_Delegate_Imp2125(
      object p0,
      object p1,
      object p2,
      bool p3,
      object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2126(PDSDKGood p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<PDSDKGood> __Gen_Delegate_Imp2127()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2128(object p0, StoreType p1, int p2, int p3, PDSDKGoodType p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2129(object p0, object p1, PDSDKGoodType p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2130(
      object p0,
      int p1,
      StoreType p2,
      bool p3,
      double p4,
      object p5,
      int p6,
      object p7,
      bool p8,
      object p9)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2131(
      object p0,
      object p1,
      double p2,
      PDSDKGoodType p3,
      object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2132(object p0, double p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Image __Gen_Delegate_Imp2133(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Text __Gen_Delegate_Imp2134(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PDSDKGoodType __Gen_Delegate_Imp2135(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public StoreHeroSkinItemUIController __Gen_Delegate_Imp2136(
      object p0,
      int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2137(object p0, StoreId p1, StoreType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2138(object p0, StoreId p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2139(object p0, StoreId p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public StoreId __Gen_Delegate_Imp2140(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp2141(object p0, StoreId p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2142(object p0, StoreId p1, int p2, int p3, int p4, object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2143(
      object p0,
      object p1,
      ref UISpineGraphic p2,
      object p3,
      object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public StoreSoldierSkinDetailUITask __Gen_Delegate_Imp2144(
      StoreId p0,
      int p1,
      StoreSoldierSkinDetailUITask.StartTaskFromType p2,
      object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2145(object p0, StoreId p1, int? p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2146(object p0, PDSDKGoodType p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PDSDKGood __Gen_Delegate_Imp2147(int p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataStoreCurrencyInfo __Gen_Delegate_Imp2148(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public StoreType __Gen_Delegate_Imp2149(object p0, StoreId p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2150(object p0, StoreId p1, int p2, int p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2151(object p0, StoreId p1, int p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2152(object p0, StoreType p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2153(object p0, object p1, StoryUITask.StateType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2154(object p0, StoryUITask.StateType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 __Gen_Delegate_Imp2155(object p0, float p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float __Gen_Delegate_Imp2156(object p0, float p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2157(object p0, int p1, StoryUITask.StateType p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2158(
      object p0,
      object p1,
      GameFunctionType p2,
      int p3,
      bool p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<TeamRoom> __Gen_Delegate_Imp2159(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2160(object p0, GameFunctionType p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2161(object p0, int p1, GameFunctionType p2, int p3, ulong p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2162(object p0, object p1, TeamRoomInviteeInfoType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Transform __Gen_Delegate_Imp2163(object p0, int p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2164(object p0, TeamRoomInvitePlayerType p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2165(object p0, object p1, object p2, int p3, int p4, bool p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2166(object p0, object p1, TeamRoomPlayerInviteState p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2167(object p0, TeamRoomPlayerInviteState p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomInviteUITask.OnlinePlayerStatus __Gen_Delegate_Imp2168(
      object p0,
      object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2169(object p0, TeamRoomInvitePlayerType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamGameFunctionTypeListItemUIController __Gen_Delegate_Imp2170(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamLocationListItemUIController __Gen_Delegate_Imp2171(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp2172(GameFunctionType p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp2173(GameFunctionType p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UIIntentReturnable __Gen_Delegate_Imp2174(
      object p0,
      GameFunctionType p1,
      int p2,
      int p3,
      int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public byte[] __Gen_Delegate_Imp2175(byte[] p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp2176(byte[] p0, ref byte[] p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GMManager __Gen_Delegate_Imp2177()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GmCommandBase __Gen_Delegate_Imp2178(object p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp2179(object p0, ref int p1, ref int p2, float p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp2180(
      object p0,
      ref int p1,
      ref int p2,
      float p3,
      object p4,
      bool p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp2181(object p0, ref int p1, ref int p2, float p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2182(object p0, int p1, ref int p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2183(object p0, ref int p1, ref int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp2184(TestUI.ObjectAndSize p0, TestUI.ObjectAndSize p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 __Gen_Delegate_Imp2185(object p0, Vector2i p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2186(object p0, TestListType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TestListItemUIController __Gen_Delegate_Imp2187(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp2188(bool p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2189(object p0, bool p1, object p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2190(object p0, bool p1, TestListType p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp2191(int p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp2192(int p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp2193(ulong p0, object p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp2194(
      int p0,
      int p1,
      int p2,
      int p3,
      int p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp2195(int p0, int p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp2196(ulong p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> __Gen_Delegate_Imp2197(int p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp2198(ulong p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp2199(ulong p0, ulong p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp2200(GoodsType p0, int p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp2201(int p0, GoodsType p1, int p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp2202(object p0, double p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataThearchyTrialLevelInfo __Gen_Delegate_Imp2203(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTreasureLevelInfo __Gen_Delegate_Imp2204(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public char __Gen_Delegate_Imp2205(object p0, object p1, int p2, char p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2206(object p0, float p1, object p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UITaskBase __Gen_Delegate_Imp2207(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 __Gen_Delegate_Imp2208(Vector3 p0, object p1, object p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 __Gen_Delegate_Imp2209(Vector2 p0, object p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp2210(object p0, Color p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp2211(TimeSpan p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp2212(DateTime p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp2213(TimeSpan p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp2214(TweenMain[] p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2215(TweenMain[] p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp2216(char p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp2217(GoodsType p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp2218(GoodsType p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp2219(GoodsType p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp2220(GuildTitle p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp2221(int p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp2222(GoodsType p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCharImageInfo __Gen_Delegate_Imp2223(int p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GameObject __Gen_Delegate_Imp2224(object p0, int p1, bool p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp2225(object p0, int p1, ref string p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp2226(int p0, bool p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroInformationInfo __Gen_Delegate_Imp2227(int p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroInformationInfo __Gen_Delegate_Imp2228(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp2229(bool p0, DateTime p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp2230(GameFunctionType p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PDSDKGoodType __Gen_Delegate_Imp2231(object p0, bool p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PDSDKGoodType __Gen_Delegate_Imp2232(
      double p0,
      double p1,
      bool p2,
      bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp2233(bool p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2234(int p0, int p1, int p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float __Gen_Delegate_Imp2235(object p0, object p1, float p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp2236(PropertyModifyType p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp2237(BattleType p0, int p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2238(object p0, Vector3 p1, Vector2 p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UnchartedBlessingDescUITask __Gen_Delegate_Imp2239(
      int p0,
      Vector3 p1,
      Vector3 p2,
      BattleType p3,
      int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityInfo __Gen_Delegate_Imp2240(
      object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2241(object p0, BattleType p1, int p2, bool p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2242(object p0, BattleType p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2243(
      object p0,
      object p1,
      int p2,
      int p3,
      object p4,
      int p5,
      bool p6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataScoreLevelInfo __Gen_Delegate_Imp2244(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataChallengeLevelInfo __Gen_Delegate_Imp2245(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UserGuideSetNetTask __Gen_Delegate_Imp2246(int p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2247(object p0, Vector2 p1, Vector2 p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GameObject __Gen_Delegate_Imp2248(object p0, Vector2 p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp2249(object p0, UserGuideTrigger p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp2250(UserGuideTrigger p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp2251(
      UserGuideTrigger p0,
      object p1,
      UserGuideTrigger p2,
      object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp2252(UserGuideCondition p0, object p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2253(int p0, int p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2254(object p0, object p1, out Action<GameObject> p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp2255(object p0, ref Vector2 p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp2256(Vector2 p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2257(object p0, string[] p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string __Gen_Delegate_Imp2258(object p0, string[] p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp2259(object p0, UserGuideAction p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp2260(UserGuideTrigger p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BuyEnergyUITask __Gen_Delegate_Imp2261(bool p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public EnergyStatusUITask __Gen_Delegate_Imp2262(Vector3 p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2263(object p0, EPeakArenaDurringStateType p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2264(bool p0, bool p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorld __Gen_Delegate_Imp2265(object p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2266(BattleType p0, int p1, ulong p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp2267(object p0, BattleType p1, int p2, ulong p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2268(object p0, int p1, object p2, bool p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2269(object p0, object p1, FadeStyle p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2270(object p0, BagListUIController.DisplayType p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2271(object p0, ArenaUIType p1, int p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2272(object p0, PeakArenaPanelType p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2273(object p0, ArenaUIType p1, int p2, object p3, int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2274(object p0, object p1, PeakArenaPanelType p2, int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2275(object p0, GetPathType p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2276(
      object p0,
      GameFunctionType p1,
      int p2,
      int p3,
      int p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2277(
      object p0,
      BattleType p1,
      int p2,
      int p3,
      bool p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2278(BattleType p0, int p1, bool p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2279(object p0, BattleType p1, int p2, bool p3, object p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2280(
      object p0,
      object p1,
      bool p2,
      bool p3,
      object p4,
      object p5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2281(object p0, object p1, BattleType p2, object p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2282(
      object p0,
      object p1,
      PeakArenaBattleReportSourceType p2,
      int p3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void __Gen_Delegate_Imp2283(
      object p0,
      object p1,
      object p2,
      PeakArenaBattleReportSourceType p3,
      int p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int __Gen_Delegate_Imp2284(object p0, ref FadeStyle p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<string, string[]> __Gen_Delegate_Imp2285()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp2286(string[] p0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Texture2D __Gen_Delegate_Imp2287(object p0, ImageFilterMode p1, float p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator<object> __Gen_Delegate_Imp2288(object p0, object p1, object p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GameObject __Gen_Delegate_Imp2289(object p0, object p1, bool p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator __Gen_Delegate_Imp2290(object p0, long p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool __Gen_Delegate_Imp2291(object p0, object p1, out int p2)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
