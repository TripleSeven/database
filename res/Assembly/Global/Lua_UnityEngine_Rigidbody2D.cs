﻿// Decompiled with JetBrains decompiler
// Type: Lua_UnityEngine_Rigidbody2D
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: CFE0B0F1-D1DD-4FFD-A8A2-4765B2FA0AD7
// Assembly location: D:\User\Desktop\ldlls\pe000a423904.dll

using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

[Preserve]
public class Lua_UnityEngine_Rigidbody2D : LuaObject
{
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int GetAttachedColliders(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int OverlapCollider(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int Cast(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int GetContacts(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int MovePosition(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int MoveRotation(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int IsSleeping(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int IsAwake(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int Sleep(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int WakeUp(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int IsTouching(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int IsTouchingLayers(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int OverlapPoint(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int Distance(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int AddForce(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int AddRelativeForce(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int AddForceAtPosition(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int AddTorque(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int GetPoint(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int GetRelativePoint(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int GetVector(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int GetRelativeVector(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int GetPointVelocity(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int GetRelativePointVelocity(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_position(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_position(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_rotation(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_rotation(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_velocity(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_velocity(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_angularVelocity(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_angularVelocity(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_useAutoMass(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_useAutoMass(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_mass(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_mass(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_sharedMaterial(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_sharedMaterial(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_centerOfMass(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_centerOfMass(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_worldCenterOfMass(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_inertia(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_inertia(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_drag(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_drag(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_angularDrag(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_angularDrag(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_gravityScale(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_gravityScale(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_bodyType(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_bodyType(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_useFullKinematicContacts(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_useFullKinematicContacts(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_isKinematic(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_isKinematic(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_freezeRotation(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_freezeRotation(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_constraints(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_constraints(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_simulated(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_simulated(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_interpolation(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_interpolation(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_sleepMode(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_sleepMode(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_collisionDetectionMode(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_collisionDetectionMode(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_attachedColliderCount(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static void reg(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }
}
