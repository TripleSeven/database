﻿// Decompiled with JetBrains decompiler
// Type: SLua.LuaDelegation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 972A2846-EBE4-4323-8745-F884A18FA509
// Assembly location: D:\Games\Langrisser5\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using Assets.Script.ProjectL.Runtime.UI.Activity;
using BlackJack.BJFramework.Runtime.Lua;
using BlackJack.BJFramework.Runtime.PlayerContext;
using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.Resource;
using BlackJack.BJFramework.Runtime.Scene;
using BlackJack.BJFramework.Runtime.TaskNs;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.AR;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Protocol;
using BlackJack.ProjectL.Scene;
using BlackJack.ProjectL.UI;
using BlackJack.ProjectLBasic;
using FixMath.NET;
using PD.SDK;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.XR.iOS;

namespace SLua
{
  public class LuaDelegation : LuaObject
  {
    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_BJFramework_Runtime_Scene_SceneLayerBase(
      LuaFunction ld,
      SceneLayerBase a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_BJFramework_Runtime_TaskNs_Task(
      LuaFunction ld,
      Task a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_BJFramework_Runtime_UI_UIControllerBase(
      LuaFunction ld,
      UIControllerBase a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_BJFramework_Runtime_UI_UITaskBase(
      LuaFunction ld,
      UITaskBase a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_bool(LuaFunction ld, bool a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_Common_BagItemBase(
      LuaFunction ld,
      BagItemBase a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_Common_BattleHero(
      LuaFunction ld,
      BattleHero a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_Common_BattleType(
      LuaFunction ld,
      BattleType a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_Common_BusinessCard(
      LuaFunction ld,
      BusinessCard a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_Common_ChatChannel(
      LuaFunction ld,
      ChatChannel a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_Common_ChatPeakArenaBattleReportMessage(
      LuaFunction ld,
      ChatPeakArenaBattleReportMessage a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_Common_EquipmentBagItem(
      LuaFunction ld,
      EquipmentBagItem a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_Common_Hero(LuaFunction ld, Hero a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_Common_OperationalActivityBase(
      LuaFunction ld,
      OperationalActivityBase a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_Common_RealTimePVPMode(
      LuaFunction ld,
      RealTimePVPMode a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ConfigData_ConfigDataAnikiGymInfo(
      LuaFunction ld,
      ConfigDataAnikiGymInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ConfigData_ConfigDataAnikiLevelInfo(
      LuaFunction ld,
      ConfigDataAnikiLevelInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ConfigData_ConfigDataChallengeLevelInfo(
      LuaFunction ld,
      ConfigDataChallengeLevelInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ConfigData_ConfigDataCollectionActivityChallengeLevelInfo(
      LuaFunction ld,
      ConfigDataCollectionActivityChallengeLevelInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ConfigData_ConfigDataCollectionActivityLootLevelInfo(
      LuaFunction ld,
      ConfigDataCollectionActivityLootLevelInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ConfigData_ConfigDataCollectionActivityScenarioLevelInfo(
      LuaFunction ld,
      ConfigDataCollectionActivityScenarioLevelInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ConfigData_ConfigDataCooperateBattleLevelInfo(
      LuaFunction ld,
      ConfigDataCooperateBattleLevelInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ConfigData_ConfigDataEternalShrineLevelInfo(
      LuaFunction ld,
      ConfigDataEternalShrineLevelInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ConfigData_ConfigDataHeroAnthemInfo(
      LuaFunction ld,
      ConfigDataHeroAnthemInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ConfigData_ConfigDataHeroAnthemLevelInfo(
      LuaFunction ld,
      ConfigDataHeroAnthemLevelInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ConfigData_ConfigDataHeroDungeonLevelInfo(
      LuaFunction ld,
      ConfigDataHeroDungeonLevelInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ConfigData_ConfigDataHeroPhantomLevelInfo(
      LuaFunction ld,
      ConfigDataHeroPhantomLevelInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ConfigData_ConfigDataHeroTrainningLevelInfo(
      LuaFunction ld,
      ConfigDataHeroTrainningLevelInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ConfigData_ConfigDataJobConnectionInfo(
      LuaFunction ld,
      ConfigDataJobConnectionInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ConfigData_ConfigDataMemoryCorridorLevelInfo(
      LuaFunction ld,
      ConfigDataMemoryCorridorLevelInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ConfigData_ConfigDataRiftChapterInfo(
      LuaFunction ld,
      ConfigDataRiftChapterInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ConfigData_ConfigDataScoreLevelInfo(
      LuaFunction ld,
      ConfigDataScoreLevelInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ConfigData_ConfigDataSoldierInfo(
      LuaFunction ld,
      ConfigDataSoldierInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ConfigData_ConfigDataST_CN(
      LuaFunction ld,
      ConfigDataST_CN a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ConfigData_ConfigDataST_EN(
      LuaFunction ld,
      ConfigDataST_EN a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ConfigData_ConfigDataThearchyTrialLevelInfo(
      LuaFunction ld,
      ConfigDataThearchyTrialLevelInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ConfigData_ConfigDataTreasureLevelInfo(
      LuaFunction ld,
      ConfigDataTreasureLevelInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ConfigData_GetPathData(
      LuaFunction ld,
      GetPathData a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ConfigData_Goods(LuaFunction ld, Goods a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_int(LuaFunction ld, int a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_List_BlackJack_ProjectL_Common_BusinessCardHeroSet_(
      LuaFunction ld,
      List<BusinessCardHeroSet> a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_List_BlackJack_ProjectL_Common_GuildLog_(
      LuaFunction ld,
      List<GuildLog> a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_List_BlackJack_ProjectL_Common_UserSummary_(
      LuaFunction ld,
      List<UserSummary> a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_List_BlackJack_ConfigData_Goods_(
      LuaFunction ld,
      List<Goods> a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_List_System_Int32_(LuaFunction ld, List<int> a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_List_System_String_(LuaFunction ld, List<string> a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_PD_SDK_LoginSuccessMsg(
      LuaFunction ld,
      LoginSuccessMsg a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_PlayerContext_ChatComponent_ChatMessageClient(
      LuaFunction ld,
      ChatComponent.ChatMessageClient a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectLBasic_DialogBoxResult(
      LuaFunction ld,
      DialogBoxResult a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_Protocol_FriendGetUserSummaryAck(
      LuaFunction ld,
      FriendGetUserSummaryAck a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_Scene_ClientBattleActor(
      LuaFunction ld,
      ClientBattleActor a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_Scene_SkipCombatMode(
      LuaFunction ld,
      SkipCombatMode a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_string(LuaFunction ld, string a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_System_Action(LuaFunction ld, Action a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_System_Int64(LuaFunction ld, long a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_System_Object(LuaFunction ld, object a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_System_UInt64(LuaFunction ld, ulong a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_UnityEngine_EventSystems_PointerEventData(
      LuaFunction ld,
      PointerEventData a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_UnityEngine_GameObject(LuaFunction ld, GameObject a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_UnityEngine_Object(LuaFunction ld, UnityEngine.Object a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_UnityEngine_Vector2(LuaFunction ld, Vector2 a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_UnityEngine_WWW(LuaFunction ld, WWW a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_AnikiLevelListItemUIController(
      LuaFunction ld,
      AnikiLevelListItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_ArenaOpponentListItemUIController(
      LuaFunction ld,
      ArenaOpponentListItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_AssistanceTeamUIController(
      LuaFunction ld,
      AssistanceTeamUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_BattlePrepareStageActor(
      LuaFunction ld,
      BattlePrepareStageActor a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_BeforeJoinSingleGuildUIController(
      LuaFunction ld,
      BeforeJoinSingleGuildUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_BusinessCardHeroCharItemUIController(
      LuaFunction ld,
      BusinessCardHeroCharItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_BusinessCardHeroEquipmentItemController(
      LuaFunction ld,
      BusinessCardHeroEquipmentItemController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_BusinessCardHeroListItemUIController(
      LuaFunction ld,
      BusinessCardHeroListItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_ClimbTowerFloorItemUIController(
      LuaFunction ld,
      ClimbTowerFloorItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_CollectionActivityButton(
      LuaFunction ld,
      CollectionActivityButton a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_CollectionActivityExchangeMissionItemUIController(
      LuaFunction ld,
      CollectionActivityExchangeMissionItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_CollectionActivityLevelListItemUIController(
      LuaFunction ld,
      CollectionActivityLevelListItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_CooperateBattleButton(
      LuaFunction ld,
      CooperateBattleButton a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_CooperateBattleLevelListItemUIController(
      LuaFunction ld,
      CooperateBattleLevelListItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_CourseItemUIController(
      LuaFunction ld,
      CourseItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_DayButtonUIController(
      LuaFunction ld,
      DayButtonUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_DrillRoomToggleUIController(
      LuaFunction ld,
      DrillRoomToggleUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_EternalShrineLevelListItemUIController(
      LuaFunction ld,
      EternalShrineLevelListItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_EventGiftBoxToggleUIController(
      LuaFunction ld,
      EventGiftBoxToggleUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_EventRiftLevelButton(
      LuaFunction ld,
      EventRiftLevelButton a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_FettersConfessionSkillItemUIController(
      LuaFunction ld,
      FettersConfessionSkillItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_FriendBigItemUIController(
      LuaFunction ld,
      FriendBigItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_FriendGroupUIController(
      LuaFunction ld,
      FriendGroupUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_FriendSmallItemUIController(
      LuaFunction ld,
      FriendSmallItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_GainRewardButton(
      LuaFunction ld,
      GainRewardButton a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_GuildInviteMemberInfoItemUIController(
      LuaFunction ld,
      GuildInviteMemberInfoItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_GuildMemberInfoItemUIController(
      LuaFunction ld,
      GuildMemberInfoItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_HeroAnthemChapterItemUIController(
      LuaFunction ld,
      HeroAnthemChapterItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_HeroAnthemLevelItemUIController(
      LuaFunction ld,
      HeroAnthemLevelItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_HeroCharSkinItemUIController(
      LuaFunction ld,
      HeroCharSkinItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_HeroDragButton(
      LuaFunction ld,
      HeroDragButton a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_HeroDungeonLevelListItemUIController(
      LuaFunction ld,
      HeroDungeonLevelListItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_HeroExpItemUIController(
      LuaFunction ld,
      HeroExpItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_HeroHeadUIController(
      LuaFunction ld,
      HeroHeadUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_HeroOrSoliderSkinUIController(
      LuaFunction ld,
      HeroOrSoliderSkinUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_HeroPhantomButton(
      LuaFunction ld,
      HeroPhantomButton a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_HeroPhantomLevelListItemUIController(
      LuaFunction ld,
      HeroPhantomLevelListItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_HeroSkillItemUIController(
      LuaFunction ld,
      HeroSkillItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_HeroTrainningLevelListItemUIController(
      LuaFunction ld,
      HeroTrainningLevelListItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_IntegralGoodsUIController(
      LuaFunction ld,
      IntegralGoodsUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_MemoryCorridorLevelListItemUIController(
      LuaFunction ld,
      MemoryCorridorLevelListItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_OfflineArenaBattleReportListItemUIController(
      LuaFunction ld,
      OfflineArenaBattleReportListItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_OnlineArenaBattleReportListItemUIController(
      LuaFunction ld,
      OnlineArenaBattleReportListItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_OpenServiceMissonUIController(
      LuaFunction ld,
      OpenServiceMissonUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_PeakArenaBpStageHeroItemUIController(
      LuaFunction ld,
      PeakArenaBpStageHeroItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_PeakArenaEquipItemUIController(
      LuaFunction ld,
      PeakArenaEquipItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_PeakArenaHeroCardCircleUIController(
      LuaFunction ld,
      PeakArenaHeroCardCircleUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_PeakArenaHeroCardUIController(
      LuaFunction ld,
      PeakArenaHeroCardUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_PeakArenaHeroDetailUITask(
      LuaFunction ld,
      PeakArenaHeroDetailUITask a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_PeakArenaMapItemUIController(
      LuaFunction ld,
      PeakArenaMapItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_PeakArenaPanelType(
      LuaFunction ld,
      PeakArenaPanelType a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_PeakArenaSkillItemUIController(
      LuaFunction ld,
      PeakArenaSkillItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_RecommendGiftBoxToggleUIController(
      LuaFunction ld,
      RecommendGiftBoxToggleUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_RiftLevelButton(
      LuaFunction ld,
      RiftLevelButton a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_SmallExpressionItemContrller(
      LuaFunction ld,
      SmallExpressionItemContrller a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_StoreItemUIController(
      LuaFunction ld,
      StoreItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_SummonToggleUIController(
      LuaFunction ld,
      SummonToggleUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_TeamRoomInvitePlayerType(
      LuaFunction ld,
      TeamRoomInvitePlayerType a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_TeamRoomListItemUIController(
      LuaFunction ld,
      TeamRoomListItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_ThearchyLevelListItemUIController(
      LuaFunction ld,
      ThearchyLevelListItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_TrainingSkillItemUIController(
      LuaFunction ld,
      TrainingSkillItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_TreasureLevelListItemUIController(
      LuaFunction ld,
      TreasureLevelListItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_UnchartedScoreButton(
      LuaFunction ld,
      UnchartedScoreButton a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_1_BlackJack_ProjectL_UI_UnchartedScoreLevelListItemUIController(
      LuaFunction ld,
      UnchartedScoreLevelListItemUIController a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_BlackJack_BJFramework_Runtime_UI_UIControllerBase_bool(
      LuaFunction ld,
      UIControllerBase a1,
      bool a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_bool_string(LuaFunction ld, bool a1, string a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_bool_UnityEngine_WWW(LuaFunction ld, bool a1, WWW a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_bool_BlackJack_ProjectL_UI_AnikiGymListItemUIController(
      LuaFunction ld,
      bool a1,
      AnikiGymListItemUIController a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_bool_BlackJack_ProjectL_UI_FriendSmallItemUIController(
      LuaFunction ld,
      bool a1,
      FriendSmallItemUIController a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_bool_BlackJack_ProjectL_UI_TeamGameFunctionTypeListItemUIController(
      LuaFunction ld,
      bool a1,
      TeamGameFunctionTypeListItemUIController a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_bool_BlackJack_ProjectL_UI_TeamLocationListItemUIController(
      LuaFunction ld,
      bool a1,
      TeamLocationListItemUIController a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_BlackJack_ProjectL_Common_BattleType_int(
      LuaFunction ld,
      BattleType a1,
      int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_BlackJack_ProjectL_Common_ChatMessage_UnityEngine_GameObject(
      LuaFunction ld,
      ChatMessage a1,
      GameObject a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_BlackJack_ProjectL_Common_Hero_BlackJack_BJFramework_Runtime_UI_UIIntent(
      LuaFunction ld,
      Hero a1,
      UIIntent a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_BlackJack_ProjectL_Common_Hero_int(
      LuaFunction ld,
      Hero a1,
      int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_BlackJack_ProjectL_Common_RealTimePVPMode_bool(
      LuaFunction ld,
      RealTimePVPMode a1,
      bool a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_BlackJack_ConfigData_ConfigDataRiftLevelInfo_BlackJack_ProjectL_UI_NeedGoods(
      LuaFunction ld,
      ConfigDataRiftLevelInfo a1,
      NeedGoods a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_BlackJack_ConfigData_GameFunctionType_int(
      LuaFunction ld,
      GameFunctionType a1,
      int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_BlackJack_ConfigData_GetPathData_BlackJack_ProjectL_UI_NeedGoods(
      LuaFunction ld,
      GetPathData a1,
      NeedGoods a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_int_Dictionary_System_Int32_object_(
      LuaFunction ld,
      int a1,
      Dictionary<int, object> a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_int_int(LuaFunction ld, int a1, int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_int_string(LuaFunction ld, int a1, string a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_int_System_Action(LuaFunction ld, int a1, Action a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_int_System_DateTime(
      LuaFunction ld,
      int a1,
      DateTime a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_List_BlackJack_ConfigData_Goods__List_System_Int32_(
      LuaFunction ld,
      List<Goods> a1,
      List<int> a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_string_bool(LuaFunction ld, string a1, bool a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_string_int(LuaFunction ld, string a1, int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_string_System_Nullable_UnityEngine_SceneManagement_Scene_(
      LuaFunction ld,
      string a1,
      UnityEngine.SceneManagement.Scene? a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_string_UnityEngine_Object(
      LuaFunction ld,
      string a1,
      UnityEngine.Object a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_System_Int64_System_Int64(
      LuaFunction ld,
      long a1,
      long a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_System_UInt64_BlackJack_ProjectL_Common_ChatBagItemMessage(
      LuaFunction ld,
      ulong a1,
      ChatBagItemMessage a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_System_UInt64_string(
      LuaFunction ld,
      ulong a1,
      string a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_UnityEngine_EventSystems_PointerEventData_InputButton_UnityEngine_Vector2(
      LuaFunction ld,
      PointerEventData.InputButton a1,
      Vector2 a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_BlackJack_ProjectL_UI_BattlePrepareStageActor_BlackJack_ProjectL_Common_GridPosition(
      LuaFunction ld,
      BattlePrepareStageActor a1,
      GridPosition a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_BlackJack_ProjectL_UI_BattlePrepareStageActor_BlackJack_ProjectL_UI_BattlePrepareStageActor(
      LuaFunction ld,
      BattlePrepareStageActor a1,
      BattlePrepareStageActor a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_BlackJack_ProjectL_UI_FettersGiftItemUIController_bool(
      LuaFunction ld,
      FettersGiftItemUIController a1,
      bool a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_BlackJack_ProjectL_UI_GuildApplyMemberInfoItemUIController_bool(
      LuaFunction ld,
      GuildApplyMemberInfoItemUIController a1,
      bool a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_BlackJack_ProjectL_UI_HeroDragButton_UnityEngine_EventSystems_PointerEventData(
      LuaFunction ld,
      HeroDragButton a1,
      PointerEventData a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_2_BlackJack_ProjectL_UI_PeakArenaBattleReportType_System_UInt64(
      LuaFunction ld,
      PeakArenaBattleReportType a1,
      ulong a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_3_BlackJack_ProjectL_Common_BattleHero_BlackJack_ProjectL_Common_GridPosition_int(
      LuaFunction ld,
      BattleHero a1,
      GridPosition a2,
      int a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_3_BlackJack_ProjectL_Common_BattleType_int_List_System_Int32_(
      LuaFunction ld,
      BattleType a1,
      int a2,
      List<int> a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_3_BlackJack_ProjectL_Common_ChatChannel_string_string(
      LuaFunction ld,
      ChatChannel a1,
      string a2,
      string a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_3_BlackJack_ConfigData_GoodsType_int_int(
      LuaFunction ld,
      GoodsType a1,
      int a2,
      int a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_3_int_BlackJack_ConfigData_GameFunctionType_int(
      LuaFunction ld,
      int a1,
      GameFunctionType a2,
      int a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_3_int_int_int(LuaFunction ld, int a1, int a2, int a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_3_int_int_string(
      LuaFunction ld,
      int a1,
      int a2,
      string a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_3_int_int_System_Action(
      LuaFunction ld,
      int a1,
      int a2,
      Action a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_3_System_UInt64_int_Assets_Script_ProjectL_Runtime_UI_Activity_ActivityRewardUIController(
      LuaFunction ld,
      ulong a1,
      int a2,
      ActivityRewardUIController a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_3_System_UInt64_int_List_BlackJack_ConfigData_Goods_(
      LuaFunction ld,
      ulong a1,
      int a2,
      List<Goods> a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_4_bool_int_System_UInt32_bool(
      LuaFunction ld,
      bool a1,
      int a2,
      uint a3,
      bool a4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_4_BlackJack_ConfigData_GoodsType_int_int_int(
      LuaFunction ld,
      GoodsType a1,
      int a2,
      int a3,
      int a4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_4_int_BlackJack_ProjectL_Common_BagItemBase_int_System_Action(
      LuaFunction ld,
      int a1,
      BagItemBase a2,
      int a3,
      Action a4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action_4_int_int_int_System_Action(
      LuaFunction ld,
      int a1,
      int a2,
      int a3,
      Action a4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static int Lua_System_Comparison_1_int(LuaFunction ld, int a1, int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static int Lua_System_Comparison_1_string(LuaFunction ld, string a1, string a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static ResourceManager Lua_System_Func_1_BlackJack_BJFramework_Runtime_Resource_ResourceManager(
      LuaFunction ld)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static int Lua_System_Func_1_int(LuaFunction ld)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static List<string> Lua_System_Func_1_List_System_String_(LuaFunction ld)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static string Lua_System_Func_1_string(LuaFunction ld)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static IEnumerator Lua_System_Func_1_System_Collections_IEnumerator(
      LuaFunction ld)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static bool Lua_System_Func_2_BlackJack_BJFramework_Runtime_Scene_SceneLayerBase_bool(
      LuaFunction ld,
      SceneLayerBase a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static bool Lua_System_Func_2_BlackJack_BJFramework_Runtime_UI_UIManager_UIProcessQueueItem_bool(
      LuaFunction ld,
      UIManager.UIProcessQueueItem a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static bool Lua_System_Func_2_int_bool(LuaFunction ld, int a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static bool Lua_System_Func_4_int_string_bool_bool(
      LuaFunction ld,
      int a1,
      string a2,
      bool a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static object Lua_System_Func_4_System_IO_Stream_System_Type_int_System_Object(
      LuaFunction ld,
      Stream a1,
      System.Type a2,
      int a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_BJFramework_Runtime_Lua_LuaHotFixChecker_WithOutParam_Int32_LuaHotFixExample_LuaTestStruct_Int32_Single_Boolean_ObjectLuaHotFixState(
      LuaHotFixChecker obj,
      LuaFunction ld,
      int a1,
      out LuaHotFixExample a2,
      out LuaTestStruct a3,
      out int a4,
      out float a5,
      out bool a6,
      out ObjectLuaHotFixState a7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int Lua_BlackJack_BJFramework_Runtime_Lua_LuaHotFixChecker_GetOutParam_LuaHotFixExample_Int32_LuaTestStruct(
      LuaHotFixChecker obj,
      LuaFunction ld,
      out LuaHotFixExample a1,
      out int a2,
      out LuaTestStruct a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int Lua_BlackJack_BJFramework_Runtime_Lua_LuaHotFixChecker_GetRefParam_LuaHotFixExample_Int32_LuaTestStruct(
      LuaHotFixChecker obj,
      LuaFunction ld,
      out LuaHotFixExample a1,
      ref int a2,
      out LuaTestStruct a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_BJFramework_Runtime_Lua_LuaHotFixChecker_WithOutParamAndArray_Stringbe_Int32(
      LuaHotFixChecker obj,
      LuaFunction ld,
      string[] a1,
      out int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_BJFramework_Runtime_Lua_LuaHotFixExample_WithOutParam_Int32_LuaHotFixExample_LuaTestStruct_Int32_Single_Boolean_ObjectLuaHotFixState(
      LuaHotFixExample obj,
      LuaFunction ld,
      int a1,
      out LuaHotFixExample a2,
      out LuaTestStruct a3,
      out int a4,
      out float a5,
      out bool a6,
      out ObjectLuaHotFixState a7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int Lua_BlackJack_BJFramework_Runtime_Lua_LuaHotFixExample_GetOutParam_LuaHotFixExample_LuaHotFixExample_LuaTestStruct_LuaTestStruct_Int32_Int32_Single_Single_Boolean_Boolean_ObjectLuaHotFixState_ObjectLuaHotFixState(
      LuaHotFixExample obj,
      LuaFunction ld,
      out LuaHotFixExample a1,
      LuaHotFixExample a2,
      out LuaTestStruct a3,
      LuaTestStruct a4,
      out int a5,
      int a6,
      out float a7,
      float a8,
      out bool a9,
      bool a10,
      out ObjectLuaHotFixState a11,
      ObjectLuaHotFixState a12)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int Lua_BlackJack_BJFramework_Runtime_Lua_LuaHotFixExample_GetOutParam_LuaHotFixExample_Int32_LuaTestStruct(
      LuaHotFixExample obj,
      LuaFunction ld,
      out LuaHotFixExample a1,
      out int a2,
      out LuaTestStruct a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int Lua_BlackJack_BJFramework_Runtime_Lua_LuaHotFixExample_GetRefParam_LuaHotFixExample_Int32_LuaTestStruct(
      LuaHotFixExample obj,
      LuaFunction ld,
      out LuaHotFixExample a1,
      ref int a2,
      out LuaTestStruct a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_BJFramework_Runtime_PlayerContext_PlayerContextBase_IsDataCacheDirtyByPlayerInfoInitAck_Object_Boolean(
      PlayerContextBase obj,
      LuaFunction ld,
      object a1,
      out bool a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_BJFramework_Runtime_Prefab_PrefabResourceContainerBase_TryGetAsset_String_Object(
      PrefabResourceContainerBase obj,
      LuaFunction ld,
      string a1,
      out UnityEngine.Object a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_BJFramework_Runtime_Resource_ResourceManager_RegBundleLoadingCtx_String_BundleLoadingCtx(
      ResourceManager obj,
      LuaFunction ld,
      string a1,
      out ResourceManager.BundleLoadingCtx a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_BJFramework_Runtime_UI_UIIntentCustom_TryGetParam_String_Object(
      UIIntentCustom obj,
      LuaFunction ld,
      string a1,
      out object a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_BJFramework_Runtime_UI_UIIntentReturnable___callBase_TryGetParam_String_Object(
      UIIntentReturnable obj,
      LuaFunction ld,
      string a1,
      out object a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_AR_ARPlaneTrace_Raycast_Vector3_Vector3(
      ARPlaneTrace obj,
      LuaFunction ld,
      Vector3 a1,
      out Vector3 a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_AR_HWARPlaneTrace_Raycast_Vector3_Vector3(
      HWARPlaneTrace obj,
      LuaFunction ld,
      Vector3 a1,
      out Vector3 a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_AR_IOSARPlaneTrace_Raycast_Vector3_Vector3(
      IOSARPlaneTrace obj,
      LuaFunction ld,
      Vector3 a1,
      out Vector3 a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_AR_IOSARPlaneTrace_HitTestWithResultType_ARPoint_ARHitTestResultType_Vector3(
      IOSARPlaneTrace obj,
      LuaFunction ld,
      ARPoint a1,
      ARHitTestResultType a2,
      out Vector3 a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_Battle_BattleActor_FindRandomEmptyPosition_Int32_Int32_GridPosition(
      BattleActor obj,
      LuaFunction ld,
      int a1,
      int a2,
      ref GridPosition a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GridPosition Lua_BlackJack_ProjectL_Battle_BattleActor_FindPositionToMoveToTarget_GridPosition_BattleActor(
      BattleActor obj,
      LuaFunction ld,
      GridPosition a1,
      out BattleActor a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_Battle_BattleActor_CollectBuffPropertyModifiersAndFightTags_BattlePropertyModifier_UInt32(
      BattleActor obj,
      LuaFunction ld,
      BattlePropertyModifier a1,
      ref uint a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_Battle_BattleActor_CollectBuffPropertyModifiersAndFightTags_BattlePropertyModifier_UInt32_Boolean_Boolean(
      BattleActor obj,
      LuaFunction ld,
      BattlePropertyModifier a1,
      ref uint a2,
      bool a3,
      bool a4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_Battle_BattleActor_CollectOtherActorBuffPropertyModifiersAndFightTags_BattlePropertyModifier_UInt32(
      BattleActor obj,
      LuaFunction ld,
      BattlePropertyModifier a1,
      ref uint a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_Battle_BattleActor_CollectBuffPropertyModifiersAndFightTagsInCombat_BattlePropertyModifier_UInt32_BattleActor_Boolean_Int32_ConfigDataSkillInfo(
      BattleActor obj,
      LuaFunction ld,
      BattlePropertyModifier a1,
      ref uint a2,
      BattleActor a3,
      bool a4,
      int a5,
      ConfigDataSkillInfo a6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int Lua_BlackJack_ProjectL_Battle_BattleActor_BuffReboundDamage_ConfigDataSkillInfo_Int32_Int32(
      BattleActor obj,
      LuaFunction ld,
      ConfigDataSkillInfo a1,
      int a2,
      out int a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_Battle_BattleActor_AddBuffArmyRelationAttack_Int32_Boolean_ArmyRelationData_Boolean(
      BattleActor obj,
      LuaFunction ld,
      int a1,
      bool a2,
      ref ArmyRelationData a3,
      bool a4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_Battle_BattleActor_AddBuffArmyRelationDefend_Int32_Boolean_ArmyRelationData_Boolean(
      BattleActor obj,
      LuaFunction ld,
      int a1,
      bool a2,
      ref ArmyRelationData a3,
      bool a4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_Battle_BattleActor_AddBuffArmyRelationModifyAttack_Int32_Boolean_ArmyRelationData_Boolean(
      BattleActor obj,
      LuaFunction ld,
      int a1,
      bool a2,
      ref ArmyRelationData a3,
      bool a4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_Battle_BattleActor_AddBuffArmyRelationModifyDefend_Int32_Boolean_ArmyRelationData_Boolean(
      BattleActor obj,
      LuaFunction ld,
      int a1,
      bool a2,
      ref ArmyRelationData a3,
      bool a4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_Battle_BattleActor_CombatBuffAttackAid_BattleActor_BattleActor_Int32(
      BattleActor obj,
      LuaFunction ld,
      BattleActor a1,
      BattleActor a2,
      ref int a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_Battle_BattleActor_CombatBuffDefenseAid_BattleActor_BattleActor_Int32(
      BattleActor obj,
      LuaFunction ld,
      BattleActor a1,
      BattleActor a2,
      ref int a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_Battle_BattleActor_ComputeBuffSkillHpModify_ConfigDataSkillInfo_Int32_Int32_BattleActor_Int32_Int32(
      BattleActor obj,
      LuaFunction ld,
      ConfigDataSkillInfo a1,
      int a2,
      int a3,
      BattleActor a4,
      out int a5,
      out int a6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_Battle_BattleActor_GetBuffCombatAidModify_Boolean_Int32_Int32(
      BattleActor obj,
      LuaFunction ld,
      bool a1,
      out int a2,
      out int a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ConfigDataSkillInfo> Lua_BlackJack_ProjectL_Battle_BattlePlayer_GetTrainingTechSoldierSkillInfos_IConfigDataLoader_ConfigDataSoldierInfo_Int32(
      BattlePlayer obj,
      LuaFunction ld,
      IConfigDataLoader a1,
      ConfigDataSoldierInfo a2,
      out int a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_Battle_CombatFlyObject_ComputeParabolicPosition_Fix64_Vector2i_Fix64(
      CombatFlyObject obj,
      LuaFunction ld,
      Fix64 a1,
      out Vector2i a2,
      out Fix64 a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int Lua_BlackJack_ProjectL_Common_BagComponentCommon_IsGoodsEnough_List1_List1(
      BagComponentCommon obj,
      LuaFunction ld,
      List<Goods> a1,
      out List<BagItemBase> a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_Common_CooperateBattleCompomentCommon_CheckCooperateBattleAvailable_Int32_Int32(
      CooperateBattleCompomentCommon obj,
      LuaFunction ld,
      int a1,
      ref int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_Common_CooperateBattleCompomentCommon_CheckCooperateBattleLevelAvailable_Int32_Int32_Int32(
      CooperateBattleCompomentCommon obj,
      LuaFunction ld,
      int a1,
      int a2,
      ref int a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_Common_CooperateBattleCompomentCommon_CheckPlayerOutOfBattle_Int32(
      CooperateBattleCompomentCommon obj,
      LuaFunction ld,
      ref int a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_Common_CooperateBattleCompomentCommon_CheckEnergy_Int32_Int32_Int32(
      CooperateBattleCompomentCommon obj,
      LuaFunction ld,
      int a1,
      int a2,
      ref int a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_Common_CooperateBattleCompomentCommon_CheckBag_Int32_Int32_Int32(
      CooperateBattleCompomentCommon obj,
      LuaFunction ld,
      int a1,
      int a2,
      ref int a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_Common_GlobalRankingListComponentCommon_CheckRankingListInfoQuery_RankingListType_Int32(
      GlobalRankingListComponentCommon obj,
      LuaFunction ld,
      RankingListType a1,
      out int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_Common_HeroPhantomCompomentCommon_CheckHeroPhantomAvailable_Int32_Int32(
      HeroPhantomCompomentCommon obj,
      LuaFunction ld,
      int a1,
      ref int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_Common_HeroPhantomCompomentCommon_CheckHeroPhantomLevelAvailable_Int32_Int32(
      HeroPhantomCompomentCommon obj,
      LuaFunction ld,
      int a1,
      ref int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_Common_HeroPhantomCompomentCommon_CheckPlayerOutOfBattle_Int32(
      HeroPhantomCompomentCommon obj,
      LuaFunction ld,
      ref int a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_Common_HeroPhantomCompomentCommon_CheckEnergy_Int32_Int32(
      HeroPhantomCompomentCommon obj,
      LuaFunction ld,
      int a1,
      ref int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_Common_HeroPhantomCompomentCommon_CheckBag_Int32_Int32(
      HeroPhantomCompomentCommon obj,
      LuaFunction ld,
      int a1,
      ref int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_Common_PeakArenaComponentCommon_GetScoreThresholdAfterSingleMatch_Int32_Int32_Int32(
      PeakArenaComponentCommon obj,
      LuaFunction ld,
      int a1,
      out int a2,
      out int a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_Common_RealTimePVPComponentCommon_TryGetBotParamsForNovice_RealTimePVPMode_Int32_Int32_Int32_Int32_Int32(
      RealTimePVPComponentCommon obj,
      LuaFunction ld,
      RealTimePVPMode a1,
      int a2,
      out int a3,
      out int a4,
      out int a5,
      out int a6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_Common_RealTimePVPComponentCommon_TryGetBotParamsForLoser_RealTimePVPMode_Int32_Int32_Int32_Int32_Int32_Int32(
      RealTimePVPComponentCommon obj,
      LuaFunction ld,
      RealTimePVPMode a1,
      int a2,
      int a3,
      out int a4,
      out int a5,
      out int a6,
      out int a7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int Lua_BlackJack_ProjectL_Common_SelectCardComponentCommon_GetCrystalCount_CardPool_Boolean_Boolean(
      SelectCardComponentCommon obj,
      LuaFunction ld,
      CardPool a1,
      bool a2,
      out bool a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_Common_TeamComponentCommon_GetTeamRoomInfo_GameFunctionType_Int32(
      TeamComponentCommon obj,
      LuaFunction ld,
      out GameFunctionType a1,
      out int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int Lua_BlackJack_ProjectL_PlayerContext_BagComponent___callBase_IsGoodsEnough_List1_List1(
      BagComponent obj,
      LuaFunction ld,
      List<Goods> a1,
      out List<BagItemBase> a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int Lua_BlackJack_ProjectL_PlayerContext_ClimbTowerComponent_TryRaid_Int32_Int32(
      ClimbTowerComponent obj,
      LuaFunction ld,
      out int a1,
      out int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_PlayerContext_CollectionActivityComponent_GetCollectionActivityOpenTime_Int32_DateTime_DateTime(
      CollectionActivityComponent obj,
      LuaFunction ld,
      int a1,
      out DateTime a2,
      out DateTime a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_PlayerContext_CollectionActivityComponent_GetWaypointState_Int32_CollectionActivityWaypointStateType_String(
      CollectionActivityComponent obj,
      LuaFunction ld,
      int a1,
      out CollectionActivityWaypointStateType a2,
      out string a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_PlayerContext_CollectionActivityComponent_IsChallengeLevelPlayerLevelVaild_Int32_Int32(
      CollectionActivityComponent obj,
      LuaFunction ld,
      int a1,
      out int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_PlayerContext_CollectionActivityComponent_IsLootLevelPlayerLevelVaild_Int32_Int32(
      CollectionActivityComponent obj,
      LuaFunction ld,
      int a1,
      out int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_PlayerContext_CooperateBattleCompoment___callBase_CheckCooperateBattleAvailable_Int32_Int32(
      CooperateBattleCompoment obj,
      LuaFunction ld,
      int a1,
      ref int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_PlayerContext_CooperateBattleCompoment___callBase_CheckCooperateBattleLevelAvailable_Int32_Int32_Int32(
      CooperateBattleCompoment obj,
      LuaFunction ld,
      int a1,
      int a2,
      ref int a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_PlayerContext_CooperateBattleCompoment___callBase_CheckPlayerOutOfBattle_Int32(
      CooperateBattleCompoment obj,
      LuaFunction ld,
      ref int a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_PlayerContext_CooperateBattleCompoment___callBase_CheckEnergy_Int32_Int32_Int32(
      CooperateBattleCompoment obj,
      LuaFunction ld,
      int a1,
      int a2,
      ref int a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_PlayerContext_CooperateBattleCompoment___callBase_CheckBag_Int32_Int32_Int32(
      CooperateBattleCompoment obj,
      LuaFunction ld,
      int a1,
      int a2,
      ref int a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_PlayerContext_GlobalRankingListComponent_IsAbleQueryRankingListInfo_RankingListType_Int32(
      GlobalRankingListComponent obj,
      LuaFunction ld,
      RankingListType a1,
      out int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_PlayerContext_Happening_GetGameFunctionTypeAndLocationId_GameFunctionType_Int32(
      Happening obj,
      LuaFunction ld,
      out GameFunctionType a1,
      out int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_PlayerContext_Happening_GetStarCondition_Int32_Int32(
      Happening obj,
      LuaFunction ld,
      out int a1,
      out int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_PlayerContext_HeroPhantomCompoment___callBase_CheckHeroPhantomAvailable_Int32_Int32(
      HeroPhantomCompoment obj,
      LuaFunction ld,
      int a1,
      ref int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_PlayerContext_HeroPhantomCompoment___callBase_CheckHeroPhantomLevelAvailable_Int32_Int32(
      HeroPhantomCompoment obj,
      LuaFunction ld,
      int a1,
      ref int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_PlayerContext_HeroPhantomCompoment___callBase_CheckPlayerOutOfBattle_Int32(
      HeroPhantomCompoment obj,
      LuaFunction ld,
      ref int a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_PlayerContext_HeroPhantomCompoment___callBase_CheckEnergy_Int32_Int32(
      HeroPhantomCompoment obj,
      LuaFunction ld,
      int a1,
      ref int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_PlayerContext_HeroPhantomCompoment___callBase_CheckBag_Int32_Int32(
      HeroPhantomCompoment obj,
      LuaFunction ld,
      int a1,
      ref int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_PlayerContext_PeakArenaComponent___callBase_GetScoreThresholdAfterSingleMatch_Int32_Int32_Int32(
      PeakArenaComponent obj,
      LuaFunction ld,
      int a1,
      out int a2,
      out int a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_PlayerContext_ProjectLPlayerContext_IsDataCacheDirtyByPlayerInfoInitAck_Object_Boolean(
      ProjectLPlayerContext obj,
      LuaFunction ld,
      object a1,
      out bool a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int Lua_BlackJack_ProjectL_PlayerContext_ProjectLPlayerContext_ClimbTowerTryRaid_Int32_Int32(
      ProjectLPlayerContext obj,
      LuaFunction ld,
      out int a1,
      out int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_PlayerContext_ProjectLPlayerContext_GetCollectionActivityOpenTime_Int32_DateTime_DateTime(
      ProjectLPlayerContext obj,
      LuaFunction ld,
      int a1,
      out DateTime a2,
      out DateTime a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_PlayerContext_ProjectLPlayerContext_GetCollectionActivityWaypointState_Int32_CollectionActivityWaypointStateType_String(
      ProjectLPlayerContext obj,
      LuaFunction ld,
      int a1,
      out CollectionActivityWaypointStateType a2,
      out string a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_PlayerContext_ProjectLPlayerContext_IsCollectionActivityChallengeLevelPlayerLevelVaild_Int32_Int32(
      ProjectLPlayerContext obj,
      LuaFunction ld,
      int a1,
      out int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_PlayerContext_ProjectLPlayerContext_IsCollectionActivityLootLevelPlayerLevelVaild_Int32_Int32(
      ProjectLPlayerContext obj,
      LuaFunction ld,
      int a1,
      out int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_PlayerContext_ProjectLPlayerContext_GetWaypointEvent_Int32_ConfigDataEventInfo_RandomEvent(
      ProjectLPlayerContext obj,
      LuaFunction ld,
      int a1,
      out ConfigDataEventInfo a2,
      out RandomEvent a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_PlayerContext_ProjectLPlayerContext_HandleTimeIsOverEndTimeInSameDay_DateTime_DateTime_Int32_DateTime_Boolean(
      ProjectLPlayerContext obj,
      LuaFunction ld,
      DateTime a1,
      DateTime a2,
      int a3,
      ref DateTime a4,
      ref bool a5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_PlayerContext_ProjectLPlayerContext_SetChangedGoodsStatus_ProGoods_Boolean_Boolean_Boolean(
      ProjectLPlayerContext obj,
      LuaFunction ld,
      ProGoods a1,
      ref bool a2,
      ref bool a3,
      ref bool a4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_PlayerContext_ProjectLPlayerContext_IsAbleQueryRankingListInfo_RankingListType_Int32(
      ProjectLPlayerContext obj,
      LuaFunction ld,
      RankingListType a1,
      out int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_PlayerContext_ProjectLPlayerContext_GetUnchartedScoreOpenTime_Int32_DateTime_DateTime(
      ProjectLPlayerContext obj,
      LuaFunction ld,
      int a1,
      out DateTime a2,
      out DateTime a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_PlayerContext_UnchartedScoreComponent_GetUnchartedScoreOpenTime_Int32_DateTime_DateTime(
      UnchartedScoreComponent obj,
      LuaFunction ld,
      int a1,
      out DateTime a2,
      out DateTime a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_Scene_BattleCamera___callBase_GetViewSize_Single_Single(
      BattleCamera obj,
      LuaFunction ld,
      out float a1,
      out float a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_Scene_CameraBase_GetViewSize_Single_Single(
      CameraBase obj,
      LuaFunction ld,
      out float a1,
      out float a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_Scene_WorldCamera___callBase_GetViewSize_Single_Single(
      WorldCamera obj,
      LuaFunction ld,
      out float a1,
      out float a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_Scene_WorldPathfinder_GetWaypointPosition_Int32_Vector2(
      WorldPathfinder obj,
      LuaFunction ld,
      int a1,
      out Vector2 a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_UI_CombatSceneUIController_GetActorGraphicPosition_CombatActor_Vector3(
      CombatSceneUIController obj,
      LuaFunction ld,
      CombatActor a1,
      out Vector3 a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_DialogUIController_CreateEffectInGroup_String_ConfigDataPrefabStateInfo_GameObject_Action(
      DialogUIController obj,
      LuaFunction ld,
      ref string a1,
      ConfigDataPrefabStateInfo a2,
      GameObject a3,
      Action a4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_UI_DialogUITask_IsDialogGroupHasReward_Int32(
      DialogUITask obj,
      LuaFunction ld,
      out int a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_UI_DialogUITask_IsDialogGroupHasOptionDialog_Int32(
      DialogUITask obj,
      LuaFunction ld,
      out int a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ExpressionParseController.PosStringTuple> Lua_BlackJack_ProjectL_UI_ExpressionParseController_ParseEmoji_String_String(
      ExpressionParseController obj,
      LuaFunction ld,
      string a1,
      out string a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_FettersConfessionEvolutionMaterialUIController_InitFettersConfessionEvolutionMaterial_GoodsType_Int32_Int32_Boolean(
      FettersConfessionEvolutionMaterialUIController obj,
      LuaFunction ld,
      GoodsType a1,
      int a2,
      int a3,
      out bool a4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_HeroCommentUIController_CreateSpineGraphic_UISpineGraphic_String_GameObject_Int32_Vector2_Single_List1(
      HeroCommentUIController obj,
      LuaFunction ld,
      ref UISpineGraphic a1,
      string a2,
      GameObject a3,
      int a4,
      Vector2 a5,
      float a6,
      List<ReplaceAnim> a7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_HeroCommentUIController_DestroySpineGraphic_UISpineGraphic(
      HeroCommentUIController obj,
      LuaFunction ld,
      ref UISpineGraphic a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_HeroJobTransferUIController_CreateSpineGraphic_UISpineGraphic_ConfigDataJobConnectionInfo_GameObject_Single_String_Int32(
      HeroJobTransferUIController obj,
      LuaFunction ld,
      ref UISpineGraphic a1,
      ConfigDataJobConnectionInfo a2,
      GameObject a3,
      float a4,
      string a5,
      int a6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_HeroJobTransferUIController_CreateSoldierGraphic_UISpineGraphic_ConfigDataSoldierInfo_GameObject_Single_Int32(
      HeroJobTransferUIController obj,
      LuaFunction ld,
      ref UISpineGraphic a1,
      ConfigDataSoldierInfo a2,
      GameObject a3,
      float a4,
      int a5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_HeroJobTransferUIController_DestroyGraphic_UISpineGraphic(
      HeroJobTransferUIController obj,
      LuaFunction ld,
      ref UISpineGraphic a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_HeroSimpleItemInfoUIController_CreateSpineGraphic_ConfigDataJobConnectionInfo_UISpineGraphic_GameObject(
      HeroSimpleItemInfoUIController obj,
      LuaFunction ld,
      ConfigDataJobConnectionInfo a1,
      ref UISpineGraphic a2,
      GameObject a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_HeroSimpleItemInfoUIController_DestroySpineGraphic_UISpineGraphic(
      HeroSimpleItemInfoUIController obj,
      LuaFunction ld,
      ref UISpineGraphic a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_UI_LoginUITask_ParseLoginAgentAck_String_String_String_String_String_String(
      LoginUITask obj,
      LuaFunction ld,
      string a1,
      ref string a2,
      ref string a3,
      ref string a4,
      ref string a5,
      ref string a6)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_SelfSelectedSkinBoxUIController_CreateSpineGraphic_UISpineGraphic_String_GameObject_Int32_Vector2_Single_List1(
      SelfSelectedSkinBoxUIController obj,
      LuaFunction ld,
      ref UISpineGraphic a1,
      string a2,
      GameObject a3,
      int a4,
      Vector2 a5,
      float a6,
      List<ReplaceAnim> a7)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_StoreSoldierSkinDetailUIController_SetSpineAnim_String_UISpineGraphic_GameObject_ConfigDataSoldierInfo(
      StoreSoldierSkinDetailUIController obj,
      LuaFunction ld,
      string a1,
      ref UISpineGraphic a2,
      GameObject a3,
      ConfigDataSoldierInfo a4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_UI_TestUI_ToolButton_Int32_Int32_Single_String(
      TestUI obj,
      LuaFunction ld,
      ref int a1,
      ref int a2,
      float a3,
      string a4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_BlackJack_ProjectL_UI_TestUI_ToolToggle_Int32_Int32_Single_String_Boolean(
      TestUI obj,
      LuaFunction ld,
      ref int a1,
      ref int a2,
      float a3,
      string a4,
      bool a5)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_TestUI_TextLine_Int32_Int32_String(
      TestUI obj,
      LuaFunction ld,
      int a1,
      ref int a2,
      string a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_TestUI_GUIGMReloginButton_Int32_Int32(
      TestUI obj,
      LuaFunction ld,
      ref int a1,
      ref int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_TestUI_GUIToolToggle_Int32_Int32(
      TestUI obj,
      LuaFunction ld,
      ref int a1,
      ref int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_TestUI_GUISpeedToggle_Int32_Int32(
      TestUI obj,
      LuaFunction ld,
      ref int a1,
      ref int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_TestUI_GUIAudioToggle_Int32_Int32(
      TestUI obj,
      LuaFunction ld,
      ref int a1,
      ref int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_TestUI_GUITouchToggle_Int32_Int32(
      TestUI obj,
      LuaFunction ld,
      ref int a1,
      ref int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_TestUI_GUIMultiTouchToggle_Int32_Int32(
      TestUI obj,
      LuaFunction ld,
      ref int a1,
      ref int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_TestUI_GUIResolutionToggle_Int32_Int32(
      TestUI obj,
      LuaFunction ld,
      ref int a1,
      ref int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_TestUI_GUIFrameRateToggle_Int32_Int32(
      TestUI obj,
      LuaFunction ld,
      ref int a1,
      ref int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_TestUI_GUITimeToggle_Int32_Int32(
      TestUI obj,
      LuaFunction ld,
      ref int a1,
      ref int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_TestUI_GUIMemoryToggle_Int32_Int32(
      TestUI obj,
      LuaFunction ld,
      ref int a1,
      ref int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_TestUI_GUILowMemoryTest_Int32_Int32(
      TestUI obj,
      LuaFunction ld,
      ref int a1,
      ref int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_TestUI_GUIGCTest_Int32_Int32(
      TestUI obj,
      LuaFunction ld,
      ref int a1,
      ref int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_TestUI_GUITextPanel_Int32_Int32(
      TestUI obj,
      LuaFunction ld,
      ref int a1,
      ref int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_TestUI_UnloadAllAbTest_Int32_Int32(
      TestUI obj,
      LuaFunction ld,
      ref int a1,
      ref int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_TestUI_GUIGMCommandToggle_Int32_Int32(
      TestUI obj,
      LuaFunction ld,
      ref int a1,
      ref int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Lua_BlackJack_ProjectL_UI_TestUI_GUIGMCommand_Int32_Int32(
      TestUI obj,
      LuaFunction ld,
      ref int a1,
      ref int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Lua_System_Collections_Generic_Dictionary_2_int_string_TryGetValue_Int32_String(
      Dictionary<int, string> obj,
      LuaFunction ld,
      int a1,
      out string a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static int Lua_BlackJack_BJFramework_Runtime_Lua_LuaExportChecker_ReturnIntDelegate(
      LuaFunction ld,
      int a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static int Lua_BlackJack_ProjectL_Battle_BattleActor_ComputeActorScoreFunc(
      LuaFunction ld,
      BattleActor a1,
      int a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static bool Lua_BlackJack_ProjectL_Common_AnikiGymLevelClearCheck(
      LuaFunction ld,
      int a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static int Lua_BlackJack_ProjectL_Common_ComputeBattlePower(LuaFunction ld, Hero a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static DateTime Lua_BlackJack_ProjectL_Common_CurrentTime(LuaFunction ld)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static Hero Lua_BlackJack_ProjectL_Common_FindHero(LuaFunction ld, int a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static int Lua_BlackJack_ProjectL_Common_GetUserLevel(LuaFunction ld)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_BlackJack_ProjectL_UI_InfinityGridLayoutGroup_UpdateChildrenCallbackDelegate(
      LuaFunction ld,
      int a1,
      Transform a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_System_Action(LuaFunction ld)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static bool Lua_System_Predicate_1_BlackJack_ProjectL_Common_Announcement(
      LuaFunction ld,
      Announcement a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static bool Lua_System_Predicate_1_BlackJack_ProjectL_Common_BPStageHeroSetupInfo(
      LuaFunction ld,
      BPStageHeroSetupInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static bool Lua_System_Predicate_1_int(LuaFunction ld, int a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static bool Lua_System_Predicate_1_string(LuaFunction ld, string a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static bool Lua_System_Func_1_bool(LuaFunction ld)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static float Lua_System_Func_2_UnityEngine_UI_ILayoutElement_float(
      LuaFunction ld,
      ILayoutElement a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_UnityEngine_Application_AdvertisingIdentifierCallback(
      LuaFunction ld,
      string a1,
      bool a2,
      string a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_UnityEngine_Application_LogCallback(
      LuaFunction ld,
      string a1,
      string a2,
      LogType a3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_UnityEngine_Application_LowMemoryCallback(LuaFunction ld)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_UnityEngine_Canvas_WillRenderCanvases(LuaFunction ld)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_UnityEngine_CullingGroup_StateChanged(
      LuaFunction ld,
      CullingGroupEvent a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_UnityEngine_Display_DisplaysUpdatedDelegate(LuaFunction ld)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_UnityEngine_Events_UnityAction(LuaFunction ld)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_UnityEngine_Font_FontTextureRebuildCallback(LuaFunction ld)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_UnityEngine_RectTransform_ReapplyDrivenProperties(
      LuaFunction ld,
      RectTransform a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void Lua_UnityEngine_EventSystems_ExecuteEvents_EventFunction_1_UnityEngine_EventSystems_IEventSystemHandler(
      LuaFunction ld,
      IEventSystemHandler a1,
      BaseEventData a2)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
