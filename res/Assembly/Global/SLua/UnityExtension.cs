﻿// Decompiled with JetBrains decompiler
// Type: SLua.UnityExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 972A2846-EBE4-4323-8745-F884A18FA509
// Assembly location: D:\Games\Langrisser5\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace SLua
{
  public static class UnityExtension
  {
    public static void StartCoroutine(this MonoBehaviour mb, LuaFunction func)
    {
      mb.StartCoroutine(UnityExtension.LuaCoroutine(func));
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    internal static IEnumerator LuaCoroutine(LuaFunction func)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
