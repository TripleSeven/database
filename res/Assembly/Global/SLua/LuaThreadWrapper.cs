﻿// Decompiled with JetBrains decompiler
// Type: SLua.LuaThreadWrapper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 972A2846-EBE4-4323-8745-F884A18FA509
// Assembly location: D:\Games\Langrisser5\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace SLua
{
  public class LuaThreadWrapper : LuaVar
  {
    private IntPtr _thread;

    [MethodImpl((MethodImplOptions) 32768)]
    public LuaThreadWrapper(LuaFunction func)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    ~LuaThreadWrapper()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Dispose(bool disposeManagedResources)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool EqualsTo(IntPtr L)
    {
      return this._thread == L;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private object TopObjects(int nArgs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Resume(out object retVal)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
