﻿// Decompiled with JetBrains decompiler
// Type: XunfeiSDKWrapper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

public class XunfeiSDKWrapper : MonoBehaviour
{
  private static bool m_isXFInit;

  [DllImport("XunfeiWin")]
  private static extern bool XFWinInit(string appid, int sampleRate);

  [DllImport("XunfeiWin")]
  private static extern void XFWinTickRecognize(float time, float deltaTime);

  [DllImport("XunfeiWin")]
  private static extern bool XFWinRequestRecognize(string audioFilepath);

  [DllImport("XunfeiWin")]
  private static extern int XFWinGetRecognizeIntState();

  [DllImport("XunfeiWin")]
  private static extern IntPtr XFWinGetRecognizeResultPtr();

  [DllImport("XunfeiWin")]
  private static extern IntPtr XFWinGetErrorPtr();

  private XunfeiSDKWrapper.XFWinStatus XFWinGetRecognizeState()
  {
    return (XunfeiSDKWrapper.XFWinStatus) XunfeiSDKWrapper.XFWinGetRecognizeIntState();
  }

  private string XFWinGetRecognizeResult()
  {
    return Marshal.PtrToStringAnsi(XunfeiSDKWrapper.XFWinGetRecognizeResultPtr());
  }

  private string XFWinGetError()
  {
    return Marshal.PtrToStringAnsi(XunfeiSDKWrapper.XFWinGetErrorPtr());
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void InitSDK(string appID, int sampleRate)
  {
    // ISSUE: unable to decompile the method.
  }

  public void StartRecord()
  {
    if (XunfeiSDKWrapper.m_isXFInit)
      ;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void StopRecord(string audioFilePath)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void OnAudioRecognized(string rst)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void OnRecognizeFailed(string rst)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void Update()
  {
    // ISSUE: unable to decompile the method.
  }

  public event Action<string> OnSpeechRecognized
  {
    [MethodImpl((MethodImplOptions) 32768)] add
    {
      // ISSUE: unable to decompile the method.
    }
    [MethodImpl((MethodImplOptions) 32768)] remove
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public event Action OnSpeechRecognizeFailed
  {
    [MethodImpl((MethodImplOptions) 32768)] add
    {
      // ISSUE: unable to decompile the method.
    }
    [MethodImpl((MethodImplOptions) 32768)] remove
    {
      // ISSUE: unable to decompile the method.
    }
  }

  private enum XFWinStatus
  {
    SUCCESS = 0,
    FAILURE = 1,
    NO_MATCH = 1,
    INCOMPLETE = 2,
    NON_SPEECH_DETECTED = 3,
    SPEECH_DETECTED = 4,
    COMPLETE = 5,
    MAX_CPU_TIME = 6,
    MAX_SPEECH = 7,
    STOPPED = 8,
    REJECTED = 9,
    NO_SPEECH_FOUND = 10, // 0x0000000A
  }
}
