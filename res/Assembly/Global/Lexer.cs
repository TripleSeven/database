﻿// Decompiled with JetBrains decompiler
// Type: Lexer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.IO;
using System.Runtime.CompilerServices;
using System.Text;

internal class Lexer
{
  private static int[] fsm_return_table;
  private static Lexer.StateHandler[] fsm_handler_table;
  private bool allow_comments;
  private bool allow_single_quoted_strings;
  private bool end_of_input;
  private FsmContext fsm_context;
  private int input_buffer;
  private int input_char;
  private TextReader reader;
  private int state;
  private StringBuilder string_buffer;
  private string string_value;
  private int token;
  private int unichar;

  static Lexer()
  {
    Lexer.PopulateFsmTables();
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public Lexer(TextReader reader)
  {
    // ISSUE: unable to decompile the method.
  }

  public bool AllowComments
  {
    get
    {
      return this.allow_comments;
    }
    set
    {
      this.allow_comments = value;
    }
  }

  public bool AllowSingleQuotedStrings
  {
    get
    {
      return this.allow_single_quoted_strings;
    }
    set
    {
      this.allow_single_quoted_strings = value;
    }
  }

  public bool EndOfInput
  {
    get
    {
      return this.end_of_input;
    }
  }

  public int Token
  {
    get
    {
      return this.token;
    }
  }

  public string StringValue
  {
    get
    {
      return this.string_value;
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static int HexValue(int digit)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void PopulateFsmTables()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static char ProcessEscChar(int esc_char)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State1(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State2(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State3(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State4(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State5(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State6(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State7(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State8(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State9(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State10(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State11(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State12(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State13(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State14(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State15(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State16(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State17(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State18(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State19(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State20(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State21(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State22(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State23(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State24(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State25(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State26(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State27(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static bool State28(FsmContext ctx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private bool GetChar()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private int NextChar()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public bool NextToken()
  {
    // ISSUE: unable to decompile the method.
  }

  private void UngetChar()
  {
    this.input_buffer = this.input_char;
  }

  private delegate bool StateHandler(FsmContext ctx);
}
