﻿// Decompiled with JetBrains decompiler
// Type: Lua_UnityEngine_Screen
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 972A2846-EBE4-4323-8745-F884A18FA509
// Assembly location: D:\Games\Langrisser5\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

[Preserve]
public class Lua_UnityEngine_Screen : LuaObject
{
  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int constructor(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int SetResolution_s(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_resolutions(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_currentResolution(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_fullScreen(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_fullScreen(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_width(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_height(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_dpi(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_orientation(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_orientation(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_sleepTimeout(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_sleepTimeout(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_autorotateToPortrait(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_autorotateToPortrait(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_autorotateToPortraitUpsideDown(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_autorotateToPortraitUpsideDown(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_autorotateToLandscapeLeft(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_autorotateToLandscapeLeft(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_autorotateToLandscapeRight(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_autorotateToLandscapeRight(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_safeArea(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static void reg(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }
}
