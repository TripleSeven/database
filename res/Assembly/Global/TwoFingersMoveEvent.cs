﻿// Decompiled with JetBrains decompiler
// Type: TwoFingersMoveEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;
using UnityEngine;

public class TwoFingersMoveEvent
{
  private static Vector2 _lastTouchPos;
  private static Vector2 _curTouchPos;
  private static Vector2 _touchDownPos;
  private static int _lastTouchCount;
  private static bool _isUp;
  private static bool _isDown;
  private static bool _isLeft;
  private static bool _isRight;
  private static float _createEventDist;
  private static float _lastUpdateTime;

  public static bool Up
  {
    get
    {
      return TwoFingersMoveEvent._isUp;
    }
  }

  public static bool Down
  {
    get
    {
      return TwoFingersMoveEvent._isDown;
    }
  }

  public static bool Left
  {
    get
    {
      return TwoFingersMoveEvent._isLeft;
    }
  }

  public static bool Right
  {
    get
    {
      return TwoFingersMoveEvent._isRight;
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void Update()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void Reset()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  static TwoFingersMoveEvent()
  {
    // ISSUE: unable to decompile the method.
  }
}
