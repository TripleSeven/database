﻿// Decompiled with JetBrains decompiler
// Type: SetFontSizeCmd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

public class SetFontSizeCmd : IDebugCmd
{
  private const string _NAME = "sfs";
  private const string _DESC = "sfs [int]: set the font size of debug console.";
  private const string _SCHEMA = "i";

  [MethodImpl((MethodImplOptions) 32768)]
  public void Execute(string strParams)
  {
    // ISSUE: unable to decompile the method.
  }

  public string GetHelpDesc()
  {
    return "sfs [int]: set the font size of debug console.";
  }

  public string GetName()
  {
    return "sfs";
  }
}
