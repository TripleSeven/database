﻿// Decompiled with JetBrains decompiler
// Type: Lua_BlackJack_BJFramework_Runtime_Lua_LuaExportChecker
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 972A2846-EBE4-4323-8745-F884A18FA509
// Assembly location: D:\Games\Langrisser5\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Lua;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

[Preserve]
public class Lua_BlackJack_BJFramework_Runtime_Lua_LuaExportChecker : LuaObject
{
  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int constructor(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int PublicMethod(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int ReturnAction(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  public static int PublicStaticMethod_s(IntPtr l)
  {
    LuaExportChecker.PublicStaticMethod();
    LuaObject.pushValue(l, true);
    return 1;
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int ProtectedMethod(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int PrivateMethod(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int __callDele_PublicEvent(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int __clearDele_PublicEvent(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int __callDele_ProtectedEvent(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int __clearDele_ProtectedEvent(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int __callDele_PrivateEvent(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int __clearDele_PrivateEvent(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int __callDele_ReturnIntEvent(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int __clearDele_ReturnIntEvent(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int __callDele_m_protectedDelegate(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int __clearDele_m_protectedDelegate(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int __callDele_m_privateDelegate(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int __clearDele_m_privateDelegate(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int __callDele_ReturnInt(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int __clearDele_ReturnInt(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  public static int ProtectedStaticMethod_s(IntPtr l)
  {
    LuaExportChecker.LuaExportHelper.ProtectedStaticMethod();
    LuaObject.pushValue(l, true);
    return 1;
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  public static int PrivateStaticMethod_s(IntPtr l)
  {
    LuaExportChecker.LuaExportHelper.PrivateStaticMethod();
    LuaObject.pushValue(l, true);
    return 1;
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  public static int __callDele_PublicStaticEvent_s(IntPtr l)
  {
    LuaExportChecker.LuaExportHelper.__callDele_PublicStaticEvent();
    LuaObject.pushValue(l, true);
    return 1;
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  public static int __clearDele_PublicStaticEvent_s(IntPtr l)
  {
    LuaExportChecker.LuaExportHelper.__clearDele_PublicStaticEvent();
    LuaObject.pushValue(l, true);
    return 1;
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  public static int __callDele_ProtectedStaticEvent_s(IntPtr l)
  {
    LuaExportChecker.LuaExportHelper.__callDele_ProtectedStaticEvent();
    LuaObject.pushValue(l, true);
    return 1;
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  public static int __clearDele_ProtectedStaticEvent_s(IntPtr l)
  {
    LuaExportChecker.LuaExportHelper.__clearDele_ProtectedStaticEvent();
    LuaObject.pushValue(l, true);
    return 1;
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  public static int __callDele_PrivateStaticEvent_s(IntPtr l)
  {
    LuaExportChecker.LuaExportHelper.__callDele_PrivateStaticEvent();
    LuaObject.pushValue(l, true);
    return 1;
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  public static int __clearDele_PrivateStaticEvent_s(IntPtr l)
  {
    LuaExportChecker.LuaExportHelper.__clearDele_PrivateStaticEvent();
    LuaObject.pushValue(l, true);
    return 1;
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_m_publicStaticField(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_m_publicStaticField(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_m_protectedStaticField(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_m_protectedStaticField(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_m_privateStaticField(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_m_privateStaticField(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_m_publicField(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_m_publicField(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_m_protectedField(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_m_protectedField(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_m_privateField(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_m_privateField(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_PublicStaticEvent(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_ProtectedStaticEvent(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_PrivateStaticEvent(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_PublicEvent(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_ProtectedEvent(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_PrivateEvent(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_m_publicDelegate(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_m_protectedDelegate(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_m_privateDelegate(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_ReturnIntEvent(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_ReturnInt(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_PublicStaticProperty(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_PublicStaticProperty(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_ProtectedStaticProperty(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_ProtectedStaticProperty(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_PrivateStaticProperty(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_PrivateStaticProperty(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_PublicProperty(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_PublicProperty(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_ProtectedProperty(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_ProtectedProperty(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_PrivateProperty(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_PrivateProperty(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static void reg(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }
}
