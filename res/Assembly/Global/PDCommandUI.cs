﻿// Decompiled with JetBrains decompiler
// Type: PDCommandUI
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PDCommandUI : MonoBehaviour
{
  public GridLayoutGroup Grid;
  public GameObject CommandBtnPrefab;
  public GameObject CommandInputPrefab;
  private string stringToEdit;
  private Dictionary<string, UnityAction> btnDataModelDic;
  private InputField inputCallVebViewAction;
  private InputField inputCallVebViewCustom;
  private InputField inputUserid;
  private InputField inputRoleid;
  private InputField inputServerid;

  [MethodImpl((MethodImplOptions) 32768)]
  public PDCommandUI()
  {
    // ISSUE: unable to decompile the method.
  }

  private void Awake()
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void Start()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void ConfigData()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void ConstructCommandBtn(string title, UnityAction clickEvent)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void ConstructCommandInput(string tips, out InputField input)
  {
    // ISSUE: unable to decompile the method.
  }

  private void OnGUI()
  {
  }
}
