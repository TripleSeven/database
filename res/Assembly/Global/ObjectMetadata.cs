﻿// Decompiled with JetBrains decompiler
// Type: ObjectMetadata
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

internal struct ObjectMetadata
{
  private System.Type element_type;
  private bool is_dictionary;
  private IDictionary<string, PropertyMetadata> properties;

  public System.Type ElementType
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
    set
    {
      this.element_type = value;
    }
  }

  public bool IsDictionary
  {
    get
    {
      return this.is_dictionary;
    }
    set
    {
      this.is_dictionary = value;
    }
  }

  public IDictionary<string, PropertyMetadata> Properties
  {
    get
    {
      return this.properties;
    }
    set
    {
      this.properties = value;
    }
  }
}
