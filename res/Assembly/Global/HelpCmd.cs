﻿// Decompiled with JetBrains decompiler
// Type: HelpCmd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

public class HelpCmd : IDebugCmd
{
  public const string _NAME = "help";
  public const string _DESC = "help: print all command's description";

  public void Execute(string strParams)
  {
    DebugCmdManager.instance.PringAllCmdDescription();
  }

  public string GetHelpDesc()
  {
    return "help: print all command's description";
  }

  public string GetName()
  {
    return "help";
  }
}
