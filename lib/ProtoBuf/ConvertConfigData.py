import os
import json
from google.protobuf.json_format import MessageToDict

import importlib.util

def getBuffer(name,path):
	try:
		spec = importlib.util.spec_from_file_location(name, path)
		foo = importlib.util.module_from_spec(spec)
		spec.loader.exec_module(foo)
		return getattr(foo, 'Items', False)
	except:
		return False

def ConvertConfigData(p_raw,p_fin,protopy_path):
	name=os.path.basename(p_raw).rsplit('.',1)[0]
		
	buffer=getBuffer(name,os.path.join(protopy_path,'%s_pb2.py'%name))
	if not buffer:
		print('ProtoBuffer-Extraction',name,'fail')
		return False
	
	data=buffer()
	try:
		data.ParseFromString(open(p_raw,'rb').read())
		data=MessageToDict(data)
		if 'items' in data:
			data=data['items']
		else:
			print(p_raw,data)
		open(p_fin,'wb').write(json.dumps(data,indent='\t',ensure_ascii=False).encode('utf8'))

		#print('\\/',os.path.basename(p_fin))
	except Exception as e:
		print('x',os.path.basename(p_fin))


def ConvertConfigDataFolder(p_raw,p_fin,protopy_path):
	#print(p_raw,p_fin)
	for fp in os.listdir(p_raw):
		name=fp.rsplit('.',1)[0]

		if not os.path.isfile(os.path.join(p_raw,fp)):
			print('Missing ProtoDeocder:',fp)
		
		buffer=getBuffer(name,os.path.join(protopy_path,'%s_pb2.py'%name))
		if not buffer:
			print('ProtoBuffer-Extraction',name,'fail')
			continue
		data=buffer()
		try:
			data.ParseFromString(open(os.path.join(p_raw,fp),'rb').read())
			data2=MessageToDict(data)
			if 'items' in data2:
				data2=data2['items']
			else:
				print(fp,data2)
			open(os.path.join(p_fin,'%s.json'%name),'wb').write(json.dumps(data2,indent='\t',ensure_ascii=False).encode('utf8'))
		except Exception as e:
			print(fp,e)
