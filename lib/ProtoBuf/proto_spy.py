import io
import struct
import pprint
import sys
import json
import os

def read_int(data):
	return struct.unpack('i', data.read(4))[0]

def read_long(data):
	return struct.unpack('q', data.read(8))[0]

def read_varint(data):
	stop_bit_found =  False
	res = 0
	n = 0
	while not stop_bit_found:
		byte = struct.unpack('B', data.read(1))[0]
		stop_bit_found = (byte >> 7 == 0)
		res += (byte & 0x7F) << n
		n += 7
	return res

def read_tag(data):
	key = read_varint(data)
	type = key & 7
	index = key >> 3
	return (key, type, index)

def read_field(data):
	(key, type, index) = read_tag(data)
	
	if type == 0:
		content = read_varint(data)
	elif type == 1:
		content = read_long(data)
	elif type == 2:
		field_length = read_varint(data)
		content = data.read(field_length)

		if len(content) != field_length:
			raise Exception('Invalid wire type 2 read, bad field size -> Got:{} Expected:{}'.format(len(content), field_length))
		
		try:
			content = read_proto_message(io.BytesIO(content))
		except Exception as ex:
			pass
			#print('Not message {} -- {}'.format(ex, content))
	elif type == 5:
		content = read_int(data)
	else:
		raise Exception('Unkown type in key: {}'.format(type))
	
	#print('Index: {}, Type: {}, Content: {}\n'.format(index, type, content))

	return (index, type, content)
'''
ADD THESE WIRE TYPES
	elif type == 3:
		print('TODO')
		content = '?'
	elif type == 4:
		print('TODO')
		content = '?'
'''
def read_proto_message(data):
	fields = {}

	data.seek(0, 2)
	eof = data.tell()
	data.seek(0, 0)
	while data.tell() != eof:
		(index, field_type, content) = read_field(data)

		if field_type == 2 and type(content) == bytes:
			try:
				content=content.decode('utf8')
			except:
				content=str(content)

		obj = { "type": field_type, "content": content }

		if index not in fields:
			fields[index] = obj
		else:
			if type(fields[index]) is dict:
				fields[index] = [fields[index]]
			fields[index].append(obj)
			
	return fields